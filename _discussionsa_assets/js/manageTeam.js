﻿jQuery(function ($) {
   
    $(document).ajaxStart(function () {
         NProgress.start();
    });
    $(document).ajaxStop(function () {
        NProgress.done();
    });


    $(".mtddlProjects").select2({
        placeholder: "Select Project",
        width: '30%'
    });

    $(".mtddlTeamMembers").select2({
        placeholder: "Select Member",
        width: '30%'       
    });

    $(document).on('click', '.btnEdit', function () {

        var currentRow = $(this).closest("tr");

        var Alias = currentRow.find("td:eq(1)").text();
        var Title = currentRow.find("td:eq(2)").text();
        var Email = currentRow.find("td:eq(3)").text();
        var Role = currentRow.find("td:eq(4)").text();
        var RoleId = currentRow.find('[id*=user-roleid]').val();
        var Id = currentRow.find(".btnEdit").attr('id');
        var IsAllowed = currentRow.find('[id*=OwnUser]').val(); // User can only edit its OWNED member's email

        $("#mAlias").val(Alias);
        $("#mTitle").val(Title);
        $("#mEmail").val(Email);
        $("#lblRole").text("(Role: " + Role + " )");
        $("#hdnID").val(Id);

        if (Role == "Admin" || Role == "SuperAdmin") {
            
        }
        else {

        }

        if (IsAllowed == "notallowed")
        {
            $("#mEmail").attr('readonly', 'readonly');
            $("#hdnEditEmail").val('no');
        }
        else {
            $("#mEmail").removeAttr('readonly');
            $("#hdnEditEmail").val('yes');
        }

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageTeamMembers.aspx/GetRoles',
            data: "{'selectedrole':'" + RoleId + "' }",
            async: true,
            success: function (response) {
                $(".ddlRoles").html(response.d);
                $(".ddlRoles").select2({
                    placeholder: "Select Role",
                    width: '100%'
                });
            },
            error: function () {
                console.log(response.d);
            }
        });

        $('#editMember').modal('show');
    });

    $("#btnUpdate").on('click', function () {

        $("#btnUpdate").attr('disabled', true);

        var Alias = $("#mAlias").val();
        var Title = $("#mTitle").val();
        var Email = $("#mEmail").val();
        var DTFId = $("#hdnID").val();
        var IsActive = $("#chk_IsActive").is(':checked');
        var RoleId = $(".ddlRoles").val();
        var hdnDiscussionID = $("#hdnDiscussionID").val();
        var hdnMemberID = $("#hdnMemberID").val();
        var CanEditEmail = $("#hdnEditEmail").val();

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageTeamMembers.aspx/ProjectMembers',
            data: "{'Alias':'" + Alias + "','Title':'" + Title + "','DTFId':'"
                + DTFId + "' ,'IsActive':'" + IsActive + "' ,'DisID':'" + hdnDiscussionID + "','memberID':'" + hdnMemberID + "','RoleId':'" + RoleId + "','Email':'" + Email + "','CanEditEmail':'" + CanEditEmail + "' }",
            async: true,
            success: function (response) {
                $('#editMember').modal('hide');
                $("#btnUpdate").attr('disabled', false);
                $("#list").html(response.d);
            },
            error: function () {
                console.log(response.d.toString());
            }
        });

       
    });

    $(document).on('change', '.chkDel', function () {
        if ($("#dataH input:checkbox:checked").length > 0) {
            $("#btnRemove").attr('disabled', false);
        }
        else {
            $("#btnRemove").attr('disabled', true);
        }
    });

    $(document).on('click', '#btnRemove', function () {
     
            var Ids = [];
            $("input:checkbox[class=chkDel]:checked").each(function () {
                Ids.push($(this).attr("data-uid"));
            });

            RemoveMember(Ids);
            return false;
           
    });

    $(document).on('change', '.chkboxDel', function () {
        if ($("#dataH input:checkbox:checked").length > 0) {
            $("#btnRemoveUser").attr('disabled', false);
        }
        else {
            $("#btnRemoveUser").attr('disabled', true);
        }
    });

    $(document).on('click', '#btnRemoveUser', function () {
        var DisIds = [];

        $("input:checkbox[class=chkboxDel]:checked").each(function () {
            DisIds.push($(this).attr("data-did"));
        });

        RemovefrmAllPrjcts(DisIds);
        return false;
    });
    
   
    Datatable();

    $(window).on('beforeunload', function () {
       NProgress.start();
    });
 
});


function RemoveMember(Ids)
{    
    var DisID = $("#hdnDiscussionID").val();
    var memberID = $("#hdnMemberID").val();

    var r = confirm("Are you sure you want to Remove?");
    if (r == true) {
        $('.dataTable').fadeOut();


    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'ManageTeamMembers.aspx/RemoveMembers',
        data: "{'DisID':'" + DisID + "','memberID':'" + memberID + "','Ids':'" + Ids + "' }",
        async: true,
        success: function (response) {
            $("#btnRemove").attr('disabled', true);
            $("#list").html(response.d);
            Datatable();
            $('.dataTable').fadeIn();

        },
        error: function (response) {
            console.log(response);
        }
    });
    }
  
}

function RemovefrmAllPrjcts(DisIds)
{
    var r = confirm("Are you sure you want to Remove?");
    if (r == true) {

        $('.dataTable').fadeOut();
        var MemberID = $("#ddlTeamMembers").val();


        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageTeamMembers.aspx/RemoveProjects',
            data: "{'memberID':'" + MemberID + "','DisIds':'" + DisIds + "' }",
            async: true,
            success: function (response) {
                $("#projectslist").html(response.d);
                $("#btnRemoveUser").attr('disabled', true);
                Datatable();
                $('.dataTable').fadeIn();
                btnDisable();
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
}

function GetDiscussionIds()
{
    var DisIds = [];

    var table = $('#dataH').DataTable();
    table
        .column(0)
        .data()
        .each(function (value, index) {
            //remove comment to enable "Remove Member form those PROJECTS where its Role != SuperAdmin"
            //if ($(value).attr('disabled') != 'disabled')
            DisIds.push($(value).attr('data-did'));
        });

    if (DisIds.length > 0)
    RemovefrmAllPrjcts(DisIds);

    return false;
}

function Datatable()
{
    $("#dataH").DataTable({
        "columnDefs": [{
            "defaultContent": "-",
            "targets": "_all"
        }],
        "c": true,
        "DeferRender": true,
        "searching": false,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": false
    });
}

function btnDisable()
{
    if ($($("#dataH").find('tbody tr')[0]).text() == "No Project Found!") {
        $("#btnRemovefrmAll").attr('disabled', true);
    }
    else {
        $("#btnRemovefrmAll").attr('disabled', false);
    }
}

$(document.getElementById('list')).ready(function () {
    NProgress.done();
    
    btnDisable();

});