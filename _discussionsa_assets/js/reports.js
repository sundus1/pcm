﻿jQuery(function ($) {
    // $("#TotalTr").hide();
    //  $("#TotalTr1").hide();

    var chkd = $("#rbtnSingleProject").is(':checked');
    if (chkd == true) {
        $("#ddlProjects").show();
    }
    else {
        $("#ddlProjects").hide();
    }
    var chkd2 = $("#rbtnAllProject").is(':checked');
    if (chkd2 == true || chkd == true) {
        $("#TotalTr").show();
        $("#TotalTr1").show();
    }
    else {
        $("#TotalTr").hide();
        $("#TotalTr1").hide();
    }
    $("#rbtnSingleProject").click(function () {
        var chkd = $("#rbtnSingleProject").is(':checked');
        if (chkd == true) {
            $("#list").hide();
            $("#ddlProjects").show();
            $("#TotalTr").hide();
            $("#TotalTr1").hide();
        }
    });
    $("#rbtnAllProject").click(function () {
        var chkd = $("#rbtnAllProject").is(':checked');
        if (chkd == true) {
            $("#list").hide();
            $("#ddlProjects").hide();
            $("#TotalTr").hide();
            $("#TotalTr1").hide();
        }
    });
    $("#btnGenerate").click(function () {
        if ($("#cldFromDate").val() == "") {
            // $("#lblMessage").innerText("Select From Date...");
            $("#cldFromDate").focus();
            return false;
        }
        if ($("#cldToDate").val() == "") {
            // $("#lblMessage").innerText("Select To Date...");
            $("#cldToDate").focus();
            return false;
        }
        //var chkd = $("#btnGenerate").is(':checked');
        //if (chkd == true) {
        $("#list").show();
        $("#TotalTr").show();
        $("#TotalTr1").show();
        $("#dataH").DataTable();
        // }
    });
    $(".delete").click(function () {
        var txt = $(this)[0].innerText.toLowerCase();
        if (txt == "remove") {
            var a = $(this).data('index');
            $("#tr" + a).css("background", "gray");
            $("#" + a).text("Add");
            $("#tr" + a).css("text-decoration", "line-through");
            $("#tr" + a).css("color", "white");
            $("#" + a).css("color", "white");
            $("#" + a).css("text-decoration", "blink");
            var str = $("#tr" + a)[0].cells[2].innerText;
            //var strArr = str.split(' ');
            // var minutes = parseInt(strArr[0]) * 60 + parseInt(strArr[2]);
            var minutes = parseInt(str);
            var allProjectMints = $("#hdnAllProjectMinutes").val();
           

            ///
            var AllAddedPrjeMinut = $("#hdnAllProjectAddMin").val();
            var AllSubPrjeMinut = $("#hdnAllProjectSubMin").val();

            if (AllSubPrjeMinut == 0) {
                var resu = parseInt(allProjectMints) - parseInt(minutes);
                $("#hdnAllProjectMinutes").val(resu);
                var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                var Allprojectminutes = parseInt(resu % 60);

                var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
            }
            else {
                var check = false;
                if (str.indexOf('-') >= 0) {
                    check = true;
                }
                minutes = parseInt(replaceAll(str, '-', ''));

                if (check)
                {
                    var resu1 = parseInt(AllSubPrjeMinut) - parseInt(minutes);
                    var resu = parseInt(allProjectMints) + parseInt(minutes);
                    var resu2 = $("#hdnAllProjectAddMin").val();
                }
                else
                {
                    var resu1 = AllSubPrjeMinut;
                    var resu = parseInt(allProjectMints) - parseInt(minutes);
                    var resu2 = parseInt(AllAddedPrjeMinut) - parseInt(minutes);
                }

               
                $("#hdnAllProjectMinutes").val(resu);
                var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                var Allprojectminutes = parseInt(resu % 60);

                
                $("#hdnAllProjectSubMin").val(resu1);
                var Allprojectsubhour = (parseFloat(resu1) / 60).toString().split('.')[0];
                var Allprojectsubminutes = parseInt(resu1 % 60);
                  
                $("#hdnAllProjectAddMin").val(resu2);
                var Allprojectaddhour = (parseFloat(resu2) / 60).toString().split('.')[0];
                var Allprojectaddminutes = parseInt(resu2 % 60);

                var final = "<span class='Subtract'><b>Time Added=</b>" + Allprojectaddhour + " Hours " + Allprojectaddminutes + " Minutes <br/>";
                final += "<b>Time Subtracted=</b>" + Allprojectsubhour + " Hours " + Allprojectsubminutes + " Minutes <br/></span>";
                final += "<b><span class='netTime'>Net Time</span>=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
            }
            ///

            //  var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";

            $("#lblTotalHours1").html(final);
            $("#lblTotalHours").html(final);
            if ($('#hdnRemoveAllZeroHrs').val() == 'yes') {
                $('.Subtract').hide();
                $('.netTime').text("Total Hours Spent");
            }
        }
        else {
            var a = $(this).data('index');
            $("#tr" + a).css("background", "");
            $("#tr" + a).css("text-decoration", "");
            $("#tr" + a).css("color", "");
            $("#" + a).css("color", "");
            $("#" + a).text("Remove");
            var str = $("#tr" + a)[0].cells[2].innerText;
            //var strArr = str.split(' ');
            //var minutes = parseInt(strArr[0]) * 60 + parseInt(strArr[2]);
            var minutes = parseInt(str);
            var allProjectMints = $("#hdnAllProjectMinutes").val();
          

            ///
            var AllAddedPrjeMinut = $("#hdnAllProjectAddMin").val();
            var AllSubPrjeMinut = $("#hdnAllProjectSubMin").val();

            if (AllSubPrjeMinut == 0) {
                var resu = parseInt(allProjectMints) + parseInt(minutes);
                $("#hdnAllProjectMinutes").val(resu);
                var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                var Allprojectminutes = parseInt(resu % 60);
                var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
            }
            else {
                var check = false;
                if (str.indexOf('-') >= 0) {
                    check = true;
                }
                minutes = parseInt(replaceAll(str, '-', ''));

                if (check) {
                    var resu1 = parseInt(AllSubPrjeMinut) + parseInt(minutes);
                    var resu = parseInt(allProjectMints) - parseInt(minutes);
                    var resu2 = $("#hdnAllProjectAddMin").val();
                }
                else {
                    var resu1 = AllSubPrjeMinut;
                    var resu = parseInt(allProjectMints) + parseInt(minutes);
                    var resu2 = parseInt(AllAddedPrjeMinut) + parseInt(minutes);
                }

                $("#hdnAllProjectMinutes").val(resu);
                var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                var Allprojectminutes = parseInt(resu % 60);

                $("#hdnAllProjectSubMin").val(resu1);
                var Allprojectsubhour = (parseFloat(resu1) / 60).toString().split('.')[0];
                var Allprojectsubminutes = parseInt(resu1 % 60);

                $("#hdnAllProjectAddMin").val(resu2);
                var Allprojectaddhour = (parseFloat(resu2) / 60).toString().split('.')[0];
                var Allprojectaddminutes = parseInt(resu2 % 60);

                var final = "<span class='Subtract'><b>Time Added=</b>" + Allprojectaddhour + " Hours " + Allprojectaddminutes + " Minutes <br/>";
                final += "<b>Time Subtracted=</b>" + Allprojectsubhour + " Hours " + Allprojectsubminutes + " Minutes <br/></span>";
                final += "<b><span class='netTime'>Net Time</span>=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
            }
            ///


            //var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
                        
            $("#lblTotalHours1").html(final);
            $("#lblTotalHours").html(final);
            if ($('#hdnRemoveAllZeroHrs').val() == 'yes') {
                $('.Subtract').hide();
                $('.netTime').text("Total Hours Spent");
            }
        }
    });
    /*   $("#btnRemove").click(function () {
           var lessMinutes = parseInt($("#txtRemoveMint").val());
           $("#dataH tr").each(function () {
               var str = $(this)[0].cells[2].innerText;
               if (str != "" && str != "Total Time" && str != "In Minutes") {
                   // var strArr = str.split(' ');
                   // var minutes = parseInt(strArr[0]) * 60 + parseInt(strArr[2]);
                   var minutes = parseInt(str);
                   if (minutes < lessMinutes) {
                       var a = $(this).attr('id');
                       var arr = a.split('tr');
                       a = arr[1];
                       $("#tr" + a).css("background", "gray");
                       $("#" + a).text("Add");
                       $("#tr" + a).css("text-decoration", "line-through");
                       $("#tr" + a).css("color", "white");
                       $("#" + a).css("color", "white");
                       $("#" + a).css("text-decoration", "blink");
                       var allProjectMints = $("#hdnAllProjectMinutes").val();
                       var resu = parseInt(allProjectMints) - parseInt(minutes);
                       $("#hdnAllProjectMinutes").val(resu);
                       var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                       var Allprojectminutes = parseInt(resu % 60);
                       var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
                       $("#lblTotalHours1").html(final);
                       $("#lblTotalHours").html(final);
                   }
               }
           });
       });*/
    function MinusMinutes(lessMinutes, type) {
        $("#dataH tr").each(function () {
            var str = $(this)[0].cells[2].innerText;
            if (str != "" && str != "Total Time" && str != "In Minutes") {
                if ($(this)[0].style.display != "none") {
                    // var strArr = str.split(' ');
                    // var minutes = parseInt(strArr[0]) * 60 + parseInt(strArr[2]);
                    var minutes = parseInt(str);
                    if (minutes < lessMinutes) {
                        var a = $(this).attr('id');
                        var arr = a.split('tr');
                        a = arr[1];

                        if ($(this)[0].style.textDecoration != "line-through") {

                            var allProjectMints = $("#hdnAllProjectMinutes").val();
                            var AllAddedPrjeMinut = $("#hdnAllProjectAddMin").val();
                            var AllSubPrjeMinut = $("#hdnAllProjectSubMin").val();

                            var resu = parseInt(allProjectMints) - parseInt(minutes);
                            $("#hdnAllProjectMinutes").val(resu);
                            var Allprojecthour = (parseFloat(resu) / 60).toString().split('.')[0];
                            var Allprojectminutes = parseInt(resu % 60);

                            if (lessMinutes == 0 || AllSubPrjeMinut == 0) {
                                $('#hdnRemoveAllZeroHrs').val('yes');
                                var final = "<b>Total Hours Spent=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
                            }
                            else {
                                var check = false;
                                if (str.indexOf('-') > 0)
                                {
                                    check = true;
                                }
                                minutes = parseInt(replaceAll(str, '-', ''));
                              
                                var resu1 = parseInt(AllSubPrjeMinut) - parseInt(minutes);
                                $("#hdnAllProjectSubMin").val(resu1);
                                var Allprojectsubhour = (parseFloat(resu1) / 60).toString().split('.')[0];
                                var Allprojectsubminutes = parseInt(resu1 % 60);

                                if (check)
                                {
                                   // var resu2 = parseInt(AllAddedPrjeMinut) + parseInt(minutes);
                                }
                                else {
                                   // var resu2 = parseInt(AllAddedPrjeMinut) - parseInt(minutes);
                                }
                              
                                var resu2 =     $("#hdnAllProjectAddMin").val();
                                var Allprojectaddhour = (parseFloat(resu2) / 60).toString().split('.')[0];
                                var Allprojectaddminutes = parseInt(resu2 % 60);

                                var final = "<span class='Subtract'><b>Time Added=</b>" + Allprojectaddhour + " Hours " + Allprojectaddminutes + " Minutes <br/>";
                                final += "<b>Time Subtracted=</b>" + Allprojectsubhour + " Hours " + Allprojectsubminutes + " Minutes <br/></span>";
                                final += "<b><span class='netTime'>Net Time</span>=</b>" + Allprojecthour + " Hours " + Allprojectminutes + " Minutes";
                               }

                            $("#lblTotalHours1").html(final);
                            $("#lblTotalHours").html(final);
                            if ($('#hdnRemoveAllZeroHrs').val() == 'yes') {
                                $('.Subtract').hide();
                                $('.netTime').text("Total Hours Spent");
                            }
                        }

                        if (type == "hide") {
                            $("#tr" + a).hide();
                        }
                        else {
                            $("#tr" + a).css("background", "gray");
                            $("#" + a).text("Add");
                            $("#tr" + a).css("text-decoration", "line-through");
                            $("#tr" + a).css("color", "white");
                            $("#" + a).css("color", "white");
                            $("#" + a).css("text-decoration", "blink");
                        }
                       
                    }
                }
            }
        });
    }
    $("#ddlDelete").change(function () {
        var ddlValue = $("#ddlDelete :selected").val();
        if (ddlValue != "") {
            var lessMinutes = parseInt($("#txtRemoveMint").val());
            if ($("#txtRemoveMint").val() != "") {
                if (ddlValue.toLowerCase() == "remove") {
                    MinusMinutes(lessMinutes, "remove");
                    $("#txtRemoveMint").val("");
                    $('[id=ddlDelete] option').filter(function () {
                        return ($(this).val() == "");
                    }).prop('selected', true);
                }
                else if (ddlValue.toLowerCase() == "hide") {
                    MinusMinutes(lessMinutes, "hide");
                    $("#txtRemoveMint").val("");
                    $('[id=ddlDelete] option').filter(function () {
                        return ($(this).val() == "");
                    }).prop('selected', true);
                }
            }
            else {
                alert("Type minutes...");
                $('[id=ddlDelete] option').filter(function () {
                    return ($(this).val() == "");
                }).prop('selected', true);
                $("#txtRemoveMint").focus();
            }
        }
    });
});

function replaceAll(str, search, replace) {

    replacestring = str.replace(search, '');
    if (str == replacestring) {

        return str;
    }
    else {
        return replaceAll(replacestring, search, replace);
    }

}

$(window).load(function () {
    $(".date-picker").datepicker();
});


