﻿jQuery(function ($) {

    $(document).ajaxStart(function () {
        NProgress.start();
    });
    $(document).ajaxStop(function () {
        NProgress.done();
    });


    $(".ddlTeamMembers").select2({
        placeholder: "Select Member",
        width: "80%"
    });

    $(".ddlRepeat").select2({
        placeholder: "Repeats",
        width: "80%"
    });

    $(".ddlRepeatEvery").select2({
        placeholder: "",
        width: "80%"
    });

    $(document).on('click', '.btnEdit', function () {
        ClearSelections();
        var currentRow = $(this).closest("tr");

        var Description = currentRow.find("td:eq(1)").text();
        var AssignedTo = currentRow.find("td:eq(2)").find("[id=assignedTo]").val();
            //currentRow.find("td:eq(2)").text();
        var Repeats = currentRow.find("td:eq(4)").text();
        var RepeatEvery = currentRow.find("td:eq(5)").text().split(" ")[0];
        var Id = currentRow.find(".btnEdit").attr('id');
        var InstanceID = currentRow.find('[id*=instanceid]').val();
        var DisID = currentRow.find('[id*=DisID]').val();

        $("#eDescription").val(Description);
        //$("#eRepeats option[value='" + Repeats + "']").attr('selected', true);
        //$("#eRepeatEvery option[value='" + RepeatEvery + "']").attr('selected', true);

        $("#eRepeats").val(Repeats);
        $("#eRepeatEvery").val(RepeatEvery);
        $("#hdnID").val(Id);

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageReminders.aspx/GetMembers',
            data: "{'selected':'" + AssignedTo + "','InstanceID':'" + InstanceID + "' ,'DisID':'" + DisID + "' }",
            async: true,
            success: function (response) {
                $(".eTeamMembers").html(response.d);
                $(".eTeamMembers").select2({
                    placeholder: "Select Member",
                    width: '100%'
                });

                $('#editReminder').modal('show');
            },
            error: function () {
                console.log(response.d);
            }
        });
               
    });

    $("#btnUpdate").on('click', function () {

        $("#btnUpdate").attr('disabled', true);

        var Description = $("#eDescription").val();
        var Repeats = $("#eRepeats").val();
        var RepeatsEvery = $("#eRepeatEvery").val();
        var MemberID = $("#eTeamMembers").val();
        var ID = $("#hdnID").val();
        var DisID = $("#hdnDisID").val();
        var InstanceID = $("#hdnInstanceID").val()
        var OwnerID = $("#hdnOwnerID").val();

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageReminders.aspx/UpdateReminder',
            data: "{'Description':'" + Description + "','Repeats':'" + Repeats + "','RepeatsEvery':'" + RepeatsEvery + "','MemberID':'" + MemberID + "','ID':'" + ID + "','DisID':'" + DisID + "','InstanceID':'" + InstanceID + "','OwnerID':'" + OwnerID + "' }",
            async: true,
            success: function (response) {
                ClearSelections();
                $('#editReminder').modal('hide');
                $("#btnUpdate").attr('disabled', false);
                $("#reminderslist").html(response.d);
            },
            error: function () {
                console.log(response.d.toString());
            }
        });

    });

    $(document).on('change', '.chkDel', function () {
        if ($("#dataH input:checkbox:checked").length > 0) {
            $("#btnRemove").attr('disabled', false);
        }
        else {
            $("#btnRemove").attr('disabled', true);
        }
    });

    Datatable();

    $(window).on('beforeunload', function () {
        NProgress.start();
    });

});

function ClearSelections()
{
    $("#eRepeats").find('option').attr("selected", false);
    $("#eRepeatEvery").find('option').attr("selected", false);

}

function Remove(ID) {
    var DisID = $("#hdnDisID").val();
    var InstanceID = $("#hdnInstanceID").val();
    var OwnerID = $("#hdnOwnerID").val();

    var r = confirm("Are you sure you want to Remove?");
    if (r == true) {
        $('.dataTable').fadeOut();

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'ManageReminders.aspx/RemoveReminder',
            data: "{'ID':'" + ID + "','InstanceID':'" + InstanceID + "','DisID':'" + DisID + "','OwnerID':'" + OwnerID + "'  }",
            async: true,
            success: function (response) {
                $("#reminderslist").html(response.d);
                Datatable();
                $('.dataTable').fadeIn();

            },
            error: function (response) {
                console.log(response);
            }
        });
    }

}



function Datatable() {
    $("#dataH").DataTable({
        "columnDefs": [{
            "defaultContent": "-",
            "targets": "_all"
        }],
        "c": true,
        "DeferRender": true,
        "searching": false,
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bAutoWidth": false
    });
}

function btnDisable() {
    if ($($("#dataH").find('tbody tr')[0]).text() == "No Project Found!") {
        $("#btnRemovefrmAll").attr('disabled', true);
    }
    else {
        $("#btnRemovefrmAll").attr('disabled', false);
    }
}

$(document.getElementById('list')).ready(function () {
    NProgress.done();

    btnDisable();

});