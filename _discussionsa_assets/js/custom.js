﻿// Initialize splitter
var singleFileDelete = "0";
jQuery(function ($) {

    var dwidth = $(document).width();
    var dheight = $(document).height();
    $('#widget').css({ width: dwidth, height: dheight }).split({ orientation: 'vertical', limit: 250, position: '22%' });
    $('.txtMemDis').ckeditor();
    /*Hour and Minutes*/
    $('#chkIsHourly').change(function () {
        var chkd = $(this).is(':checked');
        if (chkd == true)
        { $('#isHourlyDiv').show(); }
        else { $('#isHourlyDiv').hide(); }
    });
    $('#chkHourRequired').change(function () {
        var chkd = $(this).is(':checked');
        if (chkd == true)
        { $('#ZeroHour').show(); }
        else { $('#ZeroHour').hide(); }
    });
    $('#chkSendSummary').change(function () {
        var chkd = $(this).is(':checked');
        if (chkd == true)
        { $('#duartion').show(); }
        else { $('#duartion').hide(); }
    });
    if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5") {
        $(".hours").hide();
    }
    else {
        if ($("#hdnIsHourlyProject").val() == "true") {
            $(".hours").show();
            $(".summary").show();
            $("#hours").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("#minutes").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });

        }
        else {
            $(".hours").hide();
            $(".summary").hide();
        }
    }
    $("#btnEdit").click(function () {
        $("#EditName").show();
        var title = $("#h1Title")[0].innerText;
        $("#lblDisName").html("Settings - " + title);
        //$("#lblDisName").innerText = title;
        $("#txtDisName").val(title);
        if ($("#hdnIsHourlyProject").val() == "true") {
            $("#chkIsHourly").attr("checked", true);
        }
        else {
            $("#chkIsHourly").attr("checked", false);
        }
        if ($("#hdnSendSummary").val() == "true") {
            $("#chkSendSummary").attr("checked", true);
            $("#duartion").show();
            $("#txtDuration").val($("#hdnDuration").val());
            $("#txtEmailContent").val($("#hdnEmailContent").val());
        }
        else {
            $("#duartion").hide();
            $("#chkSendSummary").attr("checked", false);
        }
        if ($("#HideHours").val() == "true") {
            $("#HideHoursFromClient").attr("checked", true);
        }
        if ($("#ShowSummary").val() == "true") {
            $("#ShowSummaryBox").attr("checked", true);
        }
        if ($("#hdnHourRequiredEachPost").val() == "true") {
            $("#chkHourRequired").attr("checked", true);
        }
        else { $("#chkHourRequired").attr("checked", false); }
        if ($("#hdnZeroHourPost").val() == "true") {
            $("#chkZeroHour").attr("checked", true);
        }
        else {
            $("#chkZeroHour").attr("checked", false);
        }

        if ($("#chkIsHourly").is(':checked') == true) {
            $("#isHourlyDiv").show();
            if ($("#chkHourRequired").is(':checked') == true) {
                $("#ZeroHour").show();
            }
            else {
                $("#ZeroHour").hide();
            }
        }
        else {
            $("#isHourlyDiv").hide();
        }
        if ($("#hdnRoleID").val() == 6) {
            $("#pm").hide();
        }

    });
    $("#btnGmailReply").click(function () {
        var discussionid = "2492";
        var memdisid = 0;
        var email = "sbmuhammadfaizan@hotmail.com";
        var conversation = "helllo";
        var subject = "dasdas";
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: '_Discussion.aspx/SaveMemberDiscussionByGmail',
            data: "{'memdisid':'" + memdisid + "','discussionid':'" + discussionid + "','email':'" + email + "','conversation':'" + conversation + "','subject':'" + subject + "','tags':'null','strminutes':'null'}",
            async: true,
            success: function (response) {
                alert(response.d.toString());
            },
            error: function () {
                alert(response.d.toString().split(':')[1]);
            }
        });
    });
    $('#ddlPTeamMembers').change(function () {
        var $id = $(this).val();
        $('#ddlAPTeamMembers option').each(function () {
            if ($(this).val() == $id && $id != 0) {
                $(this).hide();
                $(this).val(0);
            } else {
                $(this).show();
            }
        });
    });
    /* $("#btnAddTeamW").click(function () {
         var discussionid = $("#hdnDisID").val();
         var teamName = $("#txtTeamName").val();
         var teamAlias = $("#txtTeamAlias").val();
         var teamTitle = $("#txtTeamTitle").val();
         var teamEmail = $("#txtTeamEmail").val();
         var ownerID = $("#hdnOID").val();
         var memberId = $("#hdnMemberID").val();
         var admin = "";
         if ($("#chkAdmin").val() == "true") {
             admin = true;
         }
         else {
             admin = false;
         }
         $.ajax({
             type: 'POST',
             contentType: "application/json; charset=utf-8",
             url: '_Discussion.aspx/AddNewMember',
             data: "{'sender':'team','discussionId':'" + discussionid + "','ownerID':'" + ownerID + "','memberId':'" + memberId + "','teamName':'" + teamName + "','teamAlias':'" +
         teamAlias + "','teamTitle':'" + teamTitle + "','teamEmail':'" + teamEmail + "','admin':'" +
         admin + "','hdnSelClientID':'" + $("#hdnSelClientID").val() + "','hdnSelTeamID':'" + $("#hdnSelTeamID").val() + "','totalComments':'" + $("#hdnTotalComments").val() + "'}",
             async: true,
             success: function (response) {
                 alert(response.d.toString().split(':')[1]);
                // $("#lblInfoExTeam").html(response.d.toString().split(':')[1]);
                 $("#lblInfoTeam").html(response.d.toString().split(':')[1]);
                 $("#lblInfoTeam").css({"color": "Green" });
                // $("#lblInfoExTeam").css({ "color": "Green" });
                 $("#hdnSelTeamID").val("0");
                 //alert(response.d.toString());
             },
             error: function () {
                 $("#lblInfoExTeam").html(response.d.toString().split(':')[1]);
                 $("#lblInfoTeam").html(response.d.toString().split(':')[1]);
                 $("#lblInfoTeam").css({ "color": "Red" });
                 $("#lblInfoExTeam").css({ "color": "Red" });
 
             }
         });
     });*/
    if ($("#ddlPTeamMembers").val() != "") {
        var $id = $("#ddlPTeamMembers").val();
        $('#ddlAPTeamMembers option').each(function () {
            if ($(this).val() == $id && $id != 0) {
                $(this).hide();
            } else {

                $(this).show();
            }
        });
    }

});
/*End*/
function randomizeAll() {
    randomizeComments();
    randomizeDiscussionList();
    randomizeFiles();
    randomizeTags();
}
function randomizeComments() {
    var randomnumber = Math.floor((Math.random() * 1000000) + 1);
    $('#hdnRCom').val(randomnumber);
}
function randomizeDiscussionList() {
    var randomnumber = Math.floor((Math.random() * 1000000) + 1);
    $('#hdnRDL').val(randomnumber);
}
function randomizeFiles() {
    var randomnumber = Math.floor((Math.random() * 1000000) + 1);
    $('#hdnRFiles').val(randomnumber);
}
function randomizeTags() {
    var randomnumber = Math.floor((Math.random() * 1000000) + 1);
    $('#hdnRTags').val(randomnumber);
}


$(window).load(function () {
    // Set height and width   
    try {
        $('#widget').css('height', '100%');
        $('#widget').css('width', '100%');
        $('.right_panel').css('width', '78%');
    } catch (e) {
        console.log(e.message);
    }
    // Scroll down chat
    try {
        $(".ChatCont").animate({ scrollTop: $(document).height() });
    } catch (e) {
        console.log(e.message);
    }
    $('#txtFilterTags').tagEditor({
        clickDelete: false,
    });

});

$(function () {
    // Set height and width
    $('[data-toggle="tooltip"]').tooltip()
})


var redirectURL = "";
var DiscussionTitle = "";
var UploadedFiles = [];
try {
    CKEDITOR.disableAutoInline = true;
    CKEDITOR.config.disallowedContent = "h1";
    CKEDITOR.editorConfig = function (config) {
        config.format_tags = 'div';
    };
} catch (e) {
    console.log(e.message);
}


function initIzFM(selector, currFolder, memDisID) {
    try {
        //alert('Initalizing Uploader');
        $(selector).uploadify({
            'swf': '_assests/Uplodify/uploadify.swf',
            'uploader': 'Upload.ashx',
            //'method':'post',
            'script': 'Upload.ashx',
            'cancelImg': '_assests/Uplodify/uploadify-cancel.png',
            'buttonText': 'Attach Files',
            //'script': 'Upload.ashx',
            'folder': currFolder,
            'fileTypeDesc': 'Files',
            //'fileTypeExts': '*.png;*.jpg;*.jpeg;*.gif;*.tff;*.tf;*.js;*.swf;*.htc;*.txt;*.xml;*.rss;*.eot;*.svg;*.ttf;*.woff;*.pfb;*.pfm;*.otf;*.ffil;*.dfont;*.lwfn;*.html;*.htm;*.css;',
            'multi': true,
            'auto': true,
            'removeCompleted': false,
            'formData': { 'discussionID': $('#hdnDisID').val(), 'projectID': $('#hdnProjectID').val(), 'memDisID': memDisID },
            'onQueueComplete': function (queueData) {
                //alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');
                //setTimeout(function () {
                //    if ($('.uploadify-queue-item').size() > 0) {
                //        $('.uploadify-queue-item').hide(200, function () {
                //            $('.uploadify-queue-item').remove();
                //        });
                //    }
                //}, 500)
                //resizeIframePeriodically();
            },
            'onUploadError': function (file, errorCode, errorMsg, errorString) {
                alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
            },
            'onUploadSuccess': function (file, data, response) {
                //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                try {
                    if (data.toString().substr(0, 5).toString() == "Error") {
                        alert(data);
                        return;
                    }
                    $('#' + file.id + ' .cancel a').attr('data-disfileid', data);
                    $('#' + file.id + ' .cancel a').addClass('completed');
                    $('a[data-disfileid="' + data + '"]').click(function () {
                        DeleteDiscussionFile($(this).attr('data-disfileid'));
                    })
                    console.log("starting another");
                    console.log(data);
                    //alert(data);
                    var _uploadedFile = new Object();
                    _uploadedFile.FileName = file.name;
                    _uploadedFile.DisFileID = data;
                    UploadedFiles.push(_uploadedFile);
                    //var fileUpload = $(btnSend).parent().find('#fileUpload1').uploadify('upload');
                } catch (e) {
                    alert(e.message);
                }
                //try { resizeIframe(); } catch (e) { }
            },
            'onUploadProgress': function (file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
                try {
                    //$('#pProgress').html(totalBytesUploaded + ' bytes uploaded of ' + bytesTotal + ' bytes.');

                    //$('#divProgress').dialog('close');
                } catch (e) {
                    alert(e.message);
                }
            },
            'onDialogClose': function (queueData) {
                //resizeIframePeriodically();
            },
            'onCancel': function (file) {
                alert('The file ' + file.name + ' was cancelled.');
                //for (var i = 0; i < UploadedFiles.length; i++) {
                //    var _uploadedFile = UploadedFiles[i];
                //    if (_uploadedFile.FileName == file.name) {
                //        DeleteDiscussionFile(_uploadedFile.DisFileID);
                //    }
                //}
            }
        });

    } catch (e) {
        alert(e.message);
    }
}
/* Save Discussion Title and IsHourly */
function SaveDiscussionTitleIsHourly(discussionid, title, projectid, instanceid, status, memberid, isHourly,
    hourRequiredEachPost, zeroHourPost, projectManagerId, assitantProjectManagerId, HideHours,
    ShowSummary, sendSummary, duration, emailContent) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/SaveDiscussionTitleIsHourly',
        data: "{'discussionid':'" + discussionid + "','title':'" + title + "','projectid':'"
            + projectid + "','instanceid':'" + instanceid + "','status':'" + status + "','memberid':'"
            + memberid + "','isHourly':'" + isHourly + "','HourRequiredEachPost':'" + hourRequiredEachPost
            + "','ZeroHourPost':'" + zeroHourPost + "','projectManagerId':'" + projectManagerId +
             "','assitantProjectManagerId':'" + assitantProjectManagerId +
              "','HideHours':'" + HideHours + "','SummaryAllow':'" + ShowSummary +
               "','SendSummary':'" + sendSummary + "','Duration':'" + duration + "','EmailContent':'" + emailContent + "'   }",
        async: true,
        success: function (response) {
            console.log(response.d);
            $('#hdnDisID').val(response.d.toString().split(':')[2]);
            $("#EditName").hide();
            $('#h1Title').text(title);
            location.reload();

        },
        error: function () {
            alert(response.d.toString().split(':')[1]);
        }
    });
}
/*End*/
function SaveDiscussion(discussionid, title, projectid, instanceid, status, memberid) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/SaveDiscussion',
        data: "{'discussionid':'" + discussionid + "','title':'" + title + "','projectid':'" + projectid + "','instanceid':'" + instanceid + "','status':'" + status + "','memberid':'" + memberid + "'}",
        async: true,
        success: function (response) {
            console.log(response.d);
            $('#hdnDisID').val(response.d.toString().split(':')[2]);
            $('.activeIcon').parent().find('a').text($('#h1Title').text());
            var url = "_Discussion.aspx?disid=" + response.d.toString().split(':')[2] + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val();
        },
        error: function () {
            //$(trTeam).append("some problem in saving data");
            //createInfoSpan(trTeam, "some problem in saving data");
        }
    });
}
function SendEmail(memberdisid, discussionid, memberid, conversation, subject, folderid, minutes) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/SendEmail',
        data: "{'memberdisid':'" + memberdisid + "','discussionid':'" + discussionid + "','memberid':'" + memberid + "','conversation':'" + conversation + "','subject':'" + subject + "','folderid':'" + folderid + "','minutes':'" + minutes + "'}",
        async: true,
        success: function (response) {
            console.log(response.d);
            initPostFunction();
        },
        error: function () {
            console.log("some problem in saving data");
        }
    });
}
var linkified = false;
var linkifiedOnce = false;

function replaceAll(str, search, replace) {

    replacestring = str.replace(search, '');
    if (str == replacestring) {

        return str;
    }
    else {
        return replaceAll(replacestring, search, replace);
    }

}

function SaveMemberDiscussion(memdisid, notes, tags, discussionid, instanceid, memberid, teammember, conversation, active, folderid, subject, minutes, isnew, summary, messageid, isSubtracted, SubtractReason) {
    //linkifiedOnce = false;   

    if (conversation != "updatetagonly" && conversation != "updatenotesonly") {
        if (isnew) {
            conversation = linkify($('.txtMemDis').val());
        }
        else {
            conversation = linkify($('.txtMemDisU').val());
        }
    }
    conversation = JSON.stringify(conversation);
    conversation = conversation.slice(1, -1);
    conversation = replaceAll(conversation, '\\n', '');
    conversation = replaceAll(conversation, '\\t', '');
    conversation = replaceAll(conversation, '<p>&nbsp;</p>', '');


    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/SaveMemberDiscussion',
        //data: "{'memdisid':'" + memdisid + "','tags':'" + tags + "','discussionid':'" + discussionid + "','instanceid':'" + instanceid + "','memberid':'" + memberid + "','teammember':'" + teammember + "','conversation':'" + conversation + "','active':'" +
        //    active + "','folderid':'" + folderid + "','subject':'" + subject + "','strminutes':'" + minutes +
        //    "','Summary':'" + summary + "','FileDMId':'" + 'D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val() + "'}",
        data: '{"memdisid":"' + memdisid + '","notes":"' + notes + '","tags":"' + tags + '","discussionid":"' + discussionid +
            '","instanceid":"' + instanceid + '","memberid":"' + memberid + '","teammember":"' + teammember +
            '","conversation":"' + conversation + '","active":"' +
            active + '","folderid":"' + folderid + '","subject":"' + subject + '","strminutes":"' + minutes +
            '","Summary":"' + summary + '","FileDMId":"' + "D" + $("#hdnDisID").val() + "M" + $("#hdnMemberID").val() + '","messageid":"' + messageid +
            '","subtracthours":"' + isSubtracted + '","subtractReason":"' + SubtractReason + '"}',
        async: true,
        success: function (response) {
            if (isnew) {
                console.log(response.d);
                if (response.d.toString().split(':')[0] == "Error") {
                    alert(response.d.toString().split(':')[1]);
                    window.location = 'Controls/Info.aspx?h=Not Allowed&t=You are not allowed to view this page. This can occur in case admin has removed your role from discussion. Please check your email for information releated your removal or contact your administrator for further details';
                    return false;
                }
                var memberdisid = response.d.toString().split(':')[2]; // Return memDisID
                //var fileUpdate = UpdateDiscussionFile('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val(), memberdisid);
                var fileUpdate = response.d.split(':')[response.d.split(':').length - 1];
                if (fileUpdate == "1") {
                    getDisFiles();
                }
                randomizeAll();
                //Tooba Commeny this  
                //LoadDisList();
                LoadDisNotes();

                redirectURL = "_discussion_controls/_DiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&RoleID=" + $('#hdnRoleID').val() + "&HideHours=" + $('#HideHours').val() + "&isHourly=" + $('#hdnIsHourlyProject').val() + "&memDisID=" + response.d.toString().split(':')[2] + "&totalcomments=" + $("#totalcomments").val() + "&FileUpdate=" + fileUpdate;
                var myInterval = setInterval(function () {
                    if ($('.uploadify-queue-item').size() == 0) {
                        if ($('#divChatArea .divMD').size() > 0) {
                            $('#divChats').load(redirectURL + " .divOldDiscussions", function () {
                                console.log('loaded');
                                $('#divChats .load-cont').remove();
                                // initPostFunction();
                                $($('#divChats .divOldDiscussions').html()).insertAfter($('#divChatArea .divMD').last());
                                // SendEmail(memberdisid, discussionid, memberid, $('.txtMemDis').val(), subject, $('#hdn_folderid').val(), minutes);
                                $('.txtMemDis').val('');
                                $('.btnSend').val('Add Message');
                                $('.btnSend').removeClass('button_loading');
                                if ($('#fileUpload1 object').size() <= 0) { $('#hdnMemDisID').val(memberdisid); $('.btnUpload').click(); }
                                $('.divMD').removeClass("last");
                                //initTags();
                                // Remove all tags                                                               
                                RemoveTags('.txtMemDisTags');
                            });
                            // Comment by Tooba     LoadDistags();
                            initPostFunction();
                            clearInterval(myInterval);
                        }
                        else {
                            redirectURL = "_Discussion.aspx?disid=" + $('#hdnDisID').val() + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val() + "&instanceid=" + $('#hdnInstanceid').val() + " .formdis";
                            if ($('#hdnOID').val() == "temp-user") {
                                redirectURL = window.location.href.toString() + " .formdis";
                            }
                            $('#bodyDis').load(redirectURL, function () {
                                // SendEmail(memberdisid, discussionid, memberid, $('.txtMemDis').val(), subject, $('#hdn_folderid').val(), minutes)
                                console.log('loaded all');
                                InitializePage();
                                // initPostFunction();
                                if ($('#fileUpload1 object').size() <= 0) { $('#hdnMemDisID').val(memberdisid); $('.btnUpload').click(); }
                            });
                            //Comment by Tooba LoadDistags();
                            initPostFunction();
                            clearInterval(myInterval);
                        }
                    }
                }, 1000);
                if (tags != null && tags != "") {
                    LoadDistags();
                }
            }
            else {
                var fileUpdate = response.d.split(':')[response.d.split(':').length - 1];
                //var fileUpdate = UpdateDiscussionFile('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val(), memdisid);
                if (fileUpdate == "1") {
                    getDisFiles();
                }
                var editedTages = tags.split(',');
                var ul = "<ul class=\"tagsul tagsulfr\">";
                for (var i = 0; i < editedTages.length; i++) {
                    ul += "<li>";
                    ul += "<a href=\"javascript:void(0)\" id=\"aTag\" class=\"aTag\" data-tag=\"" + editedTages[i] + "\">" + editedTages[i] + "</a>";
                    ul += "</li>";
                }
                ul += "</ul>";
                if (conversation == "updatetagonly") {
                    if ($('.divMD[data-rowkey="' + $('#hdnTCom').val() + '"] .tagsulfr').size() > 0) {
                        $('.divMD[data-rowkey="' + $('#hdnTCom').val() + '"] .tagsulfr').replaceWith(ul);
                    }
                    else {
                        $('.divMD[data-rowkey="' + $('#hdnTCom').val() + '"] div[id*="divMemDisO"]').after(ul);
                    }
                    $('.divMD[data-rowkey="' + $('#hdnTCom').val() + '"] div[id*="divMemDisO"]').attr('data-tags', tags);
                    $('#hdnTCom').val('0');
                    $('#addtag').modal('hide');
                }

                else {
                    if ($('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] .tagsulfr').size() > 0) {
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] .tagsulfr').replaceWith(ul);
                    }
                    else {
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[id*="divMemDisO"]').after(ul);
                    }
                    $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[id*="divMemDisO"]').attr('data-tags', $('#txtMemDisTagsU').val());
                    $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[id*="divMemDisO"]').html(conversation);
                    if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5") {
                        //$('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[class*="text"]')[0].lastElementChild.innerText = "";
                    }
                    else {
                        if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {

                            //Subtraction 
                            $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] [id*= "hdnIsSubtracted"]').val(isSubtracted);
                            $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] [id*= "hdnSubtractHrsReason"]').val(SubtractReason);

                            if (isSubtracted == true || isSubtracted == "true" || isSubtracted == "True") {
                                $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] span[class*="EditHours"]')[0].innerText = "Time Subtracted: - " + $("#hoursEdit").val() + " Hours " + $("#minutesEdit").val() + " Minutes";
                            }
                            else {
                                $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] span[class*="EditHours"]')[0].innerText = "Time Spent: " + $("#hoursEdit").val() + " Hours " + $("#minutesEdit").val() + " Minutes";

                            }
                            //End

                            $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"]').find("[id*=HdnSummary]").val($("#summaryEdit").val());
                            //$('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[class*="text"]')[0].lastElementChild.value = $("#summaryEdit").val();

                            // $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] div[class*="text"]')[0].lastElementChild.innerText = "Time Spent: " + $("#hoursEdit").val() + " Hours " + $("#minutesEdit").val() + " Minutes";
                        }
                    }
                    if (fileUpdate == "1" || singleFileDelete == "1")// For new files
                    {
                        var res = GetMemberDiscussionFiles($('#hdnECom').val());
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] li[class*= "msgAttachment"]').remove();
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] ul[class*= "CommentFiles"]').append(res);
                    }
                    else { //Previous files html
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] li[class*= "msgAttachment"]').remove();
                        $('#files li input').remove();
                        var updatedFiles = $('#files li');
                        $('.divMD[data-rowkey="' + $('#hdnECom').val() + '"] ul[class*= "CommentFiles"]').append(updatedFiles.clone());
                    }
                    //if ($('#fileUpload2 object').size() <= 0) { $('#hdnMemDisID').val(memberdisid); $('.btnUpload').click(); }
                    $('#hdnECom').val('0');
                    $('#editcomment').modal('hide');
                }
                if (tags != "" && tags != null) {
                    LoadDistags();
                    LoadTagClick();
                }
            }
            if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {
                if (minutes != "" && minutes != 0) {
                    LoadSummary(discussionid);
                }
                $(".summary").show();
            }
            else {
                $(".summary").hide();
            }
            initPostFunction();
        },
        error: function (response) {
            console.log("some problem in saving data")
        }
    });
}
function LoadSummary(discussionid) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/DisplaySummary',
        data: "{'discussionid':'" + discussionid + "','isNew':'isNew'}",
        async: true,
        success: function (response) {
            $("#totalDisSubHrs").text(response.d[0]);
            $("#totalDisMin").text(response.d[1]);
            // $("#totalDisMin").text(response.d);
        }
        , error: function ()
        { console.log("some problem in loading summary"); }
    });
}
function DeleteMemberDiscussion(memdisid) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/DeleteMemberDiscussion',
        data: "{'memdisid':'" + memdisid + "', 'currmemberid':'" + $('#hdnMemberID').val() + "', 'currmembername':'" + $('#hdnMN').val() + "', 'oid':'" + $('#hdnOID').val() + "'}",
        async: false,
        success: function (response) {
            console.log(response.d);
            $('.divMD[data-rowkey="' + memdisid + '"]').find('div[id*="divMemDisO"]').css('color', '#ccc').html(response.d.toString().split(':')[2].toString() + ":" + response.d.toString().split(':')[3].toString());
            //$('.divMD[data-rowkey="' + memdisid + '"]').find('div[id*="divMemDisO"]').css('padding-bottom', '5px');
            //$('.divMD[data-rowkey="' + memdisid + '"]').find('div[id*="divMemDisO"]').parent().css('margin-top', '10px').css('background-color', '#d89f9f');
            $('.divMD[data-rowkey="' + memdisid + '"] a.lnkDeleteComment').remove();
            $('.divMD[data-rowkey="' + memdisid + '"]').find('span[class*="lblDateTime"]').remove();
            $('.divMD[data-rowkey="' + memdisid + '"]').find('.msgAttachment').parent().remove();
            $('.divMD[data-rowkey="' + memdisid + '"]').find('.cmntMenudrop').remove();
            $('.divMD[data-rowkey="' + memdisid + '"]').find('span[id*="lblHoursMinutes"]').remove();
            $('.divMD[data-rowkey="' + memdisid + '"]').find('.tagsul').remove();
            $('#removecomment').modal('hide');
            DeleteDiscussionFiles('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val());
            getDisFiles();
            randomizeAll();
            LoadDisList();
            LoadDistags();
            if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {
                LoadSummary($('#hdnDisID').val());
                $(".summary").show();
            }
            else {
                $(".summary").hide();
            }
        },
        error: function () {
            //$(trTeam).append("<span id='info'>some problem in saving data</span>");
            //createInfoSpan(trTeam, "some problem in saving data");
            //resizeIframePeriodically();
        }
    });
}

function DeleteDiscussionFile(disfileid) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/DeleteDiscussionFile',
        data: "{'disfileid':'" + disfileid + "'}",
        async: true,
        success: function (response) {
            console.log(response.d);
            //createInfoSpan(trTeam, "Member Deleted Successfully!");
            //$('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
            //getDisFiles();
            //try { resizeIframePeriodically(); } catch (e) { }
        },
        error: function () {
            //$(trTeam).append("<span id='info'>some problem in saving data</span>");
            //createInfoSpan(trTeam, "some problem in saving data");
            //resizeIframePeriodically();
        }
    });
}
function DeleteDiscussionFiles(memberdisid) {
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/DeleteDiscussionFiles',
        data: "{'memberdisid':'" + memberdisid + "'}",
        async: true,
        success: function (response) {
            console.log(response.d);
            //createInfoSpan(trTeam, "Member Deleted Successfully!");
            //$('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
            //getDisFiles();
            //try { resizeIframePeriodically(); } catch (e) { }
        },
        error: function () {
            //$(trTeam).append("<span id='info'>some problem in saving data</span>");
            //createInfoSpan(trTeam, "some problem in saving data");
            //resizeIframePeriodically();
        }
    });
}
function GetMemberDiscussionFiles(memberDisID) {
    var ret = "";
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/GetMemberDiscussionFiles',
        data: "{'memberDisID':'" + memberDisID + "'}",
        async: false,
        success: function (response) {
            //console.log(response.d);
            ret = response.d;
        },
        error: function () {
            console.log(response.d);
        }
    });
    return ret;
}
function UpdateDiscussionFile(memberDisID, updatedMemberDisID) {
    var ret = "";
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: 'Discussion.aspx/UpdateDiscussionFile',
        data: "{'memberDisID':'" + memberDisID + "', 'updatedMemberDisID':'" + updatedMemberDisID + "'}",
        async: false,
        success: function (response) {
            console.log(response.d);
            ret = response.d.split(':')[2];
        },
        error: function () {
        }
    });
    return ret;
}
function initiateContextMenu(selector) {
    //$.contextMenu({
    //    selector: selector,
    //    trigger: 'left',
    //    callback: function (key, options) {
    //        var dataid = $(this).attr('data-id');
    //        console.log(dataid);
    //        if (key == 'delete') {
    //            ConfirmDelete(".divDialog", "Delete?", "Are you sure you want to delete this comment?", function () {
    //                DeleteMemberDiscussion(dataid);
    //            });
    //        }
    //    },
    //    items: {
    //        "delete": { name: "Delete" }
    //    }
    //});
}

function initTags() {
    //$('.txtTags:visible').tagEditor({
    //    clickDelete: false
    //});
    //setTimeout(function () {
    //    if (!$('.txtTags').next('ul').hasClass('ultags')) {
    //        $('.txtTags').next('ul').addClass('ultags');
    //    }
    //}, 500)
}
function CopyTags(selectorFrom, selectorTo) {
    RemoveTags(selectorTo);
    var tags = $(selectorFrom).tagEditor('getTags')[0].tags;
    for (i = 0; i < tags.length; i++) {
        $(selectorTo).tagEditor('addTag', tags[i]);
    }
}
function RemoveTags(selectorFrom) {
    var tags = $(selectorFrom).tagEditor('getTags')[0].tags;
    for (i = 0; i < tags.length; i++) {
        $(selectorFrom).tagEditor('removeTag', tags[i]);
    }
}


function initPostFunction() {
    //$('.lnkDeleteComment').unbind('click');
    //$('.lnkDeleteComment').bind('click', function () {
    //    $('#hdnDelMDID').val($(this).attr('data-rowkey'));
    //});
    $(document).off('click', '.lnkDeleteComment').on('click', '.lnkDeleteComment', function () {
        $('#hdnDelMDID').val($(this).attr('data-rowkey'));
    });

    $(document).off('click', '.lnkEditComment').on('click', '.lnkEditComment', function () {
        var rowkey = $(this).attr('data-rowkey');
        $('#hdnECom').val($(this).attr('data-rowkey'));
        $('#hdnMID').val($(this).attr('data-messageid'));
        $('.txtMemDisU').val($('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').html());
        var tags = $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags');
        //Hour Minutes
        if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5") {
            $(".hours").hide();
        }
        else if ($("#hdnRoleID").val() == "1" || $("#hdnRoleID").val() == "2") {
            if ($("#hdnIsHourlyProject").val() == "true") {
                $(".hours").show();

                //Subtract Hours
                var IsSubtract = $('.divMD[data-rowkey="' + rowkey + '"] [id*= "hdnIsSubtracted"]').val();
                var SubReason = $('.divMD[data-rowkey="' + rowkey + '"] [id*= "hdnSubtractHrsReason"]').val();

                $("#hdnIsSubtracted").val(IsSubtract);
                $("#hdnSubReason").val(SubReason);

                if (IsSubtract == "True" || IsSubtract == "true" || IsSubtract == true) {
                    $("#ehoursSubtract").val("2");
                    $('#summaryEdit').attr('style', 'display: none !important');
                    $('#edivHrsSubReason').show();
                }
                else {
                    $("#ehoursSubtract").val("1");
                    $('#summaryEdit').attr('style', 'width: 240px; display: inline-block !important');
                    $('#edivHrsSubReason').hide();
                }

                var found = false;
                $("#eHrsSubReason option").each(function (i) {
                    if ($(this).val() == SubReason) {
                        $("#eHrsSubReason").val(SubReason);
                        $("#esubtractionreason").attr('style', 'display: none !important');
                        found = true;
                    }
                });

                if (found == false) {
                    $("#eHrsSubReason").val("Other");
                    $("#esubtractionreason").val(SubReason);
                    $("#esubtractionreason").attr('style', 'width: 280px;display: inline-block !important');
                }

                $("#ehoursSubtract").show();


                $("#hoursEdit").keypress(function (e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
                $("#minutesEdit").keypress(function (e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                    }
                });
                var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] span[class*= "EditHours"]')[0].innerText;
                // var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.innerText;
                //var summary = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.value;
                var summary = $('.divMD[data-rowkey="' + rowkey + '"] [id*="HdnSummary"]').val();
                var arr = hourmint.split(' ');
                if (IsSubtract == "True" || IsSubtract == "true" || IsSubtract == true) {
                    $('#hoursEdit').val(arr[3]);
                    $('#minutesEdit').val(arr[5]);
                }
                else {
                    $('#hoursEdit').val(replaceAll(arr[2], '-', ''));
                    $('#minutesEdit').val(replaceAll(arr[4], '-', ''));
                }
                $('#summaryEdit').val(summary);

            }
            else { $(".hours").hide(); $('#summaryEdit').hide(); }
        }
        else {
            var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] span[class*= "EditHours"]')[0].innerText;
            var summary = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.value;
            //var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.innerText;
            var arr = hourmint.split(' ');
            $('#hoursEdit').val(arr[2]);
            $('#minutesEdit').val(arr[4]);
            $(".hours").hide();
            $('#summaryEdit').val(summary);
            $('#summaryEdit').hide();
        }

        RemoveTags('#txtMemDisTagsU');
        try {
            var tags = tags.split(',');
            try {
                $('#txtMemDisTagsU').tagEditor('destroy');
            } catch (e) { }
            $('#txtMemDisTagsU').val(tags);
            $('#txtMemDisTagsU').tagEditor({
                placeholder: 'Enter tags ...',
            });
        } catch (e) { console.log(e.message); }
        initIzFM('.fileUpload2', '', "D" + $('#hdnDisID').val() + "M" + $("#hdnMemberID").val());
        $('#files').html('');
        $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"] input[class*="deleteFile"]').remove();
        var filesList = $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"]');
        var fileCopy = filesList.clone();
        for (var i = 0; i < filesList.length; i++) {
            var filename = $(filesList).find('a[class*="anchorFileName"]')[i].innerText;
            //var filename = $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"] a[class*= "anchorFileName"] ')[i].innerText;
            $($(filesList)[i]).append("<input type='button' data-info='" + rowkey + "' id='" + filename + "' value='Delete' class='deleteFile'/>");
        }
        $('.divMD[data-rowkey="' + rowkey + '"] ul[class*= "CommentFiles"]').html(fileCopy);
        $('#files').html(filesList);
    });
    //$('.lnkEditComment').unbind('click');
    //$('.lnkEditComment').click(function () {
    //    var rowkey = $(this).attr('data-rowkey');
    //    $('#hdnECom').val($(this).attr('data-rowkey'));
    //    $('.txtMemDisU').val($('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').html());
    //    //$('.txtMemDisTagsU').val($('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags'));
    //    var tags = $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags');
    //    //Hour Minutes

    //    if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5") {
    //        $(".hours").hide();
    //    }

    //    else if ($("#hdnRoleID").val() == "1" || $("#hdnRoleID").val() == "2") {
    //        if ($("#hdnIsHourlyProject").val() == "true") {
    //            $(".hours").show();
    //            $("#hoursEdit").keypress(function (e) {
    //                //if the letter is not digit then display error and don't type anything
    //                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    //                    return false;
    //                }
    //            });
    //            $("#minutesEdit").keypress(function (e) {
    //                //if the letter is not digit then display error and don't type anything
    //                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    //                    return false;
    //                }
    //            });
    //            var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] span[class*= "EditHours"]')[0].innerText;
    //            // var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.innerText;
    //            var summary = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.value;
    //            var arr = hourmint.split(' ');
    //            $('#hoursEdit').val(arr[2]);
    //            $('#minutesEdit').val(arr[4]);
    //            $('#summaryEdit').val(summary);
    //        }
    //        else { $(".hours").hide(); $('#summaryEdit').hide(); }
    //    }
    //    else {
    //        var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] span[class*= "EditHours"]')[0].innerText;
    //        var summary = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.value;
    //        //var hourmint = $('.divMD[data-rowkey="' + rowkey + '"] div[class*="text"]')[0].lastElementChild.innerText;
    //        var arr = hourmint.split(' ');
    //        $('#hoursEdit').val(arr[2]);
    //        $('#minutesEdit').val(arr[4]);
    //        $(".hours").hide();
    //        $('#summaryEdit').val(summary);
    //        $('#summaryEdit').hide();
    //    }

    //    RemoveTags('#txtMemDisTagsU');
    //    try {
    //        var tags = tags.split(',');
    //        //for (i = 0; i < tags.length; i++) {
    //        try {
    //            $('#txtMemDisTagsU').tagEditor('destroy');
    //        } catch (e) { }
    //        $('#txtMemDisTagsU').val(tags);
    //        $('#txtMemDisTagsU').tagEditor({
    //            placeholder: 'Enter tags ...',
    //            //forceLowercase: false
    //        });
    //        //$('#txtMemDisTagsU').tagEditor('addTag', tags[i]);
    //        //}
    //    } catch (e) { console.log(e.message); }
    //    //CopyTags('.txtMemDisTags', '.txtMemDisTagsU');
    //    //$('.txtMemDisTagsU').tagEditor({
    //    //    //initialTags: ['Hello', 'World', 'Example', 'Tags'],
    //    //    delimiter: ', ', /* space and comma */
    //    //    placeholder: 'Enter tags ...',
    //    //    forceLowercase: false
    //    //});
    //    //setTimeout(function () {

    //    //}, 1000);
    //    initIzFM('.fileUpload2', '', "D" + $('#hdnDisID').val() + "M" + $("#hdnMemberID").val());
    //    $('#files').html('');
    //    $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"] input[class*="deleteFile"]').remove();
    //    var filesList = $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"]');
    //    var fileCopy = filesList.clone();
    //    for (var i = 0; i < filesList.length; i++) {
    //        var filename = $(filesList).find('a[class*="anchorFileName"]')[i].innerText;
    //        //var filename = $('.divMD[data-rowkey="' + rowkey + '"] li[class*= "msgAttachment"] a[class*= "anchorFileName"] ')[i].innerText;
    //        $($(filesList)[i]).append("<input type='button' data-info='" + rowkey + "' id='" + filename + "' value='Delete' class='deleteFile'/>");
    //    }
    //    $('.divMD[data-rowkey="' + rowkey + '"] ul[class*= "CommentFiles"]').html(fileCopy);
    //    $('#files').html(filesList);
    //});

    $("body").off('click', '.deleteFile').on('click', '.deleteFile', function () {
        var input = $(this);
        var memberDisId = $(this).attr('data-info');
        var fileName = $(this).attr('id');
        //alert(fileName);
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: 'Discussion.aspx/DeleteMemberDiscussionFiles',
            data: "{'memberdisid':'" + memberDisId + "','fileName':'" + fileName + "'}",
            async: false,
            success: function (response) {
                // alert("File has been removed");
                singleFileDelete = "1";
                $(input).attr('disabled', 'disabled');
                $(input).css("text-decoration", "line-through");
                $(input).parent().css("text-decoration", "line-through");
                $(input).parent().css("display", "none");
                console.log(response.d);
            },
            error: function (response) {
                console.log(response.d);
            }
        });
    });
    //$('.lnkAddTag').unbind('click');
    //$('.lnkAddTag').click(function () {
    //    var rowkey = $(this).attr('data-rowkey');
    //    $('#hdnTCom').val($(this).attr('data-rowkey'));
    //    //$('.txtMemDisTagsU').val($('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags'));
    //    var tags = $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags');
    //    //RemoveTags('#txtAddTags');        
    //    try {
    //        //var tags = tags.split(',');
    //        //for (i = 0; i < tags.length; i++) {
    //        try {
    //            $('#txtAddTags').val('');
    //            $('#txtAddTags').tagEditor('destroy');
    //        } catch (e) { }
    //        $('#txtAddTags').attr('data-divMemDisOID', $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('id'));
    //        $('#txtAddTags').attr('data-tags', tags);
    //        //$('#txtAddTags').val(tags);
    //        $('#txtAddTags').tagEditor({
    //            placeholder: 'Enter tags ...',
    //            //forceLowercase: false
    //        });
    //        //$('#txtMemDisTagsU').tagEditor('addTag', tags[i]);
    //        //}
    //    } catch (e) { console.log(e.message); }
    //    //CopyTags('.txtMemDisTags', '.txtMemDisTagsU');
    //    //$('.txtMemDisTagsU').tagEditor({
    //    //    //initialTags: ['Hello', 'World', 'Example', 'Tags'],
    //    //    delimiter: ', ', /* space and comma */
    //    //    placeholder: 'Enter tags ...',
    //    //    forceLowercase: false
    //    //});
    //    //setTimeout(function () {

    //    //}, 1000);
    //});
    $(document).off('click', '.lnkAddTag').on('click', '.lnkAddTag', function () {
        var rowkey = $(this).attr('data-rowkey');
        $('#hdnTCom').val($(this).attr('data-rowkey'));
        var tags = $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('data-tags');
        try {
            try {
                $('#txtAddTags').val('');
                $('#txtAddTags').tagEditor('destroy');
            } catch (e) { }
            $('#txtAddTags').attr('data-divMemDisOID', $('.divMD[data-rowkey="' + rowkey + '"] div[id*="divMemDisO"]').attr('id'));
            $('#txtAddTags').attr('data-tags', tags);
            $('#txtAddTags').tagEditor({
                placeholder: 'Enter tags ...',
            });
        } catch (e) { console.log(e.message); }
    });
    $('#hours').val('');
    $('#minutes').val('');
    $('#ShowSummaryBox').val('');

    $("#hoursSubtract").val("1");
    $("#divHrsSubReason").hide();
    $('#summarywords').attr('style', 'width: 240px; display: inline-block !important');

}


function getDisFiles() {
    var url = "_discussion_controls/_FilesPerDiscussion1.aspx?disid=" + $('#hdnDisID').val() + "&C=" + $('#hdnCCC').val() + " .divDisFiles";
    $('#discussionsfiles').load(url, function () {
        $('.dis_files li').each(function (i) {
            if (!$(this).hasClass('loadmorefiles')) {
                if (i > 4) { $(this).hide(); }
            }
        });
        $('#hrefLoadMpreFiles').click(function () {
            $('.dis_files li').show();
            $('.loadmorefiles').remove();
        })
        //try { resizeIframe(); } catch (e) { }
    });
}
function InitializePage() {
    //$('.btnRefresh').click(function () {
    //    window.location.reload(true);
    //    return false;
    //});
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        //$('#fileUpload1').hide();
    }


    $('#h1Title').click(function () {
        DiscussionTitle = $(this).text();
    });
    $('#h1Title').blur(function () {
        if ($('#h1Title').text() == '') {
            $(this).text(DiscussionTitle);
        }
        SaveDiscussion($('#hdnDisID').val(), $('#h1Title').text(), 0, $('#hdninstanceid').val(), 'NA', $('#hdnMemberID').val());
    });
    //$('.btnStatus').click(function () {
    //    if ($(this).attr('data-command') == "Close") {
    //        SaveDiscussion($('#hdnDisID').val(), 'NA', 0, $('#hdninstanceid').val(), 'Close', $('hdnMemberID').val());
    //    }
    //    else {
    //        SaveDiscussion($('#hdnDisID').val(), 'NA', 0, $('#hdninstanceid').val(), 'Open', $('hdnMemberID').val());
    //    }
    //});
    getDisFiles();
    try {
        $('.txtMemDis').ckeditor();
        $('.txtMemDisU').ckeditor();
    } catch (e) {
        console.log(e.message);
    }

    initIzFM('.fileUpload1', '', "D" + $('#hdnDisID').val() + "M" + $("#hdnMemberID").val());
    $('.btnSend').click(function () {
        if ($('#h1Title').text() == "Untitled" && ($('#h1Title').attr('contenteditable') == "false" || $('#h1Title').attr('contenteditable') == undefined)) {
            alert("Provide discussion title.");
            $('#h1Title').focus();
        }
        else {
            // $('body').loader('show', { overlay: true });
            if (DisallowedContents != null) {
                var dac = "";
                for (var i = 0; i < DisallowedContents.length; i++) {
                    if (DisallowedContents[i] != "") {
                        if ($('.txtMemDis').val().indexOf(DisallowedContents[i]) > -1) {
                            dac += DisallowedContents[i];
                            if (i < DisallowedContents.length - 1) {
                                dac += ", ";
                            }
                        }
                    }
                }
                if (dac != "") {
                    alert('One or more words in your message are not allowed. Please remove the following words and try again:\n\n "' + dac + '"');
                    return false;
                }
            }

            //var conversationText = $('.txtMemDis').val();
            //linkified = false;
            //conversationText = linkify(conversationText);
            ////linkifiedOnce = false;
            //if (linkified && !linkifiedOnce) {                        
            //    $('.txtMemDis').val(conversationText);
            //    linkifiedOnce = true;
            //    return false;
            //}
            var minutes;
            var summary = "";
            if ($('#summarywords').val() == undefined) {
                summary = "";
            }
            else {
                summary = $('#summarywords').val();
                summary = JSON.stringify(summary);
                summary = summary.slice(1, -1);
                summary = replaceAll(summary, '\\n', '');
                summary = replaceAll(summary, '<p>&nbsp;</p>', '');
            }
            if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5") {
                minutes = 0;
            }
            else {
                if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {

                    if ($('#summarywords').val() == "") {
                        if (($("#hdnHourRequiredEachPost").val() == "false") && ($("#hdnZeroHourPost").val() == "false")) { }
                        else
                        {
                            if ($("#hdnZeroHourPost").val().toLowerCase() == "false" || ($("#hdnZeroHourPost").val().toLowerCase() == "true"
                            && !($('#hours').val() == "0" && $('#minutes').val() == "0") && !($('#hours').val() == "" && $('#minutes').val() == ""))) {

                                alert("Please provide few words on work you performed!");
                                return false;

                            }
                        }
                    }


                    if ($("#hdnHourRequiredEachPost").val().toLowerCase() == "true" && $("#hdnZeroHourPost").val().toLowerCase() == "false") {

                        if ($('#hours').val() == "" && $('#minutes').val() == "") {
                            alert("Hours required for each post!");
                            return false;
                        }
                        else {
                            if ($('#hours').val() == "")
                                $('#hours').val(0);
                            if ($('#minutes').val() == "")
                                $('#minutes').val(0);
                        }

                        if ($('#hours').val() == "0" && $('#minutes').val() == "0") {
                            if ($("#hdnZeroHourPost").val().toLowerCase() == "false") {
                                alert("Zero Hour cannot be posted!");
                                return false;
                            }
                        }



                    }
                    else {
                        if ($('#hours').val() == "")
                            $('#hours').val(0);
                        if ($('#minutes').val() == "")
                            $('#minutes').val(0);
                    }
                    minutes = parseInt($('#hours').val() * 60) + parseInt($('#minutes').val());
                }
                else {
                    minutes = 0;
                }
            }
            $('#summarywords').val('');

            isSubtracted = false;
            SubtractReason = "";
            if ($('#hoursSubtract').val() == 2) {
                isSubtracted = true;
                if ($("#HrsSubReason").val() != "Other") {
                    SubtractReason = $("#HrsSubReason").val();
                }
                else {
                    SubtractReason = $("#subtractionreason").val();
                    if (SubtractReason == "") {
                        alert("Hours subtraction reason required!");
                        return false;
                    }
                }
            }

            if ($('.uploadify-queue-item').size() != $('a.completed').size()) {
                if (!($(this).hasClass('button_loading'))) {
                    $(this).val('Posting...');
                    $(this).addClass('button_loading');
                    var sendInterval = setInterval(function () {
                        if ($('.uploadify-queue-item').size() == $('a.completed').size()) {
                            $('#fileUpload1').uploadify('cancel', '*');
                            if (!($(this).hasClass('button_loading'))) {
                                $(this).val('Posting...');
                                $(this).addClass('button_loading');
                                SaveMemberDiscussion('0', "", $('.txtMemDisTags').val(), $('#hdnDisID').val(), $('#hdninstanceid').val(), $("#hdnMemberID").val(), $('#hdnTeamMember').val(), $('.txtMemDis').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), minutes, true, summary, $('#totalcomments').val(), isSubtracted, SubtractReason);
                            }
                            clearInterval(sendInterval);
                        }
                    }, 400);
                }
            }
            else {
                if ($('#fileUpload1 object').size() > 0) {
                    $('#fileUpload1').uploadify('cancel', '*');
                };

                if (!($(this).hasClass('button_loading'))) {
                    $(this).val('Posting...');
                    $(this).addClass('button_loading');
                    SaveMemberDiscussion('0', "", $('.txtMemDisTags').val(), $('#hdnDisID').val(), $('#hdninstanceid').val(), $("#hdnMemberID").val(), $('#hdnTeamMember').val(), $('.txtMemDis').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), minutes, true, summary, $('#totalcomments').val(), isSubtracted, SubtractReason);
                }
            }

            // $('body').loader('hide');
        }
        return false;
    });
    $('.btnUpdate').click(function () {

        var minutes;
        var summary = "";
        if ($('#summaryEdit').val() == undefined) {
            summary = "";
        }
        else {
            summary = $('#summaryEdit').val();
            summary = JSON.stringify(summary);
            summary = summary.slice(1, -1);
            summary = replaceAll(summary, '\\n', '');
            summary = replaceAll(summary, '<p>&nbsp;</p>', '');
        }

        if ($("#hdnRoleID").val() == "4" || $("#hdnRoleID").val() == "5" || $("#hdnRoleID").val() == "6" || $("#hdnRoleID").val() == "7") {
            minutes = 0;
        }
        else {
            if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {
                if ($("#hdnHourRequiredEachPost").val().toLowerCase() == "true") {
                    if ($('#hoursEdit').val() == "" && $('#minutesEdit').val() == "") {
                        alert("Hours required for each post!");
                        return false;
                    }
                    else {
                        if ($('#hoursEdit').val() == "")
                            $('#hoursEdit').val(0);
                        if ($('#minutesEdit').val() == "")
                            $('#minutesEdit').val(0);
                    }
                    if ($('#hoursEdit').val() == "0" && $('#minutesEdit').val() == "0") {
                        if ($("#hdnZeroHourPost").val().toLowerCase() == "false") {
                            alert("Zero Hour cannot be posted!");
                            return false;
                        }
                    }
                }
                else {
                    if ($('#hoursEdit').val() == "")
                        $('#hoursEdit').val(0);
                    if ($('#minutesEdit').val() == "")
                        $('#minutesEdit').val(0);
                }
                minutes = parseInt($('#hoursEdit').val() * 60) + parseInt($('#minutesEdit').val());
            }
            else {
                minutes = 0;
            }
        }

        isSubtracted = false;
        SubtractReason = "";
        if ($('#ehoursSubtract').val() == 2) {
            isSubtracted = true;
            if ($("#eHrsSubReason").val() != "Other") {
                SubtractReason = $("#eHrsSubReason").val();
            }
            else {
                SubtractReason = $("#esubtractionreason").val();
                if (SubtractReason == "") {
                    alert("Hours subtraction reason required!");
                    return false;
                }
            }
        }


        if ($('.uploadify-queue-item').size() != $('a.completed').size()) {
            if ($('.uploadify-queue-item').size() == $('a.completed').size()) {
                $('#fileUpload2').uploadify('cancel', '*');
                SaveMemberDiscussion($('#hdnECom').val(), "", $('.txtMemDisTagsU').val(), $('#hdnDisID').val(), $('#hdninstanceid').val(), $('#hdnMemberID').val(), $('#hdnTeamMember').val(), $('.txtMemDisU').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), minutes, false, summary, $('#hdnMID').val(), isSubtracted, SubtractReason);
            }
        }
        else {
            if ($('#fileUpload2 object').size() > 0) {
                $('#fileUpload2').uploadify('cancel', '*');
            }
            SaveMemberDiscussion($('#hdnECom').val(), "", $('.txtMemDisTagsU').val(), $('#hdnDisID').val(), $('#hdninstanceid').val(), $('#hdnMemberID').val(), $('#hdnTeamMember').val(), $('.txtMemDisU').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), minutes, false, summary, $('#hdnMID').val(), isSubtracted, SubtractReason);
        }
        //$('.txtMemDisTagsU').tagEditor('destroy');
    })
    $('.btnAddTag').click(function () {
        var tags;
        var minutes = null;
        var summary = false;
        if ($('#txtAddTags').attr('data-tags') != "") { tags = $('#txtAddTags').attr('data-tags') + "," + $('#txtAddTags').val(); }
        else { tags = $('#txtAddTags').val(); }

        SaveMemberDiscussion($('#hdnTCom').val(), "", tags, $('#hdnDisID').val(), $('#hdninstanceid').val(), $('#hdnMemberID').val(), $('#hdnTeamMember').val(), 'updatetagonly', "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), minutes, false, summary, "-1", false, "");
        //$('.txtMemDisTagsU').tagEditor('destroy');
    })
    $('.btnAddTeam, .btnAddClient, .btnStatus').click(function () {
        if ($('#h1Title').text() == "Untitled" && ($('#h1Title').attr('contenteditable') == "true" || $('#h1Title').attr('contenteditable') == undefined)) {
            alert("Provide discussion title.");
            $('#h1Title').focus();
            return false;
        }
    });
    /*Update IsHourly & Title of Discussion */

    $("#btnUpdateDisName").click(function () {
        var chkd = $("#chkIsHourly").is(':checked');
        var title = $("#txtDisName").val();
        var projctManagerId = $('#ddlPTeamMembers option:selected').val();
        var assitantProjectManagerId = $('#ddlAPTeamMembers option:selected').val();
        var hourRequiredEachPost;
        var zeroHourPost;
        var HideHours = false;
        var ShowSummary = false;
        var sendSummary = false;
        var duration = "0";
        var emailContent = "";
        if (chkd == true) {
            hourRequiredEachPost = $("#chkHourRequired").is(':checked');


            HideHours = $("#HideHoursFromClient").is(':checked');
            ShowSummary = $("#ShowSummaryBox").is(':checked');
            if (hourRequiredEachPost == true) {
                zeroHourPost = $("#chkZeroHour").is(':checked');
            }
            else {
                zeroHourPost = false;
            }
            sendSummary = $("#chkSendSummary").is(':checked');
            if (sendSummary == true) {
                duration = $("#txtDuration").val();
                emailContent = $("#txtEmailContent").val();
            }
            else {
                duration = "0";
                emailContent = "";
            }
        }
        else { hourRequiredEachPost = false; zeroHourPost = false; }
        SaveDiscussionTitleIsHourly($('#hdnDisID').val(), title, 0,
          $('#hdninstanceid').val(), 'NA', $('#hdnMemberID').val(), chkd, hourRequiredEachPost, zeroHourPost,
          projctManagerId, assitantProjectManagerId, HideHours, ShowSummary, sendSummary, duration, emailContent);

    });
    /*End*/
}

function linkify(text) {
    if (text) {
        // br
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?<br>/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                return '<a href="' + full_url.toString().replace('<br>', '') + '" target="_blank">' + url.toString().replace('<br>', '') + '</a>' + '<br>';
            }
        );
        // &nbsp;
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?&nbsp;/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                if (full_url.toString().indexOf('<br ') > -1) {
                    return '<a href="' + full_url.toString().replace('<br ', '') + '" target="_blank">' + url.toString().replace('<br ', '') + '</a> <br';
                }
                else {
                    return '<a href="' + full_url.toString().replace('&nbsp;', '') + '" target="_blank">' + url.toString().replace('&nbsp;', '') + '</a>';
                }

            }
        );
        // space
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))? /gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                if (full_url.toString().indexOf('<br ') > -1) {
                    return '<a href="' + full_url.toString().replace('<br ', '') + '" target="_blank">' + url.toString().replace('<br ', '') + '</a> <br';
                }
                else {
                    return '<a href="' + full_url.toString().replace(' ', '') + '" target="_blank">' + url.toString().replace(' ', '') + '</a>' + '&nbsp;';
                }

            }
        );
        // comma
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?,/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                return '<a href="' + full_url.toString().replace(',', '') + '" target="_blank">' + url.toString().replace(',', '') + '</a>' + ', ';
            }
        );
        // paragraph
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?<\/p>/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                return '<a href="' + full_url.toString().replace('</p>', '') + '" target="_blank">' + url.toString().replace('</p>', '') + '</a>' + '</p>';
            }
        );
        // list
        text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?<\/li>/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                return '<a href="' + full_url.toString().replace('</li>', '') + '" target="_blank">' + url.toString().replace('</li>', '') + '</a>' + '</li>';
            }
        );
        // backslash
        /* text = text.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?<\/li>/gi,
            function (url) {
                var full_url = url;
                if (!full_url.match('^https?:\/\/')) {
                    full_url = 'http://' + full_url;
                }
                linkified = true;
                return '<a href="' + full_url.toString().replace('\', '\\\') + '" target="_blank">' + url.toString().replace('\', '\\\') + '</a>' + '</li>';
            }
        );*/
    }

    return text;
}

$(function () {
    InitializePage();
    DeleteDiscussionFiles('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val());
    $('.aAddTeam').click(function () {
        //$('.divAssignProject:visible').hide("blind", 400);
        //$('.divManageMember:hidden').show("blind", 400);
        //$("input#rdoTeam").prop('checked', true);
        //setCheck();
        if ($("#hdnRoleID").val() == "6") {
            $("#AdminRights").hide();
        }
        else {
            $("#AdminRights").show();
        }
        $('.divManageTeam:hidden').show("blind", 400);
        $('.chkAddNewTeam input').prop('checked', false);
        if ($('.chkAddNewTeam input').is(':checked')) {
            $('.trExTeam').hide(200);
            $('.trNewTeam').show(200);
            $('.txtTeamAlias').val('');
            $('.txtTeamName').val('');
            $('.txtTeamEmail').val('');
            $('.txtTeamTitle').val('');
            $('#hdnSelTeamID').val('0');
            $('.btnAddTeam').val("Add");
            $('.txtTeamName').focus();
            $(".trAlias").show(200);
            $(".trTitle").show(200);
            $(".AdminRights").show(200);
        }
        else {
            $('.trExTeam').hide(200);
            $('.trNewTeam').show(200);
            $('.txtTeamAlias').val('');
            $('.txtTeamName').val('');
            $('.txtTeamEmail').val('');
            $('.txtTeamTitle').val('');
            $('#hdnSelTeamID').val('0');
            $('.btnAddTeam').val("Add");
            $('.trExTeam').show(200);
            $('.trNewTeam').hide(200);
            $(".trAlias").hide(200);
            $(".trTitle").hide(200);
            $(".AdminRights").hide(200);
        }
    })
    $('.aAddClient').click(function () {
        //$('.divAssignProject:visible').hide("blind", 400);
        //$('.divManageMember:hidden').show("blind", 400);
        //$("input#rdoClient").prop('checked', true);
        //setCheck();
        $('.divManageClient:hidden').show("blind", 400);
        $('.chkAddNewClient input').prop('checked', false);
        if ($('.chkAddNewClient input').is(':checked')) {
            $('.trExClient').hide(200);
            $('.trNewClient').show(200);
            $('.txtClientAlias').val('');
            $('.txtClientName').val('');
            $('.txtClientEmail').val('');
            $('.txtClientTitle').val('');
            $('#hdnSelClientID').val('0');
            $('.btnAddClient').val("Add");
            $('.txtClientName').focus();
        }
        else {
            $('.trExClient').hide(200);
            $('.trNewClient').show(200);
            $('.txtClientAlias').val('');
            $('.txtClientName').val('');
            $('.txtClientEmail').val('');
            $('.txtClientTitle').val('');
            $('#hdnSelClientID').val('0');
            $('.btnAddClient').val("Add");
            $('.trExClient').show(200);
            $('.trNewClient').hide(200);
        }
    });
    $('.btnCancelDiscussion').click(function () {
        $('.divAssignProject:visible').hide('blind', 400);
        return false;
    })
    $('.btnCancelTeam').click(function () {
        $('.divManageTeam:visible').hide('blind', 400);
        return false;
    })
    $('.btnCancelClient').click(function () {
        $('.divManageClient:visible').hide('blind', 400);
        return false;
    })
    $('.lnkClienkEdit').click(function () {
        if (!$('.chkAddNewClient input').is(':checked')) {
            $('.chkAddNewClient input').click();
            $('.chkAddNewClient input').prop('checked', 'true');
        }
        var memberID = $(this).attr('data-id');
        var alias = $(this).attr('data-alias');
        var email = $(this).attr('data-email');
        var roleID = $(this).attr('data-roleID');
        var title = $(this).attr('data-title');
        var name = $(this).attr('data-name');
        //roleID = parseInt(roleID);
        //if (roleID == 2) {
        //    $("input#rdoTeam").prop("checked", true);
        //    $("input#chkAdmin").prop("checked", true);
        //    $(".chkAdmin").show();
        //}
        //if (roleID == 3) {
        //    $("input#rdoTeam").prop("checked", true);
        //    $("input#chkAdmin").prop("checked", false);
        //    $(".chkAdmin").show();
        //}
        //if (roleID == 4) {
        //    $("input#rdoClient").prop("checked", true);
        //    $("input#chkAdmin").prop("checked", false);
        //    $(".chkAdmin").hide();
        //}
        //if (roleID == 5) {
        //    $("input#rdoClient").prop("checked", true);
        //    $("input#chkAdmin").prop("checked", false);;
        //    $(".chkAdmin").hide();
        //}
        //$('.txtTeamFullName').val(name);

        $('.txtClientAlias').val(alias);
        $('.txtClientName').val(name);
        $('.txtClientEmail').val(email);
        $('.txtClientTitle').val(title);
        $('#hdnSelClientID').val(memberID);
        $('.btnAddClient').val("Update Member");
        $('.txtClientName').focus();
        $('.divManageClient:hidden').show("blind", 400);
        $('.trClientName').hide(200);
        return false;
    });
    $('.lnkTeamEdit').click(function () {
        try {
            if ($("#hdnRoleID").val() == "6") {
                $("#AdminRights").hide();
            }
            else {
                $("#AdminRights").show();
            }


            if (!$('.chkAddNewTeam input').is(':checked')) {
                $('.chkAddNewTeam input').click();
                $('.chkAddNewTeam input').prop('checked', 'true');
            }
            var memberID = $(this).attr('data-id');
            var alias = $(this).attr('data-alias');
            var email = $(this).attr('data-email');
            var roleID = $(this).attr('data-roleID');
            var title = $(this).attr('data-title');
            var name = $(this).attr('data-name');
            roleID = parseInt(roleID);
            if (roleID == 2 || roleID == 1) {
                $("input#chkAdmin").prop("checked", true);
                $(".chkAdmin").show();
            }
            if (roleID == 3 || roleID == 7) {
                $("input#chkAdmin").prop("checked", false);
                $(".chkAdmin").show();
            }
            //if (roleID == 4) {
            //    $("input#rdoTeam").prop("checked", true);
            //    $("input#chkAdmin").prop("checked", false);
            //    $(".chkAdmin").hide();
            //}
            //if (roleID == 5) {
            //    $("input#rdoTeam").prop("checked", true);
            //    $("input#chkAdmin").prop("checked", false);;
            //    $(".chkAdmin").hide();
            //}
            //$('.txtTeamFullName').val(name);

            $('.txtTeamAlias').val(alias);
            $('.txtTeamName').val(name);
            $('.txtTeamEmail').val(email);
            $('.txtTeamTitle').val(title);
            $('#hdnSelTeamID').val(memberID);
            $('.btnAddTeam').val("Update Member");
            $('.txtTeamName').focus();
            $('.divManageTeam:hidden').show("blind", 400);
            $('.trTeamName').hide(200);

        } catch (e) {
            console.log(e.message);
        }
        return false;
    });
    $('.chkAddNewTeam input').change(function () {
        if ($(this).is(':checked')) {
            $('.trTeam').show(200);
            $('.trExTeam').hide(200);
            $('.trNewTeam').show(200);
            $('.txtTeamAlias').val('');
            $('.txtTeamName').val('');
            $('.txtTeamEmail').val('');
            $('.txtTeamTitle').val('');
            $('#hdnSelTeamID').val('0');
            $('.btnAddTeam').val("Add");
            $('.trTeamName').show(200);
            $('.txtTeamName').focus();
            $(".trAlias").show(200);
            $(".trTitle").show(200);
            $(".AdminRights").show(200);
        }
        else {
            $('.trExTeam').hide(200);
            $('.trNewTeam').show(200);
            $('.txtTeamAlias').val('');
            $('.txtTeamName').val('');
            $('.txtTeamEmail').val('');
            $('.txtTeamTitle').val('');
            $('#hdnSelTeamID').val('0');
            $('.btnAddTeam').val("Add");
            $('.trExTeam').show(200);
            $('.trNewTeam').hide(200);
            $(".trAlias").hide(200);
            $(".trTitle").hide(200);
            $(".AdminRights").hide(200);
            //$('.txtTeamAlias').parent().parent().hide();
            //$('.txtTeamTitle').parent().parent().hide();

            if ($($('.ddlTeamMembers option')[0]).text() == "No Records Found") {
                $('.trTeam').hide(200);
            }
        }
        return false;
    });
    $('.chkAddNewClient input').change(function () {
        if ($(this).is(':checked')) {
            $('.trClient').show(200);
            $('.trExClient').hide(200);
            $('.trNewClient').show(200);
            $('.txtClientAlias').val('');
            $('.txtClientName').val('');
            $('.txtClientEmail').val('');
            $('.txtClientTitle').val('');
            $('#hdnSelClientID').val('0');
            $('.btnAddClient').val("Add");
            $('.trClientName').show(200);
            $('.txtClientName').focus();
        }
        else {
            $('.trExClient').hide(200);
            $('.trNewClient').show(200);
            $('.txtClientAlias').val('');
            $('.txtClientName').val('');
            $('.txtClientEmail').val('');
            $('.txtClientTitle').val('');
            $('#hdnSelClientID').val('0');
            $('.btnAddClient').val("Add");
            $('.trExClient').show(200);
            $('.trNewClient').hide(200);
            if ($($('.ddlClientMembers option')[0]).text() == "No Records Found") {
                $('.trClient').hide(200);
            }
        }
        return false;
    });
    if ($('.trExTeam:visible').size() > 0) {
        $('.chkAddNewTeam input').prop('checked', false)
    }
    if ($('.trExClient:visible').size() > 0) {
        $('.chkAddNewClient input').prop('checked', false)
    }
    var clearLblInterval = setInterval(function () {
        //console.log("chaling");
        setTimeout(function () {
            $('.lblInfo').delay(2000).text('');
            if ($('.lblInfo').text() == '') {
                clearInterval(clearLblInterval);
            }
            //console.log("visible chaling");
        }, 10000);
    }, 100);
});

var saveSelection, restoreSelection;
var savedSelection;
if (window.getSelection && document.createRange) {
    saveSelection = function (containerEl) {
        var range = window.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function (containerEl, savedSel) {
        var charIndex = 0, range = document.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection && document.body.createTextRange) {
    saveSelection = function (containerEl) {
        var selectedTextRange = document.selection.createRange();
        var preSelectionTextRange = document.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function (containerEl, savedSel) {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}



function doSave() {
    savedSelection = saveSelection(document.getElementsByClassName('cke_wysiwyg_frame')[0].contentWindow.document.body);
}

function doRestore() {
    if (savedSelection) {
        restoreSelection(document.getElementsByClassName('cke_wysiwyg_frame')[0].contentWindow.document.body);
    }
}

$(window).load(function () {
    //initiateContextMenu('.disAction');
    initPostFunction();
    if ($('#hdnMoveToBottom').val() == "1") {
        $("#divMemberDiscussionContainer").animate({ scrollTop: $('#divMemberDiscussion').height() }, 1000);
    }
    $('.txtMemDis').focus();



    //var textChanged = true;  
    //CKEDITOR.instances['txtMemDis'].on('change', function() {                    
    //    if (textChanged) {                    
    //        doSave();
    //        var linkfiedText = linkify($('.txtMemDis').val());
    //        if (linkified) {
    //            textChanged = false;
    //            $('.txtMemDis').val(linkfiedText);
    //            try {
    //                doRestore();
    //            } catch (e) {
    //                alert(e.message);
    //            }

    //            linkified = false;
    //            setTimeout(function(){
    //                textChanged = true; 
    //            },1000);
    //        }                    

    //    }
    //});

});




var DisallowedContents;
var asked_link;
$(function () {
    if ($('#hdnRoleID').val() == 5) {
        $('.lnkClienkEdit').remove();
        $('.lnkClientRemove').remove();
        $('.lnkTeamEdit').remove();
        $('.lnkTeamRemove').remove();
        $('.spanTeam').remove();
        $('.divTeamMembers li').each(function () {
            var title = $(this).attr('title');
            var titleParts = title.toString().split('-');
            $(this).attr('title', titleParts[1].substr(1, titleParts[1].length));
        });
    }
    else if ($('#hdnRoleID').val() == 4) {
        $('.lnkClienkEdit').remove();
        $('.lnkClientRemove').remove();
        $('.lnkTeamEdit').remove();
        $('.lnkTeamRemove').remove();
        $('.spanTeam').remove();
        $('.divTeamMembers li').each(function () {
            var title = $(this).attr('title');
            var titleParts = title.toString().split('-');
            $(this).attr('title', titleParts[1].substr(1, titleParts[1].length));
        });
    }
    else if ($('#hdnRoleID').val() == 3) {
        $('.lnkClienkEdit').remove();
        $('.lnkClientRemove').remove();
        $('.lnkTeamEdit').remove();
        $('.lnkTeamRemove').remove();
        $('.spanTeam').remove();
        $('.spanClient').remove();
        $('.divClientMembers li').each(function () {
            var title = $(this).attr('title');
            var titleParts = title.toString().split('-');
            $(this).attr('title', titleParts[1].substr(1, titleParts[1].length));
        });
    }
    else if ($('#hdnRoleID').val() == 7) {
        $('.lnkClienkEdit').remove();
        $('.lnkClientRemove').remove();
        $('.lnkTeamEdit').remove();
        $('.lnkTeamRemove').remove();
        $('.spanTeam').remove();
        $('.spanClient').remove();
        $('.divClientMembers li').each(function () {
            var title = $(this).attr('title');
            var titleParts = title.toString().split('-');
            $(this).attr('title', titleParts[1].substr(1, titleParts[1].length));
        });
    }
    else if ($('#hdnRoleID').val() == 2) {
        $('.lnkTeamEdit').remove();
        $('.lnkTeamRemove').remove();
        $('.divClientMembers li').each(function () {
            var title = $(this).attr('title');
            var titleParts = title.toString().split('-');
            $(this).attr('title', titleParts[1].substr(1, titleParts[1].length));
        });
        //alert('no role def')
    }
    else if ($('#hdnRoleID').val() == 1) {
        //alert('no role def')
    }

    var div = $('.divDetails').clone().show();
    //$('.imgDisInfo').tooltipster({
    //    content: $(div),
    //    interactive: true,
    //    trigger: 'click',
    //    theme: 'tooltipster-light',
    //    position: 'bottom'
    //});
    //$('.tooltip').tooltipster({
    //    contentAsHTML: true
    //});


    var Asked = false;

    $('.lnkClientRemove').click(function () {
        asked_link = $(this);
        $('#hdnClientToRemoveID').val($(this).attr('data-mid'));
    });

    $('.lnkTeamRemove').click(function () {
        asked_link = $(this);
        $('#hdnTeamToRemoveID').val($(this).attr('data-mid'));
    });

    if ($('#hdnDiscussionIsNew').val() == "1") {
        OpenSaveDialog('.divAskTitle', 'Discussion Title', 'auto', function () {
            SaveDiscussion($('#hdnDisID').val(), $('.txtDiscussionTitle').val(), 0, $('#hdninstanceid').val(), 'NA', $('#hdnMemberID').val());
            $('#h1Title').text($('.txtDiscussionTitle').val());
            $('.active[data-id="aLinkDiscussion"]').text($('#h1Title').text());
            $('#hdnDiscussionIsNew').val('0');
            $('.divAskTitle').dialog('close');
        });
    }
    q_asked = false;
    $('.aStartDiscussion').click(function (e) {
        e.preventDefault();
        OpenStartDiscussionDialog('.divAskTitle', 'Start Discussion', 'auto', function () {
            var href = $('.aStartDiscussion').attr('href') + "&dtitle=" + $('.txtDiscussionTitle').val();
            window.open(href, '_self');
        });

    });
    window.onbeforeunload = function (e) {
        if ($('.txtMemDis').val() != "") {
            if (e) {
                e.returnValue = 'You were writing a message. Closing or refreshing this window might erase it!';
            }
            // For Safari
            return 'You were writing a message. Closing or refreshing this window might erase it!';
        }
        else if ($('.uploadify-queue-item').size() > 0) {
            if (e) {
                e.returnValue = 'You have files in uploading queue. Closing or refreshing this window might loss them!';
            }
            // For Safari
            return 'You have files in uploading queue. Closing or refreshing this window might loss them!';

        }
    }

    if ($('#hdnAskInfo').val() == "1") {
        OpenSaveDialog('.divAskInfo', 'Member Info', 'auto', function () {
            $('.btnSaveMemberInfo').click();
        });
    }

    //Notes Reminder Popup
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/GetReminderNotes',
        data: '{"discussionid":"' + $('#hdnDisID').val() + '"}',
        async: true,
        success: function (response) {
            if (response.d != "") {
                $('#hdnNoteReminder').val("1");
                $(".divNoteReminder").html(response.d);
            }
            if ($('#hdnNoteReminder').val() == "1") {
                OpenSaveDialog('.divNoteReminder', 'Important Note', 'auto', function () {
                });
                // $(".divNoteReminder").parent().find(".ui-dialog-buttonset").css("display", "none");
                $(".ui-dialog-buttonpane button:first").css('display', 'none');
                $(".ui-dialog-buttonpane button:nth-child(2)").css('display', 'none');
                $(".ui-dialog-buttonpane button:last").html('Close');

            }
        }
           , error: function ()
           { console.log("some problem in saving notes"); }
    });




    //- 43 - 76 - 15
    $('#divMemberDiscussionContainer').css('max-height', ($(window).height() - (140 - 67)) + 'px').css('overflow-y', 'overlay');
    $('#contentRight-Contents').css('max-height', ($(window).height() - 43 - (92 - 58)) + 'px').css('overflow-y', 'overlay');

    //$('#contentRight-Contents').enscroll({
    //    verticalTrackClass: 'track4',
    //    verticalHandleClass: 'handle4',
    //    minScrollbarLength: 28,
    //    scrollIncrement:20
    //});
    //$('#divMemberDiscussionContainer').enscroll({
    //    verticalTrackClass: 'track4',
    //    verticalHandleClass: 'handle4',
    //    minScrollbarLength: 28,
    //    scrollIncrement:100
    //});

});


function resizeTabContent() {
    var tabheight = $(window).height() - 135;
    $('.tab-content').height(tabheight);
    //$('.tab-content').css('width', 'auto');
    //$('.tab-content').css('overflow-y', 'auto');
    //$('.tab-content').css('overflow-x', 'hidden');
}

function LoadDisList() {
    var disList = "_discussion_controls/_DiscussionsList.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&DL=" + $('#hdnRDL').val();
    var divDiscussionList = $('.divDiscussionListTemp');
    $('.divDiscussionListTemp').load(disList + " #discussionlist", function () {
        $('.lblUnread').text($('.divDiscussionListTemp').find('.lblUnreadCount').text());
        $('.lblOpenCount').text($('.divDiscussionListTemp').find('#lblOpenCount').text());
        $('.lblCloseCount').text($('.divDiscussionListTemp').find('#lblArchiveCount').text());
        $('.tabopen').html($(divDiscussionList).find('#tabs-1').html());
        $('.tabclose').html($(divDiscussionList).find('#tabs-2').html());
        $('.discussionlisttab a[href="#' + $('.projects.selected').parent().attr('id') + '"]').click();
        $('.activeIcon').parent().parent().addClass('selected');
        resizeTabContent();
        $('.tab-content').enscroll({
            verticalTrackClass: 'track4',
            verticalHandleClass: 'handle4',
            minScrollbarLength: 28
        });
        setTimeout(function () {
            $('.tab-content').css('width', 'auto');
        }, 200);
        $(window).resize(function () {
            resizeTabContent();
        });

        try {
            $('#txtSearchOpen').keyup(function () {
                var searchStr = $(this).val().toString().toLowerCase();
                $('.alinkdiscussionopen').each(function () {
                    var titleStr = $(this).text().toString().toLowerCase();
                    if (titleStr.indexOf(searchStr) < 0) {
                        $(this).parent().hide();
                    }
                    else {
                        $(this).parent().show();
                    }
                })
            })
        } catch (e) {
            console.log(e.message);
        }

        try {
            $('#txtSearchClose').keyup(function () {
                var searchStr = $(this).val().toString().toLowerCase();
                $('.alinkdiscussionclose').each(function () {
                    var titleStr = $(this).text().toString().toLowerCase();
                    if (titleStr.indexOf(searchStr) < 0) {
                        $(this).parent().hide();
                    }
                    else {
                        $(this).parent().show();
                    }
                })
            })
        } catch (e) {
            console.log(e.message);
        }
    });
}

function LoadDistags() {
    var disList = "_discussion_controls/_DiscussionTags.aspx?did=" + $('#hdnDisID').val() + "&T=" + $('#hdnRTags').val();
    $('#discussiontagscontainer').load(disList + " #discussiontags", function () {
        LoadTagClick();
    });
}

function ModifyPopUp() {
    $(".txtAddNotes").removeAttr('readonly');

    $(".txtAddNotes").val("");
    $("#addnote h4").text("Add Note");
    $("#btnAddNotes").text("Add");
    $("#chk_Reminder").removeAttr('checked');


    $(".btnCancel").text("Cancel");
    $("#btnAddNotes").css('display', 'inline-block');
    //   $("#btnDelNotes").css('display', 'inline-block');
    $("#btnDelNotes").css('display', 'none');

    $(".notescheck").css('display', 'block');

    $("#isnew").val("new");
    $("#lblAddedBy").text("");
}

function DeleteNote() {
    var discussionid = $('#hdnDisID').val();
    var memberid = $('#hdnMemberID').val();
    var NoteID = $("#hdnNoteID").val();
    var notes = $("#txtAddNotes").val();
    var notifyTeam = false;
    var notifyClient = false;

    var r = confirm("Are you sure to delete this Note?");
    if (r == true) {
        if ($("#chk_Notify").is(':checked')) {
            notifyTeam = true;
        }
        if ($("#chk_NotifyClient").is(':checked')) {
            notifyClient = true;
        }

        NProgress.start();
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: '_Discussion.aspx/DeleteMemberNotes',
            data: '{"NoteID":"' + NoteID + '","discussionid":"' + discussionid +
                '","memberid":"' + memberid +
                '","notes":"' + notes + '","notifyTeam":"' + notifyTeam +
                '","notifyClient":"' + notifyClient +
                '"}',
            async: true,
            success: function (response) {
                // ModifyPopUp();
                LoadDisNotes();
                NProgress.done();
                location.reload();
            }
              , error: function () {
                  alert("Some problem in deleting notes");
                  LoadDisNotes();
                  NProgress.done();
                  console.log("some problem in deleting notes");
              }
        });
    }

}

//Load Notes
function LoadDisNotes() {
    $('#addnote').modal('hide');
    ModifyPopUp();

    var disList = "_discussion_controls/_DiscussionNotes.aspx?did=" + $('#hdnDisID').val() + "&T=" + $('#hdnRNotes').val();

    $('#discussionnotescontainer').load(disList + "#discussionnotes", function () {
        //    console.log($('#discussionnotescontainer').html());
        //$(".TeamMembers").find("[data-id='" + current + "']").attr("data-alias");
        $('#aAddNotes').on('click', function (e) {
            ModifyPopUp();
            $('#addnote').modal('show');
        });

    });
}

//Save Notes
function SaveMemberNotes(notes, discussionid, instanceid, memberid, teammember, active, isnew, NoteID, notifyTeam, notifyClient, reminder) {
    NProgress.start();
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '_Discussion.aspx/SaveMemberNotes',
        data: '{"notes":"' + notes + '","discussionid":"' + discussionid +
            '","instanceid":"' + instanceid + '","memberid":"' + memberid + '","teammember":"' + teammember +
            '","active":"' +
            active + '","isnew":"' + isnew +
            '","NoteID":"' + NoteID +
            '","notifyTeam":"' + notifyTeam +
            '","notifyClient":"' + notifyClient +
            '","reminder":"' + reminder +
            '"}',
        async: true,
        success: function (response) {
            LoadDisNotes();
            NProgress.done();
            location.reload();
        }
        , error: function ()
        { console.log("some problem in saving notes"); }
    });
}

//LOAD COMMENTS
function LoadPosts(refresh) {

    NProgress.start();
    var oldDisURL = "_discussion_controls/_DiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&RoleID=" + $('#hdnRoleID').val() + "&HideHours=" + $('#HideHours').val() + "&isHourly=" + $('#hdnIsHourlyProject').val() + "&C=" + $('#hdnCCC').val() + "&T=" + $('#hdnCCT').val();
    $('#divChats').load(oldDisURL + " .divOldDiscussions", function () {
        $('#divChatArea .chatGrid.divMD,#divChatArea  #divLoadMore').remove();
        $('#divChatArea').prepend($('#divChats .divOldDiscussions').html());
        $('#divChatArea').css('visibility', 'visible');
        $('#divChats').html('');
        //$('#divChatArea').shio;
        NProgress.done();
        initTags();
        var diff = parseInt($('#totalcomments').val() - $('.lblLoadPreviousCount').attr('data-commentstoload'));

        if (diff > 5) {
            $('.lblLoadPreviousCount').attr('data-commentsqty', 5)
            $('.lblLoadPreviousCount').html("Load last " + 5 + " messages");
        }
        else {
            $('.lblLoadPreviousCount').attr('data-commentsqty', diff)
            $('.lblLoadPreviousCount').html("Load last " + diff + " messages");
        }

        var hideprvbutton = false;
        if (diff <= 5) {
            hideprvbutton = true;
        }

        setTimeout(function () {
            $('.lblLoadPrevious').click(function () {
                if (!loadingComment) {
                    loadingComment = true;
                    NProgress.start();
                    $(this).parent().find('i').removeClass('fa-reply-all').addClass('fa-spinner');
                    $(this).removeClass('lblLoadPrevious');
                    $(this).text('Loading...');
                    //$('.summary').remove();
                    var oldDisURL = "_discussion_controls/_DiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&RoleID=" + $('#hdnRoleID').val() + "&HideHours=" + $('#HideHours').val() + "&isHourly=" + $('#hdnIsHourlyProject').val() + "&loadmore=" + $(this).attr('data-commentstoload');
                    var objLbl = $(this).hide().after("Loading...");
                    $('#divChats').load(oldDisURL + " .divOldDiscussions", function () {
                        $('#divChats .load-cont').remove();
                        //$('#divChats .divOldDiscussions').html().find('.summary'
                        $($('#divChats .divOldDiscussions').html()).insertAfter('#divLoadMore');
                        //$('#divChatArea').prepend($('#divChats .divOldDiscussions').html());
                        $(objLbl).parent().parent().remove();
                        $('#divLoadMore').remove();
                        NProgress.done();
                        initPostFunction();
                        $('#divChats').html('');
                        loadingComment = false;
                        initTags();
                        LoadTagClick();
                    });
                }
            });

            $('.lblLoadPreviousCount').click(function () {
                if (!loadingComment) {
                    loadingComment = true;
                    NProgress.start();
                    $(this).parent().find('i').removeClass('fa-reply-all').addClass('fa-spinner');
                    $(this).text('Loading...');
                    $(this).addClass('loading');
                    var objLbl = $(this);
                    $('.lblLoadPrevious').attr('data-commentstoload', parseInt($('.lblLoadPrevious').attr('data-commentstoload')) - 5);
                    var oldDisURL = "_discussion_controls/_DiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&RoleID=" + $('#hdnRoleID').val() + "&HideHours=" + $('#HideHours').val() + "&isHourly=" + $('#hdnIsHourlyProject').val() + "&did=" + $('#hdnDisID').val() + "&loadmorecount=" + $(this).attr('data-commentstoload') + "&loadmoreqty=" + $(this).attr('data-commentsqty') + "&totalcomments=" + $('#totalcomments').val();
                    //var objLbl = $(this).hide().after("Loading...");
                    $('#divChats').load(oldDisURL + " .divOldDiscussions", function () {
                        if ($('#divChats').html() == '') {
                            $('#divLoadMore').css('display', 'none');
                            NProgress.done();
                        }
                        else {
                            $('#divChats .load-cont').remove();
                            $($('#divChats .divOldDiscussions').html()).insertAfter('#divLoadMore');
                            //$('#divChatArea').prepend($('#divChats .divOldDiscussions').html());
                            if (parseInt($('.lblLoadPreviousCount').attr('data-commentstoload')) > parseInt($('.lblLoadPrevious').attr('data-commentstoload'))) {
                                $(objLbl).parent().parent().remove();
                            }
                            else {
                                $(objLbl).parent().find('i').removeClass('fa-spinner').addClass('fa-reply-all');
                                $(objLbl).text('Load previous 5 messages');
                            }
                            NProgress.done();
                            initPostFunction();


                            if (hideprvbutton) {
                                $('#divLoadMore').css('display', 'none');
                            }

                            $('.lblLoadPreviousCount').attr('data-commentstoload', parseInt($('.lblLoadPreviousCount').attr('data-commentstoload')) + 5)
                            if (($('#totalcomments').val() - $('.lblLoadPreviousCount').attr('data-commentstoload')) > 5) {
                                $('.lblLoadPreviousCount').attr('data-commentsqty', parseInt(5));
                            }
                            else {
                                hideprvbutton = true;
                                var diff = parseInt($('#totalcomments').val() - $('.lblLoadPreviousCount').attr('data-commentstoload'));
                                $('.lblLoadPreviousCount').attr('data-commentsqty', diff)
                                $('.lblLoadPreviousCount').html("Load last " + diff + " messages");
                            }





                            $('#divChats').html('');
                            loadingComment = false;
                            initTags();
                            LoadTagClick();
                        }
                    });
                }
            });

            initPostFunction();
            if (!refresh) {
                $('#cke_txtMemDisU').append($('#txtMemDisTagsU'));
                $('#cke_txtMemDis').append($('#txtMemDisTags'));

                $('#txtMemDisTags').tagEditor({
                    //initialTags: ['Hello', 'World', 'Example', 'Tags'],
                    delimiter: ', ', /* space and comma */
                    placeholder: 'Enter tags ...',
                    forceLowercase: false
                });
                $('#txtMemDisTagsU').tagEditor({
                    //initialTags: ['Hello', 'World', 'Example', 'Tags'],
                    delimiter: ', ', /* space and comma */
                    placeholder: 'Enter tags ...',
                    forceLowercase: false
                });
            }
            //$("html, body").animate({ scrollTop: $('#btnSend').offset().top }, 1000);
        }, 500);

        LoadTagClick();
    });

}

var loadingComment = false;
function LoadTagClick() {
    $('.aTag').unbind('click');
    $('.aTag').click(function () {
        if (!loadingComment) {
            loadingComment = true;
            NProgress.start();
            var oldDisURL = "_discussion_controls/_DiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&tag=" + $(this).attr('data-tag') + "&RoleID=" + $('#hdnRoleID').val() + "&HideHours=" + $('#HideHours').val() + "&isHourly=" + $('#hdnIsHourlyProject').val();
            $('#divChats').load(oldDisURL + " .divOldDiscussions", function () {
                $('#divChatArea .chatGrid.divMD,#divChatArea  #divLoadMore').remove();
                $('#divChats .loadmsg').remove();
                $('#divChatArea').prepend($('#divChats .divOldDiscussions').html());
                $('#divChatArea').css('visibility', 'visible');
                $('#divChats').html('');
                initPostFunction();
                initTags();
                loadingComment = false;
                NProgress.done();
                $('#h4Refresh').show();
                $('.lblRefresh').click(function () {
                    $(this).text('Loading...');
                    $(this).addClass('loading');
                    LoadPosts(true);
                });
            });
        }
    });
}

//Notes Click
function notesPopUp(ev) {
    var roleid = $("#hdnRoleID").val();
    var check = false;

    if ($('#hdnMemberID').val() == $(ev).attr("data-memberid")) {
        check = true;
    }
    if ((roleid == "1" || roleid == "2") || check) {
        //////
        $(".txtAddNotes").removeAttr('readonly');
        $("#btnAddNotes").text("Add");
        $(".btnCancel").text("Cancel");
        $("#btnAddNotes").css('display', 'inline-block');
        $("#btnDelNotes").css('display', 'inline-block');
        $(".notescheck").css('display', 'block');
        /////

        $(".txtAddNotes").val($(ev).attr('data-note'));
        $("#hdnNoteID").val($(ev).attr('data-noteid'));

        if ($(ev).attr('data-remind') != "" && $(ev).attr('data-remind') != "False")
            $("#chk_Reminder").prop("checked", true);
        else
            $("#chk_Reminder").prop("checked", false);

        $("#addnote h4").text("Edit Note");
        $("#btnAddNotes").text("Update");
        $("#isnew").val("old");
        $("#lblAddedBy").text("Added by: " + $(ev).find(".notecreater").text());
        $('#addnote').modal('show');
        return true;
    }
    else {
        $(".txtAddNotes").val($(ev).attr('data-note'));
        $(".txtAddNotes").attr('readonly', 'true');
        $("#hdnNoteID").val($(ev).attr('data-noteid'));

        if ($(ev).attr('data-remind') != "" && $(ev).attr('data-remind') != "False")
            $("#chk_Reminder").prop("checked", true);
        else
            $("#chk_Reminder").prop("checked", false);

        $("#addnote h4").text("Note");
        $(".btnCancel").text("Close");
        $("#btnAddNotes").css('display', 'none');
        $("#btnDelNotes").css('display', 'none');
        $(".notescheck").css('display', 'none');
        $("#isnew").val("old");
        $("#lblAddedBy").text("Added by: " + $(ev).find(".notecreater").text());
        $('#addnote').modal('show');
        return true;
    }

    $('#addnote').modal('hide');
    return false;

}

//Load Reminders
function LoadDisReminders() {
    var disList = "_discussion_controls/_DiscussionReminder.aspx?did=" + $('#hdnDisID').val() + "&InstanceID=" + $('#hdninstanceid').val() + "&md=" + $('#hdnMemberID').val();

    $.get(disList + "#discussionreminders").done(function (result) {
        $('#discussionremindercontainer').html($(result).find("#discussionreminders")[0].outerHTML);
    });
    //$('#discussionremindercontainer').load(disList + "#discussionreminders", function () {
    //    console.log($('#discussionremindercontainer').html());
    //});
}

// Load Sections
$(function () {
    $('.startDiscussion').click(function () {
        if ($('.txtNewDiscussionTitle').val() == '') {
            $('.txtNewDiscussionTitle').attr('placeholder', 'Title required');
            return false;
        }
    });
    $('#divMemberDiscussion').hide();
    $('.oldLoader').show();
    LoadPosts(false);
    if ($("#hdnIsHourlyProject").val().toLowerCase() == "true") {
        LoadSummary($('#hdnDisID').val());
        $(".summary").show();
    }
    else {
        $(".summary").hide();
    }
    $('#btnRemoveComment').click(function () {
        DeleteMemberDiscussion($('#hdnDelMDID').val());
    });

    LoadDisList();
    LoadDistags();
    LoadTagClick();
    LoadDisNotes();
    LoadDisReminders();

    $(window).resize(function () {
        $('#divMemberDiscussionContainer').css('max-height', ($(window).height() - (140 - 67)) + 'px').css('overflow-y', 'overlay');
        $('#contentRight-Contents').css('max-height', ($(window).height() - 43 - (92 - 58)) + 'px').css('overflow-y', 'overlay');
        $('#tabs-1').css('max-height', ($(window).height() - 108 - 89) + 'px').css('overflow-y', 'overlay');
        $('#tabs-2').css('max-height', ($(window).height() - 108) + 'px').css('overflow-y', 'overlay');
        $('#divMainDiscussionList').css('max-height', ($(window).height()) + 'px').css('overflow-y', 'overlay').css('height', 'auto');
    });


    $('#btnAddNotes').click(function () {
        var notes;
        var notifyTeam = false;
        var notifyClient = false;
        var Reminder = false;
        var isnew = $("#isnew").val();

        if ($('#txtAddNotes').val() == "") {
            alert("Please enter some text.");
            $('#txtAddNotes').focus();
            return false;
        }

        notes = $('#txtAddNotes').val();
        if (isnew == "new") {
            isnew = "true";
        }
        else if (isnew == "old") {
            isnew = "false";
        }

        if ($("#chk_Notify").is(':checked')) {
            notifyTeam = true;
        }
        if ($("#chk_NotifyClient").is(':checked')) {
            notifyClient = true;
        }

        if ($("#chk_Reminder").is(':checked')) {
            Reminder = true;
        }

        SaveMemberNotes(notes, $('#hdnDisID').val(), $('#hdninstanceid').val(), $('#hdnMemberID').val(), $('#hdnTeamMember').val(), "true", isnew, $("#hdnNoteID").val(), notifyTeam, notifyClient, Reminder);


    });

    //SubtractHours
    $('#hoursSubtract').on('change', function () {
        if ($(this).val() == 2) {
            $('#summarywords').attr('style', 'display: none !important');
            $('#divHrsSubReason').css('display', 'inline-block', 'important');
        }
        else if ($(this).val() == 1) {
            $('#summarywords').attr('style', 'width: 240px; display: inline-block !important');
            $('#divHrsSubReason').css('display', 'none', 'important');
            $("#subtractionreason").attr('style', 'display: none !important');
        }

    });


    $('#ehoursSubtract').on('change', function () {
        if ($(this).val() == 2) {
            $('#summaryEdit').attr('style', 'display: none !important');
            $('#edivHrsSubReason').show();
        }
        else if ($(this).val() == 1) {
            $('#summaryEdit').attr('style', 'width: 240px; display: inline-block !important');
            $('#edivHrsSubReason').hide();
        }

    });


    $("#HrsSubReason").on('change', function () {
        if ($(this).val() == "Other") {
            $("#subtractionreason").attr('style', 'width: 280px;display: inline-block !important');
        }
        else {
            $("#subtractionreason").attr('style', 'display: none !important');
        }
    });


    $("#eHrsSubReason").on('change', function () {
        if ($(this).val() == "Other") {
            $("#esubtractionreason").attr('style', 'width: 280px;display: inline-block !important');
        }
        else {
            $("#esubtractionreason").attr('style', 'display: none !important');
        }
    });

    $(".eCancel").on('click', function () {
        $('.hours').show();
    });
    //End

    //Team Members
    $(".ddlTeamMembers").select2({
        placeholder: "Select Member"
    });

    //Clients
    $(".ddlClientMembers").select2({
        placeholder: "Select Client"

    });

    //End

    //Multiselect Team Members & CLients
    $(".btnAddExTeam").on('click', function () {
        var str = "";
        $("#ddlTeamMembers :selected").each(function () {
            str += $(this).val() + ",";
        });

        $("#hdnIDs").val(str);
        $("#ddlTeamMembers").select2("val", "");
        $('.divManageTeam').hide('blind', 400);
    });

    $(".btnAddExClient").on('click', function () {
        var str = "";
        $("#ddlClientMembers :selected").each(function () {
            str += $(this).val() + ",";
        });

        $("#hdnClientIDs").val(str);
        $("#ddlClientMembers").select2("val", "");
        $('.divManageClient').hide('blind', 400);
    });
    //End

});

$("#hours").click(function () {
    $("#hours").placeholder = "";
});
$("#minutes").click(function () {
    $("#minutes").placeholder = "";
});

function CloseWindow() {
    $('#EditName').hide();
}

function ConfirmDelete()
{
    var r = confirm("Are you sure you want to delete discussion?")
    if (r == true) {
        CloseWindow();
        return true;
    }
    else {
        CloseWindow();
        return false;
    }

}
