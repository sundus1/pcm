﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyCCM;
using System.Web.Services;
using System.Net.Mail;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using CCM.App_Code;
using System.Text;
using System.IO;

namespace CCM
{
    /// <summary>
    /// Summary description for SendReminders
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SendReminders : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string SendReminder()
        {
            string response = "";
            var acc = new CloudStorageAccount(
                           new StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var tableDiscussionReminder = tableClient.GetTableReference("DiscussionReminders");
            var tableMemberDiscussion = tableClient.GetTableReference("MemberDiscussions");

            List<DiscussionReminder> entities = DiscussionReminder.Find(u => u.InstanceID != null).ToList();

            foreach (var entity in entities)
            {
                bool Send = EmailReminder(entity.ID);
                if (Send)
                {
                    int memberid = (int)entity.AddedBy;
                    int discussionid = (int)entity.DiscussionID;
                    string instanceid = entity.InstanceID;
                    string conversation = entity.Description;
                    string teammember = Convert.ToString(entity.MemberID);
                    int folderid = 0;
                    string str = "";

                    TableQuery<App_Code.MemberDiscussions1> rangeQuery1 = new TableQuery<App_Code.MemberDiscussions1>().Where(
        TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid.ToString()));
                    var entitiesMemberDiscussion = tableMemberDiscussion.ExecuteQuery(rangeQuery1);
                    string messageid = entitiesMemberDiscussion.Count().ToString();

                    Member memb = Member.SingleOrDefault(u => u.MemberID == memberid);

                    int minutes = 0;

                    Role role = CCM_Helper.GetRole(memberid, discussionid, folderid, instanceid);
                    if (role.RoleID == 0)
                    {
                        response += "Error:Member has no role defined under current discussion:" + memberid + "<br/>";
                    }
                    App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                    md.GetMemberDiscussionByID(discussionid.ToString());
                    MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());

                    DiscussionToFolder assignedTo = DiscussionToFolder.SingleOrDefault(u => u.MemberID == entity.MemberID && u.DiscussionID == discussionid.ToInt32());

                    str += "<b>Reminder For:</b> " + assignedTo.Alias + "<br /><b>Task Description: </b>";
                    str += conversation;
                    str += "<br/>";

                    md.MemberDis = new App_Code.MemberDiscussions()
                    {
                        DiscussionID = discussionid.ToString(),
                        PartitionKey = instanceid,
                        Active = "True",
                        Conversation = str,
                        CrtDate = DateTime.Now,
                        MemberID = memberid.ToString(),
                        ModDate = DateTime.Now,
                        RoleID = role.RoleID,
                        TeamMember = teammember,
                        Tags = "",
                        Minutes = 0,
                        Summary = "",
                        SubtractedHours = false,
                        SubtractHrsReason = ""
                    };
                    discussion.ModDate = DateTime.Now;
                    md.MemberDis.Deleted = false;
                    discussion.Update();
                    md.Add();
                    this.SaveViaEmailOrWeb(memb.Email, memberid.ToString(), role.RoleID.ToString(), discussionid.ToString(), "reply", "web");
                    if (messageid == "undefined" || messageid == null)
                    {
                        messageid = "0";
                    }
                    messageid = (Convert.ToInt32(messageid) + 1).ToString();
                    this.SendEmail(md.MemberDis.RowKey, discussionid.ToString(), memberid.ToString(), conversation, "Task Reminder", folderid.ToString(), minutes, false, messageid, teammember.ToInt32());

                    //Update Reminder
                    DiscussionReminder reminder = DiscussionReminder.SingleOrDefault(a => a.ID == entity.ID);
                    if (reminder != null)
                    {
                        if (reminder.Remind == null)
                            reminder.Remind = 1;
                        else
                            reminder.Remind = (reminder.Remind) + 1;

                        reminder.SendDate = DateTime.Now;
                        reminder.Update();
                    }

                    response += "Info:MemberDiscussion Created:" + md.MemberDis.RowKey + "<br/>";
                }
            }
            return response;
        }

        public void SendEmail(string memberdisid, string discussionid,
             string memberid, string conversation, string subject, string folderid, int minutes, bool isUpdate, string messageid, int teammember)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            AvaimaTimeZoneAPI timeAPI = new AvaimaTimeZoneAPI();
            Member senderMember = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid.ToInt32() && u.DiscussionID == discussionid.ToInt32());
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());

            //Filter Active Members & exclude Clients
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == discussionid.ToInt32() && u.Active == true && (u.RoleID != 4 && u.RoleID != 5)).ToList();

            List<DiscussionFile> files = DiscussionFile.Find(u => u.MemberDisID == memberdisid).ToList();
            Project project = Project.SingleOrDefault(u => u.ProjectID == discussion.ProjectID);
            DiscussionToFolder assignedTo = DiscussionToFolder.SingleOrDefault(u => u.MemberID == teammember && u.DiscussionID == discussionid.ToInt32());

            string strhours = "";
            int srole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), senderMember.MemberID);
            if (discussion != null)
            {
                discussion.ModDate = DateTime.Now;
                discussion.Update();
            }
            if (project == null)
            {
                project = new Project()
                {
                    ProjectID = 0,
                    Title = "Discussion Update"
                };
            }
            foreach (DiscussionToFolder item in mtds)
            {
                Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (member != null)
                {
                    int mrole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), item.MemberID.ToInt32());
                    if (srole != 4 && srole != 5)
                    {
                        strhours = "";
                    }
                    StringBuilder strbody = new StringBuilder();
                    String url = CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString());
                    //strbody.Append("Write ABOVE THIS LINE to post a reply through email OR reply on the <a href='" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "'>discussion page</a> <br/>");
                    //strbody.Append("Do not change anything in the subject of this email");
                    strbody.Append("<div style=\"padding:8px 12px 6px 12px;background: whitesmoke;\">");
                    strbody.Append("<h1 style=\"font-weight:normal\">" + discussion.Title + "<br /><span style=\"font-size:12px\">Reminder by <b>" + currDTF.Alias + "</b> - " + timeAPI.GetDate(DateTime.Now.ToShortDateString(), member.OwnerID) + " at " + Utilities.GetUserDateWithoutToday(DateTime.Now.ToShortTimeString(), member.OwnerID) + " - Message ID: " + messageid + "</span></h1>For complete details visit the<a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\"> discussion page</a><br />");
                    strbody.Append("<br />");
                    strbody.Append("<h5 style=\"margin-top:9px;padding: 14px;background: #EFC;font-size: 12px;font-weight: normal;margin: 10px -10px !important;margin-left:-12px \">");
                    if (files.Count > 0)
                    {
                        strbody.Append("<b>File attachments:</b><br />");
                        int ifiles = 0;
                        foreach (var file in files)
                        {
                            if (Path.GetExtension(file.FileName) == ".pdf")
                            {
                                strbody.AppendLine("<a href=\"" + file.FilePath + "\" download=\"" + file.FileName + "\">" + file.FileName + "</a>");
                            }
                            else
                            {
                                strbody.AppendLine("<a href=\"" + file.FilePath + "\">" + file.FileName + "</a>");
                            }
                            ifiles++;
                            if (ifiles != files.Count)
                            {
                                strbody.Append(" - ");
                            }
                        }
                        strbody.Append("<br /><hr />" + conversation);
                    }
                    else
                    {
                        strbody.Append("<b>Reminder For:</b> " + assignedTo.Alias + "<br /><b>Description: </b>");
                        strbody.Append(conversation);
                    }

                    strbody.AppendLine("</h5>");
                    strbody.AppendLine("For complete details visit the <a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\">Discussion Page</a>");
                    strbody.AppendLine("</div>");
                    strbody.AppendLine(Utilities.AvaimaEmailSignature);

                    emailAPI.send_email(member.Email, currDTF.Alias, "pcm-71579@avaima.com", strbody.ToString(), discussion.Title + " - " + subject + " [ID:" + discussionid + "]");
                    if (item.Unread == null)
                    {
                        item.Unread = 1;
                    }
                    else
                    {
                        item.Unread += 1;
                    }
                    item.Update();
                }
            }
        }

        public void SaveViaEmailOrWeb(string email, string memberId, string roleId,
          string dicussionId, string action, string source)
        {
            MemberDiscussionSource ms = new MemberDiscussionSource()
            {
                discussionId = dicussionId.ToInt32(),
                email = email,
                memberId = memberId.ToInt32(),
                roleId = roleId.ToInt32(),
                action = action,
                source = source
            };
            ms.Add();
        }

        public bool EmailReminder(int ReminderID)
        {
            bool check = false;
            bool isnew = true;
            DateTime currentDate = DateTime.Now;
            DiscussionReminder reminder = DiscussionReminder.SingleOrDefault(u => u.ID == ReminderID);
            DateTime SendDate = DateTime.Now;

            //Restrict Service to Send Reminders once in a Day
            if (reminder.SendDate == null)
            {
                check = true;
            }
            else
            {
                SendDate = (DateTime)reminder.SendDate;
                int diff = (currentDate.Date - SendDate.Date).TotalDays.ToInt32();
                if (diff > 0)
                    check = true;
               
                isnew = false;
            }

            if (check)
            {
                DateTime CrtDate = (DateTime)reminder.CrtDate;
                Double RepeatDays = (int)reminder.RepeatEvery;
                Double days = 0.0;
                int NumberofDays = 0;

                if (reminder.Repeats == "Daily")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        //if (RepeatDays == (Int32)Math.Ceiling(days))
                        if (RepeatDays == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        if (RepeatDays == days)
                            return true;
                    }

                }
                else if (reminder.Repeats == "Weekly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        if ((RepeatDays * 7) == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        if ((RepeatDays * 7) == days)
                            return true;
                    }

                }
                else if (reminder.Repeats == "Monthly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        NumberofDays = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        NumberofDays = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }

                }
                else if (reminder.Repeats == "Yearly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;

                        var date = DateTime.ParseExact(CrtDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var lastdate = currentDate;
                        var diff = lastdate - date;
                        NumberofDays = (Int32)diff.TotalDays;

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;

                        var date = DateTime.ParseExact(CrtDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var lastdate = currentDate;
                        var diff = lastdate - date;
                        NumberofDays = (Int32)diff.TotalDays;

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }


                }

            }
            return false;
        }

        [WebMethod]
        public bool EmailReminder1()
        {
            int ReminderID = 9;
            bool check = false;
            bool isnew = true;
            DateTime currentDate = DateTime.Now;
            DiscussionReminder reminder = DiscussionReminder.SingleOrDefault(u => u.ID == ReminderID);
            DateTime SendDate = (DateTime)reminder.SendDate;

            //Restrict Service to Send Reminders once in a Day
            if (reminder.SendDate == null)
            {
                check = true;
            }
            else
            {               
                int diff = (currentDate.Date - SendDate.Date).TotalDays.ToInt32();
                if (diff > 0)
                    check = true;

                isnew = false;
            }

            if (check)
            {
                DateTime CrtDate = (DateTime)reminder.CrtDate;
                Double RepeatDays = (int)reminder.RepeatEvery;
                Double days = 0.0;
                int NumberofDays = 0;

                if (reminder.Repeats == "Daily")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        //if (RepeatDays == (Int32)Math.Ceiling(days))
                        if (RepeatDays == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        if (RepeatDays == days)
                            return true;
                    }
                    
                }
                else if (reminder.Repeats == "Weekly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        if ((RepeatDays * 7) == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        if ((RepeatDays * 7) == days)
                            return true;
                    }
                       
                }
                else if (reminder.Repeats == "Monthly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;
                        NumberofDays = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                    else
                    {
                        days = (currentDate.Date - SendDate.Date).TotalDays;
                        NumberofDays = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                   
                }
                else if (reminder.Repeats == "Yearly")
                {
                    if (isnew)
                    {
                        days = (currentDate.Date - CrtDate.Date).TotalDays;

                        var date = DateTime.ParseExact(CrtDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var lastdate = currentDate;
                        var diff = lastdate - date;
                        NumberofDays = (Int32)diff.TotalDays;

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                    else {
                        days = (currentDate.Date - SendDate.Date).TotalDays;

                        var date = DateTime.ParseExact(CrtDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var lastdate = currentDate;
                        var diff = lastdate - date;
                        NumberofDays = (Int32)diff.TotalDays;

                        if ((RepeatDays * NumberofDays) == days)
                            return true;
                    }
                   

                }

            }
            return false;
        }

    }
}
