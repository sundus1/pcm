﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CCM;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["MemberId"] != null && Request.QueryString["IntanceID"] != null)
                {
                    LoadDisscussion(Request.QueryString["MemberId"].Decrypt().ToInt32(), Request.QueryString["IntanceID"]);
                }
                if (Request.QueryString["DiscussionID"] != null)
                {
                    rbtnSingleProject.Checked = true;
                    ddlProjects.SelectedValue = Request.QueryString["DiscussionID"].Decrypt().ToString();
                }

            }
            lblMessage.Text = "";
        }
        private void LoadDisscussion(int memberID, string instanceID)
        {
            DataTable dt = CCMDiscussion.GetReportDiscussions(memberID, instanceID);
            //dt.DefaultView.RowFilter = "isHourly = true ";
            ddlProjects.DataValueField = dt.Columns["DiscussionID"].ToString();
            ddlProjects.DataTextField = dt.Columns["Title"].ToString();
            ddlProjects.DataSource = dt.DefaultView;
            ddlProjects.DataBind();

        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                int memberID = Request.QueryString["MemberId"].Decrypt().ToInt32();
                string instanceID = Request.QueryString["IntanceID"].ToString();
                if (cldFromDate.Value == null || cldFromDate.Value == "")
                {
                    lblMessage.Text = "Select From Date...";
                    return;
                }
                if (cldToDate.Value == null || cldToDate.Value == "")
                {
                    lblMessage.Text = "Select To Date...";
                    return;
                }
                if (rbtnAllProject.Checked == false && rbtnSingleProject.Checked == false)
                {
                    lblMessage.Text = "Select one way to generate report either Single project or All projects.";
                    return;

                }
                string str = "";
                App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();

                lblMessage.Text = "";

                string fDate = cldFromDate.Value;
                string tDate = cldToDate.Value;

                string fromDate = cldFromDate.Value + " 00:00:00";
                string toDate = cldToDate.Value + " 23:59:59";


                if (rbtnSingleProject.Checked == true)
                {
                    str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='98%' class='table table-hover table-bordered table-striped'><tr><th>Project Name</th><th>Hours Spent</th><th></th></tr>";
                    string discussionID = ddlProjects.SelectedItem.Value.ToString();
                    var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
          new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
                    var tableClient = acc.CreateCloudTableClient();
                    var table = tableClient.GetTableReference("MemberDiscussions");
                    TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
                    TableQuery.CombineFilters(
                           TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionID),
                           TableOperators.And,
                           TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)
                      ));
                  
                    var entities1 = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate) && (a.SubtractedHours == false ||  string.IsNullOrEmpty(Convert.ToString(a.SubtractedHours))));
                    int totalminutes = entities1.Sum(x => x.Minutes);

                    //Get Subtracted Hours           
                 
                    var resultSet = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate) && a.SubtractedHours == true);
                    int totalmin = resultSet.Sum(x => x.Minutes);
                    

                                // int totalminutes = md.GetTotalMinutesByDiscussion(discussionID.ToInt32(), fromDate, toDate);
                       if (totalminutes > 0)
                    {
                        //If Subtracted Hours Found
                        if (totalmin > 0)
                        {                           
                            str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='98%' class='table table-hover table-bordered table-striped'><tr><th>Project Name</th><th>Time Added</th><th>Time Subtracted</th><th>Net Time</th><th></th></tr>";

                            string subHour = (Convert.ToDecimal(totalmin) / 60).ToString().Split('.')[0].ToString();
                            Int16 subMminutes = Convert.ToInt16(totalmin % 60);

                            //Total Time Added
                            string totalHour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 totalMinutes = Convert.ToInt16(totalminutes % 60);

                            //Net Time Spent
                            int netTotalminutes = totalminutes - totalmin;
                            string netHour = (Convert.ToDecimal(netTotalminutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 netMinutes = Convert.ToInt16(netTotalminutes % 60);

                            str += "<tr><td>" + ddlProjects.SelectedItem.Text + "</td>";
                            str += "<td>" + totalHour + " Hours " + totalMinutes.ToString() + " Minutes </td>";
                            str += "<td>" + subHour + " Hours " + subMminutes.ToString() + " Minutes </td>";
                            str += "<td>" + netHour + " Hours " + netMinutes.ToString() + " Minutes </td>";
                            str += "<td><a href='DetailedReport.aspx?disID=" + discussionID.Encrypt() + "&fromDate=" + fDate + "&ToDate=" + tDate + "&MemberId=" + Request.QueryString["MemberId"].ToString() + "&IntanceID=" + Request.QueryString["IntanceID"].ToString() + "' target='_blank'>Details</a></td>";
                            // str += "<td><a id='" + discussionID + "' class='.delete' href=''>Delete</a></td>";
                            str += "</tr>";
                            lblTotalHours.Text = "<b>Time Added= </b>" + totalHour + " Hours " + totalMinutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + subHour + " Hours " + subMminutes.ToString() + " Minutes <br/> <b>Net Time= </b>" + netHour + " Hours " + netMinutes.ToString() + " Minutes";
                            lblTotalHours1.Text = "<b>Time Added= </b>" + totalHour + " Hours " + totalMinutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + subHour + " Hours " + subMminutes.ToString() + " Minutes <br/> <b>Net Time= </b>" + netHour + " Hours " + netMinutes.ToString() + " Minutes";
                        }
                        else
                        {
                            string hour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 minutes = Convert.ToInt16(totalminutes % 60);
                            str += "<tr><td>" + ddlProjects.SelectedItem.Text + "</td>";
                            str += "<td>" + hour + " Hours " + minutes.ToString() + " Minutes </td>";
                            str += "<td><a href='DetailedReport.aspx?disID=" + discussionID.Encrypt() + "&fromDate=" + fDate + "&ToDate=" + tDate + "&MemberId=" + Request.QueryString["MemberId"].ToString() + "&IntanceID=" + Request.QueryString["IntanceID"].ToString() + "' target='_blank'>Details</a></td>";
                            // str += "<td><a id='" + discussionID + "' class='.delete' href=''>Delete</a></td>";
                            str += "</tr>";
                            lblTotalHours.Text = "<b>Total Hours Spent= </b>" + hour + " Hours " + minutes.ToString() + " Minutes";
                            lblTotalHours1.Text = "<b>Total Hours Spent= </b>" + hour + " Hours " + minutes.ToString() + " Minutes";
                        }

                    }
                    else {
                        lblTotalHours.Text = "";
                        lblTotalHours1.Text = "";
                    }
                }
                else if (rbtnAllProject.Checked == true)
                {
                    str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='98%' class='table table-hover table-bordered table-striped'><tr><th>Project Name</th><th>Time Added</th><th>Time Subtracted</th><th>Net Time</th><th></th></tr>";
                    DataTable dt = CCMDiscussion.GetReportDiscussions(memberID, instanceID);
                    int AllPrjeMinut = 0;
                    int totalAddedPrjminutes = 0;
                    int totalSubPrjminutes = 0;

                    var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
                    var tableClient = acc.CreateCloudTableClient();
                    var table = tableClient.GetTableReference("MemberDiscussions");
                    TableQuery<App_Code.MemberDiscussions1> rangeQuery;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string discussionID = dt.Rows[i]["DiscussionID"].ToString();
                        bool HideHours = dt.Rows[i]["HideHours"] == DBNull.Value ? false : dt.Rows[i]["HideHours"].ToBoolean();
                        int roleid = CCMDiscussion.GetDiscussionMemberRoleID(discussionID.ToInt32(), memberID.ToInt32());
                        if (HideHours == false || (HideHours == true && (roleid != 4 && roleid != 5)))
                        {

                            rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
                            TableQuery.CombineFilters(
                            TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionID),
                            TableOperators.And,
                            TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)
                                                    ) );
                        
                            var entities1 = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate) && (a.SubtractedHours == false || string.IsNullOrEmpty(Convert.ToString(a.SubtractedHours))));
                            int totalminutes = entities1.Sum(x => x.Minutes);

                            //Get Subtracted Hours            
                            //TableQuery<App_Code.MemberDiscussions1> rangeQuery1 = new TableQuery<App_Code.MemberDiscussions1>().Where(
                            //    TableQuery.CombineFilters(
                            //         TableQuery.CombineFilters(
                            //TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionID),
                            //TableOperators.And, TableQuery.GenerateFilterConditionForBool("SubtractedHours", QueryComparisons.Equal, true)),
                            //         TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true))
                            //);
                            var resultSet = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate) && a.SubtractedHours == true);
                            int totalmin = resultSet.Sum(x => x.Minutes);

                            int netTotalminutes = 0;
                            string subHour = "0";
                            Int16 subMminutes = 0;
                            string totalHour = "0";
                            Int16 totalMinutes = 0;

                            //int totalminutes = md.GetTotalMinutesByDiscussion(discussionID.ToInt32(), fromDate, toDate);
                            if (totalminutes > 0)
                            {
                                if (totalmin > 0)
                                {
                                     totalSubPrjminutes += totalmin;
                                     subHour = (Convert.ToDecimal(totalmin) / 60).ToString().Split('.')[0].ToString();
                                     subMminutes = Convert.ToInt16(totalmin % 60);
                                                               
                                }
                                
                                //Total Time Added
                                totalAddedPrjminutes += totalminutes;
                                totalHour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                                totalMinutes = Convert.ToInt16(totalminutes % 60);

                                //Net Time Spent
                                netTotalminutes = totalminutes - totalmin;

                                string Nethour = (Convert.ToDecimal(netTotalminutes) / 60).ToString().Split('.')[0].ToString();
                                Int16 Netminutes = Convert.ToInt16(netTotalminutes % 60);
                                str += "<tr><td>" + dt.Rows[i]["Title"].ToString() + "</td>";
                                str += "<td>" + totalHour + " Hours " + totalMinutes.ToString() + " Minutes </td>";
                                str += "<td>" + subHour + " Hours " + subMminutes.ToString() + " Minutes </td>";
                                str += "<td>" + Nethour + " Hours " + Netminutes.ToString() + " Minutes </td>";
                                str += "<td><a href='DetailedReport.aspx?disID=" + discussionID.Encrypt() + "&fromDate=" + fDate + "&ToDate=" + tDate + "&MemberId=" + Request.QueryString["MemberId"].ToString() + "&IntanceID=" + Request.QueryString["IntanceID"].ToString() + "' target='_blank'>Details</a></td>";
                                // str += "<td><a id='" + discussionID + "' class='delete' data-index='"+discussionID+"' href='#'>Delete</a></td>";
                                str += "</tr>";
                                AllPrjeMinut += netTotalminutes;
                            }
                            else
                            {
                                lblTotalHours.Text = "";
                                lblTotalHours1.Text = "";
                            }
                        }
                    }

                    string AllprojectNethour = (Convert.ToDecimal(AllPrjeMinut) / 60).ToString().Split('.')[0].ToString();
                    Int16 AllprojectNetminutes = Convert.ToInt16(AllPrjeMinut % 60);
                    string AllprojectAddedhour = (Convert.ToDecimal(totalAddedPrjminutes) / 60).ToString().Split('.')[0].ToString();
                    Int16 AllprojectAddedminutes = Convert.ToInt16(totalAddedPrjminutes % 60);
                    string AllprojectSubhour = (Convert.ToDecimal(totalSubPrjminutes) / 60).ToString().Split('.')[0].ToString();
                    Int16 AllprojectSubminutes = Convert.ToInt16(totalSubPrjminutes % 60);


                    lblTotalHours.Text = "<b>Time Added= </b>" + AllprojectAddedhour + " Hours " + AllprojectAddedminutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + AllprojectSubhour + " Hours " + AllprojectSubminutes.ToString() + " Minutes  <br/> <b>Net Time= </b>" + AllprojectNethour + " Hours " + AllprojectNetminutes.ToString() + " Minutes";
                    lblTotalHours1.Text = "<b>Time Added= </b>" + AllprojectAddedhour + " Hours " + AllprojectAddedminutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + AllprojectSubhour + " Hours " + AllprojectSubminutes.ToString() + " Minutes  <br/> <b>Net Time= </b>" + AllprojectNethour + " Hours " + AllprojectNetminutes.ToString() + " Minutes";

                }
                str += "</table>";
                list.InnerHtml = str;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
            }

        }

    }
}