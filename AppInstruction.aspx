﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppInstruction.aspx.cs" Inherits="CCM.AppInstructions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Help</title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 7px;
        }

        #para p {
            padding: 10px 0 0 10px;
        }

        #para div {
            line-height: 22px;
        }

        #para ol {
            padding: 3px 30px;
        }

        #para ul {
            padding: 5px 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="Main_leftdiv">
            </div>
            <div class="clearboth"></div>
            <h4 style="margin-left: 10px">Team Communication Application Help.
            </h4>
            <br />
            <table>
                <tr>
                    <td>
                        <div style="display: inline-block; float: left;" id="para">
                            <div class="formTitleDiv">
                                How to start?
                            </div>
                            <div style="line-height: 20px">
                                <p>
                                    If you've added this application to your dashboard, you will be presented with <a href="Default.aspx">Home</a> screen.
                                </p>
                                <p>
                                    You can start by Adding projects, you can create one or more discussions and organize them in projects/subprojects as you want.
                                </p>
                            </div>
                            <br />
                            <div class="formTitleDiv">
                                How to add a project?
                            </div>
                            <div>
                                <ul>
                                    <li>Go to <a href="Default.aspx">Home</a> page, Click <b>Create Project</b> link to navigate to Create Project Page.
                                    </li>
                                    <li>Enter Project Title and Description and click <b>Save</b>.
                                    </li>
                                    <li>Clicking <b>Save</b> will save the project and present you more options to add <b>Project Team</b> and <b>Project Client</b> members.
                                    </li>
                                </ul>
                                <br />
                                <div class="formTitleDiv">
                                    How to start a discussion?
                                </div>
                                <div>
                                    <ul>
                                        <li>Go to <a href="Default.aspx">Home</a> page.
                                        </li>
                                        <li>
                                            Navigate to any project you want to start a discussion into.
                                        </li>
                                        <li>Click <b>Create Discussion</b> link to create a discussion. This will navigate you to create discussion page.
                                        </li>
                                        <li>
                                            Give a name to your discussion replacing <b>Untitled</b>.
                                        </li>
                                        <li>
                                            Thats it. You have created discussion. You can now assign client and team members to discussion and add comment as well.
                                        </li>
                                        <li>Clicking <b>Save</b> will save the project and present you more options to add <b>Project Team</b> and <b>Project Client</b> members.
                                        </li>
                                    </ul>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is a super admin
                                    </div>
                                    <p>
                                        Super admin is a user which add Team Communication Application application from <a href="http://www.avaima.com">avaima</a> appstore
                                into his dashboard. 
                                A super admin can manage Projects, Sub Projects and Discussions. He can monitor all projects
                                and discussions under his supervision.
                                He can perform all administrative action on all projects, discussions and members(client/team) as well. 
                                <%--He can assign administrators to departments and can delete his administrators as well.--%>
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        How to make a super admin
                                    </div>
                                    <p>
                                        Super admin is a user which add Team Communication Application application from <a href="http://www.avaima.com">avaima</a> appstore
                                into his dashboard. 
                                So basically the user who add Team Communication Application Management into his dashboard first time is made super-admin.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is an Client Admin
                                    </div>
                                    <p>
                                        Admin Client can monitor projects and discussions under his/her supervision. He can also monitor and manage clients or his assigned discussion and projects.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is an Team Admin
                                    </div>
                                    <p>
                                        Team Admin can monitor projects and discussions under his/her supervision. He can also monitor and manage clients or his assigned discussion and projects.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        How to make admin
                                    </div>
                                    <p>
                                        Super user can assign admin to a department only.
                                    </p>
                                    <ul>
                                        <li>Go to <a href="Default.aspx">Home</a> page and click setting icon of a department.
                                        </li>
                                        <li>Select an employee
                                which you want to assign admin to that department and click Add.
                                        </li>
                                    </ul>
                                    <p>
                                        You can also promote an employee to admin from his dashboard.
                                    </p>
                                    <ul>
                                        <li>Go to <a href="Default.aspx">Home</a> and select the employee you want to promote to admin.
                                        </li>
                                        <li>Navigate to the bottom of the page and click on pen icon along with <b>Role</b>.
                                        </li>
                                        <li>Select a role and click Update.
                                        </li>
                                    </ul>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is a team member
                                    </div>
                                    <p>
                                        Team member is a member of project or discussion based on his assignation. A team member represents a vendor team and communicates as
                                a team member.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is a client member
                                    </div>
                                    <p>
                                        Client member is a member of project or discussion based on his assignation. A client member represents a client team(Stackholder, Customers, Users etc) and communicates as
                                a client member.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        How to add/assign members in discussion
                                    </div>
                                    <p>
                                        You can add client/team members in discussions by taking following steps
                                    </p>
                                    <ul>
                                        <li>Select any discussion by navigating to<a href="Default.aspx">your dashboard</a>.
                                        </li>
                                        <li>If you are super-admin you will <b>Add Clients</b> and <b>Add Team</b> links. 
                                        If you are client you will only be able to add clients in discussion and if you are team you can add team members only.
                                        </li>
                                        <li>By clicking Add Team/Add Client you will be presented with a popup containing members of discussion.
                                        </li>
                                        <li>You can add members by providing their name, nickname and email address.
                                        </li>
                                        <li>Click <b>Save</b> to add members. This will send invitation email to member and the steps he can take to participate.
                                        </li>
                                    </ul>
                                    <br />
                                    <div class="formTitleDiv">
                                        Why am I not getting comments without refreshing discussion?
                                    </div>
                                    <p>
                                        Team Communication Application has not been implemented with real-time communication i.e. you have to refresh discussion
                                    page in order to view any comments added into discussion.
                                    </p>
                                    <p>
                                        <b>However</b>, whenever a member will comment in discussion all the members assigned to discussion will get email notification
                                    about the comment.
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        What is temp user
                                    </div>
                                    <p>
                                        A temp user is a member who can be assgned to a discussion only and can participate as member without loging into AVAIMA.
                                    A private link is generated and is sent to temp user by navigating on temp user can participate and add discussions.
                                <br />
                                    </p>
                                    <br />
                                    <div class="formTitleDiv">
                                        How to add a temp user
                                    </div>
                                    <ul>
                                        <li>Open a discussion you want to add temp member to. 
                                        </li>
                                        <li>Click on <b>Add Client</b> or <b>Add Team</b> depending on your credentials or the role you want to assign to temp user.
                                        </li>
                                        <li>Provide name, alias and email address and select <b>temp user</b> from drop down menu.
                                        </li>
                                        <li>Hit <b>Save</b>.
                                        </li>
                                        <li>This will send a private link to the member from which he can participate in discussion withou any need of logging into AVAIMA.</li>
                                    </ul>
                                    <br />
                                </div>
                        </div>
                    </td>
                    <td>
                        <div class="Main_rightdiv" style="display: inline-block; float: right; margin-right: 16px">
                            <div class="formTitleDiv">Contact Us</div>
                            <div class="Divimproveavaima" style="margin-top: 10px;">
                                <table id="tb_gen" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="formCaptionTd">Type:
                                        </td>
                                        <td class="formFieldTd">
                                            <asp:DropDownList ID="ddlreq" runat="server">
                                                <asp:ListItem Value="Request a Problem">Request a Problem</asp:ListItem>
                                                <asp:ListItem Value="Request a Feature">Request a Feature</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="formVerticalSpacer">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formCaptionTd">Description:
                                        </td>
                                        <td class="formFieldTd">
                                            <asp:TextBox ID="txtmsg" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="auto-style1">
                                            <div class="formVerticalSpacer">
                                                &nbsp;
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formCaptionTd"></td>
                                        <td class="formFieldTd">
                                            <asp:Button ID="btnsend" Text=" Send " runat="server" ValidationGroup="mail"
                                                CssClass="standardFormButton" OnClick="btnsend_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formCaptionTd"></td>
                                        <td class="formFieldTd">
                                            <asp:Label ID="lblmsg" ForeColor="Green" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>


            </table>
        </div>
    </form>
</body>
</html>
