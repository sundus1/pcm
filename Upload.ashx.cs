﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MyCCM;
using CCM.App_Code;
using System.IO;

namespace CCM
{
    /// <summary>
    /// Summary description for Upload
    /// </summary>
    public class Upload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        //public String appid = "7401670f-a967-4da8-98e5-19e76250564b";
        //public String BusinessID = "";
        string storagepath;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Expires = -1;
            try
            {
                HttpPostedFile postedFile = context.Request.Files["Filedata"];
                string discussionID = context.Request.Form["discussionid"].ToString();
                string memDisID = "";
                //memDisID = context.Session["memDisID"].ToString();
                memDisID = context.Request.Form["memDisID"].ToString();

                string projectID = context.Request.Form["projectID"].ToString();
                //BusinessID = context.Request.Form["BusinessID"].ToString();
                storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);
                string savepath = "";
                string tempPath = "";
                tempPath = "uploads";
                savepath = context.Server.MapPath(tempPath);

                if (!System.IO.Directory.Exists(savepath))
                    System.IO.Directory.CreateDirectory(savepath);

                string filename = postedFile.FileName;
                string defaultImage = "";
                string extension = filename.Substring(filename.LastIndexOf('.') + 1, 3);

                //postedFile.SaveAs(savepath + @"\" + filename);
                //context.Response.Write(tempPath + "/" + filename);
                //context.Response.StatusCode = 200;
                MyCCM.DiscussionFile disfile = new DiscussionFile()
                {
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    DiscussionID = Convert.ToInt32(discussionID),
                    FileName = filename,
                    FileType = extension,
                    MemberDisID = memDisID,
                    ProjectID = Convert.ToInt32(projectID),
                };
                disfile.FilePath = storagepath + disfile.FileName;
                UploadAndSaveDisFile(postedFile.InputStream, disfile);
                context.Response.Write(disfile.DisFileID.ToString());
            }
            catch (Exception ex)
            {
                context.Response.Write("Error: " + ex.Message);
            }
        }

        private void UploadAndSaveDisFile(Stream fileStream, DiscussionFile disFile)
        {
            string datacontainer = "data";
            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                CloudStorageAccount storageAccount = new CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);


                string _uploaddir = String.Format("{0}{1}", storagepath, disFile.FileName);

                CloudBlockBlob blob = container.GetBlockBlobReference(_uploaddir);
                blob.Properties.ContentType = disFile.FileType;

                BlobRequestOptions objoption = new BlobRequestOptions();
                objoption.Timeout = TimeSpan.FromHours(1);

                //f.InputStream.Position = 0;
                blob.UploadFromStream(fileStream, objoption);
                disFile.Add();
            }
            catch (Exception ex) { }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}