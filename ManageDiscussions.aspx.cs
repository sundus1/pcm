﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace CCM
{
    public partial class ManageDiscussions : AvaimaWebPage
    {

        private string _OwnerId = "";
        List<Folder> _Folders = new List<Folder>();
        Member _ActiveMember = new Member();
        Boolean _IsNew = false;  // to see if user is new in CCM
        public string _MemberRole = "";
        public int _FolderCount = 0;
        public int _discussionCount = 0;
        public String _LocalInstanceID = "";

        #region - - - - HELPER METHODS - - - -
        private void BindGrid(int memberID)
        {
            int pid = 0;
            MemberRoles memberRole = MemberRoles.SuperAdmin;
            if (Request.QueryString["fid"] != null)
            {
                pid = Request.QueryString["fid"].ToInt32();
                MemberToFolder mtf = MemberToFolder.SingleOrDefault(u => u.FolderID == pid && u.MemberID == hdnMemberID.Value.ToInt32());
                if (mtf != null)
                {
                    memberRole = (MemberRoles)mtf.RoleID;
                }
            }
            if (memberRole == MemberRoles.ClientMember || memberRole == MemberRoles.ClientMember)
            {
                navAdmin.Visible = false;
            }
            else
            {
                navAdmin.Visible = true;
            }
            DataTable dtFolder = CCMFolder.GetFolders(_LocalInstanceID, hdnMemberID.Value.ToInt32(), pid);
            hdn_pfid.Value = pid.ToString();
            rptFolders.DataSource = dtFolder;
            rptFolders.DataBind();
            DataTable dtDiscussions = CCMDiscussion.GetDiscussions(_LocalInstanceID, hdnMemberID.Value.ToInt32(), hdn_pfid.Value.ToInt32());
            rptDiscussion.DataSource = dtDiscussions;
            rptDiscussion.DataBind();
            if (dtFolder.Rows.Count <= 0 && dtDiscussions.Rows.Count <= 0 && Request.QueryString["fid"] == null)
            {
                sectionDashboard.Visible = false;
                sectionWelcome.Visible = true;
            }
            List<Project> projects = Project.Find(u => u.OwnerID == hdn_oID.Value).ToList();
            projects.Insert(0, new Project() { ProjectID = 0, Title = "None" });
            //ddlDisProjects.DataSource = projects;
            //ddlDisProjects.DataTextField = "Title";
            //ddlDisProjects.DataValueField = "ProjectID";
            //ddlDisProjects.DataBind();
            BindTreeViewFolder(memberID);
        }

        private void BindTreeViewFolder(int memberID)
        {
            tvFolder.Nodes.Clear();
            tvFolder.Nodes.Add(GetFolderNodes(0, memberID));
        }

        private TreeNode GetFolderNodes(int folderID, int memberID)
        {
            MemberToFolder folderProject = MemberToFolder.SingleOrDefault(u => u.FolderID == folderID && u.MemberID == memberID);
            Folder folder = null;
            if (folderProject != null)
            {
                folder = Folder.SingleOrDefault(u => u.FolderID == folderProject.FolderID);
            }
            TreeNode tn = new TreeNode();
            if (folder != null)
            {

                tn = new TreeNode(folder.Title, folder.FolderID.ToString());
                tn.Text = "<input type='radio' name='rdoC' value ='" + folder.FolderID.ToString() + "'>" + folder.Title.ToString() + "</input>";
                tn.SelectAction = TreeNodeSelectAction.None;
                List<MemberToFolder> folders = MemberToFolder.Find(u => u.ParentFolderID == folderID && u.MemberID == memberID).ToList();
                foreach (MemberToFolder item in folders)
                {
                    tn.ChildNodes.Add(GetFolderNodes(item.FolderID.ToInt32(), memberID));
                }
            }
            else if (folderID == 0)
            {
                tn = new TreeNode("Root", "0");
                tn.Text = "<input type='radio' name='rdoC' value ='0' >Root</input>";
                tn.SelectAction = TreeNodeSelectAction.None;
                List<MemberToFolder> folders = MemberToFolder.Find(u => u.ParentFolderID == folderID && u.MemberID == memberID).ToList();
                foreach (MemberToFolder item in folders)
                {
                    tn.ChildNodes.Add(GetFolderNodes(item.FolderID.ToInt32(), memberID));
                }
            }
            return tn;
        }

        private void CountSubFoldersAndDiscussions(int FolderID)
        {
            _FolderCount += MemberToFolder.Find(u => u.ParentFolderID == FolderID && u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID).Count;
            _discussionCount += DiscussionToFolder.Find(u => u.FolderID == FolderID && u.InstanceID == _LocalInstanceID && u.MemberID == hdnMemberID.Value.ToInt32()).Count;
            List<MemberToFolder> Folders = MemberToFolder.Find(u => u.ParentFolderID == FolderID && u.MemberID == hdnMemberID.Value.ToInt32()).ToList();
            foreach (MemberToFolder folder in Folders)
            {
                //_FolderCount += Folder.Find(u => u.Parent_FolderID == proj.FolderID).Count;
                CountSubFoldersAndDiscussions(folder.FolderID.ToInt32());
            }
        }

        private void ClearFolder()
        {
            txtFolderDescription.Text = "";
            txtFolderTitle.Text = "";
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.InstanceID == "0") { _LocalInstanceID = "678d7728-915e-4e81-ac5b-d536e1a44fb9"; }
            else { _LocalInstanceID = this.InstanceID; }
            divAddFolder.Style.Add("display", "none");
            divAddDiscussion.Style.Add("display", "none");
            divManageMember.Style.Add("display", "none");
            AddinstanceWS objinst = new AddinstanceWS();
            hdnInstanceID.Value = _LocalInstanceID;
            //testInstanceID = "678d7728-915e-4e81-ac5b-d536e1a44fb9";
            _OwnerId = CCM_Helper.GetOwnerID();
            //_OwnerId = "632351f2-6bb1-4e99-845c-fb2e75341d59"; // sbmuhammadfaizan Owner SuperAdmin
            //_OwnerId = "c827ef9d-fafd-4b48-b737-27bbee6ec54f"; // b3ast667
            //_OwnerId = "d0670956-4d4c-448c-9e2e-370c295d7b5d"; // faizan outlook
            hdn_oID.Value = _OwnerId;
            //_OwnerId = "e8a92994-352a-4319-838d-882e8dc1bdeb";
            //_LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab";
            _ActiveMember = Member.SingleOrDefault(u => u.OwnerID == _OwnerId && u.InstanceID == _LocalInstanceID);
            if (_ActiveMember == null)
            {
                // If user is new to application
                AvaimaUserProfile objwebser = new AvaimaUserProfile();
                //string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
                string[] user = objwebser.Getonlineuserinfo(_OwnerId).Split('#');
                // Insert SuperAdminMember
                _ActiveMember = new Member()
                {
                    OwnerID = _OwnerId,
                    //FolderID = 0,
                    InstanceID = _LocalInstanceID,
                    FirstName = user[0] + " " + user[1],
                    LastName = "",
                    NickName = user[0] + " " + user[1],
                    Email = user[2],
                    ImagePath = "_assests/images/SuperAdmin.png",
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    Title = ""
                };
                _ActiveMember.Add();
                sectionDashboard.Visible = false;
                sectionWelcome.Visible = true;
            }
            else
            {
                sectionDashboard.Visible = true;
                sectionWelcome.Visible = false;
            }
            if (String.IsNullOrEmpty(_ActiveMember.Title) || String.IsNullOrEmpty(_ActiveMember.FirstName))
            {
                hdnAskInfo.Value = "1";
            }
            else
            {
                hdnAskInfo.Value = "0";
            }
            if (!IsPostBack)
            {
                hdnMemberID.Value = _ActiveMember.MemberID.ToString();
                txtMName.Text = _ActiveMember.FirstName;
                txtMAlias.Text = _ActiveMember.NickName;
                txtMTitle.Text = _ActiveMember.Title;
                BindGrid(_ActiveMember.MemberID);
                GenerateBreadCrumb();
            }
            String url = "Discussion.aspx?did=" + ("0").Encrypt() + "&fid=" + hdn_pfid.Value.Encrypt() + "&od=" + hdn_oID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + InstanceID;
            aStartDiscussion.Attributes["href"] = url;
            aStartDiscussion2.Attributes["href"] = url;
            //aStartDiscussion.Attributes["href"] = "Discussion.aspx?disid=0&pid=0&fid=" + hdn_folderid.Value + "&oid=" + hdn_oID.Value + "&instanceid=" + this.InstanceID;
        }

        private void GenerateBreadCrumb()
        {
            List<MemberToFolder> t_Folders = MemberToFolder.Find(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID).ToList();
            int folderID = Convert.ToInt32(hdn_pfid.Value);
            List<Folder> folders = new List<Folder>();
            GetBCList(t_Folders, folderID, folders);
            rptBC.DataSource = folders.OrderBy(u => u.FolderID).ToList();
            rptBC.DataBind();
        }

        private void GetBCList(List<MemberToFolder> _Folders, int projID, List<Folder> folders)
        {
            MemberToFolder memberToFolder = _Folders.SingleOrDefault(u => u.FolderID == projID && u.MemberID == hdnMemberID.Value.ToInt32());
            if (memberToFolder != null)
            {
                Folder folder = Folder.SingleOrDefault(u => u.FolderID == memberToFolder.FolderID);
                if (folder != null)
                {
                    folders.Add(folder);
                }

                GetBCList(_Folders, Convert.ToInt32(memberToFolder.ParentFolderID.ToInt32()), folders);
            }
        }


        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            Folder folder = new Folder();
            MemberToFolder memberToFolder = new MemberToFolder();
            Member currMember = new Member();
            if (Convert.ToInt32(hdnSelFolderID.Value) == 0)
            {
                folder = new Folder()
                {
                    InstanceID = _LocalInstanceID,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Description = txtFolderDescription.Text,
                    Title = txtFolderTitle.Text
                };
                folder.Add();

                Folder parentFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_pfid.Value.ToInt32());
                if (parentFolder != null)
                {
                    List<MemberToFolder> adminsOfFolder = MemberToFolder.Find(u => (u.RoleID == 5 || u.RoleID == 2 || u.RoleID == 1) && u.FolderID == hdn_pfid.Value.ToInt32()).ToList();
                    foreach (MemberToFolder adminOfFolder in adminsOfFolder)
                    {
                        memberToFolder = MemberToFolder.SingleOrDefault(u => u.FolderID == folder.FolderID && u.MemberID == adminOfFolder.MemberID.ToInt32() && u.InstanceID == _LocalInstanceID);
                        if (memberToFolder == null)
                        {
                            memberToFolder = new MemberToFolder()
                            {
                                InstanceID = _LocalInstanceID,
                                MemberID = adminOfFolder.MemberID,
                                ParentFolderID = hdn_pfid.Value.ToInt32(),
                                FolderID = folder.FolderID,
                                Active = true,
                                CrtDate = DateTime.Now,
                                ModDate = DateTime.Now,
                                RoleID = adminOfFolder.RoleID,
                                ApplicableTo = adminOfFolder.ApplicableTo,
                                Title = adminOfFolder.Title,
                                Alias = adminOfFolder.Alias
                            };
                            memberToFolder.Add();
                        }
                        //else
                        //{
                        //    if (memberToFolder.RoleID != 1)
                        //    {
                        //        memberToFolder.MemberID = adminOfFolder.MemberID;
                        //        memberToFolder.ModDate = DateTime.Now;
                        //        memberToFolder.RoleID = adminOfFolder.RoleID;
                        //        memberToFolder.ApplicableTo = adminOfFolder.ApplicableTo;
                        //        memberToFolder.Title = adminOfFolder.Title;
                        //        memberToFolder.Alias = adminOfFolder.Alias;
                        //        memberToFolder.Update();
                        //    }
                        //}
                    }
                }
                else
                {
                    currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
                    memberToFolder = new MemberToFolder()
                    {
                        InstanceID = _LocalInstanceID,
                        MemberID = hdnMemberID.Value.ToInt32(),
                        ParentFolderID = hdn_pfid.Value.ToInt32(),
                        FolderID = folder.FolderID,
                        Active = true,
                        CrtDate = DateTime.Now,
                        ModDate = DateTime.Now,
                        RoleID = 1,
                        ApplicableTo = "SuperAdminRights",
                        Title = currMember.Title,
                        Alias = currMember.NickName
                    };
                    memberToFolder.Add();
                }
            }
            else
            {
                folder = Folder.SingleOrDefault(u => u.FolderID == Convert.ToInt32(hdnSelFolderID.Value));
                if (folder != null)
                {
                    folder.ModDate = DateTime.Now;
                    folder.Title = txtFolderTitle.Text;
                    folder.Description = txtFolderDescription.Text;
                    folder.Update();
                }
            }
            BindGrid(Convert.ToInt32(hdnMemberID.Value));
            divAddFolder.Style.Add("display", "none");
            ClearFolder();
        }

        private Info AssignAdmin(int FolderID, int MemberID, int RoleID)
        {
            Info info = new Info("Error", "No MTF selected", "");
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            // Fetch all folders and subfolders on which MemberToFolder has to be assigned
            Member currentMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            Member enrolledMember = Member.SingleOrDefault(u => u.MemberID == MemberID);
            MemberToFolder currMTF = MemberToFolder.SingleOrDefault(u => u.MemberID == currentMember.MemberID && u.FolderID == FolderID);
            Folder folder = Folder.SingleOrDefault(u => u.FolderID == FolderID);
            String SUBJECT = "";
            String BODY = "";
            DataTable dtFolders = CCMFolder.GetChildFoldersOf(FolderID, hdnMemberID.Value.ToInt32());
            foreach (DataRow row in dtFolders.Rows)
            {
                MemberToFolder m_mtf = MemberToFolder.SingleOrDefault(u => u.FolderID == row["FolderID"].ToInt32() && u.MemberID == MemberID && u.InstanceID == _LocalInstanceID);
                if (m_mtf == null)
                {
                    m_mtf = new MemberToFolder()
                    {
                        RoleID = RoleID,
                        MemberID = MemberID,
                        InstanceID = _LocalInstanceID,
                        FolderID = row["FolderID"].ToInt32(),
                        Active = true,
                        ApplicableTo = "All", //ddlClient.SelectedItem.Value,
                        CrtDate = DateTime.Now,
                        ModDate = DateTime.Now,
                        ParentFolderID = row["ParentFolderID"].ToInt32(),
                        Alias = txtTeamAlias.Text,
                        Title = txtTeamTitle.Text,
                    };
                    if (String.IsNullOrEmpty(txtTeamTitle.Text)) { m_mtf.Title = enrolledMember.Title; }
                    if (String.IsNullOrEmpty(txtTeamAlias.Text)) { m_mtf.Alias = enrolledMember.NickName; }
                    m_mtf.Add();
                    if (FolderID == row["FolderID"].ToInt32())
                    {
                        info = new Info("Info", "Member successfully assigned to folder as \"" + CCM_Helper.GetMemberRoleSimplifiedName(m_mtf.RoleID.ToInt32()) + "\"", enrolledMember.MemberID.ToString());
                    }
                    //emailAPI.send_email = 
                }
                else
                {
                    if (m_mtf.RoleID != 1)
                    {
                        m_mtf.RoleID = RoleID;
                        m_mtf.FolderID = row["FolderID"].ToInt32();
                        m_mtf.ParentFolderID = row["ParentFolderID"].ToInt32();
                        m_mtf.ModDate = DateTime.Now;
                        if (!String.IsNullOrEmpty(txtTeamTitle.Text)) { m_mtf.Title = txtTeamTitle.Text; }
                        else { m_mtf.Title = enrolledMember.Title; }
                        if (!String.IsNullOrEmpty(txtTeamAlias.Text)) { m_mtf.Alias = txtTeamAlias.Text; }
                        else { m_mtf.Alias = enrolledMember.NickName; }
                        m_mtf.Update();
                        if (FolderID == row["FolderID"].ToInt32())
                        {
                            info = new Info("Info", "Member updated successfully", enrolledMember.MemberID.ToString());
                        }
                    }
                    else
                    {
                        if (FolderID == row["FolderID"].ToInt32())
                        {
                            info = new Info("Error", "Member you are trying to assign is already Super Admin of this discussion.", enrolledMember.MemberID.ToString());
                        }
                    }
                }

                SUBJECT = "You have been added as an administrator";
                //SUBJECT = currMTF.Alias + " has made you " + CCM_Helper.GetMemberRoleSimplifiedName(RoleID) + " over Folder: " + folder.Title + ".";
                BODY = "Hi " + m_mtf.Alias + ",";
                BODY += "<br />";
                BODY += "<br />";
                BODY += currMTF.Alias + " has made you administrator of " + folder.Title + ".";
                BODY += "<br /><br />Go to <a href=\"http://www.avaima.com/\">www.avaima.com</a> to sign in and use the application.";
                BODY += "<br /><br />If you are not a member, go to <a href=\"http://www.avaima.com/\">www.avaima.com</a> and sign up.";
                //if (RoleID == 2)
                //{
                //    BODY += "<ul>";
                //    BODY += "<li>Edit/Remove this folder and its ubfolders.</li>";
                //    BODY += "<li>Add sub folders.</li>";
                //    BODY += "<li>Add/Remove team and client members.</li>";
                //    BODY += "<li>Assign team and client admins.</li>";
                //    BODY += "</ul>";
                //}
                //else if (RoleID == 5)
                //{

                //    BODY += "<ul>";
                //    BODY += "<li>Edit this folder.</li>";
                //    BODY += "<li>Add sub folders.</li>";
                //    BODY += "<li>Add/Remove client members.</li>";
                //    BODY += "<li>Assign client admins.</li>";
                //    BODY += "</ul>";
                //}
                //BODY += "<br />";
                //BODY += "Login <a href=\"http://www.avaima.com\" >AVAIMA CCM</a> to access and manage assigned folder";
                BODY += Utilities.AvaimaEmailSignature;
                emailAPI.send_email(enrolledMember.Email, currMTF.Alias, "noreply@avaima.com", BODY, SUBJECT);

                // Fetch discussions under folder and assign DiscussionToFolder to selected member
                List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.FolderID == row["FolderID"].ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID).ToList();
                foreach (DiscussionToFolder dtf in dtfs)
                {
                    DiscussionToFolder m_dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == MemberID && u.DiscussionID == dtf.DiscussionID);
                    if (m_dtf == null)
                    {
                        // Create DiscussionToFolder 
                        m_dtf = new DiscussionToFolder()
                        {
                            DiscussionID = dtf.DiscussionID,
                            MemberID = MemberID,
                            FolderID = row["FolderID"].ToInt32(),
                            Active = true,
                            CrtDate = DateTime.Now,
                            InstanceID = _LocalInstanceID,
                            ModDate = DateTime.Now,
                            RoleID = RoleID,
                            Status = 1,
                            Alias = txtTeamAlias.Text,
                            Title = txtTeamTitle.Text,
                        };
                        if (String.IsNullOrEmpty(txtTeamTitle.Text)) { m_dtf.Title = enrolledMember.Title; }
                        if (String.IsNullOrEmpty(txtTeamAlias.Text)) { m_dtf.Alias = enrolledMember.NickName; }
                        m_dtf.Add();
                    }
                    else
                    {
                        if (m_dtf.RoleID != 1)
                        {
                            m_dtf.FolderID = row["FolderID"].ToInt32();
                            //m_dtf.RoleID = RoleID;
                            //m_dtf.Status = 1;
                            if (String.IsNullOrEmpty(txtTeamTitle.Text)) { m_dtf.Title = enrolledMember.Title; }
                            if (String.IsNullOrEmpty(txtTeamAlias.Text)) { m_dtf.Alias = enrolledMember.NickName; }

                            if (String.IsNullOrEmpty(m_dtf.Title)) { m_dtf.Title = txtTeamTitle.Text; }
                            if (String.IsNullOrEmpty(m_dtf.Alias)) { m_dtf.Title = txtTeamAlias.Text; }
                            m_dtf.Update();
                        }
                    }
                }
            }
            return info;
        }

        private void RemoveAdmin(int FolderID, int MemberID, int RoleID)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            Member currentMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            Member enrolledMember = Member.SingleOrDefault(u => u.MemberID == MemberID);
            Folder folder = Folder.SingleOrDefault(u => u.FolderID == FolderID);
            // Fetch all folders and subfolders and delete MemberToFolder
            DataTable dtFolders = CCMFolder.GetChildFoldersOf(FolderID, hdnMemberID.Value.ToInt32());
            String SUBJECT = "";
            String BODY = "";
            MemberToFolder currMTF = MemberToFolder.SingleOrDefault(u => u.MemberID == currentMember.MemberID && u.FolderID == FolderID);
            foreach (DataRow row in dtFolders.Rows)
            {
                MemberToFolder m_mtf = MemberToFolder.SingleOrDefault(u => u.FolderID == row["FolderID"].ToInt32() && u.MemberID == MemberID && u.InstanceID == _LocalInstanceID && u.RoleID == RoleID);
                if (m_mtf != null)
                {
                    m_mtf.Delete();

                    SUBJECT = "You have been removed from " + folder.Title;
                    //SUBJECT = currMTF.Alias + " has removed your role " + CCM_Helper.GetMemberRoleSimplifiedName(RoleID) + " from Folder: " + folder.Title + ".";
                    BODY = "Hi " + m_mtf.Alias + ",";
                    BODY += "<br />";
                    BODY += "<br />";
                    BODY += currMTF.Alias + " has removed your role as administrator of Folder: <b>" + folder.Title + "</b>.";
                    BODY += "<br /><br />For further details contact " + currMTF.Alias;
                    BODY += Utilities.AvaimaEmailSignature;
                    emailAPI.send_email(enrolledMember.Email, currMTF.Alias, "noreply@avaima.com", BODY, SUBJECT);
                }

                // Fetch discussions under folder and Delete DiscussionToFolder to selected member
                List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.FolderID == row["FolderID"].ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32() && u.RoleID == RoleID).ToList();
                foreach (DiscussionToFolder dtf in dtfs)
                {
                    DiscussionToFolder m_dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == MemberID && u.DiscussionID == dtf.DiscussionID);
                    if (m_dtf != null)
                    {
                        m_dtf.Delete();
                    }
                }
            }
        }

        protected void rptFolders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                _FolderCount = 0;
                _discussionCount = 0;
                Label txtSubFolders = e.Item.FindControl("txtSubFolders") as Label;
                Label txtDiscussions = e.Item.FindControl("txtDiscussions") as Label;
                HtmlControl trFolder = e.Item.FindControl("trFolder") as HtmlControl;
                LinkButton lnkEdit = e.Item.FindControl("lnkEdit") as LinkButton;
                LinkButton lnkManageMember = e.Item.FindControl("lnkManageMember") as LinkButton;
                HtmlControl divAdmin = e.Item.FindControl("divAdmin") as HtmlControl;
                CheckBox chkFolder = e.Item.FindControl("chkFolder") as CheckBox;
                Int32 FolderID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "FolderID"));
                string memberID = DataBinder.Eval(e.Item.DataItem, "MemberID").ToString();
                string roleID = DataBinder.Eval(e.Item.DataItem, "RoleID").ToString();
                CountSubFoldersAndDiscussions(Convert.ToInt32(trFolder.Attributes["data-id"].ToString()));
                txtSubFolders.Text = _FolderCount.ToString();
                txtDiscussions.Text = _discussionCount.ToString();
                if (roleID.ToInt32() == 2 || roleID.ToInt32() == 5 || roleID.ToInt32() == 1)
                {
                    divAdmin.Visible = true;
                    //lnkEdit.Visible = true;
                    //lnkManageMember.Visible = true;
                    //chkFolder.Enabled = true;
                }
                else
                {
                    divAdmin.Visible = false;
                    //lnkEdit.Visible = false;
                    //lnkManageMember.Visible = false;
                    //chkFolder.Enabled = false;
                }
                HtmlControl aDisTitle = e.Item.FindControl("aDisTitle") as HtmlControl;
                //String disID = DataBinder.Eval(e.Item.DataItem, "DiscussionID").ToString();
                String url = "Discussion.aspx?did=" + ("0").Encrypt() + "&fid=" + DataBinder.Eval(e.Item.DataItem, "FolderID").ToString().Encrypt() + "&od=" + hdn_oID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&ir=" + roleID.Encrypt() + "&instanceid=" + InstanceID;
                aDisTitle.Attributes["href"] = url;
            }
        }

        private class FolderMember
        {
            public int MemberID { get; set; }
            public String NickName { get; set; }
            public String Name { get; set; }
            public Boolean IsAdmin { get; set; }
        }

        protected void rptFolders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Open")
            {
                String url = "ManageDiscussions.aspx?fid=" + e.CommandArgument;
                this.Redirect(url);
            }
            else if (e.CommandName == "ManageAdmin")
            {
                hdnMMSelFolderID.Value = e.CommandArgument.ToString();
                LinkButton txtTitle = (LinkButton)e.Item.FindControl("txtTitle");
                hFolderName.InnerText = txtTitle.Text;
                BindExistingMembersDLL();
                BindTeam(hdnMMSelFolderID.Value.ToInt32());
                BindClients(hdnMMSelFolderID.Value.ToInt32());
                MemberRoles mr = CCMFolder.GetFolderMemberRole(hdnMMSelFolderID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                //if (mr == MemberRoles.ClientAdmin) { trTeam.Visible = false; rdoTeam.Visible = false; rdoClient.Checked = true; }
                if (mr == MemberRoles.Admin || mr == MemberRoles.SuperAdmin) { trTeam.Visible = true; rdoTeam.Checked = true; rdoClient.Checked = false; trM4.Visible = false; }
                else { trTeam.Visible = false; rdoTeam.Visible = false; rdoTeam.Checked = true; rdoClient.Checked = false; trM4.Visible = false; }
                divManageMember.Style.Remove("display");
                btnAddFolder.Text = "Update";
            }
        }


        protected void rptDiscussion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Open")
            {
                string url = "Discussion.aspx?disid=" + e.CommandArgument.ToString().Split('|')[0] + "&pid=" + e.CommandArgument.ToString().Split('|')[2] + "&fid=" + e.CommandArgument.ToString().Split('|')[1] + "&oid=" + hdn_oID.Value;
                this.Redirect(url);
            }
            else if (e.CommandName == "Edit")
            {
                this.Redirect("Discussion.aspx?disid=" + e.CommandArgument.ToString().Split('|')[0] + "&pid=" + e.CommandArgument.ToString().Split('|')[1] + "&oid=" + hdn_oID.Value + "&instanceid=" + _LocalInstanceID);
            }
        }

        protected void rptBC_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Navigate")
            {
                LinkButton lnkButton = e.Item.FindControl("lnkBC") as LinkButton;
                if (e.CommandArgument.ToString() != "0")
                {
                    this.Redirect("ManageDiscussions.aspx?fid=" + e.CommandArgument.ToString());
                }
                else
                {
                    this.Redirect("ManageDiscussions.aspx");
                }
            }
        }

        protected void btnAddDiscussion_Click(object sender, EventArgs e)
        {
            string url = CreateDiscussion();
            BindGrid(Convert.ToInt32(hdnMemberID.Value));
            divAddFolder.Style.Add("display", "none");
            txtDisTitle.Text = "";
            hdnSelDisFolderID.Value = "0";
            string jScript = "<script>" + String.Format("window.open('{0}','_blank')", ResolveUrl(url)) + ";</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "keyDisRedirect", jScript);
        }

        private String CreateDiscussion()
        {
            MyCCM.Discussion discussion = new MyCCM.Discussion()
            {
                ProjectID = 0,
                Active = true,
                CrtDate = DateTime.Now,
                InstanceID = _LocalInstanceID,
                ModDate = DateTime.Now,
                Status = "Open",
                Title = txtDisTitle.Text
            };
            discussion.Add();
            DiscussionToFolder disToFolder = new DiscussionToFolder();
            Folder parentFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_pfid.Value.ToInt32());
            if (parentFolder != null)
            {
                List<MemberToFolder> adminsOfFolder = MemberToFolder.Find(u => (u.RoleID == 5 || u.RoleID == 2 || u.RoleID == 1) && u.FolderID == hdn_pfid.Value.ToInt32()).ToList();
                foreach (MemberToFolder adminOfFolder in adminsOfFolder)
                {

                    disToFolder = new DiscussionToFolder()
                    {
                        InstanceID = _LocalInstanceID,
                        MemberID = adminOfFolder.MemberID,
                        FolderID = hdn_pfid.Value.ToInt32(),
                        Active = true,
                        CrtDate = DateTime.Now,
                        ModDate = DateTime.Now,
                        RoleID = adminOfFolder.RoleID,
                        Status = 1,
                        DiscussionID = discussion.DiscussionID,
                        Alias = adminOfFolder.Alias,
                        Title = adminOfFolder.Title,
                        Unread = 0
                    };
                    disToFolder.Add();
                }
            }
            else
            {
                disToFolder = new DiscussionToFolder()
                {
                    MemberID = hdnMemberID.Value.ToInt32(),
                    FolderID = 0,
                    Active = true,
                    CrtDate = DateTime.Now,
                    DiscussionID = discussion.DiscussionID,
                    InstanceID = _LocalInstanceID,
                    ModDate = DateTime.Now,
                    RoleID = 1,
                    Status = 1,
                    Alias = txtMAlias.Text,
                    Title = txtMTitle.Text,
                    Unread = 0
                };
                disToFolder.Add();
            }

            disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID && u.DiscussionID == discussion.DiscussionID);
            String url = "Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + hdn_pfid.Value.Encrypt() + "&od=" + hdn_oID.Value.Encrypt() + "&ir=" + disToFolder.RoleID.ToString().Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + InstanceID;
            return url;
        }

        protected void btnAddnStartDis_Click(object sender, EventArgs e)
        {
            //MyCCM.Discussion disc = CreateDiscussion();
            //this.Redirect("Discussion.aspx?disid=" + disc.DiscussionID + "&pid=" + disc.ProjectID.ToString() + "&fid=" + disc.FolderID + "&oid=" + hdn_oID.Value);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string infoString = "No entry point";
            // Delete Folders and its discussions
            foreach (RepeaterItem item in rptFolders.Items)
            {
                CheckBox chk = item.FindControl("chkFolder") as CheckBox;
                int folderID = Convert.ToInt32(((HtmlControl)item.FindControl("trFolder")).Attributes["data-id"].ToString());
                if (chk.Checked)
                {
                    MemberRoles mr = CCMFolder.GetFolderMemberRole(folderID, hdnMemberID.Value.ToInt32());
                    if (mr == MemberRoles.Admin || mr == MemberRoles.SuperAdmin)
                    {
                        infoString += CCM_Helper.DeleteFolderAndItsSubFolders(folderID, hdnMemberID.Value.ToInt32());
                    }
                    //infoString += info.Title + ": " + info.Description;
                }
            }

            // DeleteDiscussion
            foreach (RepeaterItem item in rptDiscussion.Items)
            {
                CheckBox chk = item.FindControl("chkFolder") as CheckBox;
                if (chk.Checked)
                {
                    int discussionid = Convert.ToInt32(((HtmlControl)item.FindControl("trDiscussion")).Attributes["data-id"].ToString());

                    Info info = CCM_Helper.DeleteDiscussionData(discussionid, hdnMemberID.Value.ToInt32());
                    infoString += info.Title + ": " + info.Description;

                }
            }            //lblInfo.Text = infoString;
            BindGrid(Convert.ToInt32(hdnMemberID.Value));
        }

        protected void btnMoveToFolder_Click(object sender, EventArgs e)
        {
            // Move Folder
            foreach (RepeaterItem item in rptFolders.Items)
            {
                CheckBox chk = item.FindControl("chkFolder") as CheckBox;
                int folderID = Convert.ToInt32(((HtmlControl)item.FindControl("trFolder")).Attributes["data-id"].ToString());
                if (chk.Checked)
                {
                    MemberToFolder memberToFolder = MemberToFolder.SingleOrDefault(u => u.FolderID == folderID && u.MemberID == hdnMemberID.Value.ToInt32());
                    if (memberToFolder != null)
                    {
                        memberToFolder.ParentFolderID = Convert.ToInt32(hdnSelMoveFolderID.Value);
                        memberToFolder.Update();
                    }
                }
            }

            // Move Discussions
            foreach (RepeaterItem item in rptDiscussion.Items)
            {
                CheckBox chk = item.FindControl("chkFolder") as CheckBox;
                if (chk.Checked)
                {
                    int discussionid = Convert.ToInt32(((HtmlControl)item.FindControl("trDiscussion")).Attributes["data-id"].ToString());

                    MyCCM.Discussion disc = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid);
                    if (disc != null)
                    {
                        DiscussionToFolder disFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == disc.DiscussionID && u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID);
                        disFolder.FolderID = Convert.ToInt32(hdnSelMoveFolderID.Value);
                        disFolder.Update();
                    }
                }
            }

            BindGrid(Convert.ToInt32(hdnMemberID.Value));
        }

        protected void lnkCreateDiscussion_Click(object sender, EventArgs e)
        {
            MyCCM.Discussion disc = new MyCCM.Discussion()
            {
                Active = true,
                CrtDate = DateTime.Now,
                ModDate = DateTime.Now,
                //FolderID = CurrFolderID,
                InstanceID = _LocalInstanceID,
                ProjectID = 0,
                Status = "Open",
                Title = txtDisTitle.Text
            };
            disc.Add();
            this.Redirect("Discussion.aspx?disid=" + disc.DiscussionID + "&pid=0&fid=" + hdn_folderid.Value + "&oid=" + hdn_oID.Value + "&instanceid=" + _LocalInstanceID);
        }

        public int CurrFolderID
        {
            get
            {
                return Convert.ToInt32(hdn_folderid.Value);
            }
        }

        protected void lnkManageProjects_Click(object sender, EventArgs e)
        {
            this.Redirect("ProjectManagement.aspx");
        }

        protected void rptDiscussion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                CCM.App_Code.MemberDiscussion md = new CCM.App_Code.MemberDiscussion();
                HtmlControl aDisTitle = e.Item.FindControl("aDisTitle") as HtmlControl;
                Label lblCount = e.Item.FindControl("lblCount") as Label;
                String disID = DataBinder.Eval(e.Item.DataItem, "DiscussionID").ToString();
                string roleID = DataBinder.Eval(e.Item.DataItem, "RoleID").ToString();
                int count = md.GetMemberDiscussionCountByDiscussionID(disID);
                lblCount.Text = "(" + count + ")";
                String url = "Discussion.aspx?did=" + disID.Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdn_oID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&ir=" + roleID.Encrypt() + "&instanceid=" + _LocalInstanceID;
                aDisTitle.Attributes["href"] = url;
            }
        }

        protected void lnkNewFolder_Click(object sender, EventArgs e)
        {
            List<MemberToFolder> inheritedMTFS = MemberToFolder.Find(u => u.FolderID == hdn_pfid.Value.ToInt32() && (u.RoleID == 5 || u.RoleID == 2)).ToList();
            hdnSelFolderID.Value = "0";
            BindExistingMembersDLL();
            BindTeam(0);
            BindClients(0);
            btnAddFolder.Text = "Save";
            txtFolderTitle.Focus();
        }

        protected void rdoClient_CheckedChanged(object sender, EventArgs e)
        {
            BindExistingMembersDLL();
            ClearMemberFields();
            chkAddNew.Checked = true;
            divManageMember.Style.Remove("display");
        }

        private void ClearMemberFields()
        {
            txtTeamAlias.Text = "";
            txtTeamEmail.Text = "";
            txtTeamTitle.Text = "";
        }

        private void BindExistingMembersDLL()
        {
            ddlClientMembers.DataSource = CCM_Helper.GetMembers(_LocalInstanceID, MemberRoles.ClientMember, hdnMemberID.Value.ToInt32());
            ddlClientMembers.DataTextField = "NameAlias";
            ddlClientMembers.DataValueField = "MemberID";
            ddlClientMembers.DataBind();
            ddlTeamMembers.DataSource = CCM_Helper.GetMembers(_LocalInstanceID, MemberRoles.VendorMember, hdnMemberID.Value.ToInt32());
            ddlTeamMembers.DataTextField = "NameAlias";
            ddlTeamMembers.DataValueField = "MemberID";
            ddlTeamMembers.DataBind();
        }

        protected void btnAddMember_Click(object sender, EventArgs e)
        {
            string response = AddMember();
            if (response.Split(':')[0].Contains("Info"))
            {
                //ShowInfo(resposen.Split(':')[1]); 
                lblExInfo.Text = response.Split(':')[1];
                lblExInfo.ForeColor = System.Drawing.Color.Green;
                ClearMemberFields();
            }
            else if (response.Split(':')[0].Contains("Error"))
            {
                //ShowError(resposen.Split(':')[1]); 
                lblExInfo.Text = response.Split(':')[1];
                lblExInfo.ForeColor = System.Drawing.Color.Red;
            }
            BindExistingMembersDLL();
            BindTeam(hdnMMSelFolderID.Value.ToInt32());
            BindClients(hdnMMSelFolderID.Value.ToInt32());
            divManageMember.Style.Remove("display");
            DisplayNewFields();
        }

        private string AddMember()
        {
            Member currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID);
            AddinstanceWS objaddinst = new AddinstanceWS();
            AvaimaUserProfile objwebser = new AvaimaUserProfile();
            string userid = objaddinst.AddInstance(txtTeamEmail.Text, Utilities.AppID, _LocalInstanceID, HttpContext.Current.User.Identity.Name);
            Member member = Member.SingleOrDefault(u => u.Email.Contains(txtTeamEmail.Text) && u.InstanceID == this.InstanceID);
            if (member == null)
            {
                Info info = CreateMemberWithMTF(currMember, member, userid);
                ClearMemberFields();
                return info.Title + ":" + info.Description + ":" + member.MemberID;
            }
            else
            {
                //Info info = CreateMemberWithMTF(hdnDisID.Value.ToInt32(), Discussion, currMember, member, userid);
                Info info = CreateMemberWithMTF(currMember, member, userid);
                ClearMemberFields();
                return info.Title + ":" + info.Description + ":" + member.MemberID;
            }

        }

        private void SetDisplayNone()
        {
            //trM1.Style.Add("display", "none");
            //trM2.Style.Add("display", "none");
            trM3.Style.Add("display", "none");
            trM5.Style.Add("display", "none");
            trEx.Style.Remove("display");
        }

        private void DisplayNewFields()
        {
            //trM1.Style.Remove("display");
            //trM2.Style.Remove("display");
            trM3.Style.Remove("display");
            trM5.Style.Remove("display");
            trEx.Style.Add("display", "none");
        }

        private Info CreateMemberWithMTF(Member currMember, Member enrolledMember, String enrolledUserID)
        {
            MemberRoles mr;
            Info info;
            if (rdoClient.Checked)
            {
                mr = MemberRoles.ClientAdmin;
            }
            else
            {
                mr = MemberRoles.Admin;
            }

            if (enrolledMember == null)
            {
                enrolledMember = new Member()
                {
                    OwnerID = enrolledUserID,
                    //ProjectID = 0,
                    InstanceID = _LocalInstanceID,
                    FirstName = "",
                    LastName = "",
                    NickName = txtTeamAlias.Text,
                    Title = txtTeamTitle.Text,
                    Email = txtTeamEmail.Text,
                    //Type = MemberRoles.SuperAdmin.ToString(),
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,


                };
                enrolledMember.Add();
            }

            //if (rdoClient.Checked)
            //{
            //    info = AssignAdmin(hdnMMSelFolderID.Value.ToInt32(), enrolledMember.MemberID, 5);
            //    AddOwnerToMember(currMember, enrolledMember, 4);
            //}
            //else
            //{
            info = AssignAdmin(hdnMMSelFolderID.Value.ToInt32(), enrolledMember.MemberID, 2);
            AddOwnerToMember(currMember, enrolledMember, 3);
            //}
            return info;
        }

        private void AddOwnerToMember(Member currMember, Member member, Int32 RoleID)
        {
            OwnerToMember otm = OwnerToMember.SingleOrDefault(u => u.Email.Contains(txtTeamEmail.Text) && u.InstanceID == this.InstanceID);
            if (otm == null)
            {
                otm = new OwnerToMember()
                {
                    OwnerMemberID = currMember.MemberID,
                    InstanceID = _LocalInstanceID,
                    Email = txtTeamEmail.Text,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    MemberID = member.MemberID,
                    NameAlias = member.FirstName,
                    RoleID = RoleID
                };
                otm.Add();
            }
        }
        protected void rptClientMembers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                RemoveAdmin(hdnMMSelFolderID.Value.ToInt32(), e.CommandArgument.ToInt32(), 5);
                BindClients(hdnMMSelFolderID.Value.ToInt32());
                divManageMember.Style.Remove("display");
            }
        }

        public MemberRoles SelMemberRole
        {
            get
            {
                if (rdoClient.Checked)
                {
                    return MemberRoles.ClientMember;
                }
                else
                {
                    return MemberRoles.VendorMember;
                }

            }
        }

        protected void rptTeamMember_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                RemoveAdmin(hdnMMSelFolderID.Value.ToInt32(), e.CommandArgument.ToInt32(), 2);
                BindTeam(hdnMMSelFolderID.Value.ToInt32());
                divManageMember.Style.Remove("display");
            }
        }

        private void BindTeam(Int32 FolderID)
        {
            List<MemberPreview> members = new List<MemberPreview>();
            if (hdn_pfid.Value.ToInt32() == 0 && FolderID == 0)
            {
                rptTeamMember.DataSource = null;
                rptTeamMember.DataBind();
                trTeam.Visible = false;
                return;
            }
            else if (FolderID == 0)
            {
                List<MemberToFolder> parentMemberToFolders = MemberToFolder.Find(u => u.FolderID == hdn_pfid.Value.ToInt32() && u.RoleID == 2 && u.Active == true).ToList();
                foreach (MemberToFolder item in parentMemberToFolders)
                {
                    Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                    if (tempMem != null)
                    {
                        members.Add(new MemberPreview(tempMem, item.RoleID.ToInt32(), item.Alias, item.Title));
                    }
                }
                if (members.Count > 0)
                {
                    trTeam.Visible = true;
                }
                else
                {
                    trTeam.Visible = false;
                }
                rptTeamMember.DataSource = members;
                rptTeamMember.DataBind();
                return;
            }
            List<MemberToFolder> memberToFolders = MemberToFolder.Find(u => u.FolderID == FolderID && u.RoleID == 2 && u.Active == true).ToList();
            foreach (MemberToFolder item in memberToFolders)
            {
                Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMem != null)
                {
                    members.Add(new MemberPreview(tempMem, item.RoleID.ToInt32(), item.Alias, item.Title));
                }
            }
            if (members.Count > 0)
            {
                rptTeamMember.DataSource = members;
                rptTeamMember.DataBind();
                trTeam.Visible = true;
            }
            else
            {
                trTeam.Visible = false;
            }
        }

        private void BindClients(Int32 FolderID)
        {
            List<MemberPreview> members = new List<MemberPreview>();
            if (hdn_pfid.Value.ToInt32() == 0 && FolderID == 0)
            {
                rptTeamMember.DataSource = null;
                rptTeamMember.DataBind();
                trClient.Visible = false;
                return;
            }
            else if (FolderID == 0)
            {
                List<MemberToFolder> parentMemberToFolders = MemberToFolder.Find(u => u.FolderID == hdn_pfid.Value.ToInt32() && u.RoleID == 5 && u.Active == true).ToList();
                foreach (MemberToFolder item in parentMemberToFolders)
                {
                    Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                    if (tempMem != null)
                    {
                        members.Add(new MemberPreview(tempMem, item.RoleID.ToInt32(), item.Alias, item.Title));
                    }
                }
                if (members.Count > 0)
                {
                    trClient.Visible = true;
                }
                else
                {
                    trClient.Visible = false;
                }
                rptTeamMember.DataSource = members;
                rptTeamMember.DataBind();

                return;
            }
            List<MemberToFolder> memberToFolders = MemberToFolder.Find(u => u.FolderID == FolderID && u.RoleID == 5 && u.Active == true).ToList();
            foreach (MemberToFolder item in memberToFolders)
            {
                Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMem != null)
                {
                    members.Add(new MemberPreview(tempMem, item.RoleID.ToInt32(), item.Alias, item.Title));
                }
            }
            if (members.Count > 0)
            {
                rptClientMembers.DataSource = members;
                rptClientMembers.DataBind();
                trClient.Visible = true;
            }
            else
            {
                trClient.Visible = false;
            }
        }

        protected void btnAddEx_Click(object sender, EventArgs e)
        {
            Member member;
            //if (rdoClient.Checked) {
            //    member = Member.SingleOrDefault(u => u.MemberID == ddlClientMembers.SelectedValue.ToInt32());
            //}
            //else {
            member = Member.SingleOrDefault(u => u.MemberID == ddlTeamMembers.SelectedValue.ToInt32());
            //}

            if (member != null)
            {
                //txtTeamFullName.Text = "";
                txtTeamEmail.Text = member.Email;
                if (String.IsNullOrEmpty(txtTeamAlias.Text)) { txtTeamAlias.Text = member.NickName; }

                if (String.IsNullOrEmpty(txtTeamAlias.Text)) { txtTeamAlias.Text = member.Title; }

                hdnUserMemberID.Value = "0";
                btnAddMember.Text = "Add Member";
                txtTeamAlias.Focus();
                btnAddMember_Click(sender, e);
                SetDisplayNone();
            }
            else
            {
                lblExInfo.Text = "No member found";
                lblExInfo.ForeColor = System.Drawing.Color.Red;
                SetDisplayNone();
            }
        }

        protected void btnSaveMemberInfo_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            if (member != null)
            {
                member.FirstName = txtMName.Text;
                member.NickName = txtMAlias.Text;
                member.Title = txtMTitle.Text;
                member.Update();
                hdnAskInfo.Value = "0";
            }
        }

        protected void ddlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtDiscussions = CCMDiscussion.GetDiscussions(_LocalInstanceID, hdnMemberID.Value.ToInt32(), hdn_pfid.Value.ToInt32());
            if (ddlList.SelectedValue.ToString().ToLower() == "open")
            {
                DataView view = dtDiscussions.AsDataView();
                view.RowFilter = "Status='Open'";
                rptDiscussion.DataSource = view;
                rptDiscussion.DataBind();
            }
            else if (ddlList.SelectedValue.ToString().ToLower() == "close")
            {
                DataView view = dtDiscussions.AsDataView();
                view.RowFilter = "Status='Close'";
                rptDiscussion.DataSource = view;
                rptDiscussion.DataBind();
            }
            else {
                rptDiscussion.DataSource = dtDiscussions;
                rptDiscussion.DataBind();
            }
        }
    }
}