﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageDiscussions.aspx.cs" Inherits="CCM.ManageDiscussions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Team Communication Application</title>
    <script src="_assests/jQuit/js/jquery-1.10.2.js"></script>
    <script src="_assests/jQuit/js/jquery-ui.js"></script>
    <link href="_assests/jQuit/css/jquery-ui.css" rel="stylesheet" />
    <%--<script src="_assests/jQuit/js/jquery-ui-1.8.16.custom.min.js"></script>--%>
    <link href="_assests/Styles/style.css" rel="stylesheet" />
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
    <script>
        $(function () {
            $('.aStartDiscussion').click(function () {
                window.location.reload(true);
            });
            var clearLblInterval = setInterval(function () {
                //console.log("chaling");
                setTimeout(function () {
                    $('.lblExInfo').delay(2000).text('');
                    if ($('.lblExInfo').text() == '') {
                        clearInterval(clearLblInterval);
                    }
                    //console.log("visible chaling");
                }, 10000);
            }, 100);


            $('.lnkNewFolder, .lnkCreateFolder').click(function () {

                $('.divAddDiscussion:visible').hide('blind', 400);
                $('.divManageMember:visible').hide('blind', 400);
                $('.divAddFolder:hidden').show('blind', 400, function () {
                    resizeIframe();
                    $('.txtFolderTitle').focus();
                });
                $('.btnAddFolder').val('Add');
                $('#hdnSelFolderID').val('0');
                return false;
            });
            $('.lnkCreateStartDiscussion').click(function () {
                $('#hdnSelDisFolderID').val('0');
                $('.divAddFolder:visible').hide('blind', 400);
                $('.divManageMember:visible').hide('blind', 400);
                $('.divAddDiscussion:hidden').show('blind', 400, function () {
                    resizeIframe();
                    $('.txtDisTitle').focus();
                });
                $('.btnAddDiscussion').val('Add and Start');
                return false;
            })
            $('.btnCancelFolder').click(function () {
                $('.divAddFolder:visible').hide('blind', 400);
                return false;
            });
            $('.btnCancelDiscussion').click(function () {
                $('.divAddDiscussion:visible').hide('blind', 400);
                return false;
            });
            $('.lnkEdit').click(function () {
                $('.divManageMember').hide('blind', 400);
                var folderid = $(this).parent().parent().parent().attr('data-id');
                var title = $(this).parent().parent().parent().find('.txtTitle').text();
                var description = $(this).parent().parent().parent().find('.txtDescription').text();
                $('#hdnSelFolderID').val(folderid);
                $('.txtFolderTitle').val(title);
                $('.txtFolderDescription').val(description);
                $('.btnAddFolder').val('Update');
                $('.divAddFolder:hidden').show('blind', 400, function () {
                    resizeIframe();
                    $('.txtFolderTitle').focus();
                });
                return false;
            });
            $('.lnkMEdit').click(function () {
                if (!$('.chkAddNew input').is(':checked')) {
                    $('.chkAddNew input').click();
                    $('.chkAddNew input').prop('checked', 'true');
                }
                var memberID = $(this).parent().parent().attr('data-id');
                var name = $(this).parent().parent().find('.txtFullName').text();
                var alias = $(this).parent().parent().find('.txtAlias').text();
                var email = $(this).parent().parent().find('.txtEmail').text();
                var memberas = $(this).parent().parent().attr('data-as');
                var roleID = $(this).parent().parent().find('.lblRoleID').text();
                var title = $(this).parent().parent().find('.txtTitle').text();
                roleID = parseInt(roleID);
                if (roleID == 2) {
                    $("input#rdoTeam").prop("checked", true)
                    $("input#chkAdmin").prop("checked", true)
                }
                if (roleID == 3) {
                    $("input#rdoTeam").prop("checked", true)
                    $("input#chkAdmin").prop("checked", false)
                }
                if (roleID == 4) {
                    $("input#rdoClient").prop("checked", true)
                    $("input#chkAdmin").prop("checked", false)
                }
                if (roleID == 5) {
                    $("input#rdoClient").prop("checked", true)
                    $("input#chkAdmin").prop("checked", true)
                }

                //$('.txtTeamFullName').val(name);
                $('.txtTeamAlias').val(alias);
                $('.txtTeamEmail').val(email);
                $('.txtTeamTitle').val(title);
                $('#hdnUserMemberID').val(memberID);
                $('.btnAddMember').val("Update Member");
                $('.txtTeamAlias').focus();
                return false;
            });
            function setCheck() {
                if ($("input#rdoTeam").is(":checked")) {
                    $('.chkAdmin').show();
                    $('.ddlClientMembers').hide();
                    $('.ddlTeamMembers').show();
                }
                else {
                    $('.chkAdmin').hide();
                    $('.chkAdmin input').prop('checked', false)
                    $('.ddlClientMembers').hide();
                    $('.ddlTeamMembers').show();
                }
            }
            $("input#rdoTeam, input#rdoClient").change(function () {
                setCheck();
            });
            setCheck();
            //$('.lnkAddDiscussion').click(function () {
            //    $('.lnkCreateDiscussion').click();
            //    var folderid = $(this).parent().parent().attr('data-id');
            //    $('#hdnSelDisFolderID').val(folderid);
            //    $('.txtDisTitle').focus();
            //    return false;
            //})
            $('.lnkDelete').click(function () {
                if ($('.chkFolder input:checked').size() == 0) {
                    alert('No items selected');
                }
                else {
                    ConfirmDelete('.divDialog', 'Delete?', "Are you sure you want to delete selected items?", clickDelete);
                }
                return false;
            });
            $('.lnkMove').click(function () {
                if ($('.chkFolder input:checked').size() == 0) {
                    alert('No items selected');
                    return false;
                }
                OpenSimpleDialog($('.divFolderTree'), 'Move to', 'auto', moveToFolder);

                return false;
            });
            function moveToFolder() {
                $('#hdnSelMoveFolderID').val($('input[name="rdoC"]:checked').val());
                $('.btnMoveToFolder').click();
            }

            //$('input[name="rdoC"]').click(function () { })

            function clickDelete() {
                $('.btnDelete').click();
            }
            $('.chkAddNew input').change(function () {
                if ($(this).is(':checked')) {
                    $('.trEx').hide(200);
                    $('.trNew').show(200);
                    $('.txtTeamFullName').focus();
                    $('.txtTeamFullName').val('');
                    $('.txtTeamAlias').val('');
                    $('.txtTeamEmail').val('');
                    $('#hdnUserMemberID').val('0');
                    $('.btnAddMember').val("Add Member");
                    $('.txtTeamAlias').focus();
                    resizeIframe();
                }
                else {
                    $('.trEx').show(200);
                    $('.trNew').hide(200);
                }
                return false;
            });
            $('.btnCancelMember').click(function () {
                $('.divManageMember').hide('blind', 400);
                return false;
            });

            if ($('.sectionDashboard').size() > 0) {
                $('.divCommands').after($('.divAddFolder'));
                $('.divCommands').after($('<br />'));
                $('.divCommands').after($('.divAddDiscussion'));
            }
            resizeIframe();
            $('.chkAll input').change(function () {
                $('.chkFolder input').prop('checked', $(this).is(':checked'));
            })
            if ($('#hdnAskInfo').val() == "1") {
                OpenSaveDialog('.divAskInfo', 'Member Info', 'auto', function () {
                    $('.btnSaveMemberInfo').click();
                });
            }
            $('.lnkEditProfile').click(function () {
                OpenSaveDialog('.divAskInfo', 'Member Info', 'auto', function () {
                    $('.btnSaveMemberInfo').click();
                });
                return false;
            })
        });
    </script>
    <style>
        .tblForm tr td:nth-child(2n + 1) {
            /*padding: 6px 5px 6px 50px;*/
            /*text-align: right;*/
        }

        .tblForm tr td:nth-child(n+1) {
            /*height: 24px;*/
        }

        input[type="submit"] {
            padding: 3px 5px;
        }

        .ui-dialog {
            z-index: 2 !important;
        }

        .hide {
            display: none;
        }

        .ddlDisProjects {
        }
    </style>
    <link href="_assests/Styles/ccm_style.css" rel="stylesheet" />
</head>
<body id="mainBody">
    <form id="form1" runat="server">
        <div id="divAddFolder" class="divAddFolder divDataForm" runat="server" style="margin-left: 4px;">
            <h3 style="border-bottom: 1px solid #ccc; margin-left: 40px;">Add Folder
            </h3>
            <asp:ImageButton ID="btnCancelFolder1" CssClass="btnCancelFolder" runat="server" ImageUrl="_assests/Images/16 x 16/Untick.png" Style="border-width: 0px; width: 20px; float: right; margin-top: -28px;" />

            <br />
            <table class="tblForm">
                <tr>
                    <td class="captiontd">Title
                    </td>
                    <td>
                        <asp:TextBox ID="txtFolderTitle" CssClass="txtFolderTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="captiontd">Description
                    </td>
                    <td>
                        <asp:TextBox ID="txtFolderDescription" CssClass="txtFolderDescription" runat="server" TextMode="MultiLine" Height="46px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnAddFolder" CssClass="btnAddFolder" runat="server" Text="Add" OnClick="btnAddFolder_Click" />

                        <asp:HiddenField ID="hdnSelFolderID" ClientIDMode="Static" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>

        <div id="divAddDiscussion" class="divDataForm divAddDiscussion" runat="server" style="margin-left: 4px;">
            <h3 id="h3" style="border-bottom: 1px solid #ccc">Start Discussion
            </h3>
            <br />
            <table class="tblForm">
                <tr>
                    <td class="captiontd">Title
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisTitle" CssClass="txtDisTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnAddDiscussion" CssClass="btnAddDiscussion" runat="server" Text="Add and Start" OnClick="btnAddDiscussion_Click" OnClientClick="aspnetForm.target ='_blank';" />
                        &nbsp;
                            <asp:Button ID="btnCancelDiscussion" CssClass="btnCancelDiscussion btnCancel" runat="server" Text="Cancel" />
                        <asp:HiddenField ID="hdnSelDisFolderID" ClientIDMode="Static" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </div>
        <section id="sectionDashboard" class="sectionDashboard" runat="server" visible="false" style="margin: 10px;">
            <div id="divCommands" class="divCommands" runat="server" style="width: 575px">
                <asp:LinkButton ID="lnkEditProfile" CssClass="lnkEditProfile" runat="server" Text="Update Profile" Style="float: right" Visible="false"></asp:LinkButton>
                <%--<asp:LinkButton ID="lnkManageProjects" CssClass="lnkManageProjects" runat="server" data-title="Manage Projects" Text="Manage Projects" OnClick="lnkManageProjects_Click"></asp:LinkButton>
                &nbsp;&nbsp;|&nbsp;&nbsp;--%>
                <div id="navAdmin" runat="server" style="display: inline">
                    <asp:LinkButton ID="lnkCreateDiscussion" CssClass="lnkCreateStartDiscussion" runat="server" Text="New Discussion" OnClick="lnkCreateDiscussion_Click"></asp:LinkButton>
                    <a href="#" id="aStartDiscussion" runat="server" target="_blank" class="aStartDiscussion" visible="false">New Discussion</a>
                    |
                    <asp:LinkButton ID="lnkNewFolder" CssClass="lnkNewFolder" OnClick="lnkNewFolder_Click" runat="server" data-title="Add Folder" Text="New Folder"></asp:LinkButton>
                    |
                </div>
                <asp:LinkButton ID="lnkMove" CssClass="lnkMove" runat="server" Text="Move"></asp:LinkButton>
                <asp:Label ID="lblDashSpace" runat="server" Text=" | "></asp:Label>
                <asp:LinkButton ID="lnkDelete" CssClass="lnkDelete" runat="server" Text="Delete"></asp:LinkButton>
               
                &nbsp;<asp:Button ID="btnDelete" runat="server" CssClass="btnDelete" Style="display: none" OnClick="btnDelete_Click" />
                <asp:Button ID="btnMoveToFolder" runat="server" CssClass="btnMoveToFolder" Style="display: none" OnClick="btnMoveToFolder_Click" />
                 <hr />
                Search by Status:
                <asp:DropDownList ID="ddlList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlList_SelectedIndexChanged">
                    <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                    <asp:ListItem Text="Close" Value="Close"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div id="divManageMember" class="divDataForm divManageMember" runat="server" style="margin-left: 4px;">
                <h3 id="hFolderName" runat="server" style="border-bottom: 1px solid #ccc">Manage Members</h3>
                <br />
                <table class="tblForm">
                    <tr id="trM4" runat="server">
                        <td class="formCaptionTd">Add as
                        </td>
                        <td style="font-size: 18px">
                            <asp:RadioButton ID="rdoClient" runat="server" Text="Client" GroupName="memberas" Checked="true" Style="float: left" />
                            &nbsp;&nbsp;&nbsp;
                                        <asp:RadioButton ID="rdoTeam" runat="server" Text="Team" GroupName="memberas" />
                        </td>
                    </tr>
                    <tr id="trM10" runat="server">
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkAddNew" CssClass="chkAddNew" runat="server" Text="Add New Member" Checked="true" />
                        </td>
                    </tr>
                    <tr class="trEx" id="trEx" runat="server" style="display: none">
                        <td class="formCaptionTd">Existing
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClientMembers" CssClass="ddlClientMembers" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlTeamMembers" CssClass="ddlTeamMembers" runat="server" Style="display: none">
                            </asp:DropDownList>
                            <asp:Button ID="btnAddEx" CssClass="btnAddEx" runat="server" Text="Select" OnClick="btnAddEx_Click" />
                            <asp:Button ID="btnCancelEx" CssClass="btnCancelMember" Text="Cancel" runat="server" />
                            <asp:Label ID="lblExInfo" EnableViewState="false" runat="server" CssClass="lblExInfo"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Alias
                        </td>
                        <td>
                            <asp:TextBox ID="txtTeamAlias" CssClass="txtTeamAlias" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamAlias" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Title
                        </td>
                        <td>
                            <asp:TextBox ID="txtTeamTitle" CssClass="txtTeamTitle" runat="server"></asp:TextBox>
                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamTitle" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                            <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnUserMemberID" Value="0" />
                        </td>
                    </tr>
                    <tr id="trM3" runat="server" class="trNew">
                        <td class="formCaptionTd">Email
                        </td>
                        <td>
                            <asp:TextBox ID="txtTeamEmail" CssClass="txtTeamEmail" runat="server" Width="200px"></asp:TextBox>
                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamEmail" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                            &nbsp;&nbsp;
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="MemberValidation" ControlToValidate="txtTeamEmail"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr id="trM5" runat="server" class="trNew">
                        <td></td>
                        <td>
                            <asp:Button ID="btnAddMember" CssClass="btnAddMember" Text="Assign admin" runat="server" OnClick="btnAddMember_Click" ValidationGroup="MemberValidation" />
                            <asp:Button ID="btnCancelMember" CssClass="btnCancelMember" Text="Cancel" runat="server" />
                            <asp:HiddenField ID="hdnMMSelFolderID" ClientIDMode="Static" runat="server" />
                            <asp:Label ID="lblMMInfo" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trClient" runat="server">
                        <td class="formCaptionTd">Clients</td>
                        <td>
                            <asp:Repeater ID="rptClientMembers" runat="server" OnItemCommand="rptClientMembers_ItemCommand">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td>Full Name
                                            </td>
                                            <td>Alias
                                            </td>
                                            <td>Title
                                            </td>
                                            <td>Email
                                            </td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="trTeamData" runat="server" data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-as="client">
                                        <td>
                                            <asp:Label ID="txtFullName" CssClass="txtFullName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtAlias" CssClass="txtAlias" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Alias")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtTitle" CssClass="txtTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Title")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtEmail" CssClass="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>'></asp:Label>
                                            <asp:Label Style="display: none" ID="lblRoleID" CssClass="lblRoleID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>'></asp:Label>
                                        </td>
                                        <%--<td>
                                            <asp:Label ID="Label6" CssClass="txtEmail" runat="server" ></asp:Label>
                                        </td>--%>
                                        <td>
                                            <asp:LinkButton ID="lnkMEdit" runat="server" CssClass="lnkMEdit">Edit</asp:LinkButton>
                                            <asp:LinkButton ID="lnkRemove" runat="server" CssClass="lnkRemove" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'>Remove</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr id="trTeam" runat="server">
                        <td class="formCaptionTd">Admins:
                        </td>
                        <td>
                            <asp:Repeater ID="rptTeamMember" runat="server" OnItemCommand="rptTeamMember_ItemCommand">
                                <HeaderTemplate>
                                    <table class="smallGrid">
                                        <tr class="smallGridHead">
                                            <td>Full Name
                                            </td>
                                            <td>Alias
                                            </td>
                                            <td>Title
                                            </td>
                                            <td>Email
                                            </td>
                                            <td></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="trTeamData" runat="server" data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-as="team">
                                        <td>
                                            <asp:Label ID="txtFullName" CssClass="txtFullName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtAlias" CssClass="txtAlias" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Alias")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtTitle" CssClass="txtTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Title")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="txtEmail" CssClass="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>'></asp:Label>
                                            <asp:Label Style="display: none" ID="lblRoleID" CssClass="lblRoleID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkMEdit" runat="server" CssClass="lnkMEdit">Edit</asp:LinkButton>
                                            <asp:LinkButton ID="lnkRemove" runat="server" CssClass="lnkRemove" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'>Remove</asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="divBreadCrumb" style="margin-top: -5px;">
                <asp:Repeater ID="rptBC" runat="server" OnItemCommand="rptBC_ItemCommand">
                    <HeaderTemplate>
                        <asp:LinkButton data-pid="0" ID="lnkRoot" CssClass="lnkBC" runat="server" Text="Root" CommandName="Navigate" CommandArgument="0"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        /
                            <asp:LinkButton CommandName="Navigate" CssClass="lnkBC" data-pid='<%# DataBinder.Eval(Container.DataItem, "FolderID") %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FolderID") %>' ID="lnkFolders" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <asp:Label ID="lblInfo" runat="server"></asp:Label>
            <div id="divFolders">
                <table class="smallGrid">
                    <tr class="smallGridHead">
                        <td>
                            <asp:CheckBox ID="chkAll" CssClass="chkAll" runat="server" />
                        </td>
                        <td></td>
                        <td>Title
                        </td>
                        <%--<td>Sub Folders
                        </td>
                        <td>Discussions
                        </td>--%>
                        <td>Created
                        </td>
                        <td>Modified
                        </td>
                        <%--<td>Status
                        </td>--%>
                        <td>Actions
                        </td>
                    </tr>
                    <asp:Repeater ID="rptFolders" runat="server" OnItemDataBound="rptFolders_ItemDataBound" OnItemCommand="rptFolders_ItemCommand">
                        <ItemTemplate>
                            <tr data-id='<%# DataBinder.Eval(Container.DataItem,"FolderID") %>' runat="server" id="trFolder" class="trFolder" data-folder="true">
                                <td>
                                    <asp:CheckBox ID="chkFolder" CssClass="chkFolder" runat="server" />
                                </td>
                                <td>
                                    <a class="folderLink"></a>
                                </td>
                                <td>
                                    <asp:LinkButton ID="txtTitle" CssClass="txtTitle" runat="server" data-roleid='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem,"FolderID") %>' Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>' ToolTip='<%# DataBinder.Eval(Container.DataItem,"Description") %>' CommandName="Open"></asp:LinkButton>
                                    &nbsp;(<asp:Label ID="txtSubFolders" runat="server"></asp:Label>
                                    folders,
                                    <asp:Label ID="txtDiscussions" CssClass="txtDiscussions" runat="server"></asp:Label>
                                    discussions)
                                    <asp:Label ID="txtDescription" Style="display: none" CssClass="txtDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Description") %>'></asp:Label>
                                    <a href="javascript:void(0)" runat="server" id="aDisTitle" class="aDisTitle " target="_blank" style="float: right; display: none">
                                        <img src="_assests/Images/16%20x%2016/Add-Discussion.png" />
                                    </a>
                                    <asp:LinkButton ID="lnkAddDiscussion" Style="float: right" CssClass="lnkAddDiscussion" runat="server" Visible="false" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"FolderID") %>' Text='' CommandName="Add Discussion"></asp:LinkButton>
                                </td>
                                <%--<td>
                                    
                                </td>
                                <td>
                                    <asp:Label ID="txtDiscussions" runat="server"></asp:Label>
                                </td>--%>
                                <td>
                                    <asp:Label ID="txtCrtDate" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"CrtDate")).ToShortDateString() %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="txtModDate" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"ModDate")).ToShortDateString() %>'></asp:Label>
                                </td>
                                <%--<td>
                                    <asp:Label ID="txtStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Status") %>'></asp:Label>
                                </td>--%>
                                <td>
                                    <div id="divAdmin" runat="server" style="display: inline">
                                        <asp:LinkButton ID="lnkEdit" CssClass="lnkEdit" runat="server" Text='Edit' CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"FolderID") %>'></asp:LinkButton>
                                        -
                                    <asp:LinkButton ID="lnkManageMember" CssClass="lnkManageMember" runat="server" Text='Manage Admin' CommandName="ManageAdmin" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"FolderID") %>'></asp:LinkButton>
                                    </div>

                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptDiscussion" runat="server" OnItemCommand="rptDiscussion_ItemCommand" OnItemDataBound="rptDiscussion_ItemDataBound">
                        <ItemTemplate>
                            <tr data-id='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' runat="server" id="trDiscussion" class="trDiscussion" data-folder="false">
                                <td>
                                    <asp:CheckBox ID="chkFolder" CssClass="chkFolder" runat="server" />
                                </td>
                                <td>
                                    <img src="_assests/Images/Conversation.png" style="width: 14px" />
                                </td>
                                <td>
                                    <a href="javascript:void(0)" runat="server" id="aDisTitle" class="aDisTitle" target="_blank"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                    <asp:Label runat="server" ID="lblCount" CssClass="lblCount"></asp:Label>
                                    <%--<asp:LinkButton Visible="false" ID="txtTitle" CssClass="txtTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>' CommandName="Open" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") + "|" +  DataBinder.Eval(Container.DataItem,"FolderID") + "|" +  DataBinder.Eval(Container.DataItem,"ProjectID")%>'></asp:LinkButton>--%>
                                </td>
                                <td>
                                    <asp:Label ID="txtCrtDate" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"CrtDate")).ToShortDateString() %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="txtModDate" runat="server" Text='<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem,"ModDate")).ToShortDateString() %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="txtStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Status") %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
                <div class="ui-widget inform hide">
                    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px;">
                        <p>
                            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            <span id="iText">Sample ui-state-highlight style.</span>
                        </p>
                    </div>
                </div>
                <div class="ui-widget error hide">
                    <div class="ui-state-error ui-corner-all">
                        <p>
                            <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            <span id="Span1">Sample ui-state-highlight style.</span>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <div id="divDialog" class="divDialog" title="">
            <p id="paraDialog">
            </p>
            <a href="#" runat="server" id="aCloseDialog"></a>
        </div>
        <section id="sectionWelcome" class="sectionWelcome" title="Welcome!" runat="server" style="margin-left: 4px;" visible="false">
            <div class="breadCrumb" style="margin-left: 4px;">
                Welcome to Team Communication Application
            </div>
            <table class="" id="tblproducts" cellpadding="0" cellspacing="2" style="margin-left: 10px;">
                <tbody>
                    <tr id="MainContent_tr_dmenu">
                        <td colspan="9">
                            <div>
                                <p>
                                    Start by Creating a discussion. You can create as many discussions and manage them into folders.
                                </p>
                                <ul style="margin-left: 18px; padding: 11px">
                                    <li>
                                        <asp:LinkButton ID="lnkCreateStartDiscussion" CssClass="lnkCreateStartDiscussion" Text="Create a discussion" runat="server" OnClick="lnkNewFolder_Click"></asp:LinkButton>
                                    </li>
                                    <li style="display: none">
                                        <a href="#" id="aStartDiscussion2" class="aStartDiscussion" runat="server" target="_blank">Start Discussion</a>
                                    </li>
                                </ul>
                                <p>
                                    For learning more about CCM please go to <a href="AppInstructions.aspx">Help</a>.
                                </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
        <div id="divFolderTree" class="divFolderTree hide">
            <asp:TreeView ID="tvFolder" runat="server">
            </asp:TreeView>
        </div>
        <div id="divAskInfo" class="divAskInfo" style="display: none">
            <table class="tblForm">
                <tr>
                    <td class="formCaptionTd">Name: 
                    </td>
                    <td>
                        <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Alias:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMAlias" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Title:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="display: none"></td>
                </tr>
            </table>
        </div>
        <asp:Button ID="btnSaveMemberInfo" CssClass="btnSaveMemberInfo" runat="server" OnClick="btnSaveMemberInfo_Click" Style="display: none" />
        <asp:HiddenField ID="hdn_pfid" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdn_folderid" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdn_oID" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnMemberID" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnInstanceID" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnSelMoveFolderID" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnAskInfo" runat="server" Value="0" ClientIDMode="Static" />
    </form>
</body>
</html>

