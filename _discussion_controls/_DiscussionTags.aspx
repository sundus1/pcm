﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_DiscussionTags.aspx.cs" Inherits="CCM._discussion_controls._DiscussionTags" %>

<%@ OutputCache Duration="2700" VaryByParam="did;T" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="discussiontags">
                <h4><i class="fa fa-tag"></i>&nbsp;Tags</h4>
                <%--<asp:TextBox runat="server" ID="txtFilterTags" CssClass="txtFilterTags"></asp:TextBox>--%>
                <div id="divTags" runat="server">
                    <asp:Repeater runat="server" ID="rptTags">
                        <HeaderTemplate>
                            <ul class="tagsul">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <a href="javascript:void(0)" id="aTag" class="aTag" data-tag='<%# Container.DataItem.ToString() %>' runat="server"><%# Container.DataItem.ToString() %></a>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <asp:Label ID="lblTags" runat="server" Text="No tags found..." Style="margin-left: 30px"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
