﻿using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM._discussion_controls
{
    public partial class _DiscussionTags : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["did"] != null)
            {
                CCM.App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                rptTags.DataSource = md.GetTags(Request.QueryString["did"].ToString()).Split(',').ToList();
                rptTags.DataBind();
                if (md.GetTags(Request.QueryString["did"].ToString()).Split(',').ToList().Count > 0)
                {
                    if (String.IsNullOrEmpty(md.GetTags(Request.QueryString["did"].ToString()).Split(',')[0]) && md.GetTags(Request.QueryString["did"].ToString()).Split(',').Count() == 1)
                    { divTags.Visible = false; lblTags.Visible = true; }
                    else
                    { divTags.Visible = true; lblTags.Visible = false; }
                }
                else
                { divTags.Visible = false; lblTags.Visible = true; }
            }
        }
    }
}