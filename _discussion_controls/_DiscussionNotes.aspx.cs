﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class _DiscussionNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["did"] != null)
                {
                    BindRepeater();
                }
            }
        }

        public void BindRepeater()
        {
            CCM.App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            List<MemberNote> datasource = md.GetNotes(Request.QueryString["did"].ToString());

            //Response.Write(Request.QueryString["did"].ToString() + "-------" + datasource.Count);
            //Response.End();

            rptNotes.DataSource = MemberNote.Find(u => u.DiscussionID == (Request.QueryString["did"].ToString()).ToInt32() && u.Active == true).ToList().OrderBy(u => u.CrtDate).ToList();
            rptNotes.DataBind();

            // if (md.GetNotes(Request.QueryString["did"].ToString()).Count > 0)
            if (datasource.Count > 0)
            {
                divNotes.Visible = true; lblNotes.Visible = false;
            }
            else
            { divNotes.Visible = false; lblNotes.Visible = true; }
        }

        public string MemberName(string id, string discussionid)
        {
            DiscussionToFolder dtf = DiscussionToFolder.Find(u => u.MemberID.ToString() == id && u.DiscussionID == discussionid.ToInt32()).FirstOrDefault();
            if (dtf != null)
                return dtf.Alias;
            return "";
        }

        public int MemberRoleId(string discussionid, string memberid)
        {
            int mrole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), memberid.ToInt32());
            return mrole;
        }
        
    }
}