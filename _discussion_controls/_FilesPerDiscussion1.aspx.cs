﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;


namespace CCM._discussion_controls
{
    public partial class _FilesPerDiscussion1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["disid"] != null)
            {
                List<DiscussionFile> files = DiscussionFile.Find(u => u.DiscussionID == Convert.ToInt32(Request.QueryString["disid"])).OrderByDescending(u => u.ModDate).ToList();
                if (files.Count > 0)
                {
                    rptFiles.Visible = true;
                    //aInfo.Visible = false;
                    lblFiles.Text = files.Count.ToString();
                    rptFiles.DataSource = files;
                    rptFiles.DataBind();
                }
                else
                {
                    lblFiles.Text = "0";
                    rptFiles.Visible = false;
                    //aInfo.Visible = true;
                }

            }
        }

        public String GetFileName(String FileName)
        {
            //string FileName = "This on.e has some bog file name.png";
            string name = "";
            string firstLetters = "";
            string secondLetters = "";
            string[] _fileName = FileName.Split('.');           
            name = FileName.Substring(0, FileName.LastIndexOf('.')); ;
            Console.WriteLine(name);
            if (name.Length > 17)
            {
                firstLetters = name.Substring(0, 11);
                Console.WriteLine(firstLetters);
                Console.WriteLine(name.Length);
                Console.WriteLine(name.Length - 4);
                secondLetters = name.Substring(name.Length - 3, 3);
                Console.WriteLine(secondLetters);
                return firstLetters + "..." + secondLetters + FileName.Substring(FileName.LastIndexOf('.'), _fileName[_fileName.Length - 1].Length + 1);

            }
            else
            {
                return FileName;
            }
        }

        public string GetDateTime(string datetime)
        {
            return Utilities.GetUserDateTime(datetime, "test-123");
        }

        protected void rptFiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlAnchor anchor = e.Item.FindControl("lnkFileName") as HtmlAnchor;
                string FileName = anchor.Attributes["alt"];
                string _filename = Path.GetExtension(FileName);

                if (_filename == ".pdf")
                {
                    anchor.Attributes.Add("download", FileName);
                }
            }

        }
    }
}