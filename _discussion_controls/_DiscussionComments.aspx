﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_DiscussionComments.aspx.cs" Inherits="CCM._discussion_controls._DiscussionComments" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divOldDiscussions" class="divOldDiscussions">
            <div class="load-cont" id="divLoadMore" runat="server">

                <h4 class="loadMsg loadmsg" id="h4loadMsg" runat="server">
                    <a style="color: #800000" href="#">
                        <i class="fa fa-reply-all"></i>&nbsp;&nbsp;
                        <asp:Label Text="Load previous 5 messages" runat="server" ID="lblLoadPreviousCount" CssClass="lblLoadPreviousCount"></asp:Label>
                    </a>
                </h4>
                <h4 class="loadMsg" id="h4Refresh" runat="server" style="display: none">
                    <a style="color: #800000" href="#">
                        <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                        <asp:Label Text="Refresh" runat="server" ID="lblRefresh" CssClass="lblRefresh"></asp:Label>
                    </a>
                </h4>
                <div class="clearfix"></div>
            </div>
            <asp:Repeater ID="rptDiscussions" runat="server" OnItemDataBound="rptDiscussions_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="chatGrid divMD" data-team='<%# DataBinder.Eval(Container.DataItem,"TeamMember") %>' data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' id="divCom" runat="server">
                        <div class="image">
                            <img class="imgMember" id="imgMember" runat="server" src="" style="width: 60px; height: 60px; margin-top: 0px; margin-left: -4px;" />
                            <%--<img src="_discussionsa_assets/img/user.png">--%>
                        </div>
                        <div class="text">
                            <h4 class="client-text" runat="server" id="h4Name">
                                <asp:Label ID="lblMemberTemp" CssClass="lblMember" runat="server" Text=""></asp:Label></h4>
                            <ul class="nav nav-pills cmntMenudrop" id="ulManageCommment" runat="server">
                                <li role="presentation" class="dropdown"><a class="dropdown-toggle cmntmenubtn" data-toggle="dropdown" href="#" role="button" aria-expanded="false"><span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="javascript:void(0)" class="lnkEditComment" runat="server" id="lnkEditComment" data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' data-messageid="" title="Edit" visible="false"
                                                data-toggle="modal" data-target="#editcomment"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</a></li>
                                        <li>
                                            <a href="javascript:void(0)" class="lnkDeleteComment" runat="server" id="lnkDeleteComment" data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' title="Delete" visible="false"
                                                data-toggle="modal" data-target="#removecomment"><i class="fa fa-times"></i>&nbsp;Delete</a></li>
                                        <li>
                                            <a href="javascript:void(0)" class="lnkAddTag" runat="server" id="lnkAddTag" data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' title="Add tag"
                                                data-toggle="modal" data-target="#addtag"><i class="fa fa-tag"></i>&nbsp;Add tag</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>                            
                            <asp:Label runat="server" ID="lblMID" CssClass="lblMID" ToolTip="Message ID">MID: 10</asp:Label>
                            <asp:Label runat="server" ID="lblDateTime" CssClass="date lblDateTime" Style="margin-right: 15px">Today, 12:54 PM</asp:Label>
                            <div class="clearfix"></div>
                            <div id="divMemDisO" runat="server" data-tags='<%# DataBinder.Eval(Container.DataItem,"Tags") %>'>
                            </div>
                            <asp:Repeater runat="server" ID="rptTags">
                                <HeaderTemplate>
                                    <ul class="tagsul tagsulfr">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <a href="javascript:void(0)" id="aTag" class="aTag" data-tag='<%# Container.DataItem.ToString() %>' runat="server"><%# Container.DataItem.ToString() %></a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%--<textarea id="txtTags" class="txtTags" runat="server"><%# DataBinder.Eval(Container.DataItem,"Tags") %></textarea>--%>
                            <div class="clearfix"></div>
                            <asp:Repeater ID="rptFiles" runat="server" OnItemDataBound="rptFiles_ItemDataBound">
                                <HeaderTemplate>
                                    <ul style="padding: 0px; margin: 0px;" class="CommentFiles">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li class="msgAttachment">
                                        <i class="fa fa-link"></i>&nbsp;&nbsp; 
                                        <a target="_blank" class="anchorFileName" href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="lnkFileName" runat="server" alt='<%# GetFileName(DataBinder.Eval(Container.DataItem,"FileName").ToString()) %>'><%# GetFileName(DataBinder.Eval(Container.DataItem,"FileName").ToString()) %></a>&nbsp;&nbsp; 
                                        <i style="color: #063 !important" class=""></i>
                                        <a class="fa fa-download" target="_blank" style="color: #063 !important" download='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>' href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="A1" runat="server"></a>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            </ul>
                           
                            <asp:Label ID="lblHoursMinutes" Text="0 Hours 0 Minutes" class="EditHours" runat="server"></asp:Label>
                            <asp:HiddenField ID="HdnSummary" Value="" runat="server" />
                            <asp:HiddenField ID="hdnIsSubtracted" Value='<%# DataBinder.Eval(Container.DataItem, "SubtractedHours") %>' runat="server"/>
                            <asp:HiddenField ID="hdnSubtractHrsReason" Value='<%# DataBinder.Eval(Container.DataItem, "SubtractHrsReason") %>' runat="server"/>
                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
            <asp:HiddenField runat="server" ID="hdnTotalDisComments" Value="0" />
            <asp:HiddenField runat="server" ID="totalcomments" Value="0" />


        </div>
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />

    </form>
</body>
</html>
