﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_DiscussionReminder.aspx.cs" Inherits="CCM._discussion_controls._DiscussionReminder" validateRequest="false" enableEventValidation="false" viewStateEncryptionMode ="Never" EnableViewState="false" EnableViewStateMAC="false" %>
<%@ OutputCache Duration="2700" VaryByParam="did;InstanceID;md"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <div id="discussionreminders">
                <h4><i class="fa fa-calendar"></i>&nbsp;Reminders&nbsp;&nbsp;
                    <a id="aAddReminder" class="aAddReminder" target="_blank" title="Automated Work/Task Assignment." runat="server" ><i style="color: #B90000" class="fa fa-plus-circle"></i></a>
                </h4>
                <div id="divReminders" runat="server"> 
                    <a id="lnkReminder" target="_blank" title="Automated Work/Task Assignment." runat="server" ></a>
                </div>
                <asp:Label ID="lblReminders" runat="server" Text="No reminders available.." Style="margin-left: 30px"></asp:Label>
               <%-- <asp:HiddenField  runat="server" ID="hdnInstanceID" Value="0"/>
                <asp:HiddenField  runat="server" ID="hdnMemberID" Value="0"/>
                <asp:HiddenField  runat="server" ID="hdnDisID" Value="0"/>--%>
            </div>
    </form>
</body>
</html>
