﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using Microsoft.Win32;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using MyCCM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM._discussion_controls
{
    public partial class _DiscussionReminder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (!IsPostBack)
            {                
                aAddReminder.HRef = "ManageReminders.aspx?MemberId=" +  Request.QueryString["md"].Encrypt().ToString() + "&InstanceID=" + Request.QueryString["InstanceID"].ToString() + "&DiscussionID=" + Request.QueryString["did"].ToString().Encrypt();
                GetReminders();
            }
        }

        public void GetReminders()
        {
            List<DiscussionReminder> reminders = DiscussionReminder.Find(u => u.InstanceID == Request.QueryString["InstanceID"].ToString() && u.DiscussionID == Request.QueryString["did"].ToString().ToInt32()).ToList();
            if (reminders.Count > 0)
            {
                lnkReminder.InnerText = "Reminders Available (" + reminders.Count.ToString() + ")";
                lnkReminder.HRef = "ManageReminders.aspx?MemberId=" +  Request.QueryString["md"].Encrypt().ToString() + "&InstanceID=" + Request.QueryString["InstanceID"].ToString() + "&DiscussionID=" + Request.QueryString["did"].ToString().Encrypt();
                divReminders.Visible = true;
                lblReminders.Visible = false;
            }
            else
            {
                lblReminders.Visible = true;
                divReminders.Visible = false;
            }
        }
    }
}