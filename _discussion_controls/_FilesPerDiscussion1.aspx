﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_FilesPerDiscussion1.aspx.cs" Inherits="CCM._discussion_controls._FilesPerDiscussion1" %>

<%@ OutputCache Duration="2700" VaryByParam="disid;C" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Files </title>
    <link href="_assests/jQuit/css/jquery-ui.css" rel="stylesheet" />
    <script src="_assests/jQuit/js/jquery-1.6.2.min.js"></script>
    <script src="_assests/jQuit/js/jquery-ui-1.8.16.custom.min.js"></script>
    <link href="_assests/Styles/style.css" rel="stylesheet" />
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divDisFiles" class="divDisFiles">
            <h4><i class="fa fa-files-o"></i>&nbsp;&nbsp;All Files  (<asp:Label ID="lblFiles" runat="server" Text="0"></asp:Label>)</h4>
            <asp:Repeater ID="rptFiles" runat="server" OnItemDataBound="rptFiles_ItemDataBound">
                <HeaderTemplate>
                    <ul class="TeamMembers dis_files">
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;
                        <a target="_blank" href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="lnkFileName" runat="server" alt='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>' title='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() + " uploaded on " + GetDateTime(DataBinder.Eval(Container.DataItem,"ModDate").ToString()) %>'><%# GetFileName(DataBinder.Eval(Container.DataItem,"FileName").ToString()) %></a>
                        &nbsp;&nbsp;&nbsp;
                        <a target="_blank" alt='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>' title='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() + " uploaded on " + GetDateTime(DataBinder.Eval(Container.DataItem,"ModDate").ToString()) %>' download='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>' href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="A2" class="fa fa-download" runat="server"></a>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    <li class="loadmorefiles"></li>
                    <li class="loadmorefiles">
                        <i class="fa fa-reply-all"></i>&nbsp;&nbsp;&nbsp;
                        <a href="#" id="hrefLoadMpreFiles" style="color: #800000">Load All</a>
                    </li>
                    </ul>                        
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <%--<a href="javascript:void(0)" style="color: red" runat="server" id="aInfo">No Files Found</a>--%>
        </div>
    </form>
</body>
</html>
