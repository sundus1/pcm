﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_DiscussionNotes.aspx.cs" Inherits="CCM._DiscussionNotes" %>

<%@ OutputCache Duration="2700" VaryByParam="did;T" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="discussionnotes">
                <h4><i class="fa fa-list"></i>&nbsp;Notes&nbsp;&nbsp;
                    <a href="javascript:void(0)" id="aAddNotes" class="aAddNotes spanNote" data-target="#addnote"><i style="color: #B90000" class="fa fa-plus-circle"></i></a>
                </h4>
                <div id="divNotes" runat="server">
                    <asp:Repeater runat="server" ID="rptNotes">
                        <HeaderTemplate>
                            <ul class="notesul">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <a href="javascript:void(0)" onclick="return notesPopUp(this);" id="aNote" class="aNote" data-note='<%# DataBinder.Eval(Container.DataItem, "Notes")%>' data-noteid='<%# DataBinder.Eval(Container.DataItem, "NotesID")%>' data-memberid='<%# DataBinder.Eval(Container.DataItem, "MemberID")%>' data-roleid='<%# MemberRoleId(DataBinder.Eval(Container.DataItem,"DiscussionID").ToString(), DataBinder.Eval(Container.DataItem, "MemberID").ToString())%>' data-remind='<%# DataBinder.Eval(Container.DataItem, "Remind")%>' runat="server" data-target="#addnote">
                                    <div class="shortnote"><%# DataBinder.Eval(Container.DataItem, "Notes")%></div>
                                    <span class="notecreater">&nbsp;(<%# MemberName(DataBinder.Eval(Container.DataItem, "MemberID").ToString(),DataBinder.Eval(Container.DataItem,"DiscussionID").ToString())%>) </span>

                                </a>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <asp:Label ID="lblNotes" runat="server" Text="No notes available.." Style="margin-left: 30px"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
