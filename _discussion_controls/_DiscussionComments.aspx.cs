﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using System.Configuration;
using System.Web.Services;
using System.IO;

namespace CCM._discussion_controls
{
    public partial class _DiscussionComments : System.Web.UI.Page
    {
        public String GetFileName(String FileName)
        {
            //string FileName = "This on.e has some bog file name.png";
            string name = "";
            string firstLetters = "";
            string secondLetters = "";
            string[] _fileName = FileName.Split('.');
            name = FileName.Substring(0, FileName.LastIndexOf('.')); ;
            Console.WriteLine(name);
            if (name.Length > 17)
            {
                firstLetters = name.Substring(0, 11);
                Console.WriteLine(firstLetters);
                Console.WriteLine(name.Length);
                Console.WriteLine(name.Length - 4);
                secondLetters = name.Substring(name.Length - 3, 3);
                Console.WriteLine(secondLetters);
                return firstLetters + "..." + secondLetters + FileName.Substring(FileName.LastIndexOf('.'), _fileName[_fileName.Length - 1].Length + 1);
            }
            else
            {
                return FileName;
            }
        }
        bool isHourly = false;
        bool HideHours = false;
        int roleid;
        DataTable dt = new DataTable();
        DataTable dtAlias = new DataTable();
        List<DiscussionFile> disFiles;
        Dictionary<string, int> MID = new Dictionary<string, int>();

        protected void Page_Load(object sender, EventArgs e)
        {


            if (Request.QueryString["isHourly"].ToString() != null && Request.QueryString["isHourly"].ToString() != "")
            {
                isHourly = Convert.ToBoolean(Request.QueryString["isHourly"].ToString());
            }
            if (Request.QueryString["HideHours"].ToString() != null && Request.QueryString["HideHours"].ToString() != "")
            {
                HideHours = Convert.ToBoolean(Request.QueryString["HideHours"].ToString());
            }

            if (!IsPostBack)
            {

                if (Request.QueryString["iid"] != null)
                {
                    hdnInstanceID.Value = Request.QueryString["iid"].ToString();
                }
                if (Request.QueryString["RoleID"] != null)
                {
                    roleid = Request.QueryString["RoleID"].ToInt32();
                }
                if (Request.QueryString["mid"] != null)
                {
                    hdnMemberID.Value = Request.QueryString["mid"].ToString();
                }
                if (Request.QueryString["oid"] != null)
                {
                    hdnOID.Value = Request.QueryString["oid"].ToString();
                }
                if (Request.QueryString["did"] != null)
                {
                    DiscussionCommentsNumber(Request.QueryString["did"].ToString());
                    hdnDisID.Value = Request.QueryString["did"].ToString();
                    dt = CCMDiscussion.GetDiscussionMemberRoles(hdnDisID.Value.ToInt32());
                    dtAlias = CCMDiscussion.GetDiscussionMemberAlias(hdnDisID.Value.ToInt32(), hdnInstanceID.Value.ToString());
                    if (Request.QueryString["memDisID"] != null)
                    {
                        App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                        md.GetMemberDiscussionsByID(Request.QueryString["memDisID"].ToString());
                        if (Request.QueryString["FileUpdate"] == "1")
                        {
                            disFiles = DiscussionFile.Find(u => Request.QueryString["memDisID"].ToString().Contains(u.MemberDisID)).ToList();
                        }
                        else
                        { }
                        rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.CrtDate).ToList();
                        rptDiscussions.DataBind();
                        hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();
                        totalcomments.Value = Request.QueryString["totalcomments"].ToString();
                    }
                    else if (Request.QueryString["loadmorecount"] != null)
                    {
                        BindMemberDiscussions(Request.QueryString["did"].ToString(), Request.QueryString["loadmorecount"].ToString().ToInt32(), Request.QueryString["loadmoreqty"].ToString().ToInt32());
                        totalcomments.Value = Request.QueryString["totalcomments"].ToString();

                    }
                    else if (Request.QueryString["tag"] != null)
                    {
                        BindMemberDiscussionsByTag(Request.QueryString["did"].ToString(), Request.QueryString["tag"].ToString());
                    }
                    else
                    {
                        BindMemberDiscussions(Request.QueryString["did"].ToString());
                    }

                }
            }
        }

        //Message ID Function
        private void DiscussionCommentsNumber(string discussionid)
        {
            var acc = new CloudStorageAccount(
                        new StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var table = tableClient.GetTableReference("MemberDiscussions");

            TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid));
            var entities1 = table.ExecuteQuery(rangeQuery);
            var CommentsList = entities1.OrderBy(u => u.CrtDate).ToList();

            int mid = 1;
            foreach (MemberDiscussions1 Comment in CommentsList)
            {
                MID.Add(Comment.RowKey, mid);
                mid++;
            }
        }

        private void BindMemberDiscussions(string discussionid)
        {

            var acc = new CloudStorageAccount(
                         new StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var table = tableClient.GetTableReference("MemberDiscussions");

            TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid));
            var entities1 = table.ExecuteQuery(rangeQuery);
            totalcomments.Value = entities1.Count().ToString();
            var entities = entities1.OrderByDescending(u => u.CrtDate).Take(3).OrderBy(u => u.CrtDate).ToList(); ;
            
            int count = entities.Count();
            string[] memberDisID;
            memberDisID = new string[count];
            int i = 0;
            foreach (MemberDiscussions1 entity in entities)
            {
                memberDisID[i++] = entity.RowKey;
            }

            disFiles = DiscussionFile.Find(u => memberDisID.Contains(u.MemberDisID)).ToList();


            rptDiscussions.DataSource = entities;
            rptDiscussions.DataBind();

            if (entities.Count() < 3) { lblLoadPreviousCount.Visible = h4loadMsg.Visible = false; divLoadMore.Visible = false; }
            else { lblLoadPreviousCount.Visible = h4loadMsg.Visible = true; divLoadMore.Visible = true; }

            lblLoadPreviousCount.Attributes["data-commentstoload"] = "3";
            lblLoadPreviousCount.Attributes["data-commentsqty"] = "5";

            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
            disToFolder.Unread = 0;
            disToFolder.Update();

            //hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();

            //int totalComments = md.GetMemberDiscussionCountByDiscussionID(discussionid);


        }

        private void BindMemberDiscussions(string discussionid, int loadmore, bool loadAll)
        {

            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();

            md.NewMemberDiscussion();
            if (loadAll) { md.GetMoreMemberDiscussionsByDiscussion(discussionid, loadmore); }
            else { md.GetMoreMemberDiscussionsByDiscussionCount(discussionid, loadmore); }
            rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.CrtDate).ToList();
            rptDiscussions.DataBind();
            hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();
        }

        private void BindMemberDiscussions(string discussionid, int index, int qty)
        {
            var acc = new CloudStorageAccount(
                       new StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var table = tableClient.GetTableReference("MemberDiscussions");

            TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid));
            try
            {
                var entities = table.ExecuteQuery(rangeQuery).ToList().OrderByDescending(u => u.CrtDate).ToList().GetRange(index, qty).ToList().OrderBy(u => u.CrtDate).ToList();
                int count = entities.Count();
                string[] memberDisID;
                memberDisID = new string[count];
                int i = 0;
                foreach (MemberDiscussions1 entity in entities)
                {
                    memberDisID[i++] = entity.RowKey;
                }

                disFiles = DiscussionFile.Find(u => memberDisID.Contains(u.MemberDisID)).ToList();


                rptDiscussions.DataSource = entities;
                rptDiscussions.DataBind();

            }
            catch
            {
                Response.Write("END");
                Response.End();
            }




            //App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            //md.NewMemberDiscussion();
            //md.GetMoreMemberDiscussionsFromIndex(discussionid, index);
            ////int totalComments = md.GetMemberDiscussionCountByDiscussionID(discussionid);
            //rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.CrtDate).ToList();
            //rptDiscussions.DataBind();
            //hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();
        }

        private void BindMemberDiscussionsByTag(string discussionid, string tag)
        {



            var acc = new CloudStorageAccount(
                      new StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var table = tableClient.GetTableReference("MemberDiscussions");

            TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid));



            try
            {
                var entities = table.ExecuteQuery(rangeQuery).Where(a => (a.Tags == null ? false : a.Tags.Contains(tag))).ToList().OrderBy(u => u.CrtDate).ToList();
                int count = entities.Count();
                string[] memberDisID;
                memberDisID = new string[count];
                int i = 0;
                foreach (MemberDiscussions1 entity in entities)
                {
                    memberDisID[i++] = entity.RowKey;
                }

                disFiles = DiscussionFile.Find(u => memberDisID.Contains(u.MemberDisID)).ToList();


                rptDiscussions.DataSource = entities;
                rptDiscussions.DataBind();

            }
            catch
            {
                Response.Write("END");
                Response.End();
            }
        }


        protected void rptDiscussions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string rolename = "";
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl imgMember = e.Item.FindControl("imgMember") as HtmlControl;
                Label lblDateTime = e.Item.FindControl("lblDateTime") as Label;
                Label lblHourMin = e.Item.FindControl("lblHoursMinutes") as Label;
                HiddenField hdnSummary = e.Item.FindControl("HdnSummary") as HiddenField;
                HtmlContainerControl lnkDeleteComment = e.Item.FindControl("lnkDeleteComment") as HtmlContainerControl;
                HtmlContainerControl lnkEditComment = e.Item.FindControl("lnkEditComment") as HtmlContainerControl;
                HtmlContainerControl ulManageCommment = e.Item.FindControl("ulManageCommment") as HtmlContainerControl;
                HtmlContainerControl myDivMemDisO = e.Item.FindControl("divMemDisO") as HtmlContainerControl;
                Repeater rptTags = e.Item.FindControl("rptTags") as Repeater;
                Label lblMemberTemp = e.Item.FindControl("lblMemberTemp") as Label;
                HtmlControl h4Name = e.Item.FindControl("h4Name") as HtmlControl;
                Int32 memberID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MemberID"));
                String memberDisID = DataBinder.Eval(e.Item.DataItem, "RowKey").ToString();
                Member member = Member.SingleOrDefault(u => u.MemberID == memberID);
                HtmlControl div = (HtmlControl)e.Item.FindControl("divCom");
                Label MessageID = (Label)e.Item.FindControl("lblMID");
                HiddenField IsSubtracted = (HiddenField)e.Item.FindControl("hdnIsSubtracted");
                HiddenField SubtractHrsReason = (HiddenField)e.Item.FindControl("hdnSubtractHrsReason");
                String tags;
                Int16 totalminutes;
                bool IsDeletedCheck = DataBinder.Eval(e.Item.DataItem, "Deleted").ToBoolean();

                if (string.IsNullOrEmpty(SubtractHrsReason.Value))
                {
                    SubtractHrsReason.Value = "";
                }

                if (MID.ContainsKey(memberDisID))
                {
                    MessageID.Text = "MID: "+MID[memberDisID].ToString();
                    lnkEditComment.Attributes["data-messageid"] = MID[memberDisID].ToString();
                }

                try
                { tags = DataBinder.Eval(e.Item.DataItem, "Tags").ToString(); }
                catch (Exception)
                { tags = ""; }

                if (DataBinder.Eval(e.Item.DataItem, "Summary") == null || DataBinder.Eval(e.Item.DataItem, "Summary").ToString() == "")
                {
                    hdnSummary.Value = "";
                }
                else
                {
                    hdnSummary.Value = DataBinder.Eval(e.Item.DataItem, "Summary").ToString();
                }
                if (member != null)
                {
                    // MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), member.MemberID);
                    DataRow[] result = dt.Select("MemberRoles='" + member.MemberID + "'");
                    if (result.Count() > 0)
                    {
                        MemberRoles mr = (MemberRoles)result[0][1];
                        imgMember.Attributes["src"] = "_assests/Images/" + mr.ToString() + ".png";
                    }
                    else
                    {
                        imgMember.Attributes["src"] = "_assests/Images/VendorMember.png";
                    }
                    if (String.IsNullOrEmpty(tags))
                    { rptTags.Visible = false; }
                    else
                    {
                        rptTags.Visible = true;
                        rptTags.DataSource = tags.Split(',').ToList();
                        rptTags.DataBind();
                    }
                    if (IsDeletedCheck)
                    {
                        ulManageCommment.Visible = false;
                        rptTags.Visible = false;
                        lblDateTime.Text = "";
                        // DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == memberID);
                        DataRow[] Alias = dtAlias.Select("MemberID='" + memberID + "'");
                        DataRow[] DeletedBy = dtAlias.Select("MemberID='" + DataBinder.Eval(e.Item.DataItem, "DeletedByMemberID").ToInt32() + "'");
                        // DiscussionToFolder dtfDeletedBy = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == DataBinder.Eval(e.Item.DataItem, "DeletedByMemberID").ToInt32());

                        if (Alias != null)
                        {
                            if (String.IsNullOrEmpty(Alias[0][1].ToString())) { lblMemberTemp.Text = member.NickName; }
                            else { lblMemberTemp.Text = Alias[0][1].ToString(); }
                        }
                        else { lblMemberTemp.Text = member.NickName; }
                        string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                        myDivMemDisO.Style.Add("padding-bottom", "5px");
                        myDivMemDisO.Style.Add("color", "#ccc");
                        if (DeletedBy != null)
                        {
                            myDivMemDisO.InnerHtml += "This message was deleted by " + DeletedBy[0][1] + " on " + Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                            myDivMemDisO.InnerHtml += "</br>";
                        }
                        else
                        {
                            myDivMemDisO.InnerHtml += "This message was deleted on " + Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                            myDivMemDisO.InnerHtml += "</br>";
                        }
                        if (roleTitle == "ClientMember") { h4Name.Attributes["Class"] = "client-text";
                            div.Attributes.Add("style", "background-color:rgba(255, 255, 166, 0.57);");
                        }
                        else if (roleTitle == "VendorMember") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "Admin") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "SuperAdmin") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "ClientAdmin") { h4Name.Attributes["Class"] = "client-text";
                            div.Attributes.Add("style", "background-color:rgba(255, 255, 166, 0.57);");
                        }
                        else if (roleTitle == "ProjectManager") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "AsstProjectManager") { h4Name.Attributes["Class"] = ""; }
                        rolename = roleTitle;
                    }
                    else
                    {
                        Image img = new Image();
                        img.ID = "disAction";
                        img.CssClass = "disAction";
                        img.ImageUrl = "_assests/Images/downarrow.png";
                        img.Attributes.Add("data-id", memberDisID);
                        lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                        // DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == memberID);
                        DataRow[] Alias = dtAlias.Select("MemberID='" + memberID + "'");
                        if (Alias != null)
                        {

                            if (Alias.Count() > 0)
                            {
                                if (String.IsNullOrEmpty(Alias[0][1].ToString())) { lblMemberTemp.Text = member.NickName; }
                                else { lblMemberTemp.Text = Alias[0][1].ToString(); }
                            }
                            else
                            {
                                lblMemberTemp.Text = member.NickName;
                            }

                        }
                        else { lblMemberTemp.Text = member.NickName; }
                        string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                        myDivMemDisO.InnerHtml += DataBinder.Eval(e.Item.DataItem, "Conversation").ToString();
                        myDivMemDisO.InnerHtml += "</br>";
                        //DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                        DataRow[] dtfCurrMember = dt.Select("MemberRoles='" + hdnMemberID.Value.ToInt32() + "'");
                        if (member.MemberID == Convert.ToInt32(hdnMemberID.Value))
                        {
                            lnkDeleteComment.Visible = true; lnkEditComment.Visible = true;
                        }
                        else
                        {
                            if (dtfCurrMember != null)
                            {
                                if (dtfCurrMember[0]["RoleID"].ToInt32() == 1 || dtfCurrMember[0]["RoleID"].ToInt32() == 2)
                                {
                                    lnkDeleteComment.Visible = true;
                                    lnkEditComment.Visible = true;
                                }
                            }
                        }
                        if (roleTitle == "ClientMember") { h4Name.Attributes["Class"] = "client-text";
                            div.Attributes.Add("style", "background-color:rgba(255, 255, 166, 0.57);");
                        }
                        else if (roleTitle == "VendorMember") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "Admin") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "SuperAdmin") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "ClientAdmin") { h4Name.Attributes["Class"] = "client-text";                         
                            div.Attributes.Add("style", "background-color:rgba(255, 255, 166, 0.57);");
                        }
                        else if (roleTitle == "ProjectManager") { h4Name.Attributes["Class"] = ""; }
                        else if (roleTitle == "AsstProjectManager") { h4Name.Attributes["Class"] = ""; }
                        rolename = roleTitle;
                        Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;

                        if (disFiles != null)
                        {
                            List<DiscussionFile> disFiles1 = disFiles.FindAll(u => u.MemberDisID == memberDisID);


                            if (disFiles1.Count > 0)
                            {

                                rptFiles.DataSource = disFiles1.ToList();
                                rptFiles.DataBind();
                            }
                            else { }
                        }
                    }
                }



                if (DataBinder.Eval(e.Item.DataItem, "RoleID").ToInt32() == 4 ||
                    DataBinder.Eval(e.Item.DataItem, "RoleID").ToInt32() == 5)
                {
                    lblHourMin.Text = "";
                }
                else
                {

                    if (isHourly == true && (HideHours == false || (HideHours == true && (roleid != 5 && roleid != 4))))
                    {
                        if (DataBinder.Eval(e.Item.DataItem, "Minutes").ToString() == "" || DataBinder.Eval(e.Item.DataItem, "Minutes") == null)
                        {
                            lblHourMin.Text = "Time Spent: 0 Hours 0 Minutes";
                        }
                        else
                        {
                            totalminutes = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "Minutes").ToString());
                            string hour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 minutes = Convert.ToInt16(totalminutes % 60);

                            //Subtract Hours
                            if (IsSubtracted.Value == "True" && (!IsDeletedCheck))
                            {
                                // myDivMemDisO.Style.Add("color", "rgb(221, 36, 36)");
                               // myDivMemDisO.InnerHtml += SubtractHrsReason.Value + "<br/>";
                                lblHourMin.Style.Add("color", "rgb(221, 36, 36)");
                                lblHourMin.Text = "Time Subtracted: - " + hour + " Hours " + minutes.ToString() + " Minutes";
                            }
                            //End
                            else
                            {
                                lblHourMin.Style.Add("color", "#000000");
                                lblHourMin.Text = "Time Spent: " + hour + " Hours " + minutes.ToString() + " Minutes";
                            }
                        }
                        lblHourMin.Visible = true;
                    }
                    else
                    {
                        lblHourMin.Visible = false;
                    }
                }
                if (DataBinder.Eval(e.Item.DataItem, "Deleted").ToBoolean())
                {
                    lblHourMin.Visible = false;
                }
                else
                {
                    lblHourMin.Visible = true;
                }


            }
        }

        protected void rptFiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlAnchor anchor = e.Item.FindControl("lnkFileName") as HtmlAnchor;
                string FileName = anchor.Attributes["alt"];
                string _filename = Path.GetExtension(FileName);

                if (_filename == ".pdf")
                {
                    anchor.Attributes.Add("download", FileName);
                }
            }
        }
    }
}