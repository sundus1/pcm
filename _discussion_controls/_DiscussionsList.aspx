﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_DiscussionsList.aspx.cs" Inherits="CCM._discussion_controls._DiscussionsList" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="discussionlist">
            <asp:Label runat="server" ID="lblUnread" CssClass="lblUnreadCount" Style="display: none"></asp:Label>
            <asp:Label ID="lblOpenCount" runat="server"></asp:Label>
            <asp:Label ID="lblArchiveCount" runat="server"></asp:Label>
            <div id="tabs-1">
                <asp:Repeater ID="rptDiscussionList" runat="server" OnItemDataBound="rptDiscussionList_ItemDataBound">
                    <HeaderTemplate>
                        <div class="spacer20"></div>
                        <div class="search">
                            <input type="search" id="txtSearchOpen" placeholder="Search..." value="" /><i class="fa fa-search"></i>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="projects" runat="server" id="divdis"><a class="alinkdiscussionopen" href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="opendis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' style="width: 94% !important; padding: 10px; color: #ccc"></a></div>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
            <div id="tabs-2">
                <asp:Repeater ID="rptDiscussionListClosed" runat="server" OnItemDataBound="rptDiscussionListClosed_ItemDataBound">
                    <HeaderTemplate>
                        <div class="spacer20"></div>
                        <div class="search">
                            <input type="search" id="txtSearchClose" placeholder="Search..." /><i class="fa fa-search"></i>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="projects" runat="server" id="divdis"><a class="alinkdiscussionclose" href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="closedis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' style="width: 94% !important; padding: 10px; color: #ccc"></a></div>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </asp:Repeater>
            </div>
        </div>


        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
    </form>
</body>
</html>
