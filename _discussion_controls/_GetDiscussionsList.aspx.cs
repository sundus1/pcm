﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CCM.App_Code;
using System.Web.UI.HtmlControls;
using System.Data;

namespace CCM
{
    public partial class _GetDiscussionsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["iid"] != null)
                {
                    hdnInstanceID.Value = Request.QueryString["iid"].ToString();
                }
                if (Request.QueryString["mid"] != null)
                {
                    hdnMemberID.Value = Request.QueryString["mid"].ToString();
                }
                if (Request.QueryString["oid"] != null)
                {
                    hdnOID.Value = Request.QueryString["oid"].ToString();
                }
                if (Request.QueryString["did"] != null)
                {
                    hdnDisID.Value = Request.QueryString["did"];
                }
                BindDiscussionList();

            }
        }

        private void BindDiscussionList()
        {            

            DataTable dt = CCMDiscussion.GetAllMemberDiscussions(hdnMemberID.Value.ToInt32(), hdnInstanceID.Value);
            Int32 TotalUnread = dt.Compute("SUM(Unread)", "").ToInt32();
            if (TotalUnread > 0)
            {
                lblUnread.Text = "(" + TotalUnread.ToString() + ")";
            }
            else
            {
                lblUnread.Text = "";
            }

            dt.DefaultView.RowFilter = "Status = 'Open'";
            dt.DefaultView.Sort = "ModDate desc";
            rptDiscussionList.DataSource = dt.DefaultView;
            rptDiscussionList.DataBind();

            TotalUnread = dt.DefaultView.ToTable().Compute("SUM(Unread)", "").ToInt32();
            if (TotalUnread > 0)
            {
                lblOpenCount.Text = "(" + TotalUnread.ToString() + ")";
            }
            else
            {
                lblOpenCount.Text = "";
            }

            dt.DefaultView.RowFilter = "Status = 'Close'";
            dt.DefaultView.Sort = "ModDate desc";
            rptDiscussionListClosed.DataSource = dt.DefaultView;
            rptDiscussionListClosed.DataBind();

            TotalUnread = dt.DefaultView.ToTable().Compute("SUM(Unread)", "").ToInt32();
            if (TotalUnread > 0)
            {
                lblArchiveCount.Text = "(" + TotalUnread.ToString() + ")";
            }
            else
            {
                lblArchiveCount.Text = "";
            }

            
            SetActiveDiscussion(hdnDisID.Value.ToString());
        }

        private void SetActiveDiscussion(String CommandArgument)
        {
            foreach (RepeaterItem item in rptDiscussionList.Items)
            {
                HtmlControl aLinkDiscussion = (HtmlControl)item.FindControl("aLinkDiscussion");
                aLinkDiscussion.Attributes.Remove("Class");
                if (aLinkDiscussion.Attributes["data-argument"] == CommandArgument)
                {                    aLinkDiscussion.Attributes.Add("Class", "active");                }
            }
            foreach (RepeaterItem item in rptDiscussionListClosed.Items)
            {
                HtmlControl aLinkDiscussion = (HtmlControl)item.FindControl("aLinkDiscussion");
                aLinkDiscussion.Attributes.Remove("Class");
                if (aLinkDiscussion.Attributes["data-argument"] == CommandArgument)
                { aLinkDiscussion.Attributes.Add("Class", "active"); }
            }
        }

        public string GetDiscussionLink(Int32 discussionID, Int32 roleID)
        {
            String url = "Discussion.aspx?did=" + discussionID.ToString().Encrypt() + "&fid=" + "0".Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&ir=" + roleID.ToString().Encrypt() + "&instanceid=" + hdnInstanceID.Value;
            return url;
        }

        protected void rptDiscussionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 Unread = DataBinder.Eval(e.Item.DataItem, "Unread").ToInt32();
                Int32 DisID = DataBinder.Eval(e.Item.DataItem, "DiscussionID").ToInt32();
                HtmlContainerControl aLinkDiscussion = e.Item.FindControl("aLinkDiscussion") as HtmlContainerControl;
                App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                Int32 CoundComment = md.GetMemberDiscussionCountByDiscussionID(DisID.ToString());
                aLinkDiscussion.InnerText = DataBinder.Eval(e.Item.DataItem, "Title") + " (" + CoundComment + ")";
                if (Unread > 0)
                {
                    aLinkDiscussion.Style.Add("font-weight", "bold");
                    //aLinkDiscussion.InnerText += " (" + CoundComment + ")";
                }
                else
                {
                    aLinkDiscussion.Style.Remove("font-weight");
                }

            }
        }

        protected void rptDiscussionListClosed_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 Unread = DataBinder.Eval(e.Item.DataItem, "Unread").ToInt32();
                Int32 DisID = DataBinder.Eval(e.Item.DataItem, "DiscussionID").ToInt32();
                HtmlContainerControl aLinkDiscussion = e.Item.FindControl("aLinkDiscussion") as HtmlContainerControl;
                App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                Int32 CoundComment = md.GetMemberDiscussionCountByDiscussionID(DisID.ToString());
                aLinkDiscussion.InnerText = DataBinder.Eval(e.Item.DataItem, "Title") + " (" + CoundComment + ")";
                if (Unread > 0)
                {
                    aLinkDiscussion.Style.Add("font-weight", "bold");
                    //aLinkDiscussion.InnerText += " (" + CoundComment + ")";
                }
                else
                {
                    aLinkDiscussion.Style.Remove("font-weight");
                }

            }
        }
    }
}