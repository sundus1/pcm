﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageTeamMembers.aspx.cs" Inherits="CCM.ManageTeamMembers" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title id="title" runat="server">Team Management- Hourly Project</title>
    <meta name="Description" content="Team Communication Application - CCM" />
    <link rel="Shortcut Icon" type="image/x-icon" href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/images/favicon.ico" />
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link href="<%= "_discussionsa_assets/css/bootstrap.css" %>" rel="stylesheet" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="<%= "_discussionsa_assets/js/bootstrap.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/font-awesome.css" %>" rel="stylesheet" />
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="<%= "_discussionsa_assets/css/style.css"  %>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="<%="_discussionsa_assets/js/manageTeam.js" %>"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <%-- jQuery Select2 --%>
    <script src="<%= "_discussionsa_assets/multiselection/select2.min.js" %>"></script>
    <link href="<%= "_discussionsa_assets/multiselection/select2.min.css" %>" rel="stylesheet" />

    <%-- CSS --%>
    <link rel="stylesheet" type="text/css" href="_discussionsa_assets/css/manageteam.css" />

     <link href="<%= "_assests/nProgress/nprogress.css?" + DateTime.Now.ToBinary() %>" rel="stylesheet" />
        <script src="<%= "_assests/nProgress/nprogress.js?" + DateTime.Now.ToBinary() %>"></script>


    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

    <style type="text/css">
        td {
            padding: 3px;
            margin: 3px;
        }

        .loaderDiv {
            position: absolute;
            z-index: 99;
            top: 200px;
            left: 550px;
            filter: Alpha(Opacity=90);
            -moz-opacity: 0.9;
            border-width: 0px;
        }


        #load {
            width: 100%;
            height: 100%;
            position: fixed;
            z-index: 9999;
            background: url("https://www.creditmutuel.fr/cmne/fr/banques/webservices/nswr/images/loading.gif") no-repeat center center rgba(0,0,0,0.25);
        }
    </style>
    <script type="text/javascript">
        //var isshow = localStorage.getItem('isshow');
        //if (isshow == null) {
        //    localStorage.setItem('isshow', 1);
        //    window.onbeforeunload = function () {
        //        document.write('<div id="loading"><p>Please wait while content is <b>LOADING</b>...</p></div><style type="text/css">#wrapper960{visibility:hidden}#loading{margin:100px auto;width:480px;border:1px solid #777;padding:20px;text-align:left;}</style>');
        //    };
        //}

        //$(window).load(function () {
        //    $('#load').fadeOut();
        //});

        //document.onreadystatechange = function () {
        //    var state = document.readyState
        //    if (state == 'interactive') {
        //        document.getElementById('form1').style.visibility = "hidden";
        //    } else if (state == 'complete') {
        //        setTimeout(function () {
        //            document.getElementById('interactive');
        //            document.getElementById('load').style.visibility = "hidden";
        //            document.getElementById('form1').style.visibility = "visible";
        //        }, 1000);
        //    }
        //}
    </script>

</head>
<body id="bodyDis">
    <%--  <div id="load"></div>--%>
    <form id="form1" runat="server" style="height: 100%" class="formdis">

        <%--Team Management Start--%>
        <div runat="server" id="manageteam">
            <div id="widget">
                <div>
                    <div id="bar">
                        <div class="col-md-12 chat-heading">
                            <h1 id="h1Title" runat="server" style="display: inline-block;">Team Management
                            </h1>

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9 ChatCont" style="padding-left: 50px; width: 100%">
                            <table width="100%" cellpadding="5" cellspacing="5" class="dataTable">
                                <tr>
                                    <td style="width: 300px">
                                        <asp:Label runat="server" ID="lblProjects" Text="Projects " Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 300px">
                                        <asp:DropDownList ID="ddlProjects" runat="server" CssClass="mtddlProjects" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <div id="list" runat="server">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <button id="btnRemove" class="btn btn-danger" disabled="disabled">Remove</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" />
                                    </td>
                                </tr>

                            </table>
                        </div>


                    </div>
                </div>

                <div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Edit User -->
            <div class="modal fade" id="editMember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel7"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content eidtTitlePopup">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel6">Edit Member</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Name (Alias):</label>
                                    &nbsp;
                        <asp:TextBox ID="mAlias" runat="server" class="form-control"></asp:TextBox>
                                    <br />
                                </div>
                                <div class="col-xs-6">
                                    <label>Title:</label>
                                    &nbsp;
                        <asp:TextBox ID="mTitle" runat="server" class="form-control"></asp:TextBox>
                                    <br />
                                </div>
                                <div class="col-xs-12">
                                    <label>Role:</label>
                                    &nbsp;
                       <asp:DropDownList runat="server" ID="ddlRoles" CssClass="ddlRoles"></asp:DropDownList>
                                </div>
                                <br />
                                <div class="col-xs-12">
                                    <label>Email:</label>
                                    &nbsp;
                  <asp:TextBox ID="mEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-xs-12" id="ActiveCheck">
                                    <asp:Label runat="server" ID="lblRole" Font-Bold="true" ForeColor="Silver"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btnCancel" data-dismiss="modal">
                                Cancel</button>
                            <button type="button" class="btn btn-primary btnAddNotes" id="btnUpdate">
                                Update</button>
                            <asp:HiddenField ID="hdnID" runat="server" />
                            <asp:HiddenField ID="hdnEditEmail" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--Team Management End--%>

        <%--Projects Management Start--%>
        <div runat="server" id="manageprojects">
            <div id="widget">
                <div>
                    <div id="bar">
                        <div class="col-md-12 chat-heading">
                            <h1 id="h1" runat="server" style="display: inline-block;">Project Management
                            </h1>

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9 ChatCont" style="padding-left: 50px; width: 100%">
                            <table width="100%" cellpadding="5" cellspacing="5" class="dataTable">
                                <tr>
                                    <td style="width: 300px">
                                        <asp:Label runat="server" ID="Label1" Text="Users " Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 300px">
                                        <asp:DropDownList ID="ddlTeamMembers" runat="server" CssClass="mtddlTeamMembers" OnSelectedIndexChanged="ddlTeamMembers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <div id="projectslist" runat="server">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <button id="btnRemoveUser" class="btn btn-danger" disabled="disabled">Remove</button>
                                        &nbsp;&nbsp; 
                                          <button id="btnRemovefrmAll" class="btn btn-danger" onclick="return GetDiscussionIds();" runat="server">Remove from All Projects</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="Label2" runat="server" Text="" ForeColor="Red" />
                                    </td>
                                </tr>

                            </table>
                        </div>


                    </div>
                </div>

                <div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
        <%--Projects Management End--%>
               
        <%--Hidden Fields Start--%>
        <asp:HiddenField runat="server" ID="hdnMemberID" />
        <asp:HiddenField runat="server" ID="hdnDiscussionID" />
        <asp:HiddenField runat="server" ID="hdnIntanceID" />
        <%--Hidden Fields End--%>
    </form>

</body>
</html>

