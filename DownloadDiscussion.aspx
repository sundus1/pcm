﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DownloadDiscussion.aspx.cs" Inherits="CCM.DownloadDiscussion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title id="title" runat="server">Download Discussion Communication</title>
    <meta name="Description" content="Team Communication Application - CCM" />
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link href="<%= "_discussionsa_assets/css/bootstrap.css" %>" rel="stylesheet" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="<%= "_discussionsa_assets/js/bootstrap.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/font-awesome.css" %>" rel="stylesheet" />
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="<%= "_discussionsa_assets/css/style.css"  %>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
    <script src="<%="_discussionsa_assets/js/reports.js?a="+DateTime.Now.ToBinary() %>"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="_discussionsa_assets/jspdf/excel/FileSaver.min.js"></script>
    <script type="text/javascript" src="_discussionsa_assets/jspdf/excel/xlsx.core.min.js"></script>
    <script type="text/javascript" src="_discussionsa_assets/jspdf/excel/tableExport.js"></script>
    <script type="text/javascript" src="_discussionsa_assets/jspdf/excel/tableExport.min.js"></script>

    <style type="text/css">
        td {
            padding: 3px;
            margin: 3px;
        }

        .dLink {
            float: right;
            color: #fff !important;
            background-color: #337ab7 !important;
            padding: 2px 8px 3px !important;
            font-size: 16px !important;
            margin: 0 0 0 6px !important;
            border-radius: 5px !important;
        }
    </style>
    <script>

        jQuery(function ($) {
            $(".dLink").click(function () {
                $('#dataH').tableExport({ fileName: $("#hdnDisName").val(), type: 'excel' });

            });
        });


    </script>
</head>


<body id="bodyDis">
    <form id="form1" runat="server" style="height: 100%" class="formdis">
        <div id="widget">
            <div>
                <div id="bar">
                    <div class="col-md-12 chat-heading">
                        <h1 id="lblProjectName" runat="server" style="display: inline-block;">Download Discussion Communication:
                        </h1>

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-9 ChatCont" style="padding-left: 50px; width: 100%">
                        <table width="100%" cellpadding="5" cellspacing="5" id="MyTable">
                            <tr>
                                <td colspan="3">
                                    <a href="#" id="dLink" class="dLink">DOWNLOAD</a>
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="list" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lblTotalHours" runat="server" Text="0 Hours 0 Minutes"></asp:Label>

                                </td>

                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:HiddenField ID="hdnAllProjectMinutes" runat="server" />
                                    <asp:HiddenField ID="hdnAllProjectAddMin" runat="server" />
                                    <asp:HiddenField ID="hdnAllProjectSubMin" runat="server" />
                                    <asp:HiddenField ID="hdnDisName" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</body>
</html>
