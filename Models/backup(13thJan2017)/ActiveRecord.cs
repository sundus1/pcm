


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SubSonic.DataProviders;
using SubSonic.Extensions;
using System.Linq.Expressions;
using SubSonic.Schema;
using System.Collections;
using SubSonic;
using SubSonic.Repository;
using System.ComponentModel;
using System.Data.Common;

namespace MyCCM
{
    
    
    /// <summary>
    /// A class which represents the Roles table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Role: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Role> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Role>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Role> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Role item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Role item=new Role();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Role> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Role(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Role.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Role>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Role(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Role(Expression<Func<Role, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Role> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Role> _repo;
            
            if(db.TestMode){
                Role.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Role>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Role> GetRepo(){
            return GetRepo("","");
        }
        
        public static Role SingleOrDefault(Expression<Func<Role, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Role single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Role SingleOrDefault(Expression<Func<Role, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Role single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Role, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Role, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Role> Find(Expression<Func<Role, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Role> Find(Expression<Func<Role, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Role> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Role> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Role> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Role> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Role> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Role> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "RoleID";
        }

        public object KeyValue()
        {
            return this.RoleID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.RoleTitle.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Role)){
                Role compare=(Role)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.RoleID;
        }
        
        public string DescriptorValue()
        {
                            return this.RoleTitle.ToString();
                    }

        public string DescriptorColumn() {
            return "RoleTitle";
        }
        public static string GetKeyColumn()
        {
            return "RoleID";
        }        
        public static string GetDescriptorColumn()
        {
            return "RoleTitle";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _RoleID;
        public int RoleID
        {
            get { return _RoleID; }
            set
            {
                if(_RoleID!=value){
                    _RoleID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _RoleTitle;
        public string RoleTitle
        {
            get { return _RoleTitle; }
            set
            {
                if(_RoleTitle!=value){
                    _RoleTitle=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleTitle");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _RoleDescription;
        public string RoleDescription
        {
            get { return _RoleDescription; }
            set
            {
                if(_RoleDescription!=value){
                    _RoleDescription=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleDescription");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Role, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the Projects table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Project: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Project> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Project>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Project> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Project item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Project item=new Project();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Project> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Project(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Project.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Project>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Project(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Project(Expression<Func<Project, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Project> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Project> _repo;
            
            if(db.TestMode){
                Project.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Project>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Project> GetRepo(){
            return GetRepo("","");
        }
        
        public static Project SingleOrDefault(Expression<Func<Project, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Project single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Project SingleOrDefault(Expression<Func<Project, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Project single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Project, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Project, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Project> Find(Expression<Func<Project, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Project> Find(Expression<Func<Project, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Project> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Project> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Project> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Project> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Project> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Project> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "ProjectID";
        }

        public object KeyValue()
        {
            return this.ProjectID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Project)){
                Project compare=(Project)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.ProjectID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "ProjectID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _ProjectID;
        public int ProjectID
        {
            get { return _ProjectID; }
            set
            {
                if(_ProjectID!=value){
                    _ProjectID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ProjectID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _OwnerID;
        public string OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if(_OwnerID!=value){
                    _OwnerID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="OwnerID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _Parent_ProjectID;
        public int? Parent_ProjectID
        {
            get { return _Parent_ProjectID; }
            set
            {
                if(_Parent_ProjectID!=value){
                    _Parent_ProjectID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Parent_ProjectID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                if(_Description!=value){
                    _Description=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Description");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                if(_Status!=value){
                    _Status=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Status");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Project, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the MemberToProject table in the avaimaTest0001 Database.
    /// </summary>
    public partial class MemberToProject: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<MemberToProject> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<MemberToProject>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<MemberToProject> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(MemberToProject item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                MemberToProject item=new MemberToProject();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<MemberToProject> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public MemberToProject(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                MemberToProject.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberToProject>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public MemberToProject(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public MemberToProject(Expression<Func<MemberToProject, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<MemberToProject> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<MemberToProject> _repo;
            
            if(db.TestMode){
                MemberToProject.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberToProject>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<MemberToProject> GetRepo(){
            return GetRepo("","");
        }
        
        public static MemberToProject SingleOrDefault(Expression<Func<MemberToProject, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            MemberToProject single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static MemberToProject SingleOrDefault(Expression<Func<MemberToProject, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            MemberToProject single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<MemberToProject, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<MemberToProject, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<MemberToProject> Find(Expression<Func<MemberToProject, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<MemberToProject> Find(Expression<Func<MemberToProject, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<MemberToProject> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<MemberToProject> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<MemberToProject> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<MemberToProject> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<MemberToProject> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<MemberToProject> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "MTPID";
        }

        public object KeyValue()
        {
            return this.MTPID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(MemberToProject)){
                MemberToProject compare=(MemberToProject)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.MTPID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "MTPID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _MTPID;
        public int MTPID
        {
            get { return _MTPID; }
            set
            {
                if(_MTPID!=value){
                    _MTPID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MTPID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _RoleID;
        public int? RoleID
        {
            get { return _RoleID; }
            set
            {
                if(_RoleID!=value){
                    _RoleID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ProjectID;
        public int? ProjectID
        {
            get { return _ProjectID; }
            set
            {
                if(_ProjectID!=value){
                    _ProjectID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ProjectID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ApplicableTo;
        public int? ApplicableTo
        {
            get { return _ApplicableTo; }
            set
            {
                if(_ApplicableTo!=value){
                    _ApplicableTo=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ApplicableTo");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<MemberToProject, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the Tags table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Tag: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Tag> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Tag>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Tag> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Tag item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Tag item=new Tag();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Tag> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Tag(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Tag.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Tag>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Tag(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Tag(Expression<Func<Tag, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Tag> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Tag> _repo;
            
            if(db.TestMode){
                Tag.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Tag>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Tag> GetRepo(){
            return GetRepo("","");
        }
        
        public static Tag SingleOrDefault(Expression<Func<Tag, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Tag single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Tag SingleOrDefault(Expression<Func<Tag, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Tag single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Tag, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Tag, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Tag> Find(Expression<Func<Tag, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Tag> Find(Expression<Func<Tag, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Tag> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Tag> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Tag> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Tag> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Tag> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Tag> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "TagID";
        }

        public object KeyValue()
        {
            return this.TagID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
            			    return this.TagX.ToString();
	                }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Tag)){
                Tag compare=(Tag)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.TagID;
        }
        
        public string DescriptorValue()
        {
            			    return this.TagX.ToString();
	                }

        public string DescriptorColumn() {
            return "Tag";
        }
        public static string GetKeyColumn()
        {
            return "TagID";
        }        
        public static string GetDescriptorColumn()
        {
            return "Tag";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _TagID;
        public int TagID
        {
            get { return _TagID; }
            set
            {
                if(_TagID!=value){
                    _TagID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="TagID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _TagX;
        public string TagX
        {
            get { return _TagX; }
            set
            {
                if(_TagX!=value){
                    _TagX=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Tag");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _Created;
        public DateTime? Created
        {
            get { return _Created; }
            set
            {
                if(_Created!=value){
                    _Created=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Created");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _Modified;
        public DateTime? Modified
        {
            get { return _Modified; }
            set
            {
                if(_Modified!=value){
                    _Modified=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Modified");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Tag, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the Members table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Member: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Member> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Member>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Member> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Member item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Member item=new Member();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Member> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Member(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Member.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Member>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Member(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Member(Expression<Func<Member, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Member> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Member> _repo;
            
            if(db.TestMode){
                Member.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Member>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Member> GetRepo(){
            return GetRepo("","");
        }
        
        public static Member SingleOrDefault(Expression<Func<Member, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Member single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Member SingleOrDefault(Expression<Func<Member, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Member single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Member, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Member, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Member> Find(Expression<Func<Member, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Member> Find(Expression<Func<Member, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Member> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Member> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Member> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Member> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Member> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Member> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "MemberID";
        }

        public object KeyValue()
        {
            return this.MemberID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.OwnerID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Member)){
                Member compare=(Member)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.MemberID;
        }
        
        public string DescriptorValue()
        {
                            return this.OwnerID.ToString();
                    }

        public string DescriptorColumn() {
            return "OwnerID";
        }
        public static string GetKeyColumn()
        {
            return "MemberID";
        }        
        public static string GetDescriptorColumn()
        {
            return "OwnerID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _MemberID;
        public int MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _OwnerID;
        public string OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if(_OwnerID!=value){
                    _OwnerID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="OwnerID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if(_FirstName!=value){
                    _FirstName=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FirstName");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _NickName;
        public string NickName
        {
            get { return _NickName; }
            set
            {
                if(_NickName!=value){
                    _NickName=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="NickName");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                if(_Email!=value){
                    _Email=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Email");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _ImagePath;
        public string ImagePath
        {
            get { return _ImagePath; }
            set
            {
                if(_ImagePath!=value){
                    _ImagePath=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ImagePath");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if(_LastName!=value){
                    _LastName=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="LastName");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Member, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the MemberDiscussions table in the avaimaTest0001 Database.
    /// </summary>
    public partial class MemberDiscussion: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<MemberDiscussion> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<MemberDiscussion>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<MemberDiscussion> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(MemberDiscussion item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                MemberDiscussion item=new MemberDiscussion();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<MemberDiscussion> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public MemberDiscussion(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                MemberDiscussion.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberDiscussion>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public MemberDiscussion(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public MemberDiscussion(Expression<Func<MemberDiscussion, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<MemberDiscussion> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<MemberDiscussion> _repo;
            
            if(db.TestMode){
                MemberDiscussion.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberDiscussion>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<MemberDiscussion> GetRepo(){
            return GetRepo("","");
        }
        
        public static MemberDiscussion SingleOrDefault(Expression<Func<MemberDiscussion, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            MemberDiscussion single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static MemberDiscussion SingleOrDefault(Expression<Func<MemberDiscussion, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            MemberDiscussion single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<MemberDiscussion, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<MemberDiscussion, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<MemberDiscussion> Find(Expression<Func<MemberDiscussion, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<MemberDiscussion> Find(Expression<Func<MemberDiscussion, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<MemberDiscussion> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<MemberDiscussion> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<MemberDiscussion> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<MemberDiscussion> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<MemberDiscussion> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<MemberDiscussion> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "MemberDisID";
        }

        public object KeyValue()
        {
            return this.MemberDisID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(MemberDiscussion)){
                MemberDiscussion compare=(MemberDiscussion)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.MemberDisID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "MemberDisID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _MemberDisID;
        public int MemberDisID
        {
            get { return _MemberDisID; }
            set
            {
                if(_MemberDisID!=value){
                    _MemberDisID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberDisID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _DiscussionID;
        public int? DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _TeamMember;
        public bool? TeamMember
        {
            get { return _TeamMember; }
            set
            {
                if(_TeamMember!=value){
                    _TeamMember=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="TeamMember");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _DiscussionBlobID;
        public string DiscussionBlobID
        {
            get { return _DiscussionBlobID; }
            set
            {
                if(_DiscussionBlobID!=value){
                    _DiscussionBlobID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionBlobID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<MemberDiscussion, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the Discussions table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Discussion: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Discussion> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Discussion>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Discussion> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Discussion item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Discussion item=new Discussion();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Discussion> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Discussion(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Discussion.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Discussion>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Discussion(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Discussion(Expression<Func<Discussion, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Discussion> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Discussion> _repo;
            
            if(db.TestMode){
                Discussion.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Discussion>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Discussion> GetRepo(){
            return GetRepo("","");
        }
        
        public static Discussion SingleOrDefault(Expression<Func<Discussion, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Discussion single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Discussion SingleOrDefault(Expression<Func<Discussion, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Discussion single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Discussion, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Discussion, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Discussion> Find(Expression<Func<Discussion, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Discussion> Find(Expression<Func<Discussion, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Discussion> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Discussion> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Discussion> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Discussion> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Discussion> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Discussion> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "DiscussionID";
        }

        public object KeyValue()
        {
            return this.DiscussionID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.Title.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Discussion)){
                Discussion compare=(Discussion)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.DiscussionID;
        }
        
        public string DescriptorValue()
        {
                            return this.Title.ToString();
                    }

        public string DescriptorColumn() {
            return "Title";
        }
        public static string GetKeyColumn()
        {
            return "DiscussionID";
        }        
        public static string GetDescriptorColumn()
        {
            return "Title";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _DiscussionID;
        public int DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ProjectID;
        public int? ProjectID
        {
            get { return _ProjectID; }
            set
            {
                if(_ProjectID!=value){
                    _ProjectID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ProjectID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                if(_Status!=value){
                    _Status=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Status");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _isHourly;
        public bool? isHourly
        {
            get { return _isHourly; }
            set
            {
                if(_isHourly!=value){
                    _isHourly=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="isHourly");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _HourRequiredEachPost;
        public bool? HourRequiredEachPost
        {
            get { return _HourRequiredEachPost; }
            set
            {
                if(_HourRequiredEachPost!=value){
                    _HourRequiredEachPost=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="HourRequiredEachPost");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _ZeroHourPost;
        public bool? ZeroHourPost
        {
            get { return _ZeroHourPost; }
            set
            {
                if(_ZeroHourPost!=value){
                    _ZeroHourPost=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ZeroHourPost");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _IsHelp;
        public bool? IsHelp
        {
            get { return _IsHelp; }
            set
            {
                if(_IsHelp!=value){
                    _IsHelp=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="IsHelp");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _HelpEmail;
        public string HelpEmail
        {
            get { return _HelpEmail; }
            set
            {
                if(_HelpEmail!=value){
                    _HelpEmail=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="HelpEmail");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _HideHours;
        public bool? HideHours
        {
            get { return _HideHours; }
            set
            {
                if(_HideHours!=value){
                    _HideHours=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="HideHours");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _SummaryAllow;
        public bool? SummaryAllow
        {
            get { return _SummaryAllow; }
            set
            {
                if(_SummaryAllow!=value){
                    _SummaryAllow=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="SummaryAllow");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _Multiple;
        public int? Multiple
        {
            get { return _Multiple; }
            set
            {
                if(_Multiple!=value){
                    _Multiple=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Multiple");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Duration;
        public string Duration
        {
            get { return _Duration; }
            set
            {
                if(_Duration!=value){
                    _Duration=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Duration");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _EmailContent;
        public string EmailContent
        {
            get { return _EmailContent; }
            set
            {
                if(_EmailContent!=value){
                    _EmailContent=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="EmailContent");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _SummarySentDate;
        public DateTime? SummarySentDate
        {
            get { return _SummarySentDate; }
            set
            {
                if(_SummarySentDate!=value){
                    _SummarySentDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="SummarySentDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _sendsummary;
        public bool? sendsummary
        {
            get { return _sendsummary; }
            set
            {
                if(_sendsummary!=value){
                    _sendsummary=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="sendsummary");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Discussion, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the DiscussionFiles table in the avaimaTest0001 Database.
    /// </summary>
    public partial class DiscussionFile: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<DiscussionFile> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<DiscussionFile>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<DiscussionFile> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(DiscussionFile item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                DiscussionFile item=new DiscussionFile();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<DiscussionFile> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public DiscussionFile(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                DiscussionFile.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DiscussionFile>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public DiscussionFile(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public DiscussionFile(Expression<Func<DiscussionFile, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<DiscussionFile> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<DiscussionFile> _repo;
            
            if(db.TestMode){
                DiscussionFile.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DiscussionFile>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<DiscussionFile> GetRepo(){
            return GetRepo("","");
        }
        
        public static DiscussionFile SingleOrDefault(Expression<Func<DiscussionFile, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            DiscussionFile single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static DiscussionFile SingleOrDefault(Expression<Func<DiscussionFile, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            DiscussionFile single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<DiscussionFile, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<DiscussionFile, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<DiscussionFile> Find(Expression<Func<DiscussionFile, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<DiscussionFile> Find(Expression<Func<DiscussionFile, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<DiscussionFile> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<DiscussionFile> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<DiscussionFile> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<DiscussionFile> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<DiscussionFile> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<DiscussionFile> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "DisFileID";
        }

        public object KeyValue()
        {
            return this.DisFileID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.MemberDisID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(DiscussionFile)){
                DiscussionFile compare=(DiscussionFile)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.DisFileID;
        }
        
        public string DescriptorValue()
        {
                            return this.MemberDisID.ToString();
                    }

        public string DescriptorColumn() {
            return "MemberDisID";
        }
        public static string GetKeyColumn()
        {
            return "DisFileID";
        }        
        public static string GetDescriptorColumn()
        {
            return "MemberDisID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _DisFileID;
        public int DisFileID
        {
            get { return _DisFileID; }
            set
            {
                if(_DisFileID!=value){
                    _DisFileID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DisFileID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _MemberDisID;
        public string MemberDisID
        {
            get { return _MemberDisID; }
            set
            {
                if(_MemberDisID!=value){
                    _MemberDisID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberDisID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _DiscussionID;
        public int? DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ProjectID;
        public int? ProjectID
        {
            get { return _ProjectID; }
            set
            {
                if(_ProjectID!=value){
                    _ProjectID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ProjectID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set
            {
                if(_FileName!=value){
                    _FileName=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FileName");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _FileType;
        public string FileType
        {
            get { return _FileType; }
            set
            {
                if(_FileType!=value){
                    _FileType=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FileType");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _FilePath;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if(_FilePath!=value){
                    _FilePath=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FilePath");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _FolderID;
        public int? FolderID
        {
            get { return _FolderID; }
            set
            {
                if(_FolderID!=value){
                    _FolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<DiscussionFile, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the OwnerToMember table in the avaimaTest0001 Database.
    /// </summary>
    public partial class OwnerToMember: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<OwnerToMember> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<OwnerToMember>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<OwnerToMember> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(OwnerToMember item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                OwnerToMember item=new OwnerToMember();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<OwnerToMember> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public OwnerToMember(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                OwnerToMember.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<OwnerToMember>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public OwnerToMember(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public OwnerToMember(Expression<Func<OwnerToMember, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<OwnerToMember> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<OwnerToMember> _repo;
            
            if(db.TestMode){
                OwnerToMember.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<OwnerToMember>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<OwnerToMember> GetRepo(){
            return GetRepo("","");
        }
        
        public static OwnerToMember SingleOrDefault(Expression<Func<OwnerToMember, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            OwnerToMember single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static OwnerToMember SingleOrDefault(Expression<Func<OwnerToMember, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            OwnerToMember single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<OwnerToMember, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<OwnerToMember, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<OwnerToMember> Find(Expression<Func<OwnerToMember, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<OwnerToMember> Find(Expression<Func<OwnerToMember, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<OwnerToMember> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<OwnerToMember> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<OwnerToMember> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<OwnerToMember> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<OwnerToMember> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<OwnerToMember> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "OTMID";
        }

        public object KeyValue()
        {
            return this.OTMID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.Email.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(OwnerToMember)){
                OwnerToMember compare=(OwnerToMember)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.OTMID;
        }
        
        public string DescriptorValue()
        {
                            return this.Email.ToString();
                    }

        public string DescriptorColumn() {
            return "Email";
        }
        public static string GetKeyColumn()
        {
            return "OTMID";
        }        
        public static string GetDescriptorColumn()
        {
            return "Email";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _OTMID;
        public int OTMID
        {
            get { return _OTMID; }
            set
            {
                if(_OTMID!=value){
                    _OTMID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="OTMID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _OwnerMemberID;
        public int? OwnerMemberID
        {
            get { return _OwnerMemberID; }
            set
            {
                if(_OwnerMemberID!=value){
                    _OwnerMemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="OwnerMemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                if(_Email!=value){
                    _Email=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Email");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _NameAlias;
        public string NameAlias
        {
            get { return _NameAlias; }
            set
            {
                if(_NameAlias!=value){
                    _NameAlias=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="NameAlias");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _RoleID;
        public int? RoleID
        {
            get { return _RoleID; }
            set
            {
                if(_RoleID!=value){
                    _RoleID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _NAME;
        public string NAME
        {
            get { return _NAME; }
            set
            {
                if(_NAME!=value){
                    _NAME=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="NAME");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<OwnerToMember, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the DiscussionToFolder table in the avaimaTest0001 Database.
    /// </summary>
    public partial class DiscussionToFolder: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<DiscussionToFolder> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<DiscussionToFolder>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<DiscussionToFolder> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(DiscussionToFolder item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                DiscussionToFolder item=new DiscussionToFolder();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<DiscussionToFolder> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public DiscussionToFolder(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                DiscussionToFolder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DiscussionToFolder>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public DiscussionToFolder(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public DiscussionToFolder(Expression<Func<DiscussionToFolder, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<DiscussionToFolder> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<DiscussionToFolder> _repo;
            
            if(db.TestMode){
                DiscussionToFolder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DiscussionToFolder>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<DiscussionToFolder> GetRepo(){
            return GetRepo("","");
        }
        
        public static DiscussionToFolder SingleOrDefault(Expression<Func<DiscussionToFolder, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            DiscussionToFolder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static DiscussionToFolder SingleOrDefault(Expression<Func<DiscussionToFolder, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            DiscussionToFolder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<DiscussionToFolder, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<DiscussionToFolder, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<DiscussionToFolder> Find(Expression<Func<DiscussionToFolder, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<DiscussionToFolder> Find(Expression<Func<DiscussionToFolder, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<DiscussionToFolder> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<DiscussionToFolder> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<DiscussionToFolder> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<DiscussionToFolder> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<DiscussionToFolder> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<DiscussionToFolder> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "DTFID";
        }

        public object KeyValue()
        {
            return this.DTFID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(DiscussionToFolder)){
                DiscussionToFolder compare=(DiscussionToFolder)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.DTFID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "DTFID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _DTFID;
        public int DTFID
        {
            get { return _DTFID; }
            set
            {
                if(_DTFID!=value){
                    _DTFID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DTFID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _RoleID;
        public int? RoleID
        {
            get { return _RoleID; }
            set
            {
                if(_RoleID!=value){
                    _RoleID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _DiscussionID;
        public int? DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _FolderID;
        public int? FolderID
        {
            get { return _FolderID; }
            set
            {
                if(_FolderID!=value){
                    _FolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _Status;
        public int? Status
        {
            get { return _Status; }
            set
            {
                if(_Status!=value){
                    _Status=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Status");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Alias;
        public string Alias
        {
            get { return _Alias; }
            set
            {
                if(_Alias!=value){
                    _Alias=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Alias");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _Unread;
        public int? Unread
        {
            get { return _Unread; }
            set
            {
                if(_Unread!=value){
                    _Unread=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Unread");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<DiscussionToFolder, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the DisallowedContents table in the avaimaTest0001 Database.
    /// </summary>
    public partial class DisallowedContent: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<DisallowedContent> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<DisallowedContent>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<DisallowedContent> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(DisallowedContent item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                DisallowedContent item=new DisallowedContent();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<DisallowedContent> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public DisallowedContent(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                DisallowedContent.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DisallowedContent>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public DisallowedContent(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public DisallowedContent(Expression<Func<DisallowedContent, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<DisallowedContent> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<DisallowedContent> _repo;
            
            if(db.TestMode){
                DisallowedContent.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<DisallowedContent>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<DisallowedContent> GetRepo(){
            return GetRepo("","");
        }
        
        public static DisallowedContent SingleOrDefault(Expression<Func<DisallowedContent, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            DisallowedContent single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static DisallowedContent SingleOrDefault(Expression<Func<DisallowedContent, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            DisallowedContent single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<DisallowedContent, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<DisallowedContent, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<DisallowedContent> Find(Expression<Func<DisallowedContent, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<DisallowedContent> Find(Expression<Func<DisallowedContent, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<DisallowedContent> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<DisallowedContent> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<DisallowedContent> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<DisallowedContent> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<DisallowedContent> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<DisallowedContent> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "DACID";
        }

        public object KeyValue()
        {
            return this.DACID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(DisallowedContent)){
                DisallowedContent compare=(DisallowedContent)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.DACID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "DACID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _DACID;
        public int DACID
        {
            get { return _DACID; }
            set
            {
                if(_DACID!=value){
                    _DACID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DACID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _DiscussionID;
        public int? DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _FolderID;
        public int? FolderID
        {
            get { return _FolderID; }
            set
            {
                if(_FolderID!=value){
                    _FolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Contents;
        public string Contents
        {
            get { return _Contents; }
            set
            {
                if(_Contents!=value){
                    _Contents=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Contents");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<DisallowedContent, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the MemberNotes table in the avaimaTest0001 Database.
    /// </summary>
    public partial class MemberNote: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<MemberNote> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<MemberNote>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<MemberNote> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(MemberNote item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                MemberNote item=new MemberNote();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<MemberNote> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public MemberNote(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                MemberNote.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberNote>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public MemberNote(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public MemberNote(Expression<Func<MemberNote, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<MemberNote> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<MemberNote> _repo;
            
            if(db.TestMode){
                MemberNote.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberNote>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<MemberNote> GetRepo(){
            return GetRepo("","");
        }
        
        public static MemberNote SingleOrDefault(Expression<Func<MemberNote, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            MemberNote single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static MemberNote SingleOrDefault(Expression<Func<MemberNote, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            MemberNote single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<MemberNote, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<MemberNote, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<MemberNote> Find(Expression<Func<MemberNote, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<MemberNote> Find(Expression<Func<MemberNote, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<MemberNote> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<MemberNote> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<MemberNote> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<MemberNote> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<MemberNote> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<MemberNote> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "NotesID";
        }

        public object KeyValue()
        {
            return this.NotesID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.Notes.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(MemberNote)){
                MemberNote compare=(MemberNote)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.NotesID;
        }
        
        public string DescriptorValue()
        {
                            return this.Notes.ToString();
                    }

        public string DescriptorColumn() {
            return "Notes";
        }
        public static string GetKeyColumn()
        {
            return "NotesID";
        }        
        public static string GetDescriptorColumn()
        {
            return "Notes";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _NotesID;
        public int NotesID
        {
            get { return _NotesID; }
            set
            {
                if(_NotesID!=value){
                    _NotesID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="NotesID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Notes;
        public string Notes
        {
            get { return _Notes; }
            set
            {
                if(_Notes!=value){
                    _Notes=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Notes");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _DiscussionID;
        public int? DiscussionID
        {
            get { return _DiscussionID; }
            set
            {
                if(_DiscussionID!=value){
                    _DiscussionID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="DiscussionID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<MemberNote, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the MemberDiscussionSource table in the avaimaTest0001 Database.
    /// </summary>
    public partial class MemberDiscussionSource: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<MemberDiscussionSource> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<MemberDiscussionSource>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<MemberDiscussionSource> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(MemberDiscussionSource item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                MemberDiscussionSource item=new MemberDiscussionSource();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<MemberDiscussionSource> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public MemberDiscussionSource(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                MemberDiscussionSource.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberDiscussionSource>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public MemberDiscussionSource(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public MemberDiscussionSource(Expression<Func<MemberDiscussionSource, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<MemberDiscussionSource> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<MemberDiscussionSource> _repo;
            
            if(db.TestMode){
                MemberDiscussionSource.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberDiscussionSource>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<MemberDiscussionSource> GetRepo(){
            return GetRepo("","");
        }
        
        public static MemberDiscussionSource SingleOrDefault(Expression<Func<MemberDiscussionSource, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            MemberDiscussionSource single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static MemberDiscussionSource SingleOrDefault(Expression<Func<MemberDiscussionSource, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            MemberDiscussionSource single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<MemberDiscussionSource, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<MemberDiscussionSource, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<MemberDiscussionSource> Find(Expression<Func<MemberDiscussionSource, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<MemberDiscussionSource> Find(Expression<Func<MemberDiscussionSource, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<MemberDiscussionSource> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<MemberDiscussionSource> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<MemberDiscussionSource> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<MemberDiscussionSource> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<MemberDiscussionSource> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<MemberDiscussionSource> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "id";
        }

        public object KeyValue()
        {
            return this.id;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.email.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(MemberDiscussionSource)){
                MemberDiscussionSource compare=(MemberDiscussionSource)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.id;
        }
        
        public string DescriptorValue()
        {
                            return this.email.ToString();
                    }

        public string DescriptorColumn() {
            return "email";
        }
        public static string GetKeyColumn()
        {
            return "id";
        }        
        public static string GetDescriptorColumn()
        {
            return "email";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _id;
        public int id
        {
            get { return _id; }
            set
            {
                if(_id!=value){
                    _id=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="id");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _email;
        public string email
        {
            get { return _email; }
            set
            {
                if(_email!=value){
                    _email=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="email");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _memberId;
        public int? memberId
        {
            get { return _memberId; }
            set
            {
                if(_memberId!=value){
                    _memberId=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="memberId");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _roleId;
        public int? roleId
        {
            get { return _roleId; }
            set
            {
                if(_roleId!=value){
                    _roleId=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="roleId");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _discussionId;
        public int? discussionId
        {
            get { return _discussionId; }
            set
            {
                if(_discussionId!=value){
                    _discussionId=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="discussionId");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _action;
        public string action
        {
            get { return _action; }
            set
            {
                if(_action!=value){
                    _action=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="action");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _source;
        public string source
        {
            get { return _source; }
            set
            {
                if(_source!=value){
                    _source=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="source");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<MemberDiscussionSource, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the EmailDetails table in the avaimaTest0001 Database.
    /// </summary>
    public partial class EmailDetail: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<EmailDetail> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<EmailDetail>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<EmailDetail> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(EmailDetail item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                EmailDetail item=new EmailDetail();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<EmailDetail> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public EmailDetail(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                EmailDetail.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<EmailDetail>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public EmailDetail(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public EmailDetail(Expression<Func<EmailDetail, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<EmailDetail> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<EmailDetail> _repo;
            
            if(db.TestMode){
                EmailDetail.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<EmailDetail>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<EmailDetail> GetRepo(){
            return GetRepo("","");
        }
        
        public static EmailDetail SingleOrDefault(Expression<Func<EmailDetail, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            EmailDetail single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static EmailDetail SingleOrDefault(Expression<Func<EmailDetail, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            EmailDetail single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<EmailDetail, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<EmailDetail, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<EmailDetail> Find(Expression<Func<EmailDetail, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<EmailDetail> Find(Expression<Func<EmailDetail, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<EmailDetail> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<EmailDetail> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<EmailDetail> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<EmailDetail> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<EmailDetail> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<EmailDetail> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "ID";
        }

        public object KeyValue()
        {
            return this.ID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.ToEmail.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(EmailDetail)){
                EmailDetail compare=(EmailDetail)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.ID;
        }
        
        public string DescriptorValue()
        {
                            return this.ToEmail.ToString();
                    }

        public string DescriptorColumn() {
            return "ToEmail";
        }
        public static string GetKeyColumn()
        {
            return "ID";
        }        
        public static string GetDescriptorColumn()
        {
            return "ToEmail";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                if(_ID!=value){
                    _ID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _ToEmail;
        public string ToEmail
        {
            get { return _ToEmail; }
            set
            {
                if(_ToEmail!=value){
                    _ToEmail=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ToEmail");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _FromName;
        public string FromName
        {
            get { return _FromName; }
            set
            {
                if(_FromName!=value){
                    _FromName=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FromName");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _replyEmail;
        public string replyEmail
        {
            get { return _replyEmail; }
            set
            {
                if(_replyEmail!=value){
                    _replyEmail=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="replyEmail");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _body;
        public string body
        {
            get { return _body; }
            set
            {
                if(_body!=value){
                    _body=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="body");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _subject;
        public string subject
        {
            get { return _subject; }
            set
            {
                if(_subject!=value){
                    _subject=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="subject");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _status;
        public int? status
        {
            get { return _status; }
            set
            {
                if(_status!=value){
                    _status=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="status");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Exception;
        public string Exception
        {
            get { return _Exception; }
            set
            {
                if(_Exception!=value){
                    _Exception=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Exception");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CreateDate;
        public DateTime? CreateDate
        {
            get { return _CreateDate; }
            set
            {
                if(_CreateDate!=value){
                    _CreateDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CreateDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ResendTry;
        public int? ResendTry
        {
            get { return _ResendTry; }
            set
            {
                if(_ResendTry!=value){
                    _ResendTry=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ResendTry");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<EmailDetail, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the MemberToFolder table in the avaimaTest0001 Database.
    /// </summary>
    public partial class MemberToFolder: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<MemberToFolder> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<MemberToFolder>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<MemberToFolder> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(MemberToFolder item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                MemberToFolder item=new MemberToFolder();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<MemberToFolder> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public MemberToFolder(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                MemberToFolder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberToFolder>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public MemberToFolder(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public MemberToFolder(Expression<Func<MemberToFolder, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<MemberToFolder> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<MemberToFolder> _repo;
            
            if(db.TestMode){
                MemberToFolder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<MemberToFolder>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<MemberToFolder> GetRepo(){
            return GetRepo("","");
        }
        
        public static MemberToFolder SingleOrDefault(Expression<Func<MemberToFolder, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            MemberToFolder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static MemberToFolder SingleOrDefault(Expression<Func<MemberToFolder, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            MemberToFolder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<MemberToFolder, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<MemberToFolder, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<MemberToFolder> Find(Expression<Func<MemberToFolder, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<MemberToFolder> Find(Expression<Func<MemberToFolder, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<MemberToFolder> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<MemberToFolder> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<MemberToFolder> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<MemberToFolder> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<MemberToFolder> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<MemberToFolder> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "MTFID";
        }

        public object KeyValue()
        {
            return this.MTFID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(MemberToFolder)){
                MemberToFolder compare=(MemberToFolder)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.MTFID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "MTFID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _MTFID;
        public int MTFID
        {
            get { return _MTFID; }
            set
            {
                if(_MTFID!=value){
                    _MTFID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MTFID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _MemberID;
        public int? MemberID
        {
            get { return _MemberID; }
            set
            {
                if(_MemberID!=value){
                    _MemberID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="MemberID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _RoleID;
        public int? RoleID
        {
            get { return _RoleID; }
            set
            {
                if(_RoleID!=value){
                    _RoleID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="RoleID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _ParentFolderID;
        public int? ParentFolderID
        {
            get { return _ParentFolderID; }
            set
            {
                if(_ParentFolderID!=value){
                    _ParentFolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ParentFolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        int? _FolderID;
        public int? FolderID
        {
            get { return _FolderID; }
            set
            {
                if(_FolderID!=value){
                    _FolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _ApplicableTo;
        public string ApplicableTo
        {
            get { return _ApplicableTo; }
            set
            {
                if(_ApplicableTo!=value){
                    _ApplicableTo=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ApplicableTo");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Alias;
        public string Alias
        {
            get { return _Alias; }
            set
            {
                if(_Alias!=value){
                    _Alias=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Alias");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<MemberToFolder, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
    
    
    /// <summary>
    /// A class which represents the Folders table in the avaimaTest0001 Database.
    /// </summary>
    public partial class Folder: IActiveRecord
    {
    
        #region Built-in testing
        static TestRepository<Folder> _testRepo;
        

        
        static void SetTestRepo(){
            _testRepo = _testRepo ?? new TestRepository<Folder>(new MyCCM.avaimaTest0001DB());
        }
        public static void ResetTestRepo(){
            _testRepo = null;
            SetTestRepo();
        }
        public static void Setup(List<Folder> testlist){
            SetTestRepo();
            foreach (var item in testlist)
            {
                _testRepo._items.Add(item);
            }
        }
        public static void Setup(Folder item) {
            SetTestRepo();
            _testRepo._items.Add(item);
        }
        public static void Setup(int testItems) {
            SetTestRepo();
            for(int i=0;i<testItems;i++){
                Folder item=new Folder();
                _testRepo._items.Add(item);
            }
        }
        
        public bool TestMode = false;


        #endregion

        IRepository<Folder> _repo;
        ITable tbl;
        bool _isNew;
        public bool IsNew(){
            return _isNew;
        }
        
        public void SetIsLoaded(bool isLoaded){
            _isLoaded=isLoaded;
            if(isLoaded)
                OnLoaded();
        }
        
        public void SetIsNew(bool isNew){
            _isNew=isNew;
        }
        bool _isLoaded;
        public bool IsLoaded(){
            return _isLoaded;
        }
                
        List<IColumn> _dirtyColumns;
        public bool IsDirty(){
            return _dirtyColumns.Count>0;
        }
        
        public List<IColumn> GetDirtyColumns (){
            return _dirtyColumns;
        }

        MyCCM.avaimaTest0001DB _db;
        public Folder(string connectionString, string providerName) {

            _db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            Init();            
         }
        void Init(){
            TestMode=this._db.DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            _dirtyColumns=new List<IColumn>();
            if(TestMode){
                Folder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Folder>(_db);
            }
            tbl=_repo.GetTable();
            SetIsNew(true);
            OnCreated();       

        }
        
        public Folder(){
             _db=new MyCCM.avaimaTest0001DB();
            Init();            
        }
        
       
        partial void OnCreated();
            
        partial void OnLoaded();
        
        partial void OnSaved();
        
        partial void OnChanged();
        
        public IList<IColumn> Columns{
            get{
                return tbl.Columns;
            }
        }

        public Folder(Expression<Func<Folder, bool>> expression):this() {

            SetIsLoaded(_repo.Load(this,expression));
        }
        
       
        
        internal static IRepository<Folder> GetRepo(string connectionString, string providerName){
            MyCCM.avaimaTest0001DB db;
            if(String.IsNullOrEmpty(connectionString)){
                db=new MyCCM.avaimaTest0001DB();
            }else{
                db=new MyCCM.avaimaTest0001DB(connectionString, providerName);
            }
            IRepository<Folder> _repo;
            
            if(db.TestMode){
                Folder.SetTestRepo();
                _repo=_testRepo;
            }else{
                _repo = new SubSonicRepository<Folder>(db);
            }
            return _repo;        
        }       
        
        internal static IRepository<Folder> GetRepo(){
            return GetRepo("","");
        }
        
        public static Folder SingleOrDefault(Expression<Func<Folder, bool>> expression) {

            var repo = GetRepo();
            var results=repo.Find(expression);
            Folder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
                single.OnLoaded();
                single.SetIsLoaded(true);
                single.SetIsNew(false);
            }

            return single;
        }      
        
        public static Folder SingleOrDefault(Expression<Func<Folder, bool>> expression,string connectionString, string providerName) {
            var repo = GetRepo(connectionString,providerName);
            var results=repo.Find(expression);
            Folder single=null;
            if(results.Count() > 0){
                single=results.ToList()[0];
            }

            return single;


        }
        
        
        public static bool Exists(Expression<Func<Folder, bool>> expression,string connectionString, string providerName) {
           
            return All(connectionString,providerName).Any(expression);
        }        
        public static bool Exists(Expression<Func<Folder, bool>> expression) {
           
            return All().Any(expression);
        }        

        public static IList<Folder> Find(Expression<Func<Folder, bool>> expression) {
            
            var repo = GetRepo();
            return repo.Find(expression).ToList();
        }
        
        public static IList<Folder> Find(Expression<Func<Folder, bool>> expression,string connectionString, string providerName) {

            var repo = GetRepo(connectionString,providerName);
            return repo.Find(expression).ToList();

        }
        public static IQueryable<Folder> All(string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetAll();
        }
        public static IQueryable<Folder> All() {
            return GetRepo().GetAll();
        }
        
        public static PagedList<Folder> GetPaged(string sortBy, int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(sortBy, pageIndex, pageSize);
        }
      
        public static PagedList<Folder> GetPaged(string sortBy, int pageIndex, int pageSize) {
            return GetRepo().GetPaged(sortBy, pageIndex, pageSize);
        }

        public static PagedList<Folder> GetPaged(int pageIndex, int pageSize,string connectionString, string providerName) {
            return GetRepo(connectionString,providerName).GetPaged(pageIndex, pageSize);
            
        }


        public static PagedList<Folder> GetPaged(int pageIndex, int pageSize) {
            return GetRepo().GetPaged(pageIndex, pageSize);
            
        }

        public string KeyName()
        {
            return "FolderID";
        }

        public object KeyValue()
        {
            return this.FolderID;
        }
        
        public void SetKeyValue(object value) {
            if (value != null && value!=DBNull.Value) {
                var settable = value.ChangeTypeTo<int>();
                this.GetType().GetProperty(this.KeyName()).SetValue(this, settable, null);
            }
        }
        
        public override string ToString(){
                            return this.InstanceID.ToString();
                    }

        public override bool Equals(object obj){
            if(obj.GetType()==typeof(Folder)){
                Folder compare=(Folder)obj;
                return compare.KeyValue()==this.KeyValue();
            }else{
                return base.Equals(obj);
            }
        }

        
        public override int GetHashCode() {
            return this.FolderID;
        }
        
        public string DescriptorValue()
        {
                            return this.InstanceID.ToString();
                    }

        public string DescriptorColumn() {
            return "InstanceID";
        }
        public static string GetKeyColumn()
        {
            return "FolderID";
        }        
        public static string GetDescriptorColumn()
        {
            return "InstanceID";
        }
        
        #region ' Foreign Keys '
        #endregion
        

        int _FolderID;
        public int FolderID
        {
            get { return _FolderID; }
            set
            {
                if(_FolderID!=value){
                    _FolderID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="FolderID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _InstanceID;
        public string InstanceID
        {
            get { return _InstanceID; }
            set
            {
                if(_InstanceID!=value){
                    _InstanceID=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="InstanceID");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if(_Title!=value){
                    _Title=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Title");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                if(_Description!=value){
                    _Description=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Description");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _CrtDate;
        public DateTime? CrtDate
        {
            get { return _CrtDate; }
            set
            {
                if(_CrtDate!=value){
                    _CrtDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="CrtDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        DateTime? _ModDate;
        public DateTime? ModDate
        {
            get { return _ModDate; }
            set
            {
                if(_ModDate!=value){
                    _ModDate=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="ModDate");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }

        bool? _Active;
        public bool? Active
        {
            get { return _Active; }
            set
            {
                if(_Active!=value){
                    _Active=value;
                    var col=tbl.Columns.SingleOrDefault(x=>x.Name=="Active");
                    if(col!=null){
                        if(!_dirtyColumns.Any(x=>x.Name==col.Name) && _isLoaded){
                            _dirtyColumns.Add(col);
                        }
                    }
                    OnChanged();
                }
            }
        }



        public DbCommand GetUpdateCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToUpdateQuery(_db.Provider).GetCommand().ToDbCommand();
            
        }
        public DbCommand GetInsertCommand() {
 
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToInsertQuery(_db.Provider).GetCommand().ToDbCommand();
        }
        
        public DbCommand GetDeleteCommand() {
            if(TestMode)
                return _db.DataProvider.CreateCommand();
            else
                return this.ToDeleteQuery(_db.Provider).GetCommand().ToDbCommand();
        }
       
        
        public void Update(){
            Update(_db.DataProvider);
        }
        
        public void Update(IDataProvider provider){
        
            
            if(this._dirtyColumns.Count>0){
                _repo.Update(this,provider);
                _dirtyColumns.Clear();    
            }
            OnSaved();
       }
 
        public void Add(){
            Add(_db.DataProvider);
        }
        
        
       
        public void Add(IDataProvider provider){

            
            var key=KeyValue();
            if(key==null){
                var newKey=_repo.Add(this,provider);
                this.SetKeyValue(newKey);
            }else{
                _repo.Add(this,provider);
            }
            SetIsNew(false);
            OnSaved();
        }
        
                
        
        public void Save() {
            Save(_db.DataProvider);
        }      
        public void Save(IDataProvider provider) {
            
           
            if (_isNew) {
                Add(provider);
                
            } else {
                Update(provider);
            }
            
        }

        

        public void Delete(IDataProvider provider) {
                   
                 
            _repo.Delete(KeyValue());
            
                    }


        public void Delete() {
            Delete(_db.DataProvider);
        }


        public static void Delete(Expression<Func<Folder, bool>> expression) {
            var repo = GetRepo();
            
       
            
            repo.DeleteMany(expression);
            
        }

        

        public void Load(IDataReader rdr) {
            Load(rdr, true);
        }
        public void Load(IDataReader rdr, bool closeReader) {
            if (rdr.Read()) {

                try {
                    rdr.Load(this);
                    SetIsNew(false);
                    SetIsLoaded(true);
                } catch {
                    SetIsLoaded(false);
                    throw;
                }
            }else{
                SetIsLoaded(false);
            }

            if (closeReader)
                rdr.Dispose();
        }
        

    } 
}
