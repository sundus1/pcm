


using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using SubSonic.DataProviders;
using SubSonic.Extensions;
using SubSonic.Linq.Structure;
using SubSonic.Query;
using SubSonic.Schema;
using System.Data.Common;
using System.Collections.Generic;

namespace MyCCM
{
    public partial class avaimaTest0001DB : IQuerySurface
    {

        public IDataProvider DataProvider;
        public DbQueryProvider provider;
        
        public static IDataProvider DefaultDataProvider { get; set; }

        public bool TestMode
		{
            get
			{
                return DataProvider.ConnectionString.Equals("test", StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public avaimaTest0001DB() 
        {
            if (DefaultDataProvider == null) {
                DataProvider = ProviderFactory.GetProvider("ConnectionString");
            }
            else {
                DataProvider = DefaultDataProvider;
            }
            Init();
        }

        public avaimaTest0001DB(string connectionStringName)
        {
            DataProvider = ProviderFactory.GetProvider(connectionStringName);
            Init();
        }

		public avaimaTest0001DB(string connectionString, string providerName)
        {
            DataProvider = ProviderFactory.GetProvider(connectionString,providerName);
            Init();
        }

		public ITable FindByPrimaryKey(string pkName)
        {
            return DataProvider.Schema.Tables.SingleOrDefault(x => x.PrimaryKey.Name.Equals(pkName, StringComparison.InvariantCultureIgnoreCase));
        }

        public Query<T> GetQuery<T>()
        {
            return new Query<T>(provider);
        }
        
        public ITable FindTable(string tableName)
        {
            return DataProvider.FindTable(tableName);
        }
               
        public IDataProvider Provider
        {
            get { return DataProvider; }
            set {DataProvider=value;}
        }
        
        public DbQueryProvider QueryProvider
        {
            get { return provider; }
        }
        
        BatchQuery _batch = null;
        public void Queue<T>(IQueryable<T> qry)
        {
            if (_batch == null)
                _batch = new BatchQuery(Provider, QueryProvider);
            _batch.Queue(qry);
        }

        public void Queue(ISqlQuery qry)
        {
            if (_batch == null)
                _batch = new BatchQuery(Provider, QueryProvider);
            _batch.Queue(qry);
        }

        public void ExecuteTransaction(IList<DbCommand> commands)
		{
            if(!TestMode)
			{
                using(var connection = commands[0].Connection)
				{
                   if (connection.State == ConnectionState.Closed)
                        connection.Open();
                   
                   using (var trans = connection.BeginTransaction()) 
				   {
                        foreach (var cmd in commands) 
						{
                            cmd.Transaction = trans;
                            cmd.Connection = connection;
                            cmd.ExecuteNonQuery();
                        }
                        trans.Commit();
                    }
                    connection.Close();
                }
            }
        }

        public IDataReader ExecuteBatch()
        {
            if (_batch == null)
                throw new InvalidOperationException("There's nothing in the queue");
            if(!TestMode)
                return _batch.ExecuteReader();
            return null;
        }
			
        public Query<Role> Roles { get; set; }
        public Query<Project> Projects { get; set; }
        public Query<MemberToProject> MemberToProjects { get; set; }
        public Query<Tag> Tags { get; set; }
        public Query<Member> Members { get; set; }
        public Query<MemberDiscussion> MemberDiscussions { get; set; }
        public Query<Discussion> Discussions { get; set; }
        public Query<DiscussionFile> DiscussionFiles { get; set; }
        public Query<OwnerToMember> OwnerToMembers { get; set; }
        public Query<DiscussionToFolder> DiscussionToFolders { get; set; }
        public Query<DisallowedContent> DisallowedContents { get; set; }
        public Query<MemberNote> MemberNotes { get; set; }
        public Query<MemberDiscussionSource> MemberDiscussionSources { get; set; }
        public Query<EmailDetail> EmailDetails { get; set; }
        public Query<MemberToFolder> MemberToFolders { get; set; }
        public Query<Folder> Folders { get; set; }

			

        #region ' Aggregates and SubSonic Queries '
        public Select SelectColumns(params string[] columns)
        {
            return new Select(DataProvider, columns);
        }

        public Select Select
        {
            get { return new Select(this.Provider); }
        }

        public Insert Insert
		{
            get { return new Insert(this.Provider); }
        }

        public Update<T> Update<T>() where T:new()
		{
            return new Update<T>(this.Provider);
        }

        public SqlQuery Delete<T>(Expression<Func<T,bool>> column) where T:new()
        {
            LambdaExpression lamda = column;
            SqlQuery result = new Delete<T>(this.Provider);
            result = result.From<T>();
            result.Constraints=lamda.ParseConstraints().ToList();
            return result;
        }

        public SqlQuery Max<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = DataProvider.FindTable(objectName).Name;
            return new Select(DataProvider, new Aggregate(colName, AggregateFunction.Max)).From(tableName);
        }

        public SqlQuery Min<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Min)).From(tableName);
        }

        public SqlQuery Sum<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Sum)).From(tableName);
        }

        public SqlQuery Avg<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Avg)).From(tableName);
        }

        public SqlQuery Count<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Count)).From(tableName);
        }

        public SqlQuery Variance<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.Var)).From(tableName);
        }

        public SqlQuery StandardDeviation<T>(Expression<Func<T,object>> column)
        {
            LambdaExpression lamda = column;
            string colName = lamda.ParseObjectValue();
            string objectName = typeof(T).Name;
            string tableName = this.Provider.FindTable(objectName).Name;
            return new Select(this.Provider, new Aggregate(colName, AggregateFunction.StDev)).From(tableName);
        }

        #endregion

        void Init()
        {
            provider = new DbQueryProvider(this.Provider);

            #region ' Query Defs '
            Roles = new Query<Role>(provider);
            Projects = new Query<Project>(provider);
            MemberToProjects = new Query<MemberToProject>(provider);
            Tags = new Query<Tag>(provider);
            Members = new Query<Member>(provider);
            MemberDiscussions = new Query<MemberDiscussion>(provider);
            Discussions = new Query<Discussion>(provider);
            DiscussionFiles = new Query<DiscussionFile>(provider);
            OwnerToMembers = new Query<OwnerToMember>(provider);
            DiscussionToFolders = new Query<DiscussionToFolder>(provider);
            DisallowedContents = new Query<DisallowedContent>(provider);
            MemberNotes = new Query<MemberNote>(provider);
            MemberDiscussionSources = new Query<MemberDiscussionSource>(provider);
            EmailDetails = new Query<EmailDetail>(provider);
            MemberToFolders = new Query<MemberToFolder>(provider);
            Folders = new Query<Folder>(provider);
            #endregion


            #region ' Schemas '
        	if(DataProvider.Schema.Tables.Count == 0)
			{
            	DataProvider.Schema.Tables.Add(new RolesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new ProjectsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MemberToProjectTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new TagsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MembersTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MemberDiscussionsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new DiscussionsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new DiscussionFilesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new OwnerToMemberTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new DiscussionToFolderTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new DisallowedContentsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MemberNotesTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MemberDiscussionSourceTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new EmailDetailsTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new MemberToFolderTable(DataProvider));
            	DataProvider.Schema.Tables.Add(new FoldersTable(DataProvider));
            }
            #endregion
        }
        

        #region ' Helpers '
            
        internal static DateTime DateTimeNowTruncatedDownToSecond() {
            var now = DateTime.Now;
            return now.AddTicks(-now.Ticks % TimeSpan.TicksPerSecond);
        }

        #endregion

    }
}