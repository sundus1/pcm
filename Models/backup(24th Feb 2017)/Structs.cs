


using System;
using SubSonic.Schema;
using System.Collections.Generic;
using SubSonic.DataProviders;
using System.Data;

namespace MyCCM {
	
        /// <summary>
        /// Table: Roles
        /// Primary Key: RoleID
        /// </summary>

        public class RolesTable: DatabaseTable {
            
            public RolesTable(IDataProvider provider):base("Roles",provider){
                ClassName = "Role";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("RoleID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("RoleTitle", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("RoleDescription", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn RoleID{
                get{
                    return this.GetColumn("RoleID");
                }
            }
				
   			public static string RoleIDColumn{
			      get{
        			return "RoleID";
      			}
		    }
            
            public IColumn RoleTitle{
                get{
                    return this.GetColumn("RoleTitle");
                }
            }
				
   			public static string RoleTitleColumn{
			      get{
        			return "RoleTitle";
      			}
		    }
            
            public IColumn RoleDescription{
                get{
                    return this.GetColumn("RoleDescription");
                }
            }
				
   			public static string RoleDescriptionColumn{
			      get{
        			return "RoleDescription";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: Projects
        /// Primary Key: ProjectID
        /// </summary>

        public class ProjectsTable: DatabaseTable {
            
            public ProjectsTable(IDataProvider provider):base("Projects",provider){
                ClassName = "Project";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("ProjectID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("OwnerID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("Parent_ProjectID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Description", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Status", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn ProjectID{
                get{
                    return this.GetColumn("ProjectID");
                }
            }
				
   			public static string ProjectIDColumn{
			      get{
        			return "ProjectID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn OwnerID{
                get{
                    return this.GetColumn("OwnerID");
                }
            }
				
   			public static string OwnerIDColumn{
			      get{
        			return "OwnerID";
      			}
		    }
            
            public IColumn Parent_ProjectID{
                get{
                    return this.GetColumn("Parent_ProjectID");
                }
            }
				
   			public static string Parent_ProjectIDColumn{
			      get{
        			return "Parent_ProjectID";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
            public IColumn Description{
                get{
                    return this.GetColumn("Description");
                }
            }
				
   			public static string DescriptionColumn{
			      get{
        			return "Description";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Status{
                get{
                    return this.GetColumn("Status");
                }
            }
				
   			public static string StatusColumn{
			      get{
        			return "Status";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: MemberToProject
        /// Primary Key: MTPID
        /// </summary>

        public class MemberToProjectTable: DatabaseTable {
            
            public MemberToProjectTable(IDataProvider provider):base("MemberToProject",provider){
                ClassName = "MemberToProject";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("MTPID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("RoleID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ProjectID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ApplicableTo", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn MTPID{
                get{
                    return this.GetColumn("MTPID");
                }
            }
				
   			public static string MTPIDColumn{
			      get{
        			return "MTPID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn RoleID{
                get{
                    return this.GetColumn("RoleID");
                }
            }
				
   			public static string RoleIDColumn{
			      get{
        			return "RoleID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn ProjectID{
                get{
                    return this.GetColumn("ProjectID");
                }
            }
				
   			public static string ProjectIDColumn{
			      get{
        			return "ProjectID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn ApplicableTo{
                get{
                    return this.GetColumn("ApplicableTo");
                }
            }
				
   			public static string ApplicableToColumn{
			      get{
        			return "ApplicableTo";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: Tags
        /// Primary Key: TagID
        /// </summary>

        public class TagsTable: DatabaseTable {
            
            public TagsTable(IDataProvider provider):base("Tags",provider){
                ClassName = "Tag";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("TagID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Tag", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Created", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Modified", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn TagID{
                get{
                    return this.GetColumn("TagID");
                }
            }
				
   			public static string TagIDColumn{
			      get{
        			return "TagID";
      			}
		    }
            
            public IColumn Tag{
                get{
                    return this.GetColumn("Tag");
                }
            }
				
   			public static string TagColumn{
			      get{
        			return "Tag";
      			}
		    }
            
            public IColumn Created{
                get{
                    return this.GetColumn("Created");
                }
            }
				
   			public static string CreatedColumn{
			      get{
        			return "Created";
      			}
		    }
            
            public IColumn Modified{
                get{
                    return this.GetColumn("Modified");
                }
            }
				
   			public static string ModifiedColumn{
			      get{
        			return "Modified";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: Members
        /// Primary Key: MemberID
        /// </summary>

        public class MembersTable: DatabaseTable {
            
            public MembersTable(IDataProvider provider):base("Members",provider){
                ClassName = "Member";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("OwnerID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("FirstName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("NickName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("Email", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("ImagePath", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("LastName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });
                    
                
                
            }

            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn OwnerID{
                get{
                    return this.GetColumn("OwnerID");
                }
            }
				
   			public static string OwnerIDColumn{
			      get{
        			return "OwnerID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn FirstName{
                get{
                    return this.GetColumn("FirstName");
                }
            }
				
   			public static string FirstNameColumn{
			      get{
        			return "FirstName";
      			}
		    }
            
            public IColumn NickName{
                get{
                    return this.GetColumn("NickName");
                }
            }
				
   			public static string NickNameColumn{
			      get{
        			return "NickName";
      			}
		    }
            
            public IColumn Email{
                get{
                    return this.GetColumn("Email");
                }
            }
				
   			public static string EmailColumn{
			      get{
        			return "Email";
      			}
		    }
            
            public IColumn ImagePath{
                get{
                    return this.GetColumn("ImagePath");
                }
            }
				
   			public static string ImagePathColumn{
			      get{
        			return "ImagePath";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn LastName{
                get{
                    return this.GetColumn("LastName");
                }
            }
				
   			public static string LastNameColumn{
			      get{
        			return "LastName";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: MemberDiscussions
        /// Primary Key: MemberDisID
        /// </summary>

        public class MemberDiscussionsTable: DatabaseTable {
            
            public MemberDiscussionsTable(IDataProvider provider):base("MemberDiscussions",provider){
                ClassName = "MemberDiscussion";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("MemberDisID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("TeamMember", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("DiscussionBlobID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn MemberDisID{
                get{
                    return this.GetColumn("MemberDisID");
                }
            }
				
   			public static string MemberDisIDColumn{
			      get{
        			return "MemberDisID";
      			}
		    }
            
            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn TeamMember{
                get{
                    return this.GetColumn("TeamMember");
                }
            }
				
   			public static string TeamMemberColumn{
			      get{
        			return "TeamMember";
      			}
		    }
            
            public IColumn DiscussionBlobID{
                get{
                    return this.GetColumn("DiscussionBlobID");
                }
            }
				
   			public static string DiscussionBlobIDColumn{
			      get{
        			return "DiscussionBlobID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: Discussions
        /// Primary Key: DiscussionID
        /// </summary>

        public class DiscussionsTable: DatabaseTable {
            
            public DiscussionsTable(IDataProvider provider):base("Discussions",provider){
                ClassName = "Discussion";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ProjectID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 300
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Status", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("isHourly", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("HourRequiredEachPost", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ZeroHourPost", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("IsHelp", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("HelpEmail", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 255
                });

                Columns.Add(new DatabaseColumn("HideHours", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("SummaryAllow", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Multiple", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Duration", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("EmailContent", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("SummarySentDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("sendsummary", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn ProjectID{
                get{
                    return this.GetColumn("ProjectID");
                }
            }
				
   			public static string ProjectIDColumn{
			      get{
        			return "ProjectID";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Status{
                get{
                    return this.GetColumn("Status");
                }
            }
				
   			public static string StatusColumn{
			      get{
        			return "Status";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn isHourly{
                get{
                    return this.GetColumn("isHourly");
                }
            }
				
   			public static string isHourlyColumn{
			      get{
        			return "isHourly";
      			}
		    }
            
            public IColumn HourRequiredEachPost{
                get{
                    return this.GetColumn("HourRequiredEachPost");
                }
            }
				
   			public static string HourRequiredEachPostColumn{
			      get{
        			return "HourRequiredEachPost";
      			}
		    }
            
            public IColumn ZeroHourPost{
                get{
                    return this.GetColumn("ZeroHourPost");
                }
            }
				
   			public static string ZeroHourPostColumn{
			      get{
        			return "ZeroHourPost";
      			}
		    }
            
            public IColumn IsHelp{
                get{
                    return this.GetColumn("IsHelp");
                }
            }
				
   			public static string IsHelpColumn{
			      get{
        			return "IsHelp";
      			}
		    }
            
            public IColumn HelpEmail{
                get{
                    return this.GetColumn("HelpEmail");
                }
            }
				
   			public static string HelpEmailColumn{
			      get{
        			return "HelpEmail";
      			}
		    }
            
            public IColumn HideHours{
                get{
                    return this.GetColumn("HideHours");
                }
            }
				
   			public static string HideHoursColumn{
			      get{
        			return "HideHours";
      			}
		    }
            
            public IColumn SummaryAllow{
                get{
                    return this.GetColumn("SummaryAllow");
                }
            }
				
   			public static string SummaryAllowColumn{
			      get{
        			return "SummaryAllow";
      			}
		    }
            
            public IColumn Multiple{
                get{
                    return this.GetColumn("Multiple");
                }
            }
				
   			public static string MultipleColumn{
			      get{
        			return "Multiple";
      			}
		    }
            
            public IColumn Duration{
                get{
                    return this.GetColumn("Duration");
                }
            }
				
   			public static string DurationColumn{
			      get{
        			return "Duration";
      			}
		    }
            
            public IColumn EmailContent{
                get{
                    return this.GetColumn("EmailContent");
                }
            }
				
   			public static string EmailContentColumn{
			      get{
        			return "EmailContent";
      			}
		    }
            
            public IColumn SummarySentDate{
                get{
                    return this.GetColumn("SummarySentDate");
                }
            }
				
   			public static string SummarySentDateColumn{
			      get{
        			return "SummarySentDate";
      			}
		    }
            
            public IColumn sendsummary{
                get{
                    return this.GetColumn("sendsummary");
                }
            }
				
   			public static string sendsummaryColumn{
			      get{
        			return "sendsummary";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: DiscussionFiles
        /// Primary Key: DisFileID
        /// </summary>

        public class DiscussionFilesTable: DatabaseTable {
            
            public DiscussionFilesTable(IDataProvider provider):base("DiscussionFiles",provider){
                ClassName = "DiscussionFile";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("DisFileID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("MemberDisID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ProjectID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("FileName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("FileType", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("FilePath", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("FolderID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn DisFileID{
                get{
                    return this.GetColumn("DisFileID");
                }
            }
				
   			public static string DisFileIDColumn{
			      get{
        			return "DisFileID";
      			}
		    }
            
            public IColumn MemberDisID{
                get{
                    return this.GetColumn("MemberDisID");
                }
            }
				
   			public static string MemberDisIDColumn{
			      get{
        			return "MemberDisID";
      			}
		    }
            
            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn ProjectID{
                get{
                    return this.GetColumn("ProjectID");
                }
            }
				
   			public static string ProjectIDColumn{
			      get{
        			return "ProjectID";
      			}
		    }
            
            public IColumn FileName{
                get{
                    return this.GetColumn("FileName");
                }
            }
				
   			public static string FileNameColumn{
			      get{
        			return "FileName";
      			}
		    }
            
            public IColumn FileType{
                get{
                    return this.GetColumn("FileType");
                }
            }
				
   			public static string FileTypeColumn{
			      get{
        			return "FileType";
      			}
		    }
            
            public IColumn FilePath{
                get{
                    return this.GetColumn("FilePath");
                }
            }
				
   			public static string FilePathColumn{
			      get{
        			return "FilePath";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn FolderID{
                get{
                    return this.GetColumn("FolderID");
                }
            }
				
   			public static string FolderIDColumn{
			      get{
        			return "FolderID";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: OwnerToMember
        /// Primary Key: OTMID
        /// </summary>

        public class OwnerToMemberTable: DatabaseTable {
            
            public OwnerToMemberTable(IDataProvider provider):base("OwnerToMember",provider){
                ClassName = "OwnerToMember";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("OTMID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("OwnerMemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Email", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("NameAlias", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 500
                });

                Columns.Add(new DatabaseColumn("RoleID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("NAME", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });
                    
                
                
            }

            public IColumn OTMID{
                get{
                    return this.GetColumn("OTMID");
                }
            }
				
   			public static string OTMIDColumn{
			      get{
        			return "OTMID";
      			}
		    }
            
            public IColumn OwnerMemberID{
                get{
                    return this.GetColumn("OwnerMemberID");
                }
            }
				
   			public static string OwnerMemberIDColumn{
			      get{
        			return "OwnerMemberID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn Email{
                get{
                    return this.GetColumn("Email");
                }
            }
				
   			public static string EmailColumn{
			      get{
        			return "Email";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn NameAlias{
                get{
                    return this.GetColumn("NameAlias");
                }
            }
				
   			public static string NameAliasColumn{
			      get{
        			return "NameAlias";
      			}
		    }
            
            public IColumn RoleID{
                get{
                    return this.GetColumn("RoleID");
                }
            }
				
   			public static string RoleIDColumn{
			      get{
        			return "RoleID";
      			}
		    }
            
            public IColumn NAME{
                get{
                    return this.GetColumn("NAME");
                }
            }
				
   			public static string NAMEColumn{
			      get{
        			return "NAME";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: DiscussionToFolder
        /// Primary Key: DTFID
        /// </summary>

        public class DiscussionToFolderTable: DatabaseTable {
            
            public DiscussionToFolderTable(IDataProvider provider):base("DiscussionToFolder",provider){
                ClassName = "DiscussionToFolder";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("DTFID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("RoleID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("FolderID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Status", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Alias", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("Unread", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn DTFID{
                get{
                    return this.GetColumn("DTFID");
                }
            }
				
   			public static string DTFIDColumn{
			      get{
        			return "DTFID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn RoleID{
                get{
                    return this.GetColumn("RoleID");
                }
            }
				
   			public static string RoleIDColumn{
			      get{
        			return "RoleID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn FolderID{
                get{
                    return this.GetColumn("FolderID");
                }
            }
				
   			public static string FolderIDColumn{
			      get{
        			return "FolderID";
      			}
		    }
            
            public IColumn Status{
                get{
                    return this.GetColumn("Status");
                }
            }
				
   			public static string StatusColumn{
			      get{
        			return "Status";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn Alias{
                get{
                    return this.GetColumn("Alias");
                }
            }
				
   			public static string AliasColumn{
			      get{
        			return "Alias";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
            public IColumn Unread{
                get{
                    return this.GetColumn("Unread");
                }
            }
				
   			public static string UnreadColumn{
			      get{
        			return "Unread";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: DisallowedContents
        /// Primary Key: DACID
        /// </summary>

        public class DisallowedContentsTable: DatabaseTable {
            
            public DisallowedContentsTable(IDataProvider provider):base("DisallowedContents",provider){
                ClassName = "DisallowedContent";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("DACID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("FolderID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Contents", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn DACID{
                get{
                    return this.GetColumn("DACID");
                }
            }
				
   			public static string DACIDColumn{
			      get{
        			return "DACID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn FolderID{
                get{
                    return this.GetColumn("FolderID");
                }
            }
				
   			public static string FolderIDColumn{
			      get{
        			return "FolderID";
      			}
		    }
            
            public IColumn Contents{
                get{
                    return this.GetColumn("Contents");
                }
            }
				
   			public static string ContentsColumn{
			      get{
        			return "Contents";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: MemberNotes
        /// Primary Key: NotesID
        /// </summary>

        public class MemberNotesTable: DatabaseTable {
            
            public MemberNotesTable(IDataProvider provider):base("MemberNotes",provider){
                ClassName = "MemberNote";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("NotesID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Notes", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.String,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 250
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("DiscussionID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Remind", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn NotesID{
                get{
                    return this.GetColumn("NotesID");
                }
            }
				
   			public static string NotesIDColumn{
			      get{
        			return "NotesID";
      			}
		    }
            
            public IColumn Notes{
                get{
                    return this.GetColumn("Notes");
                }
            }
				
   			public static string NotesColumn{
			      get{
        			return "Notes";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn DiscussionID{
                get{
                    return this.GetColumn("DiscussionID");
                }
            }
				
   			public static string DiscussionIDColumn{
			      get{
        			return "DiscussionID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn Remind{
                get{
                    return this.GetColumn("Remind");
                }
            }
				
   			public static string RemindColumn{
			      get{
        			return "Remind";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: MemberDiscussionSource
        /// Primary Key: id
        /// </summary>

        public class MemberDiscussionSourceTable: DatabaseTable {
            
            public MemberDiscussionSourceTable(IDataProvider provider):base("MemberDiscussionSource",provider){
                ClassName = "MemberDiscussionSource";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("id", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("email", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("memberId", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("roleId", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("discussionId", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("action", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("source", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });
                    
                
                
            }

            public IColumn id{
                get{
                    return this.GetColumn("id");
                }
            }
				
   			public static string idColumn{
			      get{
        			return "id";
      			}
		    }
            
            public IColumn email{
                get{
                    return this.GetColumn("email");
                }
            }
				
   			public static string emailColumn{
			      get{
        			return "email";
      			}
		    }
            
            public IColumn memberId{
                get{
                    return this.GetColumn("memberId");
                }
            }
				
   			public static string memberIdColumn{
			      get{
        			return "memberId";
      			}
		    }
            
            public IColumn roleId{
                get{
                    return this.GetColumn("roleId");
                }
            }
				
   			public static string roleIdColumn{
			      get{
        			return "roleId";
      			}
		    }
            
            public IColumn discussionId{
                get{
                    return this.GetColumn("discussionId");
                }
            }
				
   			public static string discussionIdColumn{
			      get{
        			return "discussionId";
      			}
		    }
            
            public IColumn action{
                get{
                    return this.GetColumn("action");
                }
            }
				
   			public static string actionColumn{
			      get{
        			return "action";
      			}
		    }
            
            public IColumn source{
                get{
                    return this.GetColumn("source");
                }
            }
				
   			public static string sourceColumn{
			      get{
        			return "source";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: EmailDetails
        /// Primary Key: ID
        /// </summary>

        public class EmailDetailsTable: DatabaseTable {
            
            public EmailDetailsTable(IDataProvider provider):base("EmailDetails",provider){
                ClassName = "EmailDetail";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("ID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ToEmail", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = false,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("FromName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("replyEmail", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("body", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 2147483647
                });

                Columns.Add(new DatabaseColumn("subject", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 500
                });

                Columns.Add(new DatabaseColumn("status", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Exception", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 1000
                });

                Columns.Add(new DatabaseColumn("CreateDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ResendTry", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn ID{
                get{
                    return this.GetColumn("ID");
                }
            }
				
   			public static string IDColumn{
			      get{
        			return "ID";
      			}
		    }
            
            public IColumn ToEmail{
                get{
                    return this.GetColumn("ToEmail");
                }
            }
				
   			public static string ToEmailColumn{
			      get{
        			return "ToEmail";
      			}
		    }
            
            public IColumn FromName{
                get{
                    return this.GetColumn("FromName");
                }
            }
				
   			public static string FromNameColumn{
			      get{
        			return "FromName";
      			}
		    }
            
            public IColumn replyEmail{
                get{
                    return this.GetColumn("replyEmail");
                }
            }
				
   			public static string replyEmailColumn{
			      get{
        			return "replyEmail";
      			}
		    }
            
            public IColumn body{
                get{
                    return this.GetColumn("body");
                }
            }
				
   			public static string bodyColumn{
			      get{
        			return "body";
      			}
		    }
            
            public IColumn subject{
                get{
                    return this.GetColumn("subject");
                }
            }
				
   			public static string subjectColumn{
			      get{
        			return "subject";
      			}
		    }
            
            public IColumn status{
                get{
                    return this.GetColumn("status");
                }
            }
				
   			public static string statusColumn{
			      get{
        			return "status";
      			}
		    }
            
            public IColumn Exception{
                get{
                    return this.GetColumn("Exception");
                }
            }
				
   			public static string ExceptionColumn{
			      get{
        			return "Exception";
      			}
		    }
            
            public IColumn CreateDate{
                get{
                    return this.GetColumn("CreateDate");
                }
            }
				
   			public static string CreateDateColumn{
			      get{
        			return "CreateDate";
      			}
		    }
            
            public IColumn ResendTry{
                get{
                    return this.GetColumn("ResendTry");
                }
            }
				
   			public static string ResendTryColumn{
			      get{
        			return "ResendTry";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: MemberToFolder
        /// Primary Key: MTFID
        /// </summary>

        public class MemberToFolderTable: DatabaseTable {
            
            public MemberToFolderTable(IDataProvider provider):base("MemberToFolder",provider){
                ClassName = "MemberToFolder";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("MTFID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("MemberID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("RoleID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("ParentFolderID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("FolderID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ApplicableTo", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("Alias", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });
                    
                
                
            }

            public IColumn MTFID{
                get{
                    return this.GetColumn("MTFID");
                }
            }
				
   			public static string MTFIDColumn{
			      get{
        			return "MTFID";
      			}
		    }
            
            public IColumn MemberID{
                get{
                    return this.GetColumn("MemberID");
                }
            }
				
   			public static string MemberIDColumn{
			      get{
        			return "MemberID";
      			}
		    }
            
            public IColumn RoleID{
                get{
                    return this.GetColumn("RoleID");
                }
            }
				
   			public static string RoleIDColumn{
			      get{
        			return "RoleID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn ParentFolderID{
                get{
                    return this.GetColumn("ParentFolderID");
                }
            }
				
   			public static string ParentFolderIDColumn{
			      get{
        			return "ParentFolderID";
      			}
		    }
            
            public IColumn FolderID{
                get{
                    return this.GetColumn("FolderID");
                }
            }
				
   			public static string FolderIDColumn{
			      get{
        			return "FolderID";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
            public IColumn ApplicableTo{
                get{
                    return this.GetColumn("ApplicableTo");
                }
            }
				
   			public static string ApplicableToColumn{
			      get{
        			return "ApplicableTo";
      			}
		    }
            
            public IColumn Alias{
                get{
                    return this.GetColumn("Alias");
                }
            }
				
   			public static string AliasColumn{
			      get{
        			return "Alias";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: Folders
        /// Primary Key: FolderID
        /// </summary>

        public class FoldersTable: DatabaseTable {
            
            public FoldersTable(IDataProvider provider):base("Folders",provider){
                ClassName = "Folder";
                SchemaName = "schema_4a99e861_7e44_4937_a342_11fbd66f5d47";
                

                Columns.Add(new DatabaseColumn("FolderID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("InstanceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 36
                });

                Columns.Add(new DatabaseColumn("Title", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Description", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 400
                });

                Columns.Add(new DatabaseColumn("CrtDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ModDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Active", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn FolderID{
                get{
                    return this.GetColumn("FolderID");
                }
            }
				
   			public static string FolderIDColumn{
			      get{
        			return "FolderID";
      			}
		    }
            
            public IColumn InstanceID{
                get{
                    return this.GetColumn("InstanceID");
                }
            }
				
   			public static string InstanceIDColumn{
			      get{
        			return "InstanceID";
      			}
		    }
            
            public IColumn Title{
                get{
                    return this.GetColumn("Title");
                }
            }
				
   			public static string TitleColumn{
			      get{
        			return "Title";
      			}
		    }
            
            public IColumn Description{
                get{
                    return this.GetColumn("Description");
                }
            }
				
   			public static string DescriptionColumn{
			      get{
        			return "Description";
      			}
		    }
            
            public IColumn CrtDate{
                get{
                    return this.GetColumn("CrtDate");
                }
            }
				
   			public static string CrtDateColumn{
			      get{
        			return "CrtDate";
      			}
		    }
            
            public IColumn ModDate{
                get{
                    return this.GetColumn("ModDate");
                }
            }
				
   			public static string ModDateColumn{
			      get{
        			return "ModDate";
      			}
		    }
            
            public IColumn Active{
                get{
                    return this.GetColumn("Active");
                }
            }
				
   			public static string ActiveColumn{
			      get{
        			return "Active";
      			}
		    }
            
                    
        }
        
}