﻿


  
using System;
using SubSonic;
using SubSonic.Schema;
using SubSonic.DataProviders;
using System.Data;

namespace MyCCM{
	public partial class avaimaTest0001DB{

        public StoredProcedure DeleteDiscussionCompletely(int DiscussionID){
            StoredProcedure sp=new StoredProcedure("DeleteDiscussionCompletely",this.Provider);
            sp.Command.AddParameter("DiscussionID",DiscussionID,DbType.Int32);
            return sp;
        }
        public StoredProcedure DeleteOldEmail(){
            StoredProcedure sp=new StoredProcedure("DeleteOldEmail",this.Provider);
            return sp;
        }
        public StoredProcedure GetAllDiscussions(){
            StoredProcedure sp=new StoredProcedure("GetAllDiscussions",this.Provider);
            return sp;
        }
        public StoredProcedure GetAllMemberDiscussions(int MemberID,string InstanceID){
            StoredProcedure sp=new StoredProcedure("GetAllMemberDiscussions",this.Provider);
            sp.Command.AddParameter("MemberID",MemberID,DbType.Int32);
            sp.Command.AddParameter("InstanceID",InstanceID,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure GetChildFoldersOf(int FolderID,int MemberID){
            StoredProcedure sp=new StoredProcedure("GetChildFoldersOf",this.Provider);
            sp.Command.AddParameter("FolderID",FolderID,DbType.Int32);
            sp.Command.AddParameter("MemberID",MemberID,DbType.Int32);
            return sp;
        }
        public StoredProcedure GetDiscussions(string InstanceID,int MemberID,int FolderID){
            StoredProcedure sp=new StoredProcedure("GetDiscussions",this.Provider);
            sp.Command.AddParameter("InstanceID",InstanceID,DbType.AnsiString);
            sp.Command.AddParameter("MemberID",MemberID,DbType.Int32);
            sp.Command.AddParameter("FolderID",FolderID,DbType.Int32);
            return sp;
        }
        public StoredProcedure GetFolderMembersByRole(string InstanceID,int FolderID,int RoleID){
            StoredProcedure sp=new StoredProcedure("GetFolderMembersByRole",this.Provider);
            sp.Command.AddParameter("InstanceID",InstanceID,DbType.AnsiString);
            sp.Command.AddParameter("FolderID",FolderID,DbType.Int32);
            sp.Command.AddParameter("RoleID",RoleID,DbType.Int32);
            return sp;
        }
        public StoredProcedure GetFolders(string InstanceID,int MemberID,int ParentFolderID){
            StoredProcedure sp=new StoredProcedure("GetFolders",this.Provider);
            sp.Command.AddParameter("InstanceID",InstanceID,DbType.AnsiString);
            sp.Command.AddParameter("MemberID",MemberID,DbType.Int32);
            sp.Command.AddParameter("ParentFolderID",ParentFolderID,DbType.Int32);
            return sp;
        }
        public StoredProcedure GetLatestDiscussion(string MemberEmail){
            StoredProcedure sp=new StoredProcedure("GetLatestDiscussion",this.Provider);
            sp.Command.AddParameter("MemberEmail",MemberEmail,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure GetReportDiscussions(int MemberID,string InstanceID){
            StoredProcedure sp=new StoredProcedure("GetReportDiscussions",this.Provider);
            sp.Command.AddParameter("MemberID",MemberID,DbType.Int32);
            sp.Command.AddParameter("InstanceID",InstanceID,DbType.AnsiString);
            return sp;
        }
        public StoredProcedure GetSentEmails(){
            StoredProcedure sp=new StoredProcedure("GetSentEmails",this.Provider);
            return sp;
        }
	
	}
	
}
 