﻿var divDialog;
function ResetDialog(dialog_div) {
    $(dialog_div).html('<p id="paraDialog"></p>');
}
function OpenDialog(dialog_div, url, title) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    $(dialog_div).load(url);
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        width: 500,
                        buttons: null
                    });
}

function OpenSimpleDialog(dialog_div, title, divwidth) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        buttons: null,
                        width: divwidth
                    });
}

function OpenSimpleDialog(dialog_div, title, divwidth, function_yes) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        width: divwidth,
                        buttons: {
                            'Yes': function () {
                                function_yes();
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return true;
                            },
                            'No': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                            'Cancel': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                        }
                    });
    resizeIframe();
}

function OpenSaveDialog(dialog_div, title, divwidth, function_yes) {
    $(dialog_div).attr('title', title);
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        width: divwidth,
                        buttons: {
                            'Save': function () {
                                function_yes();
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return true;
                            },
                            'No': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                            'Cancel': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                        }
                    });
    divDialog.parent().appendTo($('form:first'));
    divDialog.parent().css('z-index', "1000");
    $('.ui-effects-wrapper').hide();
    resizeIframe();
}

function OpenStartDiscussionDialog(dialog_div, title, divwidth, function_yes) {
    $(dialog_div).attr('title', title);
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        width: divwidth,
                        buttons: {
                            'Start': function () {
                                function_yes();
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return true;
                            },
                            'Cancel': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                        }
                    });
    divDialog.parent().appendTo($('form:first'));
    divDialog.parent().css('z-index', "1000");
    $('.ui-effects-wrapper').hide();
    resizeIframe();
}

function OpenDialog(dialog_div, url, title, width) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    $(dialog_div).load(url, function () {
        resizeIframe();
    });
    divDialog = $(dialog_div).dialog(
                    {
                        title: title,
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        width: width,
                        buttons: null
                    });
}
function ConfirmDelete(dialog_div, title, message, function_yes) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    $('#paraDialog').text(message);
    divDialog = $(dialog_div).dialog(
                    {
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        buttons: {
                            'Yes': function () {
                                function_yes();
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return true;
                            },
                            'No': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                            'Cancel': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                                return false;
                            },
                        }
                    });
}

function MessageInform(dialog_div, title, message) {
    ResetDialog();
    $(dialog_div).attr('title', title);
    $(dialog_div).find('#paraDialog').text(message);
    divDialog = $(dialog_div).dialog(
                    {
                        autoOpen: true,
                        draggable: true,
                        resizable: false,
                        modal: true,
                        show: {
                            effect: "blind",
                        },
                        hide: {
                            effect: "blind",
                        },
                        buttons: {
                            'Ok': function () {
                                $(this).dialog('close');
                                $(dialog_div).attr('title', '');
                            }
                        }
                    });
}

var showInform = function Inform(message) {
    $('.inform #iText').text(message);
    $('.ui-widget.inform').fadeIn(1000).delay(30000000).fadeOut(1000);
}

var showError = function Error(message) {
    $('.error #iText').text(message);
    $('.error').show(500).delay(3000).hide(500);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function resizeIframe() {
    try {
        var iframe = window.parent.document.getElementById("MainContent_ifapp");
        var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
        if (innerDoc.body.offsetHeight) {
            iframe.height = innerDoc.body.offsetHeight + 32 + "px";
            console.log(innerDoc.body.offsetHeight + 32 + "px");
        }
        else if (iframe.Document && iframe.Document.body.scrollHeight) {
            iframe.style.height = iframe.Document.body.scrollHeight + "px";
            console.log(iframe.Document.body.scrollHeight + "px");
        }
    } catch (e) {
        console.log(e.message);
    }

}
function resizeIframePeriodically() {
    resizeIframe();
    setTimeout(function () {
        resizeIframe();
    }, 200);
    setTimeout(function () {
        resizeIframe();
    }, 400);
    setTimeout(function () {
        resizeIframe();
    }, 800);
}


//function changeNavigation(url, title) {
//    var obj = { 'navAnchor': url };
//    window.history.pushState(obj, title, url);
//}

