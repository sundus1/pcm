﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCM.App_Code
{
    public static class Extensions
    {
        public static Int32 ToInt32(this Object obj)
        {
            if (obj == DBNull.Value)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        public static Boolean ToBoolean(this Object obj)
        {
            return Convert.ToBoolean(obj);
        }

        public static String Encrypt(this String str)
        {
            byte[] strBytes = System.Text.Encoding.Unicode.GetBytes(str);
            string encryptString = Convert.ToBase64String(strBytes);
            return encryptString;
        }

        public static String Decrypt(this String str)
        {
            byte[] strBytes = Convert.FromBase64String(str);
            string originalString = System.Text.Encoding.Unicode.GetString(strBytes);
            return originalString;
        }
    }
}