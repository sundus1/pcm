﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetDiscussionComments.aspx.cs" Inherits="CCM.GetDiscussionComments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divOldDiscussions" class="divOldDiscussions">
            <div style="text-align: center" id="divLoadMore" runat="server" class="divLoadMore">
                <a href="javascript:void()" id="aLoadMore" class="aLoadMore" runat="server" style="background-color: #eee;padding: 6px 13px;display: block;padding: 11px 22px;/* border-radius: 20px; */width: 92%;/* margin: 0 auto; */font-size: 20px;/* border: 1px solid rgba(0,0,0,.1); */box-shadow: 1;box-shadow: 0px 3px 3px #eee;"></a>
                <img src="_assests/Images/loading.gif" id="imgGifhy" style="display: none; width: 30px;" />
                <%--<div class="divDot" style="border-radius: 36px; width: 6px; height: 6px; background-color: #0078C5; /* display: inline; */    margin: 11px auto 6px;">
                </div>
                <div class="divDot" style="border-radius: 36px; width: 6px; height: 6px; background-color: #0078C5; /* display: inline; */    margin: 6px auto;">
                </div>
                <div class="divDot" style="border-radius: 36px; width: 6px; height: 6px; background-color: #0078C5; /* display: inline; */    margin: 6px auto -8px;">
                </div>--%>
            </div>
            <asp:Repeater ID="rptDiscussions" runat="server" OnItemDataBound="rptDiscussions_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="divMD" data-team='<%# DataBinder.Eval(Container.DataItem,"TeamMember") %>' data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' style="width: 1000px">
                        <table class="tblDis" style="width: 100%">
                            <tr>
                                <td class="tdImage" id="tdImage" runat="server" style="width: 120px">
                                    <img class="imgMember" id="imgMember" runat="server" src="" />
                                    <br />
                                    <asp:Label ID="lblMemberTemp" CssClass="lblMember" runat="server" Text=""></asp:Label>
                                </td>
                                <td data-memdisid='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>'>
                                    <div id="divMemDis" runat="server" class="divMemDis" data-role="">
                                        <div id="divMemDisO" runat="server">
                                        </div>
                                        <asp:Repeater ID="rptFiles" runat="server">
                                            <HeaderTemplate>
                                                <div class="files">
                                                    <img src="_assests/Images/16%20x%2016/Attachment.png" style="width: 16px; float: left;" />
                                                    <span style="border-bottom: 1px solid gray; display: block; padding-bottom: 2px; font-weight: bold">Attached Files</span>
                                                    <ul style="list-style-type: circle; margin-top: 3px; padding-left: 25px !important">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <a target="_blank" href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="lnkFileName" runat="server"><%# GetFileName(DataBinder.Eval(Container.DataItem,"FileName").ToString()) %></a>
                                                    -
                                                                <a target="_blank" download='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>' href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="A1" runat="server">
                                                                    <img src="_assests/Images/16%20x%2016/download.png" style="border-radius: 0px; width: 12px;" />
                                                                </a>
                                                    <asp:LinkButton ID="lnkDownloadFile" CssClass="lnkDownloadFile" runat="server" Text="Download" CommandName="Download" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() + "," + DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' Visible="false">
                                                                        
                                                    </asp:LinkButton>
                                                </li>
                                                <%--<asp:LinkButton ID="lnkFileName" runat="server"   Text='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>'></asp:LinkButton>--%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                                    <br />
                                                <br />
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>

                                    <%--<asp:TextBox runat="server" ID="txtMemDis" CssClass="" TextMode="MultiLine" Width="200px"></asp:TextBox>--%>
                                </td>
                                <td class="tdImage2" id="tdImage2" runat="server" style="width: 120px">
                                    <img class="imgMember2" id="imgMember2" runat="server" src="" />
                                    <br />
                                    <asp:Label ID="lblMemberTemp2" CssClass="lblMember2" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
            <asp:HiddenField runat="server" ID="hdnTotalDisComments" Value="0" />
        </div>
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
    </form>
</body>
</html>
