﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mDiscussions.aspx.cs" Inherits="CCM.mDiscussions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title id="title"></title>
    http://localhost:2504/mGetDiscussionsList.aspx?mid=45&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab&did=1045
    <link href="_assests/jQuery%20Mobile/jquery.mobile-1.4.5.css" rel="stylesheet" />
    <script src="_assests/jQuery%20Mobile/demos/js/jquery.js"></script>
    <script src="_assests/jQuery%20Mobile/jquery.mobile-1.4.5.js"></script>

    <link href="_assests/jQuit/js/Context-Menu/src/jquery.contextMenu.css" rel="stylesheet" />
    <script src="<%= "_assests/_otf.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <%--<link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css?<%# DateTime.Now.ToBinary() %>" media="screen" type="text/css" rel="stylesheet" />--%>
    <script src="<%= "_assests/CKEditor/ckeditor.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <script src="<%= "_assests/CKEditor/adapters/jquery.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <script src="<%= "_assests/Uplodify/jquery.uploadify.min.js?" + (DateTime.Now.ToBinary().ToString()) %>"></script>
    <link href="<%= "_assests/Uplodify/uploadify.css?" + DateTime.Now.ToBinary().ToString() %>" rel="stylesheet" />

    <script>
        var redirectURL = "";
        var DiscussionTitle = "";
        var UploadedFiles = [];
        try {
            CKEDITOR.config.disallowedContent = "h1";
            CKEDITOR.editorConfig = function (config) {
                config.format_tags = 'div';
            };
        } catch (e) {
            console.log(e.message);
        }


        function initIzFM(selector, currFolder, memDisID) {
            try {
                //alert('Initalizing Uploader');
                $(selector).uploadify({
                    'swf': '_assests/Uplodify/uploadify.swf',
                    'uploader': 'Upload.ashx',
                    //'method':'post',
                    'script': 'Upload.ashx',
                    'cancelImg': '_assests/Uplodify/uploadify-cancel.png',
                    'buttonText': 'Attach Files',
                    //'script': 'Upload.ashx',
                    'folder': currFolder,
                    'fileTypeDesc': 'Files',
                    //'fileTypeExts': '*.png;*.jpg;*.jpeg;*.gif;*.tff;*.tf;*.js;*.swf;*.htc;*.txt;*.xml;*.rss;*.eot;*.svg;*.ttf;*.woff;*.pfb;*.pfm;*.otf;*.ffil;*.dfont;*.lwfn;*.html;*.htm;*.css;',
                    'multi': true,
                    'auto': true,
                    'removeCompleted': false,
                    'formData': { 'discussionID': $('#hdnDisID').val(), 'projectID': $('#hdnProjectID').val(), 'memDisID': memDisID },
                    'onQueueComplete': function (queueData) {
                        //alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');
                        //setTimeout(function () {
                        //    if ($('.uploadify-queue-item').size() > 0) {
                        //        $('.uploadify-queue-item').hide(200, function () {
                        //            $('.uploadify-queue-item').remove();
                        //        });
                        //    }
                        //}, 500)
                        //resizeIframePeriodically();
                    },
                    'onUploadError': function (file, errorCode, errorMsg, errorString) {
                        alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                    },
                    'onUploadSuccess': function (file, data, response) {
                        //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                        try {
                            if (data.toString().substr(0, 5).toString() == "Error") {
                                alert(data);
                                return;
                            }
                            $('#' + file.id + ' .cancel a').attr('data-disfileid', data);
                            $('#' + file.id + ' .cancel a').addClass('completed');
                            $('a[data-disfileid="' + data + '"]').click(function () {
                                DeleteDiscussionFile($(this).attr('data-disfileid'));
                            })
                            console.log("starting another");
                            console.log(data);
                            //alert(data);
                            var _uploadedFile = new Object();
                            _uploadedFile.FileName = file.name;
                            _uploadedFile.DisFileID = data;
                            UploadedFiles.push(_uploadedFile);
                            //var fileUpload = $(btnSend).parent().find('#fileUpload1').uploadify('upload');
                        } catch (e) {
                            alert(e.message);
                        }
                        //try { resizeIframe(); } catch (e) { }
                    },
                    'onUploadProgress': function (file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
                        try {
                            //$('#pProgress').html(totalBytesUploaded + ' bytes uploaded of ' + bytesTotal + ' bytes.');

                            //$('#divProgress').dialog('close');
                        } catch (e) {
                            alert(e.message);
                        }
                    },
                    'onDialogClose': function (queueData) {
                        //resizeIframePeriodically();
                    },
                    'onCancel': function (file) {
                        alert('The file ' + file.name + ' was cancelled.');
                        //for (var i = 0; i < UploadedFiles.length; i++) {
                        //    var _uploadedFile = UploadedFiles[i];
                        //    if (_uploadedFile.FileName == file.name) {
                        //        DeleteDiscussionFile(_uploadedFile.DisFileID);
                        //    }
                        //}
                    }
                });

            } catch (e) {
                alert(e.message);
            }
        }
        function SaveDiscussion(discussionid, title, projectid, instanceid, status, memberid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/SaveDiscussion',
                data: "{'discussionid':'" + discussionid + "','title':'" + title + "','projectid':'" + projectid + "','instanceid':'" + instanceid + "','status':'" + status + "','memberid':'" + memberid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    $('#hdnDisID').val(response.d.toString().split(':')[2]);
                    $('.active[data-id="aLinkDiscussion"]').text($('#h1Title').text());
                    var url = "Discussion.aspx?disid=" + response.d.toString().split(':')[2] + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val();
                },
                error: function () {
                    //$(trTeam).append("some problem in saving data");
                    //createInfoSpan(trTeam, "some problem in saving data");
                }
            });
        }
        function SendEmail(memberdisid, discussionid, memberid, conversation, subject, folderid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/SendEmail',
                data: "{'memberdisid':'" + memberdisid + "','discussionid':'" + discussionid + "','memberid':'" + memberid + "','conversation':'" + conversation + "','subject':'" + subject + "','folderid':'" + folderid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                },
                error: function () {
                    console.log("some problem in saving data");
                }
            });
        }

        function linkify(text) {
            if (text) {
                text = text.replace(
                    /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,
                    function (url) {
                        var full_url = url;
                        if (!full_url.match('^https?:\/\/')) {
                            full_url = 'http://' + full_url;
                        }
                        return '<a href="' + full_url + '">' + url + '</a>';
                    }
                );
            }
            return text;
        }

        function SaveMemberDiscussion(memdisid, discussionid, instanceid, memberid, teammember, conversation, active, folderid, subject, btnSend) {
            conversation = linkify(conversation);
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/SaveMemberDiscussion',
                data: "{'memdisid':'" + memdisid + "','discussionid':'" + discussionid + "','instanceid':'" + instanceid + "','memberid':'" + memberid + "','teammember':'" + teammember + "','conversation':'" + conversation + "','active':'" + active + "','folderid':'" + folderid + "','subject':'" + subject + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    if (response.d.toString().split(':')[0] == "Error") {
                        alert(response.d.toString().split(':')[1]);
                        window.location = 'Controls/Info.aspx?h=Not Allowed&t=You are not allowed to view this page. This can occur in case admin has removed your role from discussion. Please check your email for information releated your removal or contact your administrator for further details';
                        return false;
                    }
                    var memberdisid = response.d.toString().split(':')[2]; // Return memDisID
                    UpdateDiscussionFile('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val(), memberdisid);
                    //initIzFM('.fileUpload1', '', memberdisid);
                    //$("#fileUpload1").uploadify('settings', 'formData', { 'discussionID': $('#hdnDisID').val(), 'projectID': $('#hdnProjectID').val(), 'memDisID': memberdisid });
                    //'formData': { 'discussionID': $('#hdnDisID').val(), 'projectID': $('#hdnProjectID').val(), 'memDisID': memDisID },
                    //var fileUpload = $(btnSend).parent().find('#fileUpload1').uploadify('upload', '*');
                    LoadDisList();
                    redirectURL = "Controls/GetDiscussionTemp.aspx?memDisID=" + response.d.toString().split(':')[2] + "&pid=" + $('#hdnProjectID').val() + "&fid=" + $('#hdn_folderid').val() + "&disid=" + $('#hdnDisID').val() + "&memberid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + " .divMD";
                    var myInterval = setInterval(function () {
                        if ($('.uploadify-queue-item').size() == 0) {
                            if ($('#divMemberDiscussion .divMD').size() > 0) {
                                $('#tempDiv').load(redirectURL, function () {
                                    console.log('loaded');
                                    initiateContextMenu('#tempDiv .divMD .disAction');
                                    $('#divMemberDiscussion .divMD').last().after($('#tempDiv .divMD'));
                                    SendEmail(memberdisid, discussionid, memberid, $('.txtMemDis').val(), subject, $('#hdn_folderid').val());
                                    $('.txtMemDis').val('');
                                    $('.btnSend').val('Add Comment');
                                    $('.btnSend').removeClass('button_loading');
                                    getDisFiles();
                                    if ($('#fileUpload1 object').size() <= 0) { $('#hdnMemDisID').val(memberdisid); $('.btnUpload').click(); }
                                    //try { resizeIframe(); } catch (e) { }
                                    //window.setTimeout(function () {
                                    //    console.log('200');
                                    //    try { resizeIframe(); } catch (e) { }
                                    //    initiateContextMenu();
                                    //}, 1000);
                                    //window.setTimeout(function () {
                                    //    console.log('600');
                                    //    try { resizeIframe(); } catch (e) { }
                                    //    initiateContextMenu();
                                    //}, 2000);
                                });
                                clearInterval(myInterval);
                            }
                            else {
                                redirectURL = "Discussion.aspx?disid=" + $('#hdnDisID').val() + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val() + "&instanceid=" + $('#hdnInstanceid').val() + " .formdis";
                                if ($('#hdnOID').val() == "temp-user") {
                                    redirectURL = window.location.href.toString() + " .formdis";
                                }
                                $('#bodyDis').load(redirectURL, function () {
                                    SendEmail(memberdisid, discussionid, memberid, $('.txtMemDis').val(), subject, $('#hdn_folderid').val())
                                    console.log('loaded all');
                                    InitializePage();
                                    getDisFiles();
                                    initiateContextMenu();
                                    if ($('#fileUpload1 object').size() <= 0) { $('#hdnMemDisID').val(memberdisid); $('.btnUpload').click(); }
                                    //try { resizeIframe(); } catch (e) { }
                                    //window.setTimeout(function () {
                                    //    console.log('1000');
                                    //    try { resizeIframe(); } catch (e) { }
                                    //    initiateContextMenu();
                                    //}, 1000);
                                    //window.setTimeout(function () {
                                    //    console.log('2000');
                                    //    try { resizeIframe(); } catch (e) { }
                                    //    initiateContextMenu();
                                    //}, 2000);
                                });
                                //clearInterval(myInterval);
                            }
                        }
                    }, 1000);

                },
                error: function () {
                    console.log("some problem in saving data")
                }
            });
        }
        function DeleteMemberDiscussion(memdisid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/DeleteMemberDiscussion',
                data: "{'memdisid':'" + memdisid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    //createInfoSpan(trTeam, "Member Deleted Successfully!");
                    $('.divMD[data-rowkey="' + memdisid + '"]').find('div[id*="divMemDisO"]').html("this message was deleted on day, date at time");
                    $('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200, function () {
                        $('.divMD[data-rowkey="' + memdisid + '"]').remove();
                        initiateContextMenu('.disAction');
                    });
                    getDisFiles();
                    //try { resizeIframePeriodically(); } catch (e) { }
                },
                error: function () {
                    //$(trTeam).append("<span id='info'>some problem in saving data</span>");
                    //createInfoSpan(trTeam, "some problem in saving data");
                    //resizeIframePeriodically();
                }
            });
        }
        function DeleteDiscussionFile(disfileid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/DeleteDiscussionFile',
                data: "{'disfileid':'" + disfileid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    //createInfoSpan(trTeam, "Member Deleted Successfully!");
                    //$('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
                    //getDisFiles();
                    //try { resizeIframePeriodically(); } catch (e) { }
                },
                error: function () {
                    //$(trTeam).append("<span id='info'>some problem in saving data</span>");
                    //createInfoSpan(trTeam, "some problem in saving data");
                    //resizeIframePeriodically();
                }
            });
        }
        function DeleteDiscussionFiles(memberdisid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/DeleteDiscussionFiles',
                data: "{'memberdisid':'" + memberdisid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    //createInfoSpan(trTeam, "Member Deleted Successfully!");
                    //$('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
                    //getDisFiles();
                    //try { resizeIframePeriodically(); } catch (e) { }
                },
                error: function () {
                    //$(trTeam).append("<span id='info'>some problem in saving data</span>");
                    //createInfoSpan(trTeam, "some problem in saving data");
                    //resizeIframePeriodically();
                }
            });
        }
        function UpdateDiscussionFile(memberDisID, updatedMemberDisID) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/UpdateDiscussionFile',
                data: "{'memberDisID':'" + memberDisID + "', 'updatedMemberDisID':'" + updatedMemberDisID + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    //createInfoSpan(trTeam, "Member Deleted Successfully!");
                    //$('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
                    //getDisFiles();
                    //try { resizeIframePeriodically(); } catch (e) { }
                },
                error: function () {
                    //$(trTeam).append("<span id='info'>some problem in saving data</span>");
                    //createInfoSpan(trTeam, "some problem in saving data");
                    //resizeIframePeriodically();
                }
            });
        }
        function initiateContextMenu(selector) {
            $.contextMenu({
                selector: selector,
                trigger: 'left',
                callback: function (key, options) {
                    var dataid = $(this).attr('data-id');
                    console.log(dataid);
                    if (key == 'delete') {
                        ConfirmDelete(".divDialog", "Delete?", "Are you sure you want to delete this comment?", function () {
                            DeleteMemberDiscussion(dataid);
                        });
                    }
                },
                items: {
                    "delete": { name: "Delete" }
                    //,
                    //"copyto": { name: "Copy To" },
                    //"moveto": { name: "Move To" },
                    //"forward": { name: "Forward" }
                }
            });
        }
        function getDisFiles() {
            var url = "FilesPerDiscussion.aspx?disid=" + $('#hdnDisID').val() + " .divDisFiles";
            $('.container_files').load(url, function () {
                //try { resizeIframe(); } catch (e) { }
            });
        }
        function InitializePage() {
            //$('.btnRefresh').click(function () {
            //    window.location.reload(true);
            //    return false;
            //});
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                //$('#fileUpload1').hide();
            }


            $('#h1Title').click(function () {
                DiscussionTitle = $(this).text();
            });
            $('#h1Title').blur(function () {
                if ($('#h1Title').text() == '') {
                    $(this).text(DiscussionTitle);
                }
                SaveDiscussion($('#hdnDisID').val(), $('#h1Title').text(), 0, $('#hdninstanceid').val(), 'NA', $('#hdnMemberID').val());
            });
            //$('.btnStatus').click(function () {
            //    if ($(this).attr('data-command') == "Close") {
            //        SaveDiscussion($('#hdnDisID').val(), 'NA', 0, $('#hdninstanceid').val(), 'Close', $('hdnMemberID').val());
            //    }
            //    else {
            //        SaveDiscussion($('#hdnDisID').val(), 'NA', 0, $('#hdninstanceid').val(), 'Open', $('hdnMemberID').val());
            //    }
            //});
            getDisFiles();
            try {
                $('.txtMemDis').ckeditor();
            } catch (e) {
                console.log(e.message);
            }

            initIzFM('.fileUpload1', '', "D" + $('#hdnDisID').val() + "M" + $("#hdnMemberID").val());
            $('.btnSend').click(function () {
                if ($('#h1Title').text() == "Untitled" && ($('#h1Title').attr('contenteditable') == "false" || $('#h1Title').attr('contenteditable') == undefined)) {
                    alert("Provide discussion title.");
                    $('#h1Title').focus();
                }
                else {
                    if (DisallowedContents != null) {
                        var dac = "";
                        for (var i = 0; i < DisallowedContents.length; i++) {
                            if (DisallowedContents[i] != "") {
                                if ($('.txtMemDis').val().indexOf(DisallowedContents[i]) > -1) {
                                    dac += DisallowedContents[i];
                                    if (i < DisallowedContents.length - 1) {
                                        dac += ", ";
                                    }
                                }
                            }
                        }
                        if (dac != "") {
                            alert('One or more words in your message are not allowed. Please remove the following words and try again:\n\n "' + dac + '"');
                            return false;
                        }
                    }

                    if ($('.uploadify-queue-item').size() != $('a.completed').size()) {
                        if (!($(this).hasClass('button_loading'))) {
                            $(this).val('Posting...');
                            $(this).addClass('button_loading');
                            var sendInterval = setInterval(function () {
                                if ($('.uploadify-queue-item').size() == $('a.completed').size()) {
                                    $('#fileUpload1').uploadify('cancel', '*');
                                    if (!($(this).hasClass('button_loading'))) {
                                        $(this).val('Posting...');
                                        $(this).addClass('button_loading');
                                        SaveMemberDiscussion('0', $('#hdnDisID').val(), $('#hdninstanceid').val(), $("#hdnMemberID").val(), $('#hdnTeamMember').val(), $('.txtMemDis').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), this);
                                    }
                                    clearInterval(sendInterval);
                                }
                            }, 400);
                        }
                    }
                    else {
                        if ($('#fileUpload1 object').size() > 0) {
                            $('#fileUpload1').uploadify('cancel', '*');
                        };

                        if (!($(this).hasClass('button_loading'))) {
                            $(this).val('Posting...');
                            $(this).addClass('button_loading');
                            SaveMemberDiscussion('0', $('#hdnDisID').val(), $('#hdninstanceid').val(), $("#hdnMemberID").val(), $('#hdnTeamMember').val(), $('.txtMemDis').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), this);
                        }
                    }


                }
                return false;
            });
            $('.btnAddTeam, .btnAddClient, .btnStatus').click(function () {
                if ($('#h1Title').text() == "Untitled" && ($('#h1Title').attr('contenteditable') == "true" || $('#h1Title').attr('contenteditable') == undefined)) {
                    alert("Provide discussion title.");
                    $('#h1Title').focus();
                    return false;
                }
            });
        }

        $(function () {
            InitializePage();
            DeleteDiscussionFiles('D' + $('#hdnDisID').val() + 'M' + $('#hdnMemberID').val());
            $('.aAddTeam').click(function () {
                //$('.divAssignProject:visible').hide("blind", 400);
                //$('.divManageMember:hidden').show("blind", 400);
                //$("input#rdoTeam").prop('checked', true);
                //setCheck();
                $('.divManageTeam:hidden').show("blind", 400);
                $('.chkAddNewTeam input').prop('checked', false);
                if ($('.chkAddNewTeam input').is(':checked')) {
                    $('.trExTeam').hide(200);
                    $('.trNewTeam').show(200);
                    $('.txtTeamAlias').val('');
                    $('.txtTeamName').val('');
                    $('.txtTeamEmail').val('');
                    $('.txtTeamTitle').val('');
                    $('#hdnSelTeamID').val('0');
                    $('.btnAddTeam').val("Add");
                    $('.txtTeamName').focus();
                }
                else {
                    $('.trExTeam').hide(200);
                    $('.trNewTeam').show(200);
                    $('.txtTeamAlias').val('');
                    $('.txtTeamName').val('');
                    $('.txtTeamEmail').val('');
                    $('.txtTeamTitle').val('');
                    $('#hdnSelTeamID').val('0');
                    $('.btnAddTeam').val("Add");
                    $('.trExTeam').show(200);
                    $('.trNewTeam').hide(200);
                }
            })
            $('.aAddClient').click(function () {
                //$('.divAssignProject:visible').hide("blind", 400);
                //$('.divManageMember:hidden').show("blind", 400);
                //$("input#rdoClient").prop('checked', true);
                //setCheck();
                $('.divManageClient:hidden').show("blind", 400);
                $('.chkAddNewClient input').prop('checked', false);
                if ($('.chkAddNewClient input').is(':checked')) {
                    $('.trExClient').hide(200);
                    $('.trNewClient').show(200);
                    $('.txtClientAlias').val('');
                    $('.txtClientName').val('');
                    $('.txtClientEmail').val('');
                    $('.txtClientTitle').val('');
                    $('#hdnSelClientID').val('0');
                    $('.btnAddClient').val("Add");
                    $('.txtClientName').focus();
                }
                else {
                    $('.trExClient').hide(200);
                    $('.trNewClient').show(200);
                    $('.txtClientAlias').val('');
                    $('.txtClientName').val('');
                    $('.txtClientEmail').val('');
                    $('.txtClientTitle').val('');
                    $('#hdnSelClientID').val('0');
                    $('.btnAddClient').val("Add");
                    $('.trExClient').show(200);
                    $('.trNewClient').hide(200);
                }
            });
            $('.btnCancelDiscussion').click(function () {
                $('.divAssignProject:visible').hide('blind', 400);
                return false;
            })
            $('.btnCancelTeam').click(function () {
                $('.divManageTeam:visible').hide('blind', 400);
                return false;
            })
            $('.btnCancelClient').click(function () {
                $('.divManageClient:visible').hide('blind', 400);
                return false;
            })
            $('.lnkClienkEdit').click(function () {
                if (!$('.chkAddNewClient input').is(':checked')) {
                    $('.chkAddNewClient input').click();
                    $('.chkAddNewClient input').prop('checked', 'true');
                }
                var memberID = $(this).attr('data-id');
                var alias = $(this).attr('data-alias');
                var email = $(this).attr('data-email');
                var roleID = $(this).attr('data-roleID');
                var title = $(this).attr('data-title');
                var name = $(this).attr('data-name');
                //roleID = parseInt(roleID);
                //if (roleID == 2) {
                //    $("input#rdoTeam").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", true);
                //    $(".chkAdmin").show();
                //}
                //if (roleID == 3) {
                //    $("input#rdoTeam").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", false);
                //    $(".chkAdmin").show();
                //}
                //if (roleID == 4) {
                //    $("input#rdoClient").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", false);
                //    $(".chkAdmin").hide();
                //}
                //if (roleID == 5) {
                //    $("input#rdoClient").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", false);;
                //    $(".chkAdmin").hide();
                //}
                //$('.txtTeamFullName').val(name);

                $('.txtClientAlias').val(alias);
                $('.txtClientName').val(name);
                $('.txtClientEmail').val(email);
                $('.txtClientTitle').val(title);
                $('#hdnSelClientID').val(memberID);
                $('.btnAddClient').val("Update Member");
                $('.txtClientName').focus();
                $('.divManageClient:hidden').show("blind", 400);
                $('.trClientName').hide(200);
                return false;
            });
            $('.lnkTeamEdit').click(function () {
                if (!$('.chkAddNewTeam input').is(':checked')) {
                    $('.chkAddNewTeam input').click();
                    $('.chkAddNewTeam input').prop('checked', 'true');
                }
                var memberID = $(this).attr('data-id');
                var alias = $(this).attr('data-alias');
                var email = $(this).attr('data-email');
                var roleID = $(this).attr('data-roleID');
                var title = $(this).attr('data-title');
                var name = $(this).attr('data-name');
                roleID = parseInt(roleID);
                if (roleID == 2 || roleID == 1) {
                    $("input#chkAdmin").prop("checked", true);
                    $(".chkAdmin").show();
                }
                if (roleID == 3) {
                    $("input#chkAdmin").prop("checked", false);
                    $(".chkAdmin").show();
                }
                //if (roleID == 4) {
                //    $("input#rdoTeam").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", false);
                //    $(".chkAdmin").hide();
                //}
                //if (roleID == 5) {
                //    $("input#rdoTeam").prop("checked", true);
                //    $("input#chkAdmin").prop("checked", false);;
                //    $(".chkAdmin").hide();
                //}
                //$('.txtTeamFullName').val(name);

                $('.txtTeamAlias').val(alias);
                $('.txtTeamName').val(name);
                $('.txtTeamEmail').val(email);
                $('.txtTeamTitle').val(title);
                $('#hdnSelTeamID').val(memberID);
                $('.btnAddTeam').val("Update Member");
                $('.txtTeamName').focus();
                $('.divManageTeam:hidden').show("blind", 400);
                $('.trTeamName').hide(200);
                return false;
            });
            $('.chkAddNewTeam input').change(function () {
                if ($(this).is(':checked')) {
                    $('.trTeam').show(200);
                    $('.trExTeam').hide(200);
                    $('.trNewTeam').show(200);
                    $('.txtTeamAlias').val('');
                    $('.txtTeamName').val('');
                    $('.txtTeamEmail').val('');
                    $('.txtTeamTitle').val('');
                    $('#hdnSelTeamID').val('0');
                    $('.btnAddTeam').val("Add");
                    $('.trTeamName').show(200);
                    $('.txtTeamName').focus();
                }
                else {
                    $('.trExTeam').hide(200);
                    $('.trNewTeam').show(200);
                    $('.txtTeamAlias').val('');
                    $('.txtTeamName').val('');
                    $('.txtTeamEmail').val('');
                    $('.txtTeamTitle').val('');
                    $('#hdnSelTeamID').val('0');
                    $('.btnAddTeam').val("Add");
                    $('.trExTeam').show(200);
                    $('.trNewTeam').hide(200);
                    if ($($('.ddlTeamMembers option')[0]).text() == "No Records Found") {
                        $('.trTeam').hide(200);
                    }
                }
                return false;
            });
            $('.chkAddNewClient input').change(function () {
                if ($(this).is(':checked')) {
                    $('.trClient').show(200);
                    $('.trExClient').hide(200);
                    $('.trNewClient').show(200);
                    $('.txtClientAlias').val('');
                    $('.txtClientName').val('');
                    $('.txtClientEmail').val('');
                    $('.txtClientTitle').val('');
                    $('#hdnSelClientID').val('0');
                    $('.btnAddClient').val("Add");
                    $('.trClientName').show(200);
                    $('.txtClientName').focus();
                }
                else {
                    $('.trExClient').hide(200);
                    $('.trNewClient').show(200);
                    $('.txtClientAlias').val('');
                    $('.txtClientName').val('');
                    $('.txtClientEmail').val('');
                    $('.txtClientTitle').val('');
                    $('#hdnSelClientID').val('0');
                    $('.btnAddClient').val("Add");
                    $('.trExClient').show(200);
                    $('.trNewClient').hide(200);
                    if ($($('.ddlClientMembers option')[0]).text() == "No Records Found") {
                        $('.trClient').hide(200);
                    }
                }
                return false;
            });
            if ($('.trExTeam:visible').size() > 0) {
                $('.chkAddNewTeam input').prop('checked', false)
            }
            if ($('.trExClient:visible').size() > 0) {
                $('.chkAddNewClient input').prop('checked', false)
            }
            var clearLblInterval = setInterval(function () {
                //console.log("chaling");
                setTimeout(function () {
                    $('.lblInfo').delay(2000).text('');
                    if ($('.lblInfo').text() == '') {
                        clearInterval(clearLblInterval);
                    }
                    //console.log("visible chaling");
                }, 10000);
            }, 100);
        });
        $(window).load(function () {
            initiateContextMenu('.disAction');
            if ($('#hdnMoveToBottom').val() == "1") {
                $("#divMemberDiscussionContainer").animate({ scrollTop: $('#divMemberDiscussion').height() }, 1000);
            }
            $('.txtMemDis').focus();
        });
    </script>
    <link href="<%= "_assests/Styles/my_style.css?" + DateTime.Now.ToBinary().ToString() %>" rel="stylesheet" />
    <script src="<%= "_assests/jQuit/js/Context-Menu/src/jquery.ui.position.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <script src="<%= "_assests/jQuit/js/Context-Menu/src/jquery.contextMenu.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <link href="_assests/Tooltipster/css/tooltipster.css" rel="stylesheet" />
    <script src="_assests/Tooltipster/js/jquery.tooltipster.js"></script>
    <link href="_assests/Tooltipster/css/themes/tooltipster-light.css" rel="stylesheet" />
    <link href="_assests/nProgress/nprogress.css" rel="stylesheet" />
    <script src="_assests/nProgress/nprogress.js"></script>
    <script>
        var DisallowedContents;
        $(function () {
            if ($('#hdnRoleID').val() == 5) {
                $('.lnkClienkEdit').remove();
                $('.lnkClientRemove').remove();
                $('#spanTeam').remove();                
            }
            else if ($('#hdnRoleID').val() == 4) {
                $('.lnkClienkEdit').remove();
                $('.lnkClientRemove').remove();
                $('.lnkTeamEdit').remove();
                $('.lnkTeamRemove').remove();
                $('#spanTeam').remove();
            }
            else if ($('#hdnRoleID').val() == 3) {
                $('.lnkClienkEdit').remove();
                $('.lnkClientRemove').remove();
                $('.lnkTeamEdit').remove();
                $('.lnkTeamRemove').remove();
                $('#spanTeam').remove();
                $('#spanClient').remove();
                
            }
            else if ($('#hdnRoleID').val() == 2) {
                $('.lnkTeamEdit').remove();
                $('.lnkTeamRemove').remove();
                //alert('no role def')
            }
            else if ($('#hdnRoleID').val() == 1) {
                //alert('no role def')
            }

            DisallowedContents = <%= Serialize(DisallowedContents) %>


            var div = $('.divDetails').clone().show();
            $('.imgDisInfo').tooltipster({
                content: $(div),
                interactive: true,
                trigger: 'click',
                theme: 'tooltipster-light',
                position: 'bottom'
            });
            $('.tooltip').tooltipster({
                contentAsHTML: true
            });
            

            var Asked = false;
            $('.lnkRemove').click(function () {
                var lnkClientObj = $(this);
                if (!Asked) {
                    ConfirmDelete(".divDialog", "Remove?", "Are you sure you want to remove this member?", function () {
                        Asked = true;
                        $(lnkClientObj).click();
                    });
                    return false;
                }
                else {
                    Asked = false;
                }
            });
            if ($('#hdnDiscussionIsNew').val() == "1") {
                OpenSaveDialog('.divAskTitle', 'Discussion Title', 'auto', function () {
                    SaveDiscussion($('#hdnDisID').val(), $('.txtDiscussionTitle').val(), 0, $('#hdninstanceid').val(), 'NA', $('#hdnMemberID').val());
                    $('#h1Title').text($('.txtDiscussionTitle').val());
                    $('.active[data-id="aLinkDiscussion"]').text($('#h1Title').text());
                    $('#hdnDiscussionIsNew').val('0');
                    $('.divAskTitle').dialog('close');
                });
            }
            q_asked = false;
            $('.aStartDiscussion').click(function(e){
                e.preventDefault();
                OpenStartDiscussionDialog('.divAskTitle', 'Start Discussion', 'auto', function () {                        
                    var href = $('.aStartDiscussion').attr('href') + "&dtitle=" + $('.txtDiscussionTitle').val();
                    window.open(href,'_self');                                 
                });   
                
            });
            window.onbeforeunload = function (e) {
                if ($('.txtMemDis').val() != "") {
                    if (e) {
                        e.returnValue = 'You were writing a comment. Closing or refreshing this window might erase it!';
                    }
                    // For Safari
                    return 'You were writing a comment. Closing or refreshing this window might erase it!';
                }
                else if ($('.uploadify-queue-item').size() > 0) {
                    if (e) {
                        e.returnValue = 'You have files in uploading queue. Closing or refreshing this window might loss them!';
                    }
                    // For Safari
                    return 'You have files in uploading queue. Closing or refreshing this window might loss them!';

                }
            }

            if ($('#hdnAskInfo').val() == "1") {
                OpenSaveDialog('.divAskInfo', 'Member Info', 'auto', function () {
                    $('.btnSaveMemberInfo').click();
                });
            }
            //- 43 - 76 - 15
            //$('#divMemberDiscussionContainer').css('max-height', ($(window).height() - (140 - 67)) + 'px' ).css('overflow-y','overlay');
            //$('#contentRight-Contents').css('max-height', ($(window).height() - 43 - (92 - 58)) + 'px').css('overflow-y','overlay');      
            //$('#contentRight-Contents').enscroll({
            //    verticalTrackClass: 'track4',
            //    verticalHandleClass: 'handle4',
            //    minScrollbarLength: 28,
            //    scrollIncrement:20
            //});
            //$('#divMemberDiscussionContainer').enscroll({
            //    verticalTrackClass: 'track4',
            //    verticalHandleClass: 'handle4',
            //    minScrollbarLength: 28,
            //    scrollIncrement:100
            //});
            
        });
    </script>
    <script>
        function LoadDisList(){
            var disList = "mGetDiscussionsList.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val();
            $('#divMainDiscussionList').load(disList + " .divDiscussionList",function(){
                $( "#tabs" ).tabs();
                $('.lblUnread').text($('.lblUnreadCount').text());
                if($('.active[data-type="opendis"]').size() > 0){
                    $($('#tabs ul a')[0]).click();
                }
                else if($('.active[data-type="closedis"]').size() > 0){
                    $($('#tabs ul a')[1]).click();
                }
                $('#tabs-1').css('max-height', ($(window).height() - 108 - 89) + 'px').css('overflow-y','overlay');
                $('#tabs-2').css('max-height', ($(window).height() - 108) + 'px').css('overflow-y','overlay');
                $('.li-no-edit').html($('.aStartDiscussion').show());
            });
        }
        // Load Sections
        $(function(){
            $('#divMemberDiscussion').hide();
            $('.oldLoader').show();
            NProgress.start();
            var oldDisURL = "GetDiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val();
            $('#divOldDiscussions').load(oldDisURL + " .divOldDiscussions",function(){
                NProgress.done();
                setTimeout(function(){
                    $('.oldLoader').fadeOut(50,function(){
                        $('#divMemberDiscussion').fadeIn(100);                        
                        $('.aLoadMore').click(function(){                            
                            var oldDisURL = "GetDiscussionComments.aspx?mid=" + $('#hdnMemberID').val() + "&oid=" + $('#hdnOID').val() + "&iid=" + $('#hdninstanceid').val() + "&did=" + $('#hdnDisID').val() + "&loadmore=" + $(this).attr('data-commentstoload');
                            var divMoreComments = $("<div id=\"divMoreComments\" ></div>");
                            $(divMoreComments).load(oldDisURL + " .divMD",function(){
                                $('.divLoadMore').replaceWith($(divMoreComments).html());
                                $('.divDot').remove();
                                $('.aLoadMore').remove();
                            });
                            $(this).remove();
                            $('.divDot').remove();                                                                                    
                            $('#imgGifhy').show();
                        });
                        //$("html, body").animate({ scrollTop: $('#btnSend').offset().top }, 1000);
                    });
                },500);                
            });

            LoadDisList();
        });
    </script>
    <link href="_assests/Styles/Responsive.css" rel="stylesheet" />
    <style>
        #aLoadMore:active {
            box-shadow: 0px 0px 0px #eee;
        }

        /*.ui-tabs-panel a {
            color: #0078C5;
        }*/
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div data-role="page" class="jqm-demos jqm-panel-page" data-quicklinks="true">
            <%--<img id="imgDisInfo" class="imgDisInfo" runat="server" src="~/_assests/Images/16 x 16/Detail.png" style="float: right; display: inline; width: 16px; margin-right: 24px; cursor: pointer; margin-top: 24px; position: relative" />--%>
            <asp:ImageButton ID="btnStatus" CssClass="btnStatus tooltip" runat="server" ImageUrl="_assests/Images/16 x 16/Tick.png" CommandName="Close" data-command="Close" Style="border-width: 0px; float: right; margin-right: 10px; width: 26px; margin-top: 18px; position: relative" OnClick="btnStatus_Click" />
            <div style="display: none; float: right; border: 1px solid #eee;" id="divDetails" class="divDetails">
                <table cell-padding="2">
                    <tr>
                        <td>
                            <b>Modified: </b>
                        </td>
                        <td>
                            <asp:Label ID="lblLastRefresh" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Created:</b>
                        </td>
                        <td>
                            <asp:Label ID="lblCreatedOn" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div role="main" class="ui-content jqm-content">
                <div>
                    <div class="ContentRight_Text" id="divcontent" style="padding:0px !important">
                        <div style="text-align: center">
                            <img alt="img" src="http://www.avaima.com/main/Control_Panel/Assets/images/AVAIMA-Logo.jpg" />
                        </div>
                        <br />
                        <div style="text-align:center">
                            <h1 id="h1Title" runat="server" style="width: auto; display: inline-block;text-align:center;border-bottom: 1px solid #ccc;">Untitled
                            </h1>
                        </div>

                        <a href="#leftpanel3" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-btn-mini" style="position: absolute;top: 0px;left: 0px;font-size: 9px;">Discussions</a>
                        <a href="#rightpanel3" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-btn-mini" style="float: right;position: absolute;top: 0px;right: 0px;font-size: 9px;">Resources</a>
                        <a href="#" class="imgDisInfo ui-btn ui-shadow ui-corner-all ui-btn-inline ui-btn-mini" style="float: right;position: absolute;top: 31px;right: 0px;font-size: 9px;">Info</a>

                        <div id="divBreadCrumb" style="margin: 10px 10px 6px 0px">
                            <div id="divCommands" class="divCommands" runat="server">
                                <asp:Label ID="lblS" runat="server" Text=""></asp:Label>
                                <asp:LinkButton ID="lnkManageMember" CssClass="lnkManageMember" runat="server" Text="Manage Member" Style="display: none"></asp:LinkButton>
                            </div>
                            <div id="divAssignProject" class="divAssignProject divDataForm" runat="server" style="margin-left: 4px;">
                                <h3 style="border-bottom: 1px solid #ccc">Edit Discussion
                                </h3>
                                <br />
                                <table class="tblForm">
                                    <tr>
                                        <td class="captiontd">Title: 
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDisTitle" runat="server" CssClass="txtDisTitle"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="captiontd">Status: 
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlStatus" runat="server">
                                                <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                                <asp:ListItem Text="Close" Value="Close"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button ID="btnSaveDiscussion" CssClass="btnSaveDiscussion" runat="server" Text="Save" OnClick="btnSaveDiscussion_Click" />
                                            &nbsp;
                                <asp:Button ID="btnCancelDiscussion" CssClass="btnCancelDiscussion" runat="server" Text="Cancel" />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="clear: both"></div>
                            <asp:Repeater Visible="false" ID="rptBC" runat="server" OnItemCommand="rptBC_ItemCommand">
                                <HeaderTemplate>
                                    <asp:LinkButton data-pid="0" ID="lnkRoot" CssClass="lnkBC" runat="server" Text="Root" CommandName="Navigate" CommandArgument='0'></asp:LinkButton>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    /
                   <asp:LinkButton CssClass="lnkBC" data-pid='<%# DataBinder.Eval(Container.DataItem, "FolderID") %>' ID="lnkProjects" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' CommandName="Navigate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FolderID") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div class="clearboth"></div>
                        </div>
                        <div id="divDiscussion">
                            <div style="width: 100%; float: left;">
                                <div id="divMemberDiscussionContainer" style="overflow-y: overlay">
                                    <div id="divMemberDiscussion">
                                        <div id="divOldDiscussions" style="padding-top: 10px">
                                        </div>
                                        <div class="divMD" data-team='' data-rowkey='' style="width: 1000px; display: none">
                                            <table class="tblDis" style="width: 100%">
                                                <tr>
                                                    <td class="tdImage" id="tdImage" runat="server" style="width: 120px">
                                                        <img class="imgMember" id="img1" runat="server" src="" />
                                                        <br />
                                                        <asp:Label ID="Label5" CssClass="lblMember" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td data-memdisid=''>
                                                        <div id="divMemDis" runat="server" class="divMemDis" data-role="">
                                                            <div id="divMemDisO" runat="server" style="word-break: break-all;">
                                                            </div>
                                                            <img id="disAction" class="disAction" data-id="" src="_assests/Images/downarrow.png" style="border-width: 0px;">
                                                        </div>
                                                    </td>
                                                    <td class="tdImage2" id="tdImage2" runat="server" style="width: 120px">
                                                        <img class="imgMember2" id="imgMember2" runat="server" src="" />
                                                        <br />
                                                        <asp:Label ID="lblMemberTemp2" CssClass="lblMember2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="divMDa" data-team="" style="width: 100%">
                                            <table class="tblDis" style="width: 72%">
                                                <tr>
                                                    <td class="tdImage" style="width: 120px">
                                                        <asp:Image CssClass="imgMember" ID="imgMember" runat="server" ImageUrl="_assests/Images/client.png" />
                                                        <br />
                                                        <asp:Label ID="lblMemberTemp" CssClass="lblMember" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtMemDis" CssClass="txtMemDis" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                                        <br />
                                                        <asp:Button ID="btnSend" CssClass="btnSend" runat="server" Style="float: right" Text="Add Comment" Width="96px" />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:FileUpload ID="fileUpload1" CssClass="fileUpload1" runat="server" Style="float: left" />
                                                                </td>
                                                                <td>
                                                                    <%--<a href="javascript:void(0)" id="aUpload" class="aUpload">Upload</a>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="oldLoader" class="oldLoader" style="height: 100%; width: 100%; background-color: white">
                                    </div>
                                </div>
                                <div id="tempDiv" style="display: none;"></div>
                            </div>
                        </div>
                        <div id="divDialog" class="divDialog" style="display: none">
                            <p id="paraDialog"></p>
                            <p id="p1"></p>
                        </div>
                        <div id="divcpdialog" class="divcpdialog" style="display: none">
                            <p id="p2" class="paraDialog">
                            </p>
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>
            <!-- rightpanel3  -->
            <div data-role="panel" id="rightpanel3" data-position="right" data-display="overlay" data-theme="a">
                <div class="ContentRight_List" style="float: right; margin-top: 33px;">
                    <div id="contentRight-Contents">
                        <div class="container_box container_files">
                            <div id="divdisfiles" class="divdisfiles" style="width: 165px; word-break: break-all;">
                            </div>
                        </div>
                        <div class="container_box">
                            <h2>Team (<asp:Label ID="lblTeamCount" runat="server" Text="0"></asp:Label>) <span id="spanTeam" runat="server" style="font-size: 12px; font-weight: normal;">[<a href="javascript:void(0)" id="aAddTeam" class="aAddTeam">Add Team</a>]</span>
                            </h2>
                            <div id="divManageTeam" class="divManageTeam" runat="server" style="display: none">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkAddNewTeam" CssClass="chkAddNewTeam" runat="server" Text="Add New" Checked="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trExTeam" class="trExTeam" style="display: none">
                                        <td>Existing:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlTeamMembers" CssClass="ddlTeamMembers" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="trTeam trNewTeam trTeamName">
                                        <td style="width: 55px;">Name:   
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTeamName" CssClass="txtTeamName" runat="server"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamName" EnableClientScript="true" ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="trTeam">
                                        <td style="width: 55px;">Alias:   
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTeamAlias" CssClass="txtTeamAlias" runat="server"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamAlias" EnableClientScript="true" ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="trTeam">
                                        <td>Title:</td>
                                        <td>
                                            <asp:TextBox ID="txtTeamTitle" CssClass="txtTeamTitle" runat="server"></asp:TextBox>
                                            <%--&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamTitle" EnableTeamScript="true" ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>--%>
                                            <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnSelTeamID" Value="0" />
                                        </td>
                                    </tr>
                                    <tr class="trNewTeam trTeam">
                                        <td>Email:</td>
                                        <td>
                                            <asp:TextBox ID="txtTeamEmail" CssClass="txtTeamEmail" runat="server" Width="127px"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamEmail" EnableClientScript="true" ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                            &nbsp;&nbsp;
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TeamValidation" ControlToValidate="txtTeamEmail"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:CheckBox ID="chkAdmin" runat="server" CssClass="chkAdmin" Text="Admin rights" />
                                        </td>
                                    </tr>
                                    <tr class="trNewTeam trTeam">
                                        <td colspan="2">
                                            <asp:Button ID="btnAddTeam" CssClass="btnAddTeam" Text="Add" runat="server" OnClick="btnAddMember_Click" CommandName="Team" ValidationGroup="TeamValidation" />
                                            <asp:Button ID="btnCancelExTeam" CssClass="btnCancelTeam" Text="Cancel" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="trExTeam trTeam" style="display: none">
                                        <td colspan="2">
                                            <asp:Button ID="btnAddExTeam" CssClass="btnAddExTeam" CommandName="Team" runat="server" Text="Add" OnClick="btnAddEx_Click" />
                                            <asp:Button ID="btnCancelTeam" CssClass="btnCancelTeam" Text="Cancel" runat="server" />
                                            <asp:Label ID="lblInfoExTeam" runat="server" CssClass="lblInfo"></asp:Label>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Label ID="lblInfoTeam" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                            <hr />
                            <asp:Repeater ID="rptTeamMembers" runat="server" OnItemCommand="rptTeamMembers_ItemCommand">
                                <HeaderTemplate>
                                    <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <%# DataBinder.Eval(Container.DataItem,"Alias") %>
                        &nbsp;
                            <asp:ImageButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-alias='<%# DataBinder.Eval(Container.DataItem,"Alias") %>' data-title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' data-email='<%# DataBinder.Eval(Container.DataItem,"Email") %>' data-roleid='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>' data-name='<%# DataBinder.Eval(Container.DataItem,"FirstName") %>' runat="server" ID="lnkTeamEdit" class="lnkTeamEdit" CommandName="edit" ImageUrl="_assests/Images/16 x 16/pencil.png" Width="12px" ToolTip="Edit team" />
                                        &nbsp;
                            <asp:ImageButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' runat="server" ID="lnkTeamRemove" class="lnkTeamRemove lnkRemove" CommandName="Remove" ImageUrl="_assests/Images/16 x 16/Close.png" Width="12px" ToolTip="Remove team" />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="container_box">
                            <h2>Client (<asp:Label ID="lblClientCount" runat="server" Text="0"></asp:Label>) <span id="spanClient" runat="server" style="font-size: 12px; font-weight: normal;">[<a href="javascript:void(0)" id="aAddClient" class="aAddClient">Add Client</a>]</span>
                            </h2>
                            <div id="divManageClient" class="divManageClient" runat="server" style="display: none">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkAddNewClient" CssClass="chkAddNewClient" runat="server" Text="Add New" Checked="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trExClient" class="trExClient" style="display: none">
                                        <td>Existing:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlClientMembers" CssClass="ddlClientMembers" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr class="trNewClient trClient trClientName">
                                        <td style="width: 55px;">Name:   
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClientName" CssClass="txtClientName" runat="server"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" ControlToValidate="txtClientName" EnableClientScript="true" ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="trClient">
                                        <td style="width: 55px;">Alias:   
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClientAlias" CssClass="txtClientAlias" runat="server"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtClientAlias" EnableClientScript="true" ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr class="trClient">
                                        <td>Title:</td>
                                        <td>
                                            <asp:TextBox ID="txtClientTitle" CssClass="txtClientTitle" runat="server"></asp:TextBox>
                                            <%--&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtClientTitle" EnableClientScript="true" ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>--%>
                                            <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnSelClientID" Value="0" />
                                        </td>
                                    </tr>
                                    <tr class="trNewClient trClient">
                                        <td>Email:</td>
                                        <td>
                                            <asp:TextBox ID="txtClientEmail" CssClass="txtClientEmail" runat="server" Width="127px"></asp:TextBox>
                                            &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtClientEmail" EnableClientScript="true" ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                            &nbsp;&nbsp;
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ClientValidation" ControlToValidate="txtClientEmail"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr class="trNewClient trClient">
                                        <td colspan="2">
                                            <asp:Button ID="btnAddClient" CssClass="btnAddClient" Text="Add" CommandName="Client" runat="server" OnClick="btnAddMember_Click" ValidationGroup="ClientValidation" />
                                            <asp:Button ID="btnCancelClient" CssClass="btnCancelClient" Text="Cancel" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="trExClient trClient" style="display: none">
                                        <td colspan="2">
                                            <asp:Button ID="btnAddExClient" CssClass="btnAddExClient" CommandName="Client" runat="server" Text="Add" OnClick="btnAddEx_Click" />
                                            <asp:Button ID="btnCancelExClient" CssClass="btnCancelClient" Text="Cancel" runat="server" />
                                            <asp:Label ID="lblInfoExClient" runat="server" CssClass="lblInfo"></asp:Label>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Label ID="lblInfoClient" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                            <hr />
                            <asp:Repeater ID="rptClient" runat="server" OnItemCommand="rptClient_ItemCommand">
                                <HeaderTemplate>
                                    <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><%# DataBinder.Eval(Container.DataItem,"Alias") %> &nbsp;
                            <asp:ImageButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-alias='<%# DataBinder.Eval(Container.DataItem,"Alias") %>' data-title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' data-email='<%# DataBinder.Eval(Container.DataItem,"Email") %>' data-roleid='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>' data-name='<%# DataBinder.Eval(Container.DataItem,"FirstName") %>' runat="server" ID="lnkClienkEdit" class="lnkClienkEdit" CommandName="edit" ImageUrl="_assests/Images/16 x 16/pencil.png" Width="12px" ToolTip="Edit client" />
                                        &nbsp;
                            <asp:ImageButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' runat="server" ID="lnkClientRemove" class="lnkClientRemove lnkRemove" CommandName="Remove" ImageUrl="_assests/Images/16 x 16/Close.png" Width="12px" ToolTip="Remove client" />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="container_box" id="disAllowedContent" runat="server">
                            <h2>Filters</h2>
                            <hr />
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtContents" CssClass="txtContents" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSaveDAContents" CssClass="btnSaveDAContents" runat="server" OnClick="btnSaveDAContents_Click" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="line-height: 16px !important">
                                        <%--<p style="line-height: 16px">--%>
                                Set up contents which you want to disallow members to use while communication in this discussion
                                <br />
                                        <b>i.e $, John, Thanks etc </b>
                                        <%--</p>--%>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /rightpanel3 -->

            <div data-role="panel" id="leftpanel3" data-position="left" data-display="overlay" data-theme="a">
                <%--<h2>My Discussions
                <asp:Label ID="lblUnread" CssClass="lblUnread" runat="server" Text="" Visible="false"></asp:Label>--%>

                <%--</h2>--%>

                <asp:Label runat="server" ID="lblUnread" CssClass="lblUnreadCount" Style="display: none"></asp:Label>
                <div data-role="tabs" id="tabs">
                    <div data-role="navbar">
                        <ul>
                            <li><a href="#one" data-ajax="false">Open<asp:Label ID="lblOpenCount" runat="server"></asp:Label></a></li>
                            <li><a href="#two" data-ajax="false" style="min-width: 50px !important;">Closed<asp:Label ID="lblArchiveCount" runat="server"></asp:Label></a></li>
                            <li>
                                <a href="#" id="aStartDiscussion" runat="server" class="aStartDiscussion" style="display: none">
                                    <img src="_assests/Images/16%20x%2016/Add-New.png" width="14px" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="one" class="ui-body-d ui-content">
                        <asp:Repeater ID="rptDiscussionList" runat="server" OnItemDataBound="rptDiscussionList_ItemDataBound">
                            <HeaderTemplate>
                                <ul data-role="listview" style="padding-left: 0px !important">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li style="border-bottom: 1px solid #ccc; margin-bottom: 5px;">
                                    <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="opendis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>'><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                    <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div id="two">
                        <asp:Repeater ID="rptDiscussionListClosed" runat="server" OnItemDataBound="rptDiscussionListClosed_ItemDataBound">
                            <HeaderTemplate>
                                <ul data-role="listview">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="closedis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>'><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                    <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>

            </div>
            <!-- /leftpanel3 -->



            <div id="divAskTitle" class="divAskTitle" style="display: none">
                <table class="tblForm">
                    <tr>
                        <td class="formCaptionTd">Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDiscussionTitle" CssClass="txtDiscussionTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">You can edit discussion title later by clicking on title of the discussion.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="display: none"></td>
                    </tr>
                </table>
            </div>
            <div id="divAskInfo" class="divAskInfo" style="display: none">
                <table class="tblForm">
                    <tr>
                        <td class="formCaptionTd">Name: 
                        </td>
                        <td>
                            <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Alias:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMAlias" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formCaptionTd">Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="display: none"></td>
                    </tr>
                </table>
            </div>
            <asp:Button ID="btnUpload" runat="server" CssClass="btnUpload" OnClick="btnUpload_Click" Style="display: none" />
            <asp:Button ID="btnSaveMemberInfo" CssClass="btnSaveMemberInfo" runat="server" OnClick="btnSaveMemberInfo_Click" Style="display: none" />
            <%--<asp:Button ID="btnSetTitle" runat="server" OnClick="btnSetTitle_Click" />--%>
            <asp:HiddenField runat="server" ID="hdnDiscussionIsNew" Value="0" />
            <asp:HiddenField runat="server" ID="hdnProjectID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnTeamMember" Value="0" />
            <asp:HiddenField runat="server" ID="hdninstanceid" Value="0" />
            <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnMemDisID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnSelMemDisID" Value="0" />
            <asp:HiddenField runat="server" ID="hdn_folderid" Value="0" />
            <asp:HiddenField runat="server" ID="hdnRoleID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnMoveToBottom" Value="1" />
            <asp:HiddenField runat="server" ID="hdnTotalComments" Value="0" />
            <asp:HiddenField ID="hdnAskInfo" runat="server" Value="0" ClientIDMode="Static" />
        </div>
    </form>
</body>
</html>
