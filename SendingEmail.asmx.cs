﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyCCM;
using System.Web.Services;
using System.Net.Mail;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM
{
    /// <summary>
    /// Summary description for SendingEmail
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SendingEmail : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            try
            {
                string smtphost = "smtp.office365.com";//"mail.avaima.com";

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("no-reply@avaima.com");
                msg.To.Add("sundus_csit@yahoo.com");

                msg.Subject = "Testing Email";
                msg.IsBodyHtml = true;
                msg.Body = "Hi !!!!!!";

                SmtpClient mSmtpClient = new SmtpClient(smtphost);
                mSmtpClient.Credentials = new System.Net.NetworkCredential("no-reply@avaima.com", "j4n8KFM023@");
                mSmtpClient.EnableSsl = true;
                mSmtpClient.Port = 587;
                mSmtpClient.Send(msg);
                return "Email Sent";
            }
            catch (Exception ex)
            {

                return ex.Message.ToString();
            }


        }
        [WebMethod]
        public string sendEmail()
        {
            SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
            SmtpServer.Port = 587;
            string res = "";
            SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@avaima.com", "j4n8KFM023@");
            SmtpServer.EnableSsl = true;
            IList<EmailDetail> em = EmailDetail.Find(u => u.status == 0);
            for (int i = 0; i < em.Count; i++)
            {
                try
                {
                    string toEmail = em[i].ToEmail;
                    string fromname = em[i].FromName;
                    string replayemail = em[i].replyEmail;
                    string body = em[i].body;
                    string subject = em[i].subject;
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress("no-reply@avaima.com");
                    mail.To.Add(toEmail);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    mail.ReplyTo = new MailAddress(replayemail);
                    SmtpServer.Send(mail);
                    em[i].status = 1;//Email sent successfully.......
                    em[i].Update();
                    res += "Email sent: ID-" + em[i].ID + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    int resendTry = 0;
                    if (em[i].ResendTry == null)
                    {
                        resendTry = 1;
                    }
                    else
                    {
                        resendTry = Convert.ToInt16(em[i].ResendTry) + 1;
                    }
                    em[i].ResendTry = resendTry; //Exception occured..
                    em[i].Exception = ex.ToString();
                    em[i].Update();
                    res += "Exception occured:" + ex.ToString() + ": ID-" + em[i].ID + Environment.NewLine;
                }
            }
            //Delete Previous Month Record...
            CCM.App_Code.CCMDiscussion.DeleteOldEmails();
            res += "Previous emails also deleted ";
            return res;
        }
        [WebMethod]
        public string SentEmailReport()
        {
            string res = "";
            try
            {
                SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@avaima.com", "j4n8KFM023@");
                SmtpServer.EnableSsl = true;
                string subject = "Email Report";
                string toEmail = "support@avaib.com";
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("no-reply@avaima.com");
                mail.To.Add(toEmail);
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                string body = "";
                IList<EmailDetail> em = EmailDetail.Find(u => u.ResendTry > 0 && u.status != 1); // Get Failure Emails
                int totalSentEmail = CCM.App_Code.CCMDiscussion.GetSentEmails();// Get Sent Emails
                if (em != null && em.Count > 0)
                {
                    body += @" Emails were not sent today due to following exception occurred...
                                    <table border='1' cellspacing='0' cellpadding='2'>
                                        <tr>
                                            <td>ID</td>
                                            <td>To Email</td>
                                            <td>Exception</td>
                                            <td>Resend Try</td>
                                        </tr>";
                    for (int i = 0; i < em.Count; i++)
                    {
                        body += "<tr><td>" + em[i].ID + "</td><td>" + em[i].ToEmail + "</td><td>" + em[i].Exception + "</td><td>" + em[i].ResendTry + "</td></tr>";
                    }

                    body += " </table>";
                }
                /*else
                 {
                     body += "No Exception occured while sending email.";
                     body += Environment.NewLine;
                 }*/
                if (totalSentEmail.ToString() != "" && totalSentEmail > 0)
                {
                    body += Environment.NewLine;
                    body += "Total " + totalSentEmail + " emails sent successfully...";
                }
                else
                {
                    body += Environment.NewLine;
                    body += "Total 0 email sent";
                    body += Environment.NewLine;
                }


                mail.Body = body;
                SmtpServer.Send(mail);
                res = "Email Statistics Report sent..";
            }
            catch (Exception ex)
            {
                res = "Exception occured= " + ex.ToString();
            }
            return res;
        }

        [WebMethod]
        public string sendSummaryReport()
        {
            string strResult = "";
            try
            {
                IList<MyCCM.Discussion> dis = MyCCM.Discussion.Find(u => u.sendsummary == true);
                if (dis.Count <= 0)
                {
                    strResult = "No Record Found";
                }
                for (int i = 0; i < dis.Count; i++)
                {
                    string emailContent = dis[i].EmailContent;
                    int discussionId = dis[i].DiscussionID;
                    string Title = dis[i].Title;
                    DateTime LastSent = new DateTime();
                    int days = Convert.ToInt16(dis[i].Duration);
                    if (dis[i].SummarySentDate == null)
                    {
                        LastSent = DateTime.Now.AddDays(-days);
                    }
                    else
                    {
                        LastSent = dis[i].SummarySentDate.Value;
                    }
                    if (LastSent.AddDays(days).Date == DateTime.Now.Date)
                    {
                        List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == discussionId).ToList();
                        var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
                  new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
                        var tableClient = acc.CreateCloudTableClient();
                        var table = tableClient.GetTableReference("MemberDiscussions");
                        TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
                            TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionId.ToString()),
                        TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)));
                        var entities1 = table.ExecuteQuery(rangeQuery);
                        int totalminutes = entities1.Sum(x => x.Minutes);
                        string hour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                        Int16 minutes = Convert.ToInt16(totalminutes % 60);
                        string summary = hour + " Hours " + minutes.ToString() + " Minutes ";
                        emailContent = emailContent.Replace("@Summary", summary);
                        emailContent = emailContent.Replace("@ProjectName", Title);

                        SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                        //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                        //SmtpServer.Port = 587;
                        //SmtpServer.EnableSsl = true;
                        SmtpServer.Port = 587;
                        SmtpServer.Credentials = new System.Net.NetworkCredential("no-reply@avaima.com", "j4n8KFM023@");
                        // SmtpServer.Credentials = new System.Net.NetworkCredential("tooba.sabah1317@gmail.com", "000");
                        SmtpServer.EnableSsl = true;
                        string subject = "Summary Report";
                        MailMessage mail = new MailMessage();
                        // mail.From = new MailAddress("tooba.sabah1317@gmail.com");
                        mail.From = new MailAddress("no-reply@avaima.com");
                        mail.Subject = subject;
                        foreach (DiscussionToFolder item in mtds)
                        {
                            Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                            if (member != null)
                            {
                                mail.To.Clear();
                                string toEmail = member.Email;
                                mail.To.Add(toEmail);
                                mail.Body = emailContent;
                                SmtpServer.Send(mail);
                                strResult += "Sent Summary with Discussion Id=" + discussionId.ToString() + Environment.NewLine;
                            }
                        }
                        dis[i].SummarySentDate = DateTime.Now;
                        dis[i].Update();

                    }
                }
            }
            catch
                (Exception ex)
            {
                strResult += "Exception occured: " + ex.ToString();
            }
            return strResult;
        }
    }
}
