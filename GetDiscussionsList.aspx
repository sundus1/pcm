﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FilesPerDiscussion.aspx.cs" Inherits="CCM._GetDiscussionsList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divDiscussionList" class="divDiscussionList">
            <asp:Label runat="server" ID="lblUnread" CssClass="lblUnreadCount" Style="display: none"></asp:Label>
            <div id="tabs">
                <ul style="background-color: #333333;">
                    <li><a href="#tabs-1">Open
                        <asp:Label ID="lblOpenCount" runat="server"></asp:Label></a></li>
                    <li><a href="#tabs-2">Closed
                        <asp:Label ID="lblArchiveCount" runat="server"></asp:Label></a></li>
                    <li class="li-no">
                        <div class="li-no-edit" style="margin-top: 3px;">
                        </div>
                    </li>
                </ul>
                <div id="tabs-1" style="border-color: white !important; padding-left: 0px !important; padding-right: 0px !important; overflow-x: hidden; padding: 0px;">
                    <asp:Repeater ID="rptDiscussionList" runat="server" OnItemDataBound="rptDiscussionList_ItemDataBound">
                        <HeaderTemplate>
                            <ul style="list-style-type: none; padding: 0px !important;background-color:#333333">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="margin-bottom: 0px;">
                                <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="opendis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' style="width: 94% !important; padding: 10px;color:#ccc"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="tabs-2" style="border-color: white !important; padding-left: 0px !important; padding-right: 0px !important; overflow-x: hidden; padding: 0px;">
                    <asp:Repeater ID="rptDiscussionListClosed" runat="server" OnItemDataBound="rptDiscussionListClosed_ItemDataBound">
                        <HeaderTemplate>
                            <ul style="list-style-type: none; padding: 0px !important;background-color:#333333">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="margin-bottom: 0px;">
                                <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="closedis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' style="width: 94% !important; padding: 10px;color:#ccc"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>



        </div>
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
    </form>
</body>
</html>
