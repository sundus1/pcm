﻿using AvaimaThirdpartyTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class AppInstructions : AvaimaWebPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                //AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
                //AvaimaEmailAPI email = new AvaimaEmailAPI();
                //AvaimaUserProfile obj = new AvaimaUserProfile();
                //string[] user = obj.GetInstanceOwner(CCM.App_Code.Utilities.AppID, this.InstanceID).Split('#');
                //string[] onlineuser = obj.Getonlineuserinfo(HttpContext.Current.User.Identity.Name).Split('#');
                //string msgBody = "Dear " + user[0] + " " + user[1] + "<br /><br />";
                //msgBody += txtmsg.Text + "<br /><br />";
                //msgBody += "Requested Person Infomation <br /><br />";
                //msgBody += "First Name: " + onlineuser[0] + "<br />";
                //msgBody += "Last Name: " + onlineuser[1] + "<br />";
                //msgBody += "Email: " + onlineuser[2] + "<br />";
                //msgBody += "TimeZone: " + onlineuser[4] + "<br />";
                //msgBody += "Date: " + objATZ.GetDate(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name) + "<br />";
                //msgBody += "Time: " + objATZ.GetTime(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name) + "<br />";
                //msgBody += "Phone: " + onlineuser[5].Replace('|', ' ') + "<br />";
                //msgBody += "Application: Team Communication Application";


                //email.send_email(user[2], "Team Communication Application", "", msgBody, ddlreq.SelectedValue);
                //txtmsg.Text = "";
                //lblmsg.Text = "Mail Sent Successfully!";
        }

        protected void btnsend_Click(object sender, EventArgs e)
        {
            AvaimaTimeZoneAPI objATZ = new AvaimaTimeZoneAPI();
            AvaimaEmailAPI email = new AvaimaEmailAPI();
            AvaimaUserProfile obj = new AvaimaUserProfile();
            string[] user = obj.GetInstanceOwner(CCM.App_Code.Utilities.AppID, this.InstanceID).Split('#');
            string[] onlineuser = obj.Getonlineuserinfo(HttpContext.Current.User.Identity.Name).Split('#');
            string msgBody = "Dear " + user[0] + " " + user[1] + "<br /><br />";
            msgBody += txtmsg.Text + "<br /><br />";
            msgBody += "Requested Person Infomation <br /><br />";
            msgBody += "First Name: " + onlineuser[0] + "<br />";
            msgBody += "Last Name: " + onlineuser[1] + "<br />";
            msgBody += "Email: " + onlineuser[2] + "<br />";
            msgBody += "TimeZone: " + onlineuser[4] + "<br />";
            msgBody += "Date: " + objATZ.GetDate(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name) + "<br />";
            msgBody += "Time: " + objATZ.GetTime(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name) + "<br />";
            msgBody += "Phone: " + onlineuser[5].Replace('|', ' ') + "<br />";
            msgBody += "Application: Team Communication Application";


            email.send_email(user[2], "Team Communication Application", "", msgBody, ddlreq.SelectedValue);
            txtmsg.Text = "";
            lblmsg.Text = "Mail Sent Successfully!";
        }
    }
}