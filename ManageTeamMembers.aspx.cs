﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using CCM;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM
{
    public partial class ManageTeamMembers : System.Web.UI.Page
    {
        public static String _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab";
             
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                initializeData((Request.QueryString["T"] != null) ? Request.QueryString["T"].ToString() : "no");

                //Team Managemnt for selecetd Project
                if (Request.QueryString["Team"].Decrypt().ToString() == "Yes")
                {
                    manageteam.Visible = true;
                    manageprojects.Visible = false;

                    if (Request.QueryString["MemberId"] != null && Request.QueryString["IntanceID"] != null)
                    {
                        if (Request.QueryString["T"] != null)
                        {
                            LoadDisscussion(Request.QueryString["MemberId"].ToInt32(), Request.QueryString["IntanceID"]);
                        }
                        else
                            LoadDisscussion(Request.QueryString["MemberId"].Decrypt().ToInt32(), Request.QueryString["IntanceID"]);
                    }
                    if (Request.QueryString["DiscussionID"] != null)
                    {
                        if (Request.QueryString["T"] != null)
                        {
                            ddlProjects.SelectedValue = Request.QueryString["DiscussionID"].ToString();
                        }
                        else
                            ddlProjects.SelectedValue = Request.QueryString["DiscussionID"].Decrypt().ToString();
                    }

                    BindData();
                }
                //Project Management for selected User/Member
                else if (Request.QueryString["Team"].Decrypt().ToString() == "No")
                {
                    manageteam.Visible = false;
                    manageprojects.Visible = true;
                    BindProjects();
                }

            }
            lblMessage.Text = "";
        }

        //Bind Project Dropdown
        private void LoadDisscussion(int memberID, string instanceID)
        {
           // DataTable dt = CCMDiscussion.GetReportDiscussions(memberID, instanceID);
            DataTable dt = CCM_Helper.GetMemberDiscussionsList(memberID);
            ddlProjects.DataValueField = dt.Columns["DiscussionID"].ToString();
            ddlProjects.DataTextField = dt.Columns["Title"].ToString();
            ddlProjects.DataSource = dt.DefaultView;
            ddlProjects.DataBind();

            int i;
            for ( i = 0; i < ddlProjects.Items.Count; i++)
            {
                ListItem li = ddlProjects.Items[i];
                DataRow dr = dt.Rows[i];
                li.Attributes.Add("data-active",dr["Status"].ToString());
            }
            AlterDropdownList(ddlProjects);
        }

        protected void AlterDropdownList(DropDownList myDropDownList)
        {
            foreach (ListItem item in myDropDownList.Items)
            {
                if (item.Attributes["data-active"] == "Close" )
                {
                    item.Text = item.Text + " - (Closed Project)";
                }
            }            
        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(window).on('beforeunload', function () {   $('.dataTable').fadeOut();  NProgress.start(); });", true);
            BindData();
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(document.getElementById('list')).ready(function() {   $('.dataTable').fadeIn(); NProgress.done();   });  ", true);
            
        }

        //Set Hidden Fields
        private void initializeData(string check)
        {
            if (check == "no")
            {
                hdnMemberID.Value = Request.QueryString["MemberId"].Decrypt().ToString();
                hdnDiscussionID.Value = (Request.QueryString["DiscussionID"] != null) ? Request.QueryString["DiscussionID"].Decrypt().ToString() : "0";
            }
            else
            {
                hdnMemberID.Value = Request.QueryString["MemberId"].ToString();
                hdnDiscussionID.Value = (Request.QueryString["DiscussionID"] != null) ? Request.QueryString["DiscussionID"].ToString() : "0";
            }
            hdnIntanceID.Value = Request.QueryString["IntanceID"].ToString();
        }

        //Bind Data for a selected Project
        protected void BindData()
        {
            try
            {
                int memberID = hdnMemberID.Value.ToInt32();
                string instanceID = hdnIntanceID.Value;
                string DisID = ddlProjects.SelectedValue;

                hdnDiscussionID.Value = DisID;

                string Html = LoadData(DisID, memberID);
                list.InnerHtml = Html;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
            }
        }

        //List Team Members for Selected Project
        protected string LoadData(string DisID, int memberID)
        {
            string str = "";
            string disabled = "";
            OwnerToMember owner;
            string team = ("No").Encrypt();
            str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='95%' class='table table-hover table-bordered table-striped paginated'><thead><tr><th>#</th><th>Name</th><th>Title</th><th>Email</th><th>Role</th><th></th></tr></thead><tbody>";

            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == DisID.ToInt32() &&
           (u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 1 || u.RoleID == 6 || u.RoleID == 7)).Where(u => u.MemberID != memberID && u.Active == true).ToList();

            if (mtds.Count() > 0)
            {
                foreach (var member in mtds)
                {
                    //If owner to Member than enable ProjectList
                    owner = CCM_Helper.GetMemberOwner((int)member.MemberID);
                    if (owner != null)
                    {
                        if (member.RoleID == 1)
                        {
                            disabled = "disabled='disabled'";
                        }
                        else
                        {
                            disabled = "";
                        }
                        str += "<tr><td><input type='checkbox' " + disabled + " class='chkDel' data-uid='" + member.DTFID + "'></td><td>" + member.Alias + "</td>";
                        str += "<td>" + member.Title + "</td>";
                        str += "<td>" + (CCM_Helper.GetMemberDetails((int)member.MemberID)).Email + "</td>";
                        str += "<td>" + CCM_Helper.GetMemberRoleSimplifiedName((int)member.RoleID) + "<input type='hidden' value='" + member.RoleID + "' id='user-roleid'></td>";
                        str += "<td><a  href='javascript: void(0)' data-target='#editMember' class='btnEdit' id='" + member.DTFID + "'> Edit </a> &nbsp;&nbsp;  <a  href='javascript: void(0)' class='btndanger' onclick='return RemoveMember(" + member.DTFID + ");'> Remove </a>";
                        if (owner.OwnerMemberID == memberID)
                        {
                            str += " &nbsp;&nbsp;<input type='hidden' value='allowed' id='OwnUser'> <a  href='ManageTeamMembers.aspx?OwnerID=" + memberID.ToString().Encrypt() + "&MemberId=" + member.MemberID.ToString().Encrypt() + "&IntanceID=" + member.InstanceID + "&Team=" + team + "'  class='btnDetails' id='" + member.DTFID + "' target='_blank'> Projects </a> ";
                        }
                        else
                        {
                            str += " &nbsp;&nbsp;<input type='hidden' value='notallowed' id='OwnUser'> <a  href='javascript:void(0);' style='cursor: default !important;background-color:#94989c !important;' data-toggle='tooltip' title='You are not authorised to view projects of this user!'> Projects </a> ";
                        }
                        str += "</td>";
                        str += "</tr>";
                    }
                }
            }
            else
            {
                str += "<tr><td colspan='6'>No Members Available!</td></tr>";
            }

            str += "</tbody></table>";
            return str;
        }

        //Bind Team Member Drropdown & Data for a Selected Member
        protected void BindProjects()
        {
            int Owner = Request.QueryString["OwnerID"].Decrypt().ToInt32();
            List<OwnerToMember> ownerToMembers = CCM_Helper.GetMembers(_LocalInstanceID, MemberRoles.All, Owner);
            ddlTeamMembers.DataSource = ownerToMembers.Where(u => (u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 6 || u.RoleID == 7) && u.MemberID != Owner).ToList();
            ddlTeamMembers.DataTextField = "NameAlias";
            ddlTeamMembers.DataValueField = "MemberID";
            ddlTeamMembers.DataBind();

            ddlTeamMembers.SelectedValue = Request.QueryString["MemberId"].Decrypt().ToString();
            LoadTable();
        }

        [WebMethod]
        public static string ProjectMembers(string Alias, string Title, int DTFId, bool IsActive, string DisID, int memberID, int RoleId, string Email, string CanEditEmail)
        {
            DiscussionToFolder member = DiscussionToFolder.SingleOrDefault(a => a.DTFID == DTFId);
            if (member != null)
            {
                member.Title = Title;
                member.Alias = Alias;
                member.RoleID = RoleId;
                member.Active = (IsActive == false) ? true : false;
                member.Update();
            }

            //Update Email
            if (CanEditEmail == "yes")
            {
                Member MemberBasicInfo = Member.SingleOrDefault(a=> a.MemberID == (int)member.MemberID);
                OwnerToMember MemberOwner = OwnerToMember.SingleOrDefault(b=> b.MemberID == (int)member.MemberID);
                if (MemberBasicInfo != null && MemberOwner != null)
                {
                    MemberBasicInfo.Email = Email;
                    MemberBasicInfo.Update();

                    MemberOwner.Email = Email;
                    MemberOwner.Update();
                }
            }

            ManageTeamMembers obj = new ManageTeamMembers();
            string Html = obj.LoadData(DisID, memberID);
            return Html;

        }

        [WebMethod]
        public static string RemoveMembers(string DisID, int memberID, string Ids)
        {
            string[] array = Ids.Split(',');

            foreach (string Id in array)
            {
                DiscussionToFolder member = DiscussionToFolder.SingleOrDefault(a => a.DTFID == Id.ToInt32());
                if (member != null)
                {
                    member.Active = false;
                    member.Update();
                }
            }

            ManageTeamMembers obj = new ManageTeamMembers();
            string Html = obj.LoadData(DisID, memberID);
            return Html;

        }

        [WebMethod]
        public static string RemoveProjects(int memberID, string DisIds)
        {
            string[] array = DisIds.Split(',');

            foreach (string Id in array)
            {
                DiscussionToFolder member = DiscussionToFolder.SingleOrDefault(a => a.DiscussionID == Id.ToInt32() && a.MemberID == memberID);
                if (member != null)
                {
                    member.Active = false;
                    member.Update();
                }
            }

            ManageTeamMembers obj = new ManageTeamMembers();
            string Html = obj.LoadProjects(memberID);
            return Html;

        }

        [WebMethod]
        public static string GetRoles(int selectedrole)
        {
            string html = "";
            List<Role> roles = Role.Find(a => a.RoleID != 4 && a.RoleID != 5).ToList();

            foreach (var role in roles)
            {
                if (role.RoleID == selectedrole)
                    html += "<option value='" + role.RoleID + "' selected='selected'>" + CCM_Helper.GetMemberRoleSimplifiedName(role.RoleID) + "</option>";
                else
                    html += "<option value='" + role.RoleID + "'>" + CCM_Helper.GetMemberRoleSimplifiedName(role.RoleID) + "</option>";
            }
            return html;
        }

        protected void ddlTeamMembers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(window).on('beforeunload', function () {   $('.dataTable').fadeOut();  NProgress.start(); });", true);
            LoadTable();
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(document.getElementById('list')).ready(function() {   $('.dataTable').fadeIn(); NProgress.done();   });  ", true);

        }

        protected void LoadTable()
        {
            string Html = LoadProjects(ddlTeamMembers.SelectedValue.ToInt32());
            projectslist.InnerHtml = Html;
        }

        //List Projects for selected User
        protected string LoadProjects(int memberID)
        {
            string ProjectTitle = "";
            string PrjctStatus = "";
            MyCCM.Discussion discussion = null;
            string str = "";
            string disabled = "";
            string team = ("No").Encrypt();
            str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='95%' class='table table-hover table-bordered table-striped paginated'><thead><tr><th>#</th><th>Project</th><th>Title</th><th>Role</th></tr></thead><tbody>";

            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.MemberID == memberID && u.Active == true).ToList();

            if (mtds.Count() > 0)
            {
                foreach (var member in mtds)
                {
                    if (member.RoleID == 1)
                    {
                        disabled = "disabled='disabled'";
                    }
                    else
                    {
                        disabled = "";
                    }
                    discussion = CCMDiscussion.GetDiscussionData(member.DiscussionID.ToInt32());
                    if (discussion != null)
                    {
                        PrjctStatus = (discussion.Status == "Close") ? " - (Closed Project)" : "";
                        ProjectTitle = discussion.Title + PrjctStatus;

                    }
                    else
                    {
                        ProjectTitle = "";
                    }
                    //ProjectTitle = (CCMDiscussion.GetDiscussionData(member.DiscussionID.ToInt32()) != null) ? CCMDiscussion.GetDiscussionData(member.DiscussionID.ToInt32()).Title : "";
                    
                    str += "<tr><td><input type='checkbox' " + disabled + " class='chkboxDel' data-uid='" + member.DTFID + "' data-did='" + member.DiscussionID + "'></td><td>" + ProjectTitle + "</td>";
                    str += "<td>" + member.Alias + " - " + member.Title + "</td>";
                    str += "<td>" + CCM_Helper.GetMemberRoleSimplifiedName((int)member.RoleID) + "</td>";
                    str += "</tr>";
                }
            }
            else
            {
                str += "<tr><td colspan='5'>No Project Found!</td></tr>";
            }

            str += "</tbody></table>";
            return str;
        }
        
    }
}