﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using Microsoft.Win32;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using MyCCM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
//using CCM.App_Code.Extension;

namespace CCM
{
    public partial class Discussion : AvaimaWebPage
    {

        public String _LocalInstanceID = "";
        public string[] DisallowedContents;
        private string storagepath;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Redirect(Request.Url.ToString().Replace("/Discussion.aspx", "/_Discussion.aspx"));
            if (Request.QueryString["instanceid"] == null)
            {
                Response.Redirect(Request.Url.AbsoluteUri + "&instanceid=a4841f77-2e42-46d7-904f-54f2c63cafab");
                return;
            }

            if (this.InstanceID == "0") { _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab"; }
            else { _LocalInstanceID = this.InstanceID; }
            hdnMoveToBottom.Value = "0";
            if (!IsPostBack)
            {
                initalizeProjectData();
                hdnMoveToBottom.Value = "1";
            }
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID);
            if (member != null)
            {
                if (String.IsNullOrEmpty(member.Title) || String.IsNullOrEmpty(member.FirstName))
                {
                    hdnAskInfo.Value = "1";
                }
                else
                {
                    hdnAskInfo.Value = "0";
                }
            }
            else
            {

            }
            if (hdnDisID.Value.ToInt32() == 0)
            {
                MyCCM.Discussion discussion = new MyCCM.Discussion()
                {
                    ProjectID = hdnProjectID.Value.ToInt32(),
                    Active = true,
                    CrtDate = DateTime.Now,
                    //FolderID = hdn_folderid.Value.ToInt32(),
                    InstanceID = _LocalInstanceID,
                    ModDate = DateTime.Now,
                    Status = "Open",
                    Title = "Untitled"
                };
                if (Request.QueryString["dtitle"] != null)
                {
                    discussion.Title = Request.QueryString["dtitle"];
                }
                discussion.Add();
                btnStatus.ImageUrl = "_assests/Images/16 x 16/Tick.png";
                btnStatus.ToolTip = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                btnStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                DiscussionToFolder disToFolder = new DiscussionToFolder();
                Folder parentFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
                if (parentFolder != null)
                {
                    List<MemberToFolder> adminsOfFolder = MemberToFolder.Find(u => (u.RoleID == 5 || u.RoleID == 2 || u.RoleID == 1) && u.FolderID == hdn_folderid.Value.ToInt32()).ToList();
                    foreach (MemberToFolder adminOfFolder in adminsOfFolder)
                    {
                        //disToFolder = DiscussionToFolder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32() && u.MemberID == adminOfFolder.MemberID.ToInt32() && u.InstanceID == _LocalInstanceID);
                        //if (disToFolder == null)
                        //{
                        disToFolder = new DiscussionToFolder()
                        {
                            InstanceID = _LocalInstanceID,
                            MemberID = adminOfFolder.MemberID,
                            FolderID = hdn_folderid.Value.ToInt32(),
                            Active = true,
                            CrtDate = DateTime.Now,
                            ModDate = DateTime.Now,
                            RoleID = adminOfFolder.RoleID,
                            Status = 1,
                            DiscussionID = discussion.DiscussionID,
                            Alias = adminOfFolder.Alias,
                            Title = adminOfFolder.Title,
                            Unread = 0
                        };
                        disToFolder.Add();
                        //}
                        //else
                        //{
                        //    disToFolder.MemberID = adminOfFolder.MemberID;
                        //    disToFolder.ModDate = DateTime.Now;
                        //    disToFolder.RoleID = adminOfFolder.RoleID;
                        //    disToFolder.Status = 1;
                        //    disToFolder.DiscussionID = discussion.DiscussionID;
                        //    disToFolder.Update();
                        //}
                    }
                }
                else
                {
                    disToFolder = new DiscussionToFolder()
                    {
                        MemberID = hdnMemberID.Value.ToInt32(),
                        FolderID = 0,
                        Active = true,
                        CrtDate = DateTime.Now,
                        DiscussionID = discussion.DiscussionID,
                        InstanceID = _LocalInstanceID,
                        ModDate = DateTime.Now,
                        RoleID = 1,
                        Status = 1,
                        Alias = member.NickName,
                        Title = member.Title,
                        Unread = 0
                    };
                    disToFolder.Add();
                }

                disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID && u.DiscussionID == discussion.DiscussionID);
                if (disToFolder != null)
                {
                    hdnRoleID.Value = disToFolder.RoleID.ToString();
                    SetAccordingToRole(disToFolder.RoleID.ToInt32());
                }
                if (member != null)
                {
                    lblMemberTemp.Text = member.NickName;
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                    imgMember.ImageUrl = "_assests/Images/" + mr.ToString() + ".png";
                }
                String url = "Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&ir=" + disToFolder.RoleID.ToString().Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + InstanceID;
                this.Redirect(url);
            }
            else
            {
                AvaimaTimeZoneAPI ati = new AvaimaTimeZoneAPI();
                lblLastRefresh.Text = Utilities.GetGoogleFormatedDate(DateTime.Now.ToString()) + " at " + ati.GetTime(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name);
                divAssignProject.Style.Add("display", "none");
                //divManageMember.Style.Add("display", "none");
                lblInfoExClient.Text = "";
                lblInfoExTeam.Text = "";
                lblInfoTeam.Text = "";
                lblInfoClient.Text = "";
                DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                if (disToFolder == null)
                {
                    //Response.Write("You are not allowed to view this discussion.");                    
                    this.Redirect("Controls/info.aspx?h=You don't have the permission to view this discussion.&t=Contact the relevant person for details.");
                    //string jScript = "<script>alert('You are not allowed to view this discussion.');window.close();</script>";
                    //ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
                    return;
                }
                else
                {
                    //if (disToFolder.Unread > 0)
                    //{
                    //    disToFolder.Unread = 0;
                    //    disToFolder.Update();
                    //}
                    hdnRoleID.Value = disToFolder.RoleID.ToString();
                }
                DisplayAccordintToRole(hdnRoleID.Value.ToInt32());
                if (!IsPostBack)
                {
                    BindExistingMembersDLL();
                    FillMembers();
                    GenerateBreadCrumb();
                    //if (Request.QueryString["email"] != null) {
                    //    member = Member.SingleOrDefault(u => u.Email.Contains(Request.QueryString["email"].ToString()) && u.InstanceID == _LocalInstanceID);
                    //    rptBC.Visible = false;re
                    //}
                    hdnMemberID.Value = member.MemberID.ToString();
                    BindDiscussionList();
                    BindDiscussionData();
                }
                BindMemberDiscussions();
                if (member != null)
                {
                    if (String.IsNullOrEmpty(disToFolder.Alias))
                    {
                        lblMemberTemp.Text = member.NickName;
                    }
                    else
                    {
                        lblMemberTemp.Text = disToFolder.Alias;
                    }

                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                    imgMember.ImageUrl = "_assests/Images/" + mr.ToString() + ".png";
                }
            }

            h1Title.Focus();
        }

        public string GetDiscussionLink(Int32 discussionID, Int32 roleID)
        {
            String url = "Discussion.aspx?did=" + discussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&ir=" + roleID.ToString().Encrypt() + "&instanceid=" + _LocalInstanceID;
            return url;
        }

        private void DisplayAccordintToRole(Int32 RoleID)
        {
            switch (RoleID)
            {
                case 1:
                    // SuperAdmin
                    lblS.Visible = btnStatus.Visible = true;
                    spanClient.Visible = spanTeam.Visible = lnkManageMember.Visible = true;
                    h1Title.Attributes.Add("contenteditable", "true");
                    divManageTeam.Visible = divManageClient.Visible = true;
                    break;
                case 2:
                    // Team Admin
                    lblS.Visible = btnStatus.Visible = true;
                    spanClient.Visible = spanTeam.Visible = lnkManageMember.Visible = true;
                    h1Title.Attributes.Add("contenteditable", "true");
                    divManageTeam.Visible = divManageClient.Visible = true;
                    //lblS.Visible = lnkAssignProject.Visible = false;
                    //lnkManageMember.Visible = true;
                    //trTeam.Style.Clear();
                    //trClient.Style.Add("display", "none");
                    //rdoTeam.Visible = true;
                    //rdoClient.Visible = false;
                    //rdoTeam.Checked = true;
                    break;
                case 3:
                    // Team Member
                    lblS.Visible = btnStatus.Visible = false;
                    spanTeam.Visible = true;
                    spanClient.Visible = true;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = true;
                    h1Title.Attributes.Remove("contenteditable");
                    break;
                case 4:
                    // Client Member
                    btnStatus.Visible = true;
                    spanClient.Visible = lnkManageMember.Visible = true;
                    spanTeam.Visible = false;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = false;
                    h1Title.Attributes.Add("contenteditable", "false");
                    break;
                case 5:
                    // Client Admin
                    btnStatus.Visible = true;
                    spanClient.Visible = lnkManageMember.Visible = true;
                    spanTeam.Visible = false;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = false;
                    h1Title.Attributes.Add("contenteditable", "true");
                    break;
            }
        }

        public void BindDAData()
        {
            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == hdnDisID.Value.ToInt32() && u.FolderID == 0);
            DisallowedContent settingsDC = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == 0 && u.FolderID == 0);
            string disallowedContents = "";
            if (dc != null)
            {
                disallowedContents = txtContents.Text = dc.Contents;
                if (settingsDC != null)
                {
                    disallowedContents += (", " + settingsDC.Contents);
                }
                DisallowedContents = disallowedContents.Replace(" ", "").Split(',');
            }
            else
            {
                txtContents.Text = "";
            }
        }
        public static string Serialize(object o)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(o);
        }
        private void BindDiscussionData()
        {
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            String url = "Discussion.aspx?did=" + ("0").Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + _LocalInstanceID;
            aStartDiscussion.Attributes["href"] = url;
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            if (discussion != null)
            {
                title.Text = "Discussion - " + discussion.Title;
                if (discussion.Title == "Untitled")
                {
                    hdnDiscussionIsNew.Value = "1";
                }
                if (discussion.Status == "Open")
                {
                    btnStatus.ImageUrl = "_assests/Images/16 x 16/Tick.png";
                    btnStatus.CommandName = "Close";
                    btnStatus.ToolTip = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                    btnStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                }
                else
                {
                    btnStatus.ImageUrl = "_assests/Images/16 x 16/Untick.png";
                    btnStatus.CommandName = "Open";
                    btnStatus.ToolTip = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
                    btnStatus.Attributes["Title"] = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
                }
                String crtOnDate = atz.GetDate(discussion.CrtDate.ToString(), HttpContext.Current.User.Identity.Name);
                if (crtOnDate.Contains("Today"))
                {
                    crtOnDate = DateTime.Now.Date.ToShortDateString();
                }
                lblCreatedOn.Text = Utilities.GetGoogleFormatedDate(crtOnDate) + " at " + atz.GetTime(discussion.CrtDate.ToString(), HttpContext.Current.User.Identity.Name);
                //ddlProject.SelectedIndex = ddlProject.Items.IndexOf(ddlProject.Items.FindByValue(discussion.ProjectID.ToString()));
                txtDisTitle.Text = discussion.Title;
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(discussion.Status));
                h1Title.InnerText = discussion.Title;
                BindMemberDiscussions();
            }
            SetActiveDiscussion(hdnDisID.Value);
            GenerateBreadCrumb();
            BindDAData();
            //SetActiveDiscussion(discussion.DiscussionID.ToString());
        }

        private void BindMemberDiscussions()
        {
            //App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            //md.NewMemberDiscussion();
            //md.GetMemberDiscussionsByDiscussion(hdnDisID.Value);
            //rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.ModDate).ToList();
            //rptDiscussions.DataBind();
            //hdnTotalComments.Value = md.MemberDiscussions.Count.ToString();
        }

        private void BindClients()
        {
            // Get and fill Clients
            //Int32 roleID = Convert.ToInt32(MemberRoles.ClientMember);
            List<MemberPreview> _membersPreview = new List<MemberPreview>();
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value) && (u.RoleID == 4 || u.RoleID == 5)).ToList();
            foreach (var item in mtds)
            {
                Member tempMember = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMember != null)
                {
                    _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title));
                }
            }
            if (_membersPreview.Count > 0)
            {

                rptClient.DataSource = _membersPreview;
                rptClient.DataBind();

            }
            else
            {
                rptClient.DataSource = _membersPreview;
                rptClient.DataBind();
            }
            lblClientCount.Text = _membersPreview.Count.ToString();
            ClearClientFields();
        }

        private void BindTeam()
        {
            // Get and fill Team
            //Int32 roleID;
            List<MemberPreview> _membersPreview;
            List<DiscussionToFolder> mtds;
            //roleID = Convert.ToInt32(MemberRoles.VendorMember);
            _membersPreview = new List<MemberPreview>();
            mtds = DiscussionToFolder.Find(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value) && (u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 1)).ToList();
            foreach (var item in mtds)
            {
                Member tempMember = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMember != null) { _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title)); }
            }
            if (_membersPreview.Count > 0)
            {
                rptTeamMembers.DataSource = _membersPreview;
                rptTeamMembers.DataBind();
            }
            else
            {
                rptTeamMembers.DataSource = _membersPreview;
                rptTeamMembers.DataBind();
            }
            lblTeamCount.Text = _membersPreview.Count.ToString();
            ClearTeamFields();
        }

        private void FillMembers()
        {
            BindClients();
            BindTeam();
        }

        private void SetAccordingToRole(Int32 roleID)
        {
            switch (roleID)
            {
                case 1:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = true;
                    //lnkAddClient.Visible = lnkAddTeam.Visible = true;
                    //lblDash.Text = " - ";
                    break;
                case 2:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = true;
                    //lnkAddTeam.Visible = true;
                    //lnkAddClient.Visible = false;
                    //lblDash.Text = " ";
                    break;
                case 3:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = true;
                    //lnkAddClient.Visible = false;
                    h1Title.Attributes.Remove("contenteditable");
                    //lblDash.Text = " ";
                    break;
                case 4:
                    hdnTeamMember.Value = "false";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = false;
                    //lnkAddClient.Visible = true;
                    h1Title.Attributes.Remove("contenteditable");
                    //lblDash.Text = " ";
                    break;
                default:
                    hdnTeamMember.Value = "false";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = false;
                    //lnkAddClient.Visible = false;
                    h1Title.Attributes.Remove("contenteditable");
                    break;
            }
        }

        private void initalizeProjectData()
        {

            // OwnerID
            if (Request.QueryString["od"] != null)
            {
                if (String.IsNullOrEmpty(Request.QueryString["od"].Decrypt()))
                {
                    hdnOID.Value = null;
                }
                else
                {
                    hdnOID.Value = Request.QueryString["od"].Decrypt();
                }
            }
            else
            {
                hdnOID.Value = null;
            }

            // DiscussionID
            if (Request.QueryString["did"] != null)
            {
                if (String.IsNullOrEmpty(Request.QueryString["did"].Decrypt()))
                {
                    hdnDisID.Value = "0";
                }
                else
                {
                    hdnDisID.Value = Request.QueryString["did"].Decrypt();
                }
            }
            else
            {
                hdnDisID.Value = "0";
            }

            // ProjectID
            MyCCM.Discussion disc = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            if (disc != null) { hdnProjectID.Value = disc.ProjectID.ToString(); }
            else { hdnProjectID.Value = "0"; }

            // FolderID
            if (Request.QueryString["fid"] != null)
            {
                hdn_folderid.Value = Request.QueryString["fid"].Decrypt();
            }

            if (Request.QueryString["md"] != null)
            {
                hdnMemberID.Value = Request.QueryString["md"].Decrypt();
            }
            if (Request.QueryString["instanceid"] != null)
            {
                hdninstanceid.Value = Request.QueryString["instanceid"];
            }

        }

        private Role GetMemberRole(Int32 memberID)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberID));
            if (member != null)
            {
                MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(hdnProjectID.Value) && u.MemberID == Convert.ToInt32(hdnMemberID.Value));
                if (mtp != null)
                {
                    Role role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID);
                    return role;
                }
                else
                {
                    mtp = MemberToProject.SingleOrDefault(u => u.ProjectID == 0 && u.MemberID == Convert.ToInt32(hdnMemberID.Value));
                    if (mtp != null)
                    {
                        Role role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID);
                        return role;
                    }
                }
            }
            return null;
        }

        private void GenerateBreadCrumb()
        {
            List<MemberToFolder> t_Folders = MemberToFolder.Find(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID).ToList();
            int folderID = Convert.ToInt32(hdn_folderid.Value);
            List<Folder> folders = new List<Folder>();
            GetBCList(t_Folders, folderID, folders);
            rptBC.DataSource = folders.OrderBy(u => u.FolderID).ToList();
            rptBC.DataBind();
        }

        private void GetBCList(List<MemberToFolder> _Folders, int projID, List<Folder> folders)
        {
            MemberToFolder memberToFolder = _Folders.SingleOrDefault(u => u.FolderID == projID && u.MemberID == hdnMemberID.Value.ToInt32());
            if (memberToFolder != null)
            {
                Folder folder = Folder.SingleOrDefault(u => u.FolderID == memberToFolder.FolderID);
                if (folder != null)
                {
                    folders.Add(folder);
                }

                GetBCList(_Folders, Convert.ToInt32(memberToFolder.ParentFolderID.ToInt32()), folders);
            }
        }

        private string AddMember(MemberRoles MemberRole)
        {
            int discussionID = hdnDisID.Value.ToInt32();
            Int32 MemberID = 0;
            String Alias = "", Title = "", Email = "", Name = "";
            if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
            {
                MemberID = hdnSelClientID.Value.ToInt32();
                Alias = txtClientAlias.Text;
                Title = txtClientTitle.Text;
                Email = txtClientEmail.Text;
                Name = txtClientName.Text;
            }
            else
            {
                MemberID = hdnSelTeamID.Value.ToInt32();
                Alias = txtTeamAlias.Text;
                Title = txtTeamTitle.Text;
                Email = txtTeamEmail.Text;
                Name = txtTeamName.Text;
            }
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == _LocalInstanceID && u.MemberID == hdnMemberID.Value.ToInt32());
            MyCCM.Discussion Discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionID);
            if (discussionID > 0)
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(Email) && u.InstanceID == _LocalInstanceID);
                if (member == null)
                {
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        string ownerID = "clientuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (ownerID.Length > 36) { ownerID = ownerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTF(Discussion, currMember, member, ownerID, Email, Title, Alias, Name, MemberRole);
                        return info.Title + ":" + info.Description;
                    }
                    else
                    {
                        string ownerID = "teamuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (ownerID.Length > 36) { ownerID = ownerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTF(Discussion, currMember, member, ownerID, Email, Title, Alias, Name, MemberRole);
                        return info.Title + ":" + info.Description;
                        //AddinstanceWS objaddinst = new AddinstanceWS();
                        //string userid = objaddinst.AddInstance(Email, Utilities.AppID, _LocalInstanceID, HttpContext.Current.User.Identity.Name);
                        //Info info = CreateMemberWithDTF(Discussion, currMember, member, userid, Email, Title, Alias, Name, MemberRole);
                        //return info.Title + ":" + info.Description;
                    }
                }
                else
                {
                    Info info = CreateMemberWithDTF(Discussion, currMember, member, member.OwnerID, Email, Title, Alias, Name, MemberRole);
                    return info.Title + ":" + info.Description + ":" + member.MemberID;
                }
            }
            else
            {
                return "Error:No Discussion Found!";
            }
        }

        private Info CreateMemberWithDTF(MyCCM.Discussion discussion, Member currMember, Member member, string userid, string email, string title, string alias, string name, MemberRoles MemberRole)
        {
            Info info;
            if (member == null)
            {
                member = new Member()
                {
                    OwnerID = userid,
                    //ProjectID = 0,
                    InstanceID = _LocalInstanceID,
                    FirstName = name,
                    LastName = "",
                    NickName = alias,
                    Email = email,
                    Title = title,
                    //Type = MemberRoles.SuperAdmin.ToString(),
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    ImagePath = "_assests/images/" + CCM_Helper.GetMemberRoleName((int)MemberRole) + ".png"
                };
                member.Add();
            }

            //Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
            //MemberToFolder memberToFolder = new MemberToFolder();
            //if (mainFolder != null && (MemberRole == MemberRoles.Admin || MemberRole == MemberRoles.VendorMember || MemberRole == MemberRoles.SuperAdmin))
            //{
            //    memberToFolder = MemberToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.InstanceID == _LocalInstanceID & u.FolderID == mainFolder.FolderID);
            //    if (memberToFolder == null)
            //    {
            //        memberToFolder = new MemberToFolder()
            //        {
            //            Active = true,
            //            CrtDate = DateTime.Now,
            //            InstanceID = _LocalInstanceID,
            //            MemberID = member.MemberID,
            //            ModDate = DateTime.Now,
            //            ParentFolderID = 0,
            //            FolderID = mainFolder.FolderID,
            //            RoleID = GetFolderRoleID(MemberRole),
            //            Title = title,
            //            Alias = alias,
            //            ApplicableTo = CCM_Helper.GetMemberRoleName(GetFolderRoleID(MemberRole))
            //        };
            //        if (String.IsNullOrEmpty(title)) { memberToFolder.Title = member.Title; }
            //        if (String.IsNullOrEmpty(alias)) { memberToFolder.Alias = member.NickName; }

            //        memberToFolder.Add();
            //    }
            //    else
            //    {
            //        // If
            //        Folder folder = Folder.SingleOrDefault(u => u.FolderID == memberToFolder.ParentFolderID);
            //        if (folder == null)
            //        {
            //            memberToFolder.ParentFolderID = 0;
            //        }
            //        memberToFolder.Update();
            //    }
            //}
            //else
            //{
            //    memberToFolder = new MemberToFolder() { FolderID = 0 };
            //}


            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.DiscussionID == discussion.DiscussionID);
            if (disToFolder == null)
            {
                disToFolder = new DiscussionToFolder()
                {
                    FolderID = 0,
                    MemberID = member.MemberID,
                    InstanceID = _LocalInstanceID,
                    DiscussionID = discussion.DiscussionID,
                    Status = 1,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    RoleID = (int)MemberRole,
                    Title = title,
                    Alias = alias,
                };
                if (MemberRole == MemberRoles.ClientMember || MemberRole == MemberRoles.ClientAdmin)
                {
                    List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == discussion.DiscussionID && u.RoleID == 5).ToList();
                    if (dtfs.Count <= 0)
                    {
                        disToFolder.RoleID = 5;
                        MemberRole = MemberRoles.ClientAdmin;
                    }
                }
                if (String.IsNullOrEmpty(title)) { disToFolder.Title = member.Title; }
                if (String.IsNullOrEmpty(alias)) { disToFolder.Alias = member.NickName; }
                disToFolder.Unread = hdnTotalComments.Value.ToInt32();
                disToFolder.Add();
                if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                {
                    info = new Info("Info", "Client added successfully!", member.MemberID.ToString());
                }
                else
                {

                    info = new Info("Info", "Team added successfully!", member.MemberID.ToString());
                }


            }
            else
            {
                if (disToFolder.RoleID != 1)
                {
                    disToFolder.RoleID = (int)MemberRole;
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    disToFolder.Update();
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        info = new Info("Info", "Client updated successfully!", member.MemberID.ToString());
                    }
                    else
                    {

                        info = new Info("Info", "Team updated successfully!", member.MemberID.ToString());
                    }

                }
                else
                {
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    disToFolder.Update();
                    info = new Info("Info", "Superadmin updated!", member.MemberID.ToString());
                    //info = new Info("Error", "Member you are trying to assign is already Super Admin of this discussion.", member.MemberID.ToString());
                }
            }
            SendInvitationDefault(currMember, discussion, member, MemberRole);
            AddOwnerToMember(currMember, member, CCM_Helper.GetMemberRoleID(CCM_Helper.GetMemberRoleName(GetFolderRoleID(MemberRole))), email);
            return info;
        }

        //private Int32 GetDiscussionRoleID()
        //{

        //    //if (rdoClient.Checked && chkAdmin.Checked)
        //    //{
        //    //    return 5;
        //    //}
        //    if (rdoClient.Checked) {
        //        return 4;
        //    }
        //    else if (rdoTeam.Checked && chkAdmin.Checked) {
        //        return 2;
        //    }
        //    else if (rdoTeam.Checked && !chkAdmin.Checked) {
        //        return 3;
        //    }
        //    else {
        //        return 0;
        //    }
        //}
        private Int32 GetFolderRoleID(MemberRoles mr)
        {
            if (mr == MemberRoles.ClientMember || mr == MemberRoles.ClientAdmin)
            {
                return 4;
            }
            else if (mr == MemberRoles.Admin || mr == MemberRoles.VendorMember)
            {
                return 3;
            }
            else
            {
                return 0;
            }
        }

        private void AddOwnerToMember(Member currMember, Member member, Int32 RoleID, string email)
        {
            OwnerToMember otm = OwnerToMember.SingleOrDefault(u => u.Email.Contains(email) && u.InstanceID == _LocalInstanceID && u.RoleID == RoleID);
            if (otm == null)
            {
                otm = new OwnerToMember()
                {
                    OwnerMemberID = currMember.MemberID,
                    InstanceID = _LocalInstanceID,
                    Email = email,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    MemberID = member.MemberID,
                    NameAlias = member.FirstName,
                    RoleID = RoleID
                };
                otm.Add();
            }
        }

        private void SendInvitationDefault(Member currMember, MyCCM.Discussion discussion, Member member, MemberRoles MemberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == currMember.MemberID && u.DiscussionID == discussion.DiscussionID);
            DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussion.DiscussionID && u.MemberID == member.MemberID);
            String body = "Hi " + dtfMember.Alias + " " + ", ";
            body += "<br /><br />" + currDTF.Alias + "   has added you in the following discussion as " + CCM_Helper.GetMemberRoleNewSimplifiedName((int)MemberRole);

            //if (chkSendLink.Checked)
            //{
            string url = "Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&ir=" + dtfMember.RoleID.ToString().Encrypt() + "&md=" + member.MemberID.ToString().Encrypt() + "&instanceid=" + InstanceID;
            body += "<br /><b><a href=\"http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/" + url + "\">\"" + discussion.Title + "\"</a></b>";
            //}
            body += "<br /><br />Click the link above or copy/paste the following URL in your browser: <br />http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/" + url + " <br />";
            //body += "This discussion uses Avaima CCM. CCM is a software application on Avaima which lets you add one or more discussions under one or more projects. Each project has team members and clients/customers. A page for discussion allows all parties to communicate with each other while maintaining privacy of everyone.";
            //if (dtfMember != null)
            //{
            //    body += "<br /><br />You have been added as a " + CCM_Helper.GetMemberRoleSimplifiedName(GetDiscussionRoleID());
            //}

            //body += "<br /><br />You are invited to Team Communication Application by " + currMember.NickName + " to participate as " + memberRole + " in discussions under project: <b>" + project.Title + ". ";
            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited project's discussions.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "You have been added in a discussion";
            //emailAPI.send_email(member.Email, currMember.FirstName + " " + currMember.LastName, "noreply@avaima.com", body, subject);
            emailAPI.send_email(member.Email, currDTF.Alias, "noreply@avaima.com", body, subject);
        }



        protected void rptBC_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Navigate")
            {
                LinkButton lnkButton = e.Item.FindControl("lnkBC") as LinkButton;
                if (e.CommandArgument.ToString() != "0")
                {
                    this.Redirect("Default.aspx?fid=" + e.CommandArgument.ToString());
                }
                else
                {
                    this.Redirect("Default.aspx");
                }
            }
        }

        private void BindDiscussionList()
        {
            //DataTable dt = CCMDiscussion.GetAllMemberDiscussions(hdnMemberID.Value.ToInt32(), _LocalInstanceID);
            //dt.DefaultView.Sort = "ModDate desc";
            //rptDiscussionList.DataSource = dt.DefaultView;
            //rptDiscussionList.DataBind();
            //Int32 TotalUnread = dt.Compute("SUM(Unread)", "").ToInt32();
            //if (TotalUnread > 0)
            //{
            //    lblUnread.Text = "(" + TotalUnread.ToString() + ")";
            //}
            //else
            //{
            //    lblUnread.Text = "";
            //}
            //SetActiveDiscussion(hdnDisID.Value.ToString());
        }
        private static void UpdateRole(string userId, string RoleId, string discussionId)
        {
            if (userId == "0")
            {
                DiscussionToFolder disToFolderP = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID ==
                                            discussionId.ToInt32() && u.RoleID == RoleId.ToInt32());
                if (disToFolderP != null)
                {
                    disToFolderP.RoleID = 3;
                    disToFolderP.Update();
                }
            }
            else
            {
                DiscussionToFolder disToFolderP = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID ==
                                           discussionId.ToInt32() && u.RoleID == RoleId.ToInt32());
                if (disToFolderP != null)
                {
                    disToFolderP.RoleID = 3;
                    disToFolderP.Update();
                }
                DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID ==
                                           discussionId.ToInt32() && u.MemberID == userId.ToInt32());
                if (disToFolder != null)
                {
                    disToFolder.RoleID = RoleId.ToInt32();
                    disToFolder.Update();
                }
            }

        }
        [WebMethod]
        public static string SaveDiscussion(string discussionid, string title, string projectid, string instanceid, string status, string memberid)
        {
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(discussionid));
            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32() && u.MemberID == memberid.ToInt32());
            if (disToFolder.RoleID == 1 || disToFolder.RoleID == 2 || disToFolder.RoleID == 5)
            {
                if (dis != null)
                {
                    dis.Title = title;
                    dis.ModDate = DateTime.Now;
                    dis.Update();
                    return "Info:Discussion updated:" + dis.DiscussionID;
                }
                else
                {
                    return "Error:No DIscussion found:" + dis.DiscussionID;
                }
            }
            else
            {
                return "Error:You are not allowed to edit discussion information:" + dis.DiscussionID;
            }
        }
        /* Save Discussion Title and IsHourly */
        [WebMethod]
        public static string SaveDiscussionTitleIsHourly(string discussionid, string title, string projectid,
            string instanceid, string status, string memberid, bool isHourly,
            bool HourRequiredEachPost, bool ZeroHourPost, string projectManagerId,
            string assitantProjectManagerId, bool HideHours, bool SummaryAllow, bool SendSummary, string Duration, string EmailContent)
        {
            string ret = "";
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(discussionid));
            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32() && u.MemberID == memberid.ToInt32());


            if (disToFolder.RoleID == 1 || disToFolder.RoleID == 2 || disToFolder.RoleID == 5 || disToFolder.RoleID == 6)
            {
                if (dis != null)
                {
                    dis.Title = title;
                    dis.ModDate = DateTime.Now;
                    dis.isHourly = isHourly;
                    dis.HideHours = HideHours;
                    dis.SummaryAllow = SummaryAllow;
                    dis.HourRequiredEachPost = HourRequiredEachPost;
                    dis.ZeroHourPost = ZeroHourPost;
                    dis.sendsummary = SendSummary;
                    dis.Duration = Duration;
                    dis.EmailContent = EmailContent;
                    dis.Update();
                    ret = "Info:";
                    ret += "Discussion Title IsHourly updated";
                    if (disToFolder.RoleID == 1 || disToFolder.RoleID == 2 || disToFolder.RoleID == 5)
                    {
                        if (projectManagerId != null && projectManagerId != "")
                        {
                            UpdateRole(projectManagerId, "6", discussionid);
                        }

                        if (assitantProjectManagerId != null && assitantProjectManagerId != "")
                        {
                            UpdateRole(assitantProjectManagerId, "7", discussionid);
                        }
                    }
                    else

                        ret += ":" + dis.DiscussionID;

                }
                else
                {
                    ret = "Error:No DIscussion Title IsHourly found:" + dis.DiscussionID;
                }
            }
            else
            {
                ret = "Error:You are not allowed to edit discussion information:" + dis.DiscussionID;
            }
            return ret;

        }
        /*End*/
        [WebMethod]
        public static string SaveMemberDiscussion(string memdisid, string discussionid, string instanceid, string memberid, string teammember, string conversation, string active, string folderid, string subject)
        {
            Role role = CCM_Helper.GetRole(Convert.ToInt32(memberid), Convert.ToInt32(discussionid), Convert.ToInt32(folderid), instanceid);
            if (role.RoleID == 0)
            {
                return "Error:Member has no role defined under currect discussion:" + memberid;
            }
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            if (md.MemberDis != null)
            {
                md.MemberDis.DiscussionID = discussionid;
                md.MemberDis.TeamMember = teammember;
                md.MemberDis.Conversation = conversation.Replace('\n', ' ');
                md.MemberDis.ModDate = DateTime.Now;
                md.MemberDis.RoleID = role.RoleID;
                md.MemberDis.Deleted = false;
                md.Update();
                discussion.ModDate = DateTime.Now;
                discussion.Update();
                return "Info:MemberDiscussion updated";
            }
            else
            {
                md.MemberDis = new App_Code.MemberDiscussions()
                {
                    DiscussionID = discussionid,
                    PartitionKey = instanceid,
                    Active = active,
                    Conversation = conversation,
                    CrtDate = DateTime.Now,
                    MemberID = memberid,
                    ModDate = DateTime.Now,
                    RoleID = role.RoleID,
                    TeamMember = teammember
                };
                discussion.ModDate = DateTime.Now;
                md.MemberDis.Deleted = false;
                discussion.Update();
                md.Add();
                HttpContext.Current.Session.Add("memDisID", md.MemberDis.RowKey);
                //subject = SendEmail(discussionid, memberid, conversation, projectid, subject);
                return "Info:MemberDiscussion Created:" + md.MemberDis.RowKey;
            }
        }

        [WebMethod]
        public static string SendEmail(string memberdisid, string discussionid, string memberid, string conversation, string subject, string folderid, int minutes)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            AvaimaTimeZoneAPI timeAPI = new AvaimaTimeZoneAPI();
            Member senderMember = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid.ToInt32() && u.DiscussionID == discussionid.ToInt32());
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == discussionid.ToInt32()).ToList();
            List<DiscussionFile> files = DiscussionFile.Find(u => u.MemberDisID == memberdisid).ToList();
            Project project = Project.SingleOrDefault(u => u.ProjectID == discussion.ProjectID);
            string strhours = "";
            int srole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), senderMember.MemberID);
            if (discussion != null)
            {
                discussion.ModDate = DateTime.Now;
                discussion.Update();
            }
            if (project == null)
            {
                project = new Project()
                {
                    ProjectID = 0,
                    Title = "Discussion Update"
                };
            }
            foreach (DiscussionToFolder item in mtds)
            {
                Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (member != null)
                {
                    int mrole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), item.MemberID.ToInt32());
                    if (srole != 4 && srole != 5)
                    {
                        if(discussion.HideHours==null)
                        {
                            discussion.HideHours = false;
                        }
                        if (discussion.isHourly == true && (discussion.HideHours == false || (discussion.HideHours == true && (mrole != 4 && mrole != 5))))
                        {

                            string hour = (Convert.ToDecimal(minutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 minutes1 = Convert.ToInt16(minutes % 60);
                            strhours = "Total Time spent= " + hour + " Hours " + minutes1.ToString() + " Minutes ";


                        }
                        else
                        {
                            strhours = "";
                        }
                    }



                    StringBuilder strbody = new StringBuilder();
                    //strbody.AppendLine("This is CCM discussion, please dont reply to this email click <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">here</a> to reply to discussion.<br /><br />");
                    String url = CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString());
                    //strbody.Append("Write ABOVE THIS LINE to post a reply through email OR reply on the <a href='" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "'>discussion page</a> <br/>");
                    strbody.Append("<div style=\"padding:8px 12px 6px 12px;background: whitesmoke;\">");
                    strbody.Append("<h1 style=\"font-weight:normal\">" + discussion.Title + "<br /><span style=\"font-size:12px\">Comment by <b>" + currDTF.Alias + "</b> - " + timeAPI.GetDate(DateTime.Now.ToShortDateString(), member.OwnerID) + " at " + Utilities.GetUserDateWithoutToday(DateTime.Now.ToShortTimeString(), member.OwnerID) + "</span></h1>For complete details visit the<a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\"> discussion page</a><br />");
                    //if (project != null)
                    //{
                    //    strbody.Append("<h3 style=\"font-weight:normal\">Project: " + project.Title + "</h3>");
                    //}
                    strbody.Append("<br />");
                    strbody.Append("<h5 style=\"margin-top:9px;padding: 14px;background: #EFC;font-size: 12px;font-weight: normal;margin: 10px -10px !important;margin-left:-12px \">");

                    if (files.Count > 0)
                    {
                        strbody.Append("<b>File attachments:</b><br />");
                        int ifiles = 0;
                        foreach (var file in files)
                        {
                            strbody.AppendLine("<a href=\"" + file.FilePath + "\">" + file.FileName + "</a>");
                            ifiles++;
                            if (ifiles != files.Count)
                            {
                                strbody.Append(" - ");
                            }
                        }
                        strbody.Append("<br /><hr />" + conversation);
                    }
                    else
                    {
                        strbody.Append(conversation);
                    }
                    if (discussion.isHourly == true)
                    {
                        strbody.Append("<br/>" + strhours);
                    }
                    strbody.AppendLine("</h5>");
                    strbody.AppendLine("For complete details visit the <a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\">Discussion Page</a>");
                    strbody.AppendLine("</div>");

                    //strbody.AppendLine("<br />This is CCM discussion, please dont reply to this email click <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">here</a> to reply to discussion.</div>");
                    strbody.AppendLine(Utilities.AvaimaEmailSignature);
                    //if (conversation.Length > 100)
                    //{
                    //    subject = conversation.Substring(0, 100) + "...";
                    //}
                    //else
                    //{
                    subject = discussion.Title;
                    //}
                    //string noHTMLSubject = Regex.Replace(subject, @"<[^>]+>|&nbsp;", "").Trim();
                    //subject += " " + DateTime.Now.ToString("ddd, MMM yy hh:mm tt");
                    //if (member.MemberID != senderMember.MemberID)
                   // {
                        emailAPI.send_email(member.Email, currDTF.Alias, "pcm-71579@avaima.com", strbody.ToString(), subject + " [ID:" + discussionid + "]");
                    //}

                    //if (item.MemberID != senderMember.MemberID)
                   // {
                        if (item.Unread == null)
                        {
                            item.Unread = 1;
                        }
                        else
                        {
                            item.Unread += 1;
                        }
                        item.Update();
                    //}
                }
            }
            //List<MemberToDiscussion> mtds = MemberToDiscussion.Find(u => u.DiscussionID == Convert.ToInt32(discussionid)).ToList();
            //foreach (MemberToDiscussion mtd in mtds) {
            //    Member member1 = Member.SingleOrDefault(u => u.MemberID == mtd.MemberID);
            //    if (member1 != null) {
            //        StringBuilder strbody = new StringBuilder();
            //        strbody.AppendLine("This is CCM discussion, please dont reply to this email click <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">here</a> to reply to discussion.<br /><br />");
            //        strbody.Append("<div style=\"padding:8px 0px 6px 12px;background: whitesmoke;\"><h1>" + Project.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(projectid)).Title + "</h1>");
            //        strbody.Append("<h3>" + MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(discussionid)) + "</h3>");
            //        strbody.Append("<h5 style=\"margin-top:9px;padding: 14px;background: #EFC;margin: 10px -10px !important;margin-left:-12px \">" + conversation);
            //        if (files.Count > 0) {
            //            foreach (var file in files) { strbody.AppendLine("<a href=\"" + file.FilePath + "\">" + file.FileName + "</a><br />"); }
            //        }
            //        strbody.AppendLine("</h5>");

            //        strbody.AppendLine("<br />This is CCM discussion, please dont reply to this email click <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">here</a> to reply to discussion.</div>");
            //        strbody.AppendLine(Utilities.AvaimaEmailSignature);
            //        subject = "Project: " + Project.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(projectid)).Title;
            //        subject += " - Discussion: " + MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(discussionid)).Title.Replace("\n", "");
            //        subject += " " + DateTime.Now.ToString("ddd, MMM yy hh:mm tt");
            //        //subject = "Subject test";                    
            //        emailAPI.send_email(member1.Email, senderMember.FirstName + " " + senderMember.LastName, "noreply@avaima.com", strbody.ToString(), subject.ToString());
            //    }
            //}
            return "Email Sent";
        }

        [WebMethod]
        public static string DeleteMemberDiscussion(string memdisid, string currmemberid, string currmembername)
        {
            AvaimaTimeZoneAPI ati = new AvaimaTimeZoneAPI();

            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            if (md.MemberDis != null)
            {
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == md.MemberDis.RowKey).ToList();
                try
                {
                    foreach (DiscussionFile item in disFiles)
                    {
                        // Delete Blob Files
                        DeleteFile(item.FileName, md.MemberDis.DiscussionID);
                        // DataBase items
                        item.Delete();
                    }
                }
                catch (Exception) { }
                // Member Discussion
                md.DeleteMD(memdisid, currmemberid.ToInt32());
                return "Inform:Delete:This message was deleted by " + currmembername + " on " + Utilities.GetUserDateTime(md.MemberDis.ModDate.ToString(), currmemberid);
                //return "Inform:Delete:This message was deleted by " + currmembername + " on " + ati.GetDate(DateTime.UtcNow.ToLongDateString(), currmemberid) + " at " + ati.GetTime(DateTime.UtcNow.ToLongTimeString(), currmemberid);
            }
            else
            {
                return "Error:No Discussion found";
            }
        }

        [WebMethod]
        public static string DeleteDiscussionFile(string disfileid)
        {
            DiscussionFile disFile = DiscussionFile.SingleOrDefault(u => u.DisFileID == disfileid.ToInt32());
            if (disFile != null)
            {
                disFile.Delete();
                return "Info:File deleted";
            }
            else
            {
                return "Info:No file found to delete";
            }
        }

        [WebMethod]
        public static string UpdateDiscussionFile(string memberDisID, string updatedMemberDisID)
        {
            try
            {
                string str = "0";
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                foreach (DiscussionFile item in disFiles)
                {
                    item.MemberDisID = updatedMemberDisID;
                    item.Update();
                    str = "1";
                }
                return "Info:Updated:"+str;
            }
            catch (Exception ex)
            {

                return "Error:An error occured " + ex.Message;
            }
        }

        [WebMethod]
        public static string DeleteDiscussionFiles(string memberdisid)
        {
            try
            {
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberdisid).ToList();
                foreach (DiscussionFile item in disFiles)
                {
                    item.Delete();
                }
                return "Info:Deleted!";
            }
            catch (Exception ex)
            {
                return "Error:Not Delete " + ex.Message;
            }
        }
        [WebMethod]
        public static string DeleteMemberDiscussionFiles(string memberdisid, string fileName)
        {
            try
            {
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberdisid && u.FileName==fileName).ToList();
                foreach (DiscussionFile item in disFiles)
                {
                    item.Delete();
                }
                return "Info:Deleted!";
            }
            catch (Exception ex)
            {
                return "Error:Not Delete " + ex.Message;
            }
        }
        private static void DeleteFile(string filename, string discussionID)
        {
            string datacontainer = "data";
            string storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);

            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                CloudStorageAccount storageAccount = new CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);

                CloudBlockBlob blob = container.GetBlockBlobReference(String.Format("{0}{1}", storagepath, filename));
                blob.DeleteIfExists();
            }
            catch (Exception ex)
            {

            }
        }

        protected void rptDiscussions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl imgMember = e.Item.FindControl("imgMember") as HtmlControl;
                HtmlControl imgMember2 = e.Item.FindControl("imgMember2") as HtmlControl;
                HtmlControl tdImage = e.Item.FindControl("tdImage") as HtmlControl;
                HtmlControl tdImage2 = e.Item.FindControl("tdImage2") as HtmlControl;
                //Panel panel = e.Item.FindControl("divMemDis") as HtmlControl
                HtmlContainerControl myDivMemDis = e.Item.FindControl("divMemDis") as HtmlContainerControl;
                HtmlContainerControl myDivMemDisO = myDivMemDis.FindControl("divMemDisO") as HtmlContainerControl;
                Label lblMemberTemp = e.Item.FindControl("lblMemberTemp") as Label;
                Label lblMemberTemp2 = e.Item.FindControl("lblMemberTemp2") as Label;
                TextBox txtMemDis = e.Item.FindControl("txtMemDis") as TextBox;
                Int32 memberID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MemberID"));
                String memberDisID = DataBinder.Eval(e.Item.DataItem, "RowKey").ToString();
                Member member = Member.SingleOrDefault(u => u.MemberID == memberID);
                //Role role = new Role();
                //if (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")) != 0)
                //{
                //    role = Role.SingleOrDefault(u => u.RoleID == Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID").ToString()));
                //}
                //else
                //{
                //    role = CCM_Helper.GetRole(member.MemberID, Convert.ToInt32(hdnDisID.Value), Convert.ToInt32(hdnProjectID.Value), _LocalInstanceID);
                //}

                if (member != null)
                {
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), member.MemberID);
                    imgMember.Attributes["src"] = imgMember2.Attributes["src"] = "_assests/Images/" + mr.ToString() + ".png";

                    //if (member.ImagePath != "")
                    //{

                    //}
                    //else
                    //{
                    //    imgMember2.Attributes["src"] = imgMember.Attributes["src"] = "_assests/Images/" + role.RoleTitle + ".png";
                    //}
                    Image img = new Image();
                    img.ID = "disAction";
                    img.CssClass = "disAction";
                    img.ImageUrl = "_assests/Images/downarrow.png";
                    img.Attributes.Add("data-id", memberDisID);
                    Label lblDateTime = new Label();
                    lblDateTime.Attributes["class"] = "lblDateTime";
                    lblDateTime.Style.Add("float", "right");
                    lblDateTime.Style.Add("margin-right", "-29px");
                    lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                    DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdninstanceid.Value && u.MemberID == memberID);
                    if (dtf != null)
                    {
                        if (String.IsNullOrEmpty(dtf.Alias)) { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                        else { lblMemberTemp2.Text = lblMemberTemp.Text = dtf.Alias; }
                    }
                    else { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                    string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                    myDivMemDis.Attributes["class"] = "Class " + roleTitle;
                    myDivMemDis.Attributes.Add("data-role", roleTitle);
                    myDivMemDisO.InnerHtml += DataBinder.Eval(e.Item.DataItem, "Conversation").ToString();
                    myDivMemDisO.InnerHtml += "</br>";
                    DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                    if (member.MemberID == Convert.ToInt32(hdnMemberID.Value)) { myDivMemDis.Controls.Add(img); }
                    else
                    {
                        if (dtfCurrMember != null)
                        {
                            if (dtfCurrMember.RoleID == 1 || dtfCurrMember.RoleID == 2)
                            {
                                myDivMemDis.Controls.Add(img);
                            }
                        }
                    }
                    myDivMemDis.Controls.Add(lblDateTime);
                    if (roleTitle == "ClientMember") { tdImage.Visible = false; tdImage2.Visible = true; }
                    else if (roleTitle == "VendorMember") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "Admin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "SuperAdmin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "ClientAdmin") { tdImage.Visible = false; tdImage2.Visible = true; }

                    Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;
                    List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                    if (disFiles.Count > 0)
                    {
                        rptFiles.DataSource = disFiles;
                        rptFiles.DataBind();
                    }
                    else { }
                }
            }
        }

        protected void rptClient_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl trClient = e.Item.FindControl("trClient") as HtmlControl;
                Int32 memberID = Convert.ToInt32(trClient.Attributes["data-id"].ToString());
                Int32 ApplicableTo = Convert.ToInt32(trClient.Attributes["data-applicableto"].ToString());
                DropDownList ddlStatus = e.Item.FindControl("ddlStatus") as DropDownList;
                ddlStatus.SelectedIndex = ApplicableTo;
            }
        }

        protected void rptTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl trTeam = e.Item.FindControl("trTeam") as HtmlControl;
                Int32 memberID = Convert.ToInt32(trTeam.Attributes["data-id"].ToString());
                Int32 ApplicableTo = Convert.ToInt32(trTeam.Attributes["data-applicableto"].ToString());
                DropDownList ddlStatus = e.Item.FindControl("ddlStatus") as DropDownList;
                ddlStatus.SelectedIndex = ApplicableTo;
            }
        }

        protected void btnSaveDiscussion_Click(object sender, EventArgs e)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value));
            Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
            Member currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == currMember.MemberID && u.DiscussionID == dis.DiscussionID);
            if (dis != null)
            {
                dis.Title = txtDisTitle.Text;
                if (dis.Status != ddlStatus.SelectedItem.Value)
                {
                    List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == dis.DiscussionID && u.InstanceID == _LocalInstanceID).ToList();
                    foreach (DiscussionToFolder item in dtfs)
                    {
                        Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                        if (member != null)
                        {
                            emailAPI.send_email(member.Email, currDTF.Alias, "noreply@avaima.com",
                                "Hi " + item.Alias + ",<br /><br />This is to inform you that Discussion: <b>" + dis.Title + "</b> has been marked as <b>" + ddlStatus.SelectedItem.Value + "</b> by " + currDTF.Alias + "." + Utilities.AvaimaEmailSignature,
                                "Discussion: " + dis.Title + " has been marked as " + ddlStatus.SelectedItem.Value + " by " + currDTF.Alias + "."
                                );
                        }
                    }
                }
                dis.Status = ddlStatus.SelectedItem.Value;
                //dis.ProjectID = Convert.ToInt32(ddlProject.SelectedItem.Value);
                dis.ModDate = DateTime.Now;
                dis.Update();
                h1Title.InnerText = dis.Title;

                //// Delete old mtd inherited from project (Status: 0)
                //List<DiscussionToFolder> dtf = DiscussionToFolder.Find(u => u.DiscussionID == dis.DiscussionID && u.Status == 0).ToList();
                //foreach (DiscussionToFolder mtd in dtf) { mtd.Delete(); }

                //// Inherited mtps as mtds from selected project
                //List<MemberToProject> mtps = MemberToProject.Find(u => u.ProjectID == dis.ProjectID).ToList();
                //foreach (var item in mtps)
                //{
                //    DiscussionToFolder mtd = DiscussionToFolder.SingleOrDefault(u => u.MemberID == item.MemberID && u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == _LocalInstanceID);
                //    if (mtd == null)
                //    {
                //        mtd = new DiscussionToFolder()
                //        {
                //            Status = 0,
                //            DiscussionID = dis.DiscussionID,
                //            Active = true,
                //            CrtDate = DateTime.Now,
                //            InstanceID = _LocalInstanceID,
                //            MemberID = item.MemberID,
                //            ModDate = DateTime.Now,
                //            RoleID = item.RoleID,
                //            FolderID = hdn_folderid.Value.ToInt32()
                //        };
                //        mtd.Add();
                //    }

                //    Member selMember = Member.SingleOrDefault(u => u.MemberID == mtd.MemberID);
                //    MemberToFolder memberToFolder = new MemberToFolder();
                //    if (mainFolder != null)
                //    {
                //        memberToFolder = MemberToFolder.SingleOrDefault(u => u.MemberID == mtd.MemberID && u.InstanceID == _LocalInstanceID && u.FolderID == mainFolder.FolderID);
                //        if (memberToFolder == null)
                //        {
                //            memberToFolder = new MemberToFolder()
                //            {
                //                Active = true,
                //                CrtDate = DateTime.Now,
                //                InstanceID = _LocalInstanceID,
                //                MemberID = mtd.MemberID,
                //                ModDate = DateTime.Now,
                //                ParentFolderID = 0,
                //                FolderID = mainFolder.FolderID,
                //                RoleID = mtd.RoleID
                //            };
                //            memberToFolder.Add();
                //        }
                //    }
                //    if (chkSendInvitation.Checked)
                //    {
                //        try
                //        {
                //            SendInvitationDefault(Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32()), dis, selMember, Role.SingleOrDefault(u => u.RoleID == mtd.RoleID).RoleTitle);
                //        }
                //        catch (Exception)
                //        { }
                //    }
                //}
                FillMembers();
                BindDiscussionList();
                SetActiveDiscussion(dis.DiscussionID.ToString());
            }
        }

        protected void btnAddMember_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            MemberRoles mr;
            if (btnSender.CommandName == "Client")
            {
                mr = MemberRoles.ClientMember;
                string response = AddMember(mr);
                if (response.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
                    ClearClientFields();
                    hdnSelClientID.Value = "0";
                }
                else if (response.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
                }
                BindExistingMembersDLL();
                FillMembers();
                BindDiscussionData();
                //divManageClient.Style.Clear();
                BindDiscussionList();
            }
            else if (btnSender.CommandName == "Team")
            {
                if (chkAdmin.Checked)
                {
                    mr = MemberRoles.Admin;
                }
                else
                {
                    mr = MemberRoles.VendorMember;
                }
                string response = AddMember(mr);
                if (response.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
                    ClearTeamFields();
                    hdnSelTeamID.Value = "0";
                }
                else if (response.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                    lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
                }
                BindExistingMembersDLL();
                FillMembers();
                BindDiscussionData();
                //divManageTeam.Style.Clear();
            }

        }

        protected void btnAddEx_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            Member member;
            if (btnSender.CommandName == "Client")
            {
                member = Member.SingleOrDefault(u => u.MemberID == ddlClientMembers.SelectedValue.ToInt32());
                if (member != null)
                {
                    txtClientEmail.Text = member.Email;
                    txtClientName.Text = member.FirstName;
                    if (String.IsNullOrEmpty(txtClientAlias.Text)) { txtClientAlias.Text = member.NickName; }
                    if (String.IsNullOrEmpty(txtClientAlias.Text)) { txtClientAlias.Text = member.Title; }
                    hdnSelClientID.Value = "0";
                    btnAddClient.Text = "Add";
                    txtClientAlias.Focus();
                    btnAddMember_Click(sender, e);
                    ClearClientFields();
                    hdnSelClientID.Value = "0";
                    //divManageClient.Style.Clear();
                }
                else
                {
                    lblInfoExClient.Text = "No member found";
                    lblInfoExClient.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                member = Member.SingleOrDefault(u => u.MemberID == ddlTeamMembers.SelectedValue.ToInt32());
                if (member != null)
                {
                    txtTeamEmail.Text = member.Email;
                    txtTeamName.Text = member.FirstName;
                    if (String.IsNullOrEmpty(txtTeamAlias.Text)) { txtTeamAlias.Text = member.NickName; }
                    if (String.IsNullOrEmpty(txtTeamTitle.Text)) { txtTeamTitle.Text = member.Title; }
                    hdnSelTeamID.Value = "0";
                    btnAddTeam.Text = "Add";
                    txtTeamName.Focus();
                    btnAddMember_Click(sender, e);
                    ClearTeamFields();
                    hdnSelTeamID.Value = "0";
                    //divManageTeam.Style.Clear();
                }
                else
                {
                    lblInfoExTeam.Text = "No member found";
                    lblInfoExTeam.ForeColor = System.Drawing.Color.Red;
                }
            }


            BindDiscussionData();
        }

        private void BindExistingMembersDLL()
        {
            List<OwnerToMember> ownerToMembers = CCM_Helper.GetMembers(_LocalInstanceID, MemberRoles.All, hdnMemberID.Value.ToInt32());
            ddlClientMembers.DataSource = ownerToMembers.Where(u => u.RoleID == 4).ToList();
            ddlClientMembers.DataTextField = "NameAlias";
            ddlClientMembers.DataValueField = "MemberID";
            ddlClientMembers.DataBind();
            ddlTeamMembers.DataSource = ownerToMembers.Where(u => u.RoleID == 2 || u.RoleID == 3).ToList();
            ddlTeamMembers.DataTextField = "NameAlias";
            ddlTeamMembers.DataValueField = "MemberID";
            ddlTeamMembers.DataBind();
        }

        //protected void rdoClient_CheckedChanged(object sender, EventArgs e)
        //{
        //    BindExistingMembersDLL();
        //    ClearMemberFields();
        //    chkAddNew.Checked = true;
        //    divManageMember.Style.Clear();
        //}

        private void ClearTeamFields()
        {
            txtTeamAlias.Text = "";
            txtTeamEmail.Text = "";
            txtTeamTitle.Text = "";
            txtTeamName.Text = "";
        }
        private void ClearClientFields()
        {
            txtClientAlias.Text = "";
            txtClientEmail.Text = "";
            txtClientTitle.Text = "";
            txtClientName.Text = "";
        }

        //public MemberRoles SelMemberRole
        //{
        //    get
        //    {
        //        if (rdoClient.Checked) {
        //            return MemberRoles.ClientMember;
        //        }
        //        else {
        //            return MemberRoles.VendorMember;
        //        }

        //    }
        //}

        protected void btnAddMember_Click1(object sender, EventArgs e)
        {

        }

        protected void rptClientMembers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "Remove") {
            //    int memberid = Convert.ToInt32(e.CommandArgument.ToString());
            //    string resposen = RemoveMember(memberid);
            //    if (resposen.Split(':')[0].Contains("Info")) {
            //        //ShowInfo(resposen.Split(':')[1]); 
            //        lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
            //        lblExInfo.ForeColor = lblInfo.ForeColor = System.Drawing.Color.Green;
            //    }
            //    else if (resposen.Split(':')[0].Contains("Error")) {
            //        //ShowError(resposen.Split(':')[1]); 
            //        lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
            //        lblExInfo.ForeColor = lblInfo.ForeColor = System.Drawing.Color.Red;
            //    }
            //    BindClients();
            //    divManageMember.Style.Clear();
            //}
        }

        protected void rptTeamMember_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "Remove") {
            //    int memberid = Convert.ToInt32(e.CommandArgument.ToString());
            //    string resposen = RemoveMember(memberid);
            //    if (resposen.Split(':')[0].Contains("Info")) {
            //        //ShowInfo(resposen.Split(':')[1]); 
            //        lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
            //        lblExInfo.ForeColor = lblInfo.ForeColor = System.Drawing.Color.Green;
            //    }
            //    else if (resposen.Split(':')[0].Contains("Error")) {
            //        //ShowError(resposen.Split(':')[1]); 
            //        lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
            //        lblExInfo.ForeColor = lblInfo.ForeColor = System.Drawing.Color.Red;
            //    }
            //    BindTeam();
            //    divManageMember.Style.Clear();
            //}
        }

        private string RemoveMember(int memberid)
        {
            // Remove member from project            
            DiscussionToFolder mtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid) && u.DiscussionID == hdnDisID.Value.ToInt32());
            if (mtf.RoleID == 1)
            {
                return "Superadmin cannot be removed!";
            }
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == _LocalInstanceID);
            if (mtf != null)
            {
                mtf.Delete();
                DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid && u.DiscussionID == mtf.DiscussionID);
                if (disToFolder != null)
                {
                    disToFolder.Delete();
                }
                //SendMemberRemoveNotification(currMember, member, discussion, Role.SingleOrDefault(u => u.RoleID == mtf.RoleID).RoleTitle);
                BindDiscussionList();
                return "Inform:Member Removed!";
            }
            else
            {
                return "Error:No Member Found";
            }
        }

        private static void SendMemberRemoveNotification(Member currMember, Member member, MyCCM.Discussion discussion, string memberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == currMember.MemberID && u.DiscussionID == discussion.DiscussionID);
            DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.DiscussionID == discussion.DiscussionID);
            if (dtfMember == null)
            {
                dtfMember = new DiscussionToFolder()
                {
                    Title = member.Title,
                    Alias = member.NickName
                };
            }
            String body = "Hi " + dtfMember.Alias + ", ";

            // MTD
            body += "<br /><br />" + currDTF.Alias + " has removed you from the following discussion:";
            body += "<br /><br />" + discussion.Title;
            body += "<br /><br />For further details contact " + currDTF.Alias;

            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited discussion.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "You have been removed from a discussion";
            //String subject = currDTF.Alias + " has removed you from discussion: " + discussion.Title + " on " + DateTime.Now.ToString("ddd, MM yy");
            emailAPI.send_email(member.Email, currDTF.Alias, "noreply@avaima.com", body, subject);
        }

        protected void rptDiscussionList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Navigate")
            {
                hdnDisID.Value = e.CommandArgument.ToString();

                MyCCM.Discussion disc = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
                DiscussionToFolder disFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                if (disc != null) { hdnProjectID.Value = disc.ProjectID.ToString(); hdn_folderid.Value = disFolder.FolderID.ToString(); hdnRoleID.Value = disFolder.RoleID.ToString(); }
                else { hdnProjectID.Value = "0"; hdn_folderid.Value = "0"; hdnRoleID.Value = "0"; }
                BindExistingMembersDLL();
                FillMembers();
                GenerateBreadCrumb();
                BindDiscussionData();
                SetAccordingToRole(hdnRoleID.Value.ToInt32());
                DisplayAccordintToRole(hdnRoleID.Value.ToInt32());
                MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                imgMember.ImageUrl = "_assests/Images/" + mr.ToString() + ".png";

                //SetActiveDiscussion(e.CommandArgument.ToString(););
            }
        }

        private void SetActiveDiscussion(String CommandArgument)
        {
            //foreach (RepeaterItem item in rptDiscussionList.Items)
            //{
            //    HtmlControl aLinkDiscussion = (HtmlControl)item.FindControl("aLinkDiscussion");
            //    aLinkDiscussion.Attributes.Remove("Class");
            //    if (aLinkDiscussion.Attributes["data-argument"] == CommandArgument)
            //    {
            //        aLinkDiscussion.Attributes.Add("Class", "active");
            //    }
            //}
        }

        protected void btnStatus_Click(object sender, ImageClickEventArgs e)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value));
            Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
            Member currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == hdnDisID.Value.ToInt32());
            if (dis != null)
            {
                List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == dis.DiscussionID && u.InstanceID == _LocalInstanceID).ToList();
                foreach (DiscussionToFolder item in dtfs)
                {
                    Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                    DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.MemberID == item.MemberID && u.DiscussionID == dis.DiscussionID);
                    if (member != null)
                    {
                        emailAPI.send_email(member.Email, dtf.Alias, "noreply@avaima.com",
                            "Hi " + dtfMember.Alias + ",<br /><br />This is to inform you that Discussion: <b>" + dis.Title + "</b> has been marked as <b>" + btnStatus.CommandName + "</b> by " + dtf.Alias + "." + Utilities.AvaimaEmailSignature,
                            "Discussion: " + dis.Title + " has been marked as " + btnStatus.CommandName + " by " + dtf.Alias + "."
                            );
                    }
                }
                dis.Status = btnStatus.CommandName;
                dis.ModDate = DateTime.Now;
                dis.Update();
                //FillMembers();
                if (dis.Status == "Open")
                {
                    btnStatus.ImageUrl = "_assests/Images/16 x 16/Tick.png";
                    btnStatus.CommandName = "Close";
                    btnStatus.ToolTip = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                    btnStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                }
                else
                {
                    btnStatus.ImageUrl = "_assests/Images/16 x 16/Untick.png";
                    btnStatus.CommandName = "Open";
                    btnStatus.ToolTip = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
                    btnStatus.Attributes["Title"] = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
                }
                //BindDiscussionList();
                SetActiveDiscussion(dis.DiscussionID.ToString());
            }
        }

        protected void rptClient_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                int memberid = Convert.ToInt32(e.CommandArgument.ToString());
                string resposen = RemoveMember(memberid);
                if (resposen.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
                }
                else if (resposen.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
                }
                BindClients();
                //divManageClient.Style.Clear();
            }
        }

        protected void rptFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                String FileName = e.CommandArgument.ToString().Split(',')[0];
                String FilePath = e.CommandArgument.ToString().Split(',')[1];
                Response.ClearContent();
                Response.Clear();
                Response.ContentType = GetContentType(FilePath);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName + ";");
                Response.TransmitFile(Server.MapPath(FilePath));
                Response.Flush();
                Response.End();
            }
        }

        public string GetContentType(string fileName)
        {
            var extension = Path.GetExtension(Server.MapPath(fileName));

            if (String.IsNullOrWhiteSpace(extension))
            {
                return null;
            }

            var registryKey = Registry.ClassesRoot.OpenSubKey(extension);

            if (registryKey == null)
            {
                return null;
            }

            var value = registryKey.GetValue("Content Type") as string;

            return String.IsNullOrWhiteSpace(value) ? null : value;
        }

        public String GetFileName(String FileName)
        {
            //string FileName = "This on.e has some bog file name.png";
            string name = "";
            string firstLetters = "";
            string secondLetters = "";
            string[] _fileName = FileName.Split('.');
            name = FileName.Substring(0, FileName.LastIndexOf('.')); ;
            Console.WriteLine(name);
            if (name.Length > 17)
            {
                firstLetters = name.Substring(0, 11);
                Console.WriteLine(firstLetters);
                Console.WriteLine(name.Length);
                Console.WriteLine(name.Length - 4);
                secondLetters = name.Substring(name.Length - 3, 3);
                Console.WriteLine(secondLetters);
                return firstLetters + "..." + secondLetters + FileName.Substring(FileName.LastIndexOf('.'), _fileName[_fileName.Length - 1].Length + 1);

            }
            else
            {
                return FileName;
            }
        }

        protected void btnSaveDAContents_Click(object sender, EventArgs e)
        {
            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == hdnDisID.Value.ToInt32() && u.FolderID == 0);
            if (dc != null)
            {
                dc.Contents = txtContents.Text;
                dc.ModDate = DateTime.Now;
                dc.Update();
            }
            else
            {
                dc = new DisallowedContent()
                {
                    ModDate = DateTime.Now,
                    CrtDate = DateTime.Now,
                    Contents = txtContents.Text,
                    FolderID = 0,
                    DiscussionID = hdnDisID.Value.ToInt32(),
                    InstanceID = this.InstanceID,
                    Active = true,
                };
                dc.Add();
            }
            BindDAData();
        }

        protected void rptDiscussionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 Unread = DataBinder.Eval(e.Item.DataItem, "Unread").ToInt32();
                Int32 DisID = DataBinder.Eval(e.Item.DataItem, "DiscussionID").ToInt32();
                HtmlContainerControl aLinkDiscussion = e.Item.FindControl("aLinkDiscussion") as HtmlContainerControl;
                App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                Int32 CoundComment = md.GetMemberDiscussionCountByDiscussionID(DisID.ToString());
                aLinkDiscussion.InnerText = DataBinder.Eval(e.Item.DataItem, "Title") + " (" + CoundComment + ")";
                if (Unread > 0)
                {
                    aLinkDiscussion.Style.Add("font-weight", "bold");
                }
                else
                {
                    aLinkDiscussion.Style.Remove("font-weight");
                }

            }
        }

        protected void btnSaveMemberInfo_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            if (member != null)
            {
                member.FirstName = txtMName.Text;
                member.NickName = txtMAlias.Text;
                member.Title = txtMTitle.Text;
                member.Update();
                hdnAskInfo.Value = "0";
            }
        }

        protected void rptTeamMembers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                int memberid = Convert.ToInt32(e.CommandArgument.ToString());
                string resposen = RemoveMember(memberid);
                if (resposen.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
                    lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
                }
                else if (resposen.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
                    lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
                }
                BindTeam();
                //divManageClient.Style.Clear();
            }

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                HttpPostedFile postedFile = fileUpload1.PostedFile;
                string discussionID = hdnDisID.Value;
                string memDisID = "";
                //memDisID = context.Session["memDisID"].ToString();
                memDisID = hdnMemDisID.Value;

                string projectID = hdnProjectID.Value;
                //BusinessID = context.Request.Form["BusinessID"].ToString();
                storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);
                string savepath = "";
                string tempPath = "";
                tempPath = "uploads";
                savepath = Server.MapPath(tempPath);

                if (!System.IO.Directory.Exists(savepath))
                    System.IO.Directory.CreateDirectory(savepath);

                string filename = postedFile.FileName;
                string defaultImage = "";
                string extension = filename.Substring(filename.LastIndexOf('.') + 1, 3);

                //postedFile.SaveAs(savepath + @"\" + filename);
                //context.Response.Write(tempPath + "/" + filename);
                //context.Response.StatusCode = 200;
                MyCCM.DiscussionFile disfile = new DiscussionFile()
                {
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    DiscussionID = Convert.ToInt32(discussionID),
                    FileName = filename,
                    FileType = extension,
                    MemberDisID = memDisID,
                    ProjectID = Convert.ToInt32(projectID),
                };
                disfile.FilePath = storagepath + disfile.FileName;
                UploadAndSaveDisFile(postedFile.InputStream, disfile);
            }
            catch (Exception ex)
            {
                //context.Response.Write("Error: " + ex.Message);
            }
        }


        private void UploadAndSaveDisFile(Stream fileStream, DiscussionFile disFile)
        {
            string datacontainer = "data";
            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                CloudStorageAccount storageAccount = new CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);


                string _uploaddir = String.Format("{0}{1}", storagepath, disFile.FileName);

                CloudBlockBlob blob = container.GetBlockBlobReference(_uploaddir);
                blob.Properties.ContentType = disFile.FileType;

                BlobRequestOptions objoption = new BlobRequestOptions();
                objoption.Timeout = TimeSpan.FromHours(1);

                //f.InputStream.Position = 0;
                blob.UploadFromStream(fileStream, objoption);
                disFile.Add();
            }
            catch (Exception ex) { }
        }

        //protected void btnSetTitle_Click(object sender, EventArgs e)
        //{
        //    MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
        //    if (discussion != null)
        //    {
        //        discussion.Title = txtDiscussionTitle.Text;
        //        discussion.ModDate = DateTime.Now;
        //        discussion.Update();
        //    }
        //}
    }
}