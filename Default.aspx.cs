﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class Default : AvaimaWebPage
    {
        private string _OwnerId = "";
        List<Folder> _Folders = new List<Folder>();
        Member _ActiveMember = new Member();
        Boolean _IsNew = false;  // to see if user is new in CCM
        public string _MemberRole = "";
        public int _FolderCount = 0;
        public int _discussionCount = 0;
        public String _LocalInstanceID = "";
        public int hdnMemberID;
        public string txtMAlias, txtMTitle;

        public string url;
        public string meta;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.InstanceID == "0") { _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab"; }
            else { _LocalInstanceID = this.InstanceID; }
            AddinstanceWS objinst = new AddinstanceWS();

            //testInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab";
            _OwnerId = CCM_Helper.GetOwnerID();
            //_OwnerId = "teamuser-1069--8587846274950237891"; // sbmuhammadfaizan Owner SuperAdmin
            //_OwnerId = "c827ef9d-fafd-4b48-b737-27bbee6ec54f"; // b3ast667
            //_OwnerId = "d0670956-4d4c-448c-9e2e-370c295d7b5d"; // faizan outlook

            _ActiveMember = Member.SingleOrDefault(u => u.OwnerID == _OwnerId && u.InstanceID == _LocalInstanceID);
            if (_ActiveMember == null)
            {
                // If user is new to application
                AvaimaUserProfile objwebser = new AvaimaUserProfile();
                //string[] user = objwebser.Getonlineuserinfo(this.Context.User.Identity.Name).Split('#');
                string[] user = objwebser.Getonlineuserinfo(_OwnerId).Split('#');
                // Insert SuperAdminMember
                _ActiveMember = new Member()
                {
                    OwnerID = _OwnerId,
                    //FolderID = 0,
                    InstanceID = _LocalInstanceID,
                    FirstName = user[0] + " " + user[1],
                    LastName = "",
                    NickName = user[0] + " " + user[1],
                    Email = user[2],
                    ImagePath = "_assests/images/SuperAdmin.png",
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    Title = ""
                };
                _ActiveMember.Add();
            }


            if (!IsPostBack)
            {
                hdnMemberID = _ActiveMember.MemberID;

                txtMAlias = _ActiveMember.NickName;
                txtMTitle = _ActiveMember.Title;

            }

            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.HelpEmail == _ActiveMember.Email);
            if (discussion == null)
            {
                CreateWelcomeDiscussion(_ActiveMember.Email);
            }



            DataTable dtable = CCM_Helper.GetLatestDiscussion(_ActiveMember.Email);
            url = CCM_Helper.GetDiscussionURL(dtable.Rows[0][0].ToInt32(), dtable.Rows[0]["MemberId"].ToInt32());
            meta = "var l = document.getElementById('link');l.click();";







        }

        

        

        

        private class FolderMember
        {
            public int MemberID { get; set; }
            public String NickName { get; set; }
            public String Name { get; set; }
            public Boolean IsAdmin { get; set; }
        }

        

        private void CreateWelcomeDiscussion(string email)
        {
            MyCCM.Discussion discussion = new MyCCM.Discussion()
            {
                ProjectID = 0,
                Active = true,
                CrtDate = DateTime.Now,
                InstanceID = _LocalInstanceID,
                ModDate = DateTime.Now,
                Status = "Open",
                Title = "Support and Help",
                IsHelp=true,
                HelpEmail=email
            };
            discussion.Add();
            DiscussionToFolder disToFolder = new DiscussionToFolder();
            
            
                disToFolder = new DiscussionToFolder()
                {
                    MemberID = hdnMemberID,
                    FolderID = 0,
                    Active = true,
                    CrtDate = DateTime.Now,
                    DiscussionID = discussion.DiscussionID,
                    InstanceID = _LocalInstanceID,
                    ModDate = DateTime.Now,
                    RoleID = 4,
                    Status = 1,
                    Alias = txtMAlias,
                    Title = txtMTitle,
                    Unread = 0
                };
                disToFolder.Add();




            disToFolder = new DiscussionToFolder()
            {
                MemberID = 5,
                FolderID = 0,
                Active = true,
                CrtDate = DateTime.Now,
                DiscussionID = discussion.DiscussionID,
                InstanceID = _LocalInstanceID,
                ModDate = DateTime.Now,
                RoleID = 1,
                Status = 1,
                Alias = "Max Dennis",
                Title = "Customer Support Representative",
                Unread = 0
            };
            disToFolder.Add();

            disToFolder = new DiscussionToFolder()
            {
                MemberID = 1,
                FolderID = 0,
                Active = true,
                CrtDate = DateTime.Now,
                DiscussionID = discussion.DiscussionID,
                InstanceID = _LocalInstanceID,
                ModDate = DateTime.Now,
                RoleID = 2,
                Status = 1,
                Alias = "Eric Lee",
                Title = "Customer Support Representative",
                Unread = 0
            };
            disToFolder.Add();

            string memdisid, tags,  discussionid,  instanceid,  memberid,  teammember,  conversation,  active,  folderid,    strminutes;

            memdisid = 0.ToString() ;
tags="";
            discussionid = discussion.DiscussionID.ToString();
instanceid= _LocalInstanceID;
memberid = "5";
teammember = "0";
conversation= "<p><b>Welcome to Project Communication Management (PCM) !</b></p><p> ABOUT PCM: PCM is a FREE software that allows an individual or a company to manage project communication in a most efficient way. PCM is also a way for keeping all all information information in one place where the whole team and the clients have access to it.For further information on PCM visit <a href = 'http://www.avaima.com/project-communication-management-app'> Project Communication Management</a></p><p> PCM is completely FREE for up to 1GB usage.There is NO limit on number of projects or uses.</p><p> FREE CONSULTATION & SUPPORT: Use this discussion to contact us for any help, support, consultation or for requesting new features.This is completely FREE.</p> ";
active = "1";
folderid = "0";

strminutes = "0";
           Member memb = Member.SingleOrDefault(u => u.MemberID == memberid.ToInt32());

            int minutes = 0;
            if (conversation != "updatetagonly")
            {
                minutes = Convert.ToInt16(strminutes);
            }
            Role role = CCM_Helper.GetRole(Convert.ToInt32(memberid), Convert.ToInt32(discussionid), Convert.ToInt32(folderid), instanceid);
            if (role.RoleID == 0)
            {
               
            }
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            //MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            if (md.MemberDis != null)
            {
                if (conversation == "updatetagonly")
                { md.MemberDis.Tags = tags; md.Update(); }
                else
                {
                    md.MemberDis.DiscussionID = discussionid;
                    md.MemberDis.TeamMember = teammember;
                    md.MemberDis.Conversation = conversation.Replace('\n', ' ');
                    md.MemberDis.ModDate = DateTime.Now;
                    md.MemberDis.RoleID = role.RoleID;
                    md.MemberDis.Deleted = false;
                    md.MemberDis.Tags = tags;
                    if (role.RoleID != 4 && role.RoleID != 5 && role.RoleID != 6 && role.RoleID != 7)
                    {
                        md.MemberDis.Minutes = minutes;
                    }
                    md.Update();
                    discussion.ModDate = DateTime.Now;
                    discussion.Update();
                }
              

              
            }
            else
            {
                md.MemberDis = new App_Code.MemberDiscussions()
                {
                    DiscussionID = discussionid,
                    PartitionKey = instanceid,
                    Active = active,
                    Conversation = conversation.Replace('\n', ' '),
                    CrtDate = DateTime.Now,
                    MemberID = memberid,
                    ModDate = DateTime.Now,
                    RoleID = role.RoleID,
                    TeamMember = teammember,
                    Tags = tags,
                    Minutes = minutes
                };
                discussion.ModDate = DateTime.Now;
                md.MemberDis.Deleted = false;
                discussion.Update();
                md.Add();
                HttpContext.Current.Session.Add("memDisID", md.MemberDis.RowKey);
               

            }





        }

        
    }
}