﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class ManageReminders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["DiscussionID"] != null)
                {
                    hdnDisID.Value = Request.QueryString["DiscussionID"].Decrypt().ToString();
                    hdnInstanceID.Value = Request.QueryString["InstanceID"].ToString();
                    hdnOwnerID.Value = Request.QueryString["MemberId"].Decrypt().ToString();
                    TeamMembers();
                    txtstartson.Text = DateTime.Now.ToString("MM/dd/yyyy");
                }
            }
        }


        public void TeamMembers()
        {
            int Owner = Request.QueryString["MemberId"].Decrypt().ToInt32();
            int DiscussionID = Request.QueryString["DiscussionID"].Decrypt().ToInt32();
            string InstanceID = Request.QueryString["InstanceID"].ToString();

            List<DiscussionToFolder> disToFolder = DiscussionToFolder.Find(u => u.InstanceID == InstanceID && u.DiscussionID == DiscussionID && u.Active == true && (u.RoleID != 4 && u.RoleID != 5)).ToList();
            ddlTeamMembers.DataSource = disToFolder;
            ddlTeamMembers.DataTextField = "Alias";
            ddlTeamMembers.DataValueField = "MemberID";
            ddlTeamMembers.DataBind();

            ddlTeamMembers.Items.Insert(0, new ListItem("Select Member", ""));
        }

        //Load List
        protected string LoadData(int DisID, string InstanceID, int ownerID)
        {
            string str = "";
            str = "<br/><br/><fieldset><legend>Reminders List</legend><table id='dataH' border='2' cellpadding='4' cellspacing='4' width='95%' class='table table-hover table-bordered table-striped paginated'><thead><tr><th>#</th><th>Task</th><th>Assigned To</th><th>Assignee</th><th>Repeats</th><th>Repeats Every</th><th>Created On</th><th></th></tr></thead><tbody>";
            string suffix = "";

            List<DiscussionReminder> reminders = DiscussionReminder.Find(u => u.DiscussionID == DisID && u.InstanceID == InstanceID).ToList();

            if (reminders.Count() > 0)
            {
                foreach (var reminder in reminders)
                {
                    string s = (reminder.RepeatEvery == 1) ? "" : "s";

                    if (reminder.Repeats == "Daily")
                        suffix = "Day" + s;
                    else if (reminder.Repeats == "Weekly")
                        suffix = "Week" + s;
                    else if (reminder.Repeats == "Monthly")
                        suffix = "Month" + s;
                    else if (reminder.Repeats == "Yearly")
                        suffix = "Year" + s;

                    str += "<tr>";
                    str += "<td><input type='hidden' value='" + reminder.InstanceID + "' id='instanceid'><input type='hidden' value='" + reminder.DiscussionID + "' id='DisID'></td>";
                    str += "<td>" + reminder.Description + "</td>";
                    str += "<td>" + GetMemberDetails(DisID, (int)reminder.MemberID).Alias + "<input type='hidden' value='" + reminder.MemberID + "' id='assignedTo'></td>";
                    str += "<td>" + GetMemberDetails(DisID, (int)reminder.AddedBy).Alias + "<input type='hidden' value='" + reminder.AddedBy + "' id='assignee'></td>";
                    str += "<td>" + reminder.Repeats + "</td>";
                    str += "<td>" + reminder.RepeatEvery + " " + suffix + "</td>";
                    str += "<td>" + Convert.ToDateTime(reminder.CrtDate).ToString("MM-dd-yyyy") + "</td>";

                    if (reminder.AddedBy != ownerID)
                    {
                        str += "<td><a  href='javascript: void(0)' id='" + reminder.ID + "'  class='btndisable' title='You are not authorized to edit.'> Edit </a> &nbsp;&nbsp;  <a  href='javascript: void(0)' class='btndisable' title='You are not authorized to remove.' > Remove </a>";
                    }
                    else
                    {
                        str += "<td><a  href='javascript: void(0)' data-target='#editReminder' class='btnEdit' id='" + reminder.ID + "'> Edit </a> &nbsp;&nbsp;  <a  href='javascript: void(0)' class='btndanger' onclick='return Remove(" + reminder.ID + ");' > Remove </a>";
                    }

                    str += "</td>";
                    str += "</tr>";
                }
            }
            else
            {
                str += "<tr><td colspan='7'>No Reminders Available!</td></tr>";
            }

            str += "</tbody></table></fieldset>";
            return str;
        }

        [WebMethod]
        public static string GetMembers(int selected, string InstanceID, int DisID)
        {
            ManageReminders obj = new ManageReminders();
            string html = "";

            List<DiscussionToFolder> members = DiscussionToFolder.Find(u => u.InstanceID == InstanceID && u.DiscussionID == DisID && u.Active == true && (u.RoleID != 4 && u.RoleID != 5)).ToList();

            foreach (var member in members)
            {
                if (member.MemberID == selected)
                    html += "<option value='" + member.MemberID + "' selected='selected'>" + member.Alias + "</option>";
                else
                    html += "<option value='" + member.MemberID + "'>" + member.Alias + "</option>";
            }
            return html;

        }

        [WebMethod]
        public static string UpdateReminder(string Description, string Repeats, int RepeatsEvery, int MemberID, int ID, int DisID, string InstanceID, int OwnerID)
        {
            DiscussionReminder reminder = DiscussionReminder.SingleOrDefault(a => a.ID == ID);
            if (reminder != null)
            {
                reminder.Description = Description;
                reminder.Repeats = Repeats;
                reminder.RepeatEvery = RepeatsEvery;
                reminder.MemberID = MemberID;
                reminder.DateModified = DateTime.Now;
                reminder.Update();
            }

            ManageReminders obj = new ManageReminders();
            string HTML = obj.LoadData(DisID, InstanceID, OwnerID);

            return HTML;
        }

        [WebMethod]
        public static string RemoveReminder(int ID, string InstanceID, int DisID, int OwnerID)
        {
            DiscussionReminder reminder = DiscussionReminder.SingleOrDefault(a => a.ID == ID);
            if (reminder != null)
            {
                reminder.Delete();
            }

            ManageReminders obj = new ManageReminders();
            string HTML = obj.LoadData(DisID, InstanceID, OwnerID);

            return HTML;
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int memberid = ddlTeamMembers.SelectedValue.ToInt32();
            string description = txtDescription.Text;
            string repeats = ddlRepeat.SelectedValue;
            int repeatsevery = ddlRepeatEvery.SelectedValue.ToInt32();
            DateTime startson = Convert.ToDateTime(txtstartson.Text);
            string instanceid = hdnInstanceID.Value;
            int DisID = hdnDisID.Value.ToInt32();
            int ownerid = hdnOwnerID.Value.ToInt32();

            DiscussionReminder reminder = new DiscussionReminder()
            {
                Description = description,
                InstanceID = instanceid,
                DiscussionID = DisID,
                MemberID = memberid,
                AddedBy = ownerid,
                CrtDate = startson,
                Repeats = repeats,
                RepeatEvery = repeatsevery
            };
            reminder.Add();
            ClearFields();
            LoadData(DisID, instanceid, ownerid);
        }

        public void ClearFields()
        {
            ddlTeamMembers.SelectedIndex = 0;
            txtDescription.Text = "";
            ddlRepeat.SelectedIndex = 0;
            ddlRepeatEvery.SelectedIndex = 0;

        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            int DiscussionID = Request.QueryString["DiscussionID"].Decrypt().ToInt32();
            string InstanceID = Request.QueryString["InstanceID"].ToString();

            string HTML = LoadData(DiscussionID, InstanceID, hdnOwnerID.Value.ToInt32());
            reminderslist.InnerHtml = HTML;
            btnHide.Visible = true;
            btnView.Visible = false;
            reminderslist.Visible = true;
        }

        protected void btnHide_Click(object sender, EventArgs e)
        {
            btnHide.Visible = false;
            reminderslist.Visible = false;
            btnView.Visible = true;
        }

        protected DiscussionToFolder GetMemberDetails(int DisID, int memberID)
        {
            DiscussionToFolder member = DiscussionToFolder.SingleOrDefault(a => a.DiscussionID == DisID && a.MemberID == memberID);
            return member;
        }
    }
}