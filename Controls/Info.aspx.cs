﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM.Controls
{
    public partial class Info : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["h"] != null)
            {
                h1.InnerText = Request.QueryString["h"];
            }
            if (Request.QueryString["t"] != null)
            {
                h2.InnerText = Request.QueryString["t"];
            }
            title.Text = "Not Allowed";
        }
    }
}