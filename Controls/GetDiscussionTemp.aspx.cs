﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CCM.App_Code;
using System.Web.UI.HtmlControls;
using MyCCM;
using AvaimaThirdpartyTool;

namespace CCM.Controls
{
    public partial class GetDiscussionTemp : AvaimaWebPage
    {
        String _LocalInstanceID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.InstanceID == "0") { _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab"; }
            else { _LocalInstanceID = this.InstanceID; }
            if (Request.QueryString["pid"] != null)
            {
                hdnProjectID.Value = Request.QueryString["pid"];
            }
            if (Request.QueryString["oid"] != null)
            {
                hdnOID.Value = Request.QueryString["oid"];
            }
            if (Request.QueryString["fid"] != null)
            {
                hdn_folderid.Value = Request.QueryString["pid"];
            }
            if (Request.QueryString["disid"] != null)
            {
                hdnDiscussionID.Value = Request.QueryString["disid"];
            }
            if (Request.QueryString["memberid"] != null)
            {
                hdnMemberID.Value = Request.QueryString["memberid"];
            }
            if (Request.QueryString["iid"] != null)
            {
                hdnInstanceID.Value = Request.QueryString["iid"];
            }
            if (Request.QueryString["memDisID"] != null)
            {
                App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                md.GetMemberDiscussionsByID(Request.QueryString["memDisID"].ToString());
                rptTempDiscussions.DataSource = md.MemberDiscussions;
                rptTempDiscussions.DataBind();
            }
        }

        protected void rptDiscussions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl imgMember = e.Item.FindControl("imgMember") as HtmlControl;
                HtmlControl imgMember2 = e.Item.FindControl("imgMember2") as HtmlControl;
                HtmlControl tdImage = e.Item.FindControl("tdImage") as HtmlControl;
                HtmlControl tdImage2 = e.Item.FindControl("tdImage2") as HtmlControl;
                //Panel panel = e.Item.FindControl("divMemDis") as HtmlControl
                HtmlContainerControl myDivMemDis = e.Item.FindControl("divMemDis") as HtmlContainerControl;
                HtmlContainerControl myDivMemDisO = myDivMemDis.FindControl("divMemDisO") as HtmlContainerControl;
                Label lblMemberTemp = e.Item.FindControl("lblMemberTemp") as Label;
                Label lblMemberTemp2 = e.Item.FindControl("lblMemberTemp2") as Label;
                TextBox txtMemDis = e.Item.FindControl("txtMemDis") as TextBox;
                Int32 memberID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MemberID"));
                String memberDisID = DataBinder.Eval(e.Item.DataItem, "RowKey").ToString();
                Member member = Member.SingleOrDefault(u => u.MemberID == memberID);
                //Role role = new Role();
                //if (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")) != 0)
                //{
                //    role = Role.SingleOrDefault(u => u.RoleID == Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID").ToString()));
                //}
                //else
                //{
                //    role = CCM_Helper.GetRole(member.MemberID, Convert.ToInt32(hdnDisID.Value), Convert.ToInt32(hdnProjectID.Value), _LocalInstanceID);
                //}

                if (member != null)
                {
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDiscussionID.Value.ToInt32(), member.MemberID);
                    imgMember.Attributes["src"] = imgMember2.Attributes["src"] = "_assests/Images/" + mr.ToString() + ".png";

                    //if (member.ImagePath != "")
                    //{

                    //}
                    //else
                    //{
                    //    imgMember2.Attributes["src"] = imgMember.Attributes["src"] = "_assests/Images/" + role.RoleTitle + ".png";
                    //}
                    Image img = new Image();
                    img.ID = "disAction";
                    img.CssClass = "disAction";
                    img.ImageUrl = "_assests/Images/downarrow.png";
                    img.Attributes.Add("data-id", memberDisID);
                    Label lblDateTime = new Label();
                    lblDateTime.Attributes["class"] = "lblDateTime";
                    lblDateTime.Style.Add("float", "right");
                    lblDateTime.Style.Add("margin-right", "-29px");
                    lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                    DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDiscussionID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == memberID);
                    if (dtf != null)
                    {
                        if (String.IsNullOrEmpty(dtf.Alias)) { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                        else { lblMemberTemp2.Text = lblMemberTemp.Text = dtf.Alias; }
                    }
                    else { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                    string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                    myDivMemDis.Attributes["class"] = "Class " + roleTitle;
                    myDivMemDis.Attributes.Add("data-role", roleTitle);
                    myDivMemDisO.InnerHtml += DataBinder.Eval(e.Item.DataItem, "Conversation").ToString();
                    myDivMemDisO.InnerHtml += "</br>";
                    DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDiscussionID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                    if (member.MemberID == Convert.ToInt32(hdnMemberID.Value)) { myDivMemDis.Controls.Add(img); }
                    else
                    {
                        if (dtfCurrMember != null)
                        {
                            if (dtfCurrMember.RoleID == 1 || dtfCurrMember.RoleID == 2)
                            {
                                myDivMemDis.Controls.Add(img);
                            }
                        }
                    }
                    myDivMemDis.Controls.Add(lblDateTime);
                    if (roleTitle == "ClientMember") { tdImage.Visible = false; tdImage2.Visible = true; }
                    else if (roleTitle == "VendorMember") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "Admin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "SuperAdmin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "ClientAdmin") { tdImage.Visible = false; tdImage2.Visible = true; }

                    Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;
                    List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                    if (disFiles.Count > 0)
                    {
                        rptFiles.DataSource = disFiles;
                        rptFiles.DataBind();
                    }
                    else { }
                }
            }
        }
    }
}