﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetDiscussionTemp.aspx.cs" Inherits="CCM.Controls.GetDiscussionTemp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Discussion</title>
    <script src="_assests/jQuit/js/jquery-1.6.2.min.js"></script>
    <%--<script src="_assests/jQuit/js/Context-Menu/jquery-1.8.2.min.js"></script>--%>
    <script src="_assests/jQuit/js/jquery-ui-1.8.16.custom.min.js"></script>
    <%--<script src="_assests/jQuit/js/jquery-ui.js"></script>--%>
    <link href="_assests/jQuit/css/jquery-ui.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">--%>
    <link href="_assests/jQuit/js/Context-Menu/src/jquery.contextMenu.css" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script src="_assests/CKEditor/ckeditor.js"></script>
    <script src="_assests/CKEditor/adapters/jquery.js"></script>
    <script src="_assests/Uplodify/jquery.uploadify.min.js"></script>
    <link href="_assests/Uplodify/uploadify.css" rel="stylesheet" />
    <script>
        var redirectURL = "";
        function initIzFM(selector, currFolder) {
            try {
                //alert('Initalizing Uploader');
                $(selector).uploadify({
                    'swf': '_assests/Uplodify/uploadify.swf',
                    'uploader': 'Upload.ashx',
                    //'method':'post',
                    'script': 'Upload.ashx',
                    'cancelImg': '_assests/Uplodify/uploadify-cancel.png',
                    'buttonText': 'Upload Images',
                    //'script': 'Upload.ashx',
                    'folder': currFolder,
                    'fileTypeDesc': 'Files',
                    //'fileTypeExts': '*.png;*.jpg;*.jpeg;*.gif;*.tff;*.tf;*.js;*.swf;*.htc;*.txt;*.xml;*.rss;*.eot;*.svg;*.ttf;*.woff;*.pfb;*.pfm;*.otf;*.ffil;*.dfont;*.lwfn;*.html;*.htm;*.css;',
                    'multi': true,
                    'auto': false,
                    'formData': { 'discussionID': $('#hdnDisID').val(), 'projectID': $('#hdnProjectID').val() },
                    'onQueueComplete': function (queueData) {
                        //alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');
                        //$('#divProgress').dialog({
                        //    modal: true,
                        //    height: 0,
                        //    closeOnEscape: false,
                        //    open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); }
                        //});
                        //location.reload();                        
                    },
                    'onUploadError': function (file, errorCode, errorMsg, errorString) {
                        alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                    },
                    'onUploadSuccess': function (file, data, response) {
                        //alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
                        try {
                            if (data.toString().substr(0, 5).toString() == "Error") {
                                alert(data);
                            }
                            console.log("starting another");
                            var fileUpload = $(btnSend).parent().find('#fileUpload1').uploadify('upload');
                        } catch (e) {
                            alert(e.message);
                        }

                    },
                    'onUploadProgress': function (file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
                        try {
                            //$('#pProgress').html(totalBytesUploaded + ' bytes uploaded of ' + bytesTotal + ' bytes.');

                            //$('#divProgress').dialog('close');
                        } catch (e) {
                            alert(e.message);
                        }
                    }
                });

            } catch (e) {
                alert(e.message);
            }
        }
        function changeNavigation(url) {
            var obj = { 'navAnchor': url };
            window.history.pushState(obj, 'Default', url);
        }
        function SaveDiscussion(discussionid, title, projectid, instanceid, status) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/SaveDiscussion',
                data: "{'discussionid':'" + discussionid + "','title':'" + title + "','projectid':'" + projectid + "','instanceid':'" + instanceid + "','status':'" + status + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    $('#hdnDisID').val(response.d.toString().split(':')[2]);
                    var url = "Discussion.aspx?disid=" + response.d.toString().split(':')[2] + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val();
                    changeNavigation(url)
                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function SaveMemberDiscussion(memdisid, discussionid, instanceid, memberid, teammember, conversation, active, folderid, subject, btnSend) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/SaveMemberDiscussion',
                data: "{'memdisid':'" + memdisid + "','discussionid':'" + discussionid + "','instanceid':'" + instanceid + "','memberid':'" + memberid + "','teammember':'" + teammember + "','conversation':'" + conversation + "','active':'" + active + "','folderid':'" + folderid + "','subject':'" + subject + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    var fileUpload = $(btnSend).parent().find('#fileUpload1').uploadify('upload');
                    redirectURL = "Controls/GetDiscussionTemp.aspx?memDisID=" + response.d.toString().split(':')[2] + "&pid=" + $('#hdnProjectID').val() + " .divMD";
                    var myInterval = setInterval(function () {
                        if ($('.uploadify-queue-item').size() == 0) {
                            if ($('#divMemberDiscussion .divMD').size() > 0) {
                                $('#tempDiv').load(redirectURL, function () {
                                    console.log('loaded');
                                    $('#divMemberDiscussion .divMD').last().after($('#tempDiv .divMD'))
                                    $('.txtMemDis').val('');
                                    $('.btnSend').val('Send');
                                    $('.btnSend').removeClass('button_loading');
                                    resizeIframe();
                                    window.setTimeout(function () {
                                        console.log('1000');
                                        resizeIframe();
                                    }, 1000);
                                    window.setTimeout(function () {
                                        console.log('2000');
                                        resizeIframe();
                                    }, 2000);
                                });
                                clearInterval(myInterval);
                            }
                            else {
                                redirectURL = "Discussion.aspx?disid=" + $('#hdnDisID').val() + "&pid=" + $('#hdnProjectID').val() + "&oid=" + $('#hdnOID').val() + "&instanceid=" + $('#hdnInstanceID').val() + " .formdis";
                                $('#bodyDis').load(redirectURL, function () {
                                    console.log('loaded all');
                                    changeNavigation(redirectURL);
                                    InitializePage();
                                    resizeIframe();
                                    window.setTimeout(function () {
                                        console.log('1000');
                                        resizeIframe();
                                    }, 1000);
                                    window.setTimeout(function () {
                                        console.log('2000');
                                        resizeIframe();
                                    }, 2000);
                                });
                            }
                        }
                    }, 1000);

                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function DeleteMemberDiscussion(memdisid) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/DeleteMemberDiscussion',
                data: "{'memdisid':'" + memdisid + "'}",
                async: true,
                success: function (response) {
                    console.log(response.d);
                    $('.divMD[data-rowkey="' + memdisid + '"]').fadeOut(200);
                    resizeIframe();
                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function initiateContextMenu() {
            $.contextMenu({
                selector: '.disAction',
                trigger: 'left',
                callback: function (key, options) {
                    console.log($(this).attr('id'));
                    if (key == 'delete') {
                        //if (ConfirmDelete(".divDialog", "Delete Conversation", "Are you sure you want to delete this conversation?", function () {
                        DeleteMemberDiscussion($('#hdnSelMemDisID').val());
                        //}));
                    }
                    //$('.divworker').hide(200);
                    //$('.' + key).show(200);
                    //$('.lblResultMsg').text($('.' + key).attr('data-text'));
                    //$('.lblOvertimeLabel').text($('.' + key).attr('data-label'));
                    //var m = "clicked: " + key;
                    //window.console && console.log(m) || alert(m);                    
                },
                items: {
                    "delete": { name: "Delete" },
                    "copyto": { name: "Copy To" },
                    "moveto": { name: "Move To" },
                    "forward": { name: "Forward" }
                }
            });
        }
        function InitializePage() {
            $('.lnkAddClient').click(function () {
                OpenSimpleDialog('.trMainClient', 'Add Client', 800);
                return false;
            });
            $('.lnkAddTeam').click(function () {
                OpenSimpleDialog('.trMainTeam', 'Add Team', 800);
                return false;
            });
            $('.txtMemDis').ckeditor();
            $('.formTitleDiv').blur(function () {
                SaveDiscussion($('#hdnDisID').val(), $('.formTitleDiv').text(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), 'open');
            });
            initIzFM('.fileUpload1', '');
            $('.aUpload').click(function () {

            });
            $('.btnSend').click(function () {
                if (parseInt($('#hdnDisID').val()) == 0) {
                    $('.formTitleDiv').focus();
                }
                else {
                    if (!($(this).hasClass('button_loading'))) {
                        $(this).val('');
                        $(this).addClass('button_loading');
                        //SaveDiscussion($('#hdnDisID').val(), $('.formTitleDiv').text(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), 'open');
                        SaveMemberDiscussion('0', $('#hdnDisID').val(), $('#hdninstanceid').val(), $("#hdnMemberID").val(), $('#hdnTeamMember').val(), $('.txtMemDis').val(), "true", $('#hdn_folderid').val(), $('.formTitleDiv').val(), this);
                    }
                }

                return false;
            });
            $('.disAction').click(function () {
                $('#hdnSelMemDisID').val($(this).attr('data-id'));
                resizeIframe();
            });
            initiateContextMenu();
            $(window).load(function () {
                resizeIframe();
                initiateContextMenu();
                setTimeout(function () {
                    resizeIframe();
                    initiateContextMenu();
                }, 1000);
                //$("html, body").animate({ scrollTop: $(document).height() }, 1000);
            });
            BindRemoveMember();
            $('.imgAdd').click(function () {
                var newTr = $('.trTeamSample').clone();
                $(newTr).removeClass('trTeamSample').addClass('trTeam');
                if ($('.trTeam').last().size() > 0) {
                    $('.trTeam').last().after(newTr)
                }
                else {
                    $('#tblTeam').append(newTr)
                }
                $(newTr).show(200);
                $(newTr).find('.txtFirstName').focus();
                BindRemoveMember();
                BindInputValidation();
            });
            $('.imgAddClient').click(function () {
                var newTr = $('.trClientSample').clone();
                $(newTr).removeClass('trClientSample').addClass('trClient');
                if ($('.trClient').last().size() > 0) {
                    $('.trClient').last().after(newTr)
                }
                else {
                    $('#tblClient').append(newTr)
                }
                $(newTr).show(200);
                $(newTr).find('.txtFirstName').focus();
                BindRemoveMember();
                BindInputValidation();
            });
            $('.btnAddTeam').click(function () {
                ValidateTeamFields();
                if (teamValidToGo > 0) {
                    console.log('Not good to go..');
                }
                else {
                    console.log('good to go..');
                    $('.trTeam').each(function () {
                        var firstName = $(this).find('.txtFirstName').val();
                        var LastName = $(this).find('.txtLastName').val();
                        var email = $(this).find('.txtEmail').val();
                        var ddlStatus = $(this).find('select[id*="ddlStatus"]').val()
                        addMember($('#hdnDisID').val(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), firstName, LastName, email, ddlStatus, 'VendorMember', this);
                    });
                }
                return false;
            });
            $('.btnAddClient').click(function () {
                ValidateClientFields();
                if (teamValidToGo > 0) {
                    console.log('Not good to go..');
                }
                else {
                    console.log('good to go..');
                    $('.trClient').each(function () {
                        var firstName = $(this).find('.txtFirstName').val();
                        var LastName = $(this).find('.txtLastName').val();
                        var email = $(this).find('.txtEmail').val();
                        var ddlStatus = $(this).find('select[id*="ddlStatus"]').val()
                        addMember($('#hdnDisID').val(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), firstName, LastName, email, ddlStatus, 'ClientMember', this);
                    });
                }
                return false;
            });
            BindInputValidation();
            $('.txtMemDis').focus(function () {
                resizeIframe();
            });
        }
        $(function () {
            InitializePage();
        });
    </script>
    <script>
        function addMember(discussionid, projectid, instanceid, firstname, lastname, email, status, type, trTeam) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/AddMember',
                data: "{'discussionid':'" + discussionid + "','projectid':'" + projectid + "','instanceid':'" + instanceid + "','firstname':'" + firstname + "','lastname':'" + lastname + "','email':'" + email + "','status':'" + status + "','type':'" + type + "'}",
                async: false,
                success: function (response) {
                    //$(trTeam).append(response.d);
                    $(trTeam).attr('data-id', response.d.toString().split(':')[2]);
                    console.log(response.d);
                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function removeMember(memberid, discussionid, projectid, status, trTeam) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/RemoveMember',
                data: "{'memberid':'" + memberid + "', 'discussionid':'" + discussionid + "', 'projectid':'" + projectid + "', 'status':'" + status + "'}",
                async: false,
                success: function (response) {
                    //$(trTeam).append(response.d);
                    console.log(response.d);
                    $(trTeam).hide(200, function () {
                        $(trTeam).remove();
                    });
                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function getFirstLastName(email, tr, tohide) {
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: 'Discussion.aspx/GetFirstLastName',
                data: "{'email':'" + email + "'}",
                async: false,
                success: function (response) {
                    //$(tr).append(response.d);
                    console.log(response.d);
                    if (response.d.toString().indexOf("Error") <= -1) {
                        var obj = $.parseJSON(response.d.toString());
                        $(tr).find('.txtFirstName').val(obj.firstname);
                        $(tr).find('.txtLastName').val(obj.lastname);
                        if (tohide == 'team') {
                            ValidateTeamFields();
                        }
                        else {
                            ValidateClientFields();
                        }
                    }
                },
                error: function () {
                    $(trTeam).append("some problem in saving data");
                }
            });
        }
        function BindRemoveMember() {
            $('.imgRemoveMember').unbind('click');
            $('.imgRemoveMember').click(function () {
                var firstName = $(this).parent().parent().find('.txtFirstName').val();
                var lastName = $(this).parent().parent().find('.txtFirstName').val();
                var email = $(this).parent().parent().find('.txtEmail').val();
                var tr = $(this).parent().parent();
                var trdata = $(tr).attr('data-id');
                if (firstName != '' || lastName != '' || email != '') {
                    if (trdata == "") {
                        ConfirmDelete('#divcpdialog', 'Remove?', 'The fields are not empty and not saved yet, are you sure you want to remove?', function () {
                            $(tr).remove();
                        });
                    }
                    else {
                        ConfirmDelete('#divcpdialog', 'Remove Member?', 'Are you sure you want to remove this member from project?', function () {
                            var ddlstatus = tr.parent().find('select[id*="ddlStatus"]').val();
                            removeMember(trdata, $('#hdnDisID').val(), $('#hdnProjectID').val(), ddlstatus, tr);
                            //$(tr).remove();
                        });
                    }
                }
                else {
                    $(tr).remove();
                }
            });
        }
        function BindInputValidation() {
            $('#tblTeam').find('input').unbind('change');
            $('#tblTeam').find('input').change(function () {
                ValidateTeamFields();
            });
            $('#tblClient').find('input').unbind('change');
            $('#tblClient').find('input').change(function () {
                ValidateClientFields();
            });
        }
        function indicateTeamMember(trTeam, status) {
            if (status == 'ok') {
                $(trTeam).find('.memberInfoDiv').css('background-color', 'green');
            }
            else if (status == 'issue') {
                $(trTeam).find('.memberInfoDiv').css('background-color', 'red');
                //$(trTeam).find('.txtFirstName').focus();
            }
        }
        function ValidateTeamFields() {
            teamValidToGo = 0;
            $('.trTeam').each(function () {
                var firstName = $(this).find('.txtFirstName').val();
                var LastName = $(this).find('.txtLastName').val();
                var email = $(this).find('.txtEmail').val();
                if (firstName == '' && LastName == '' && email != '') {
                    getFirstLastName(email, this, 'team');
                    return false;
                }
                if (firstName == '' && LastName == '' && email == '') {
                    console.log('remove');
                    if ($('.trTeam').size() < 2) {
                        teamValidToGo++;
                        indicateTeamMember(this, 'issue');
                    }
                    else {
                        $(this).remove();
                    }
                }
                else if (firstName == '' || LastName == '' || email == '') {
                    indicateTeamMember(this, 'issue');
                    teamValidToGo++;
                }
                else {
                    if (validateEmail($(this).find('.txtEmail').val())) {
                        indicateTeamMember(this, 'ok');
                    }
                    else {
                        indicateTeamMember(this, 'issue');
                        teamValidToGo++;
                    }
                }
            });
            console.log(teamValidToGo);
        }
        function ValidateClientFields() {
            teamValidToGo = 0;
            $('.trClient').each(function () {
                var firstName = $(this).find('.txtFirstName').val();
                var LastName = $(this).find('.txtLastName').val();
                var email = $(this).find('.txtEmail').val();
                if (firstName == '' && LastName == '' && email != '') {
                    getFirstLastName(email, this, 'client');
                    return false;
                }
                if (firstName == '' && LastName == '' && email == '') {
                    console.log('remove');
                    if ($('.trClient').size() < 2) {
                        teamValidToGo++;
                        indicateTeamMember(this, 'issue');
                    }
                    else {
                        $(this).remove();
                    }
                }
                else if (firstName == '' || LastName == '' || email == '') {
                    indicateTeamMember(this, 'issue');
                    teamValidToGo++;
                }
                else {
                    if (validateEmail($(this).find('.txtEmail').val())) {
                        indicateTeamMember(this, 'ok');
                    }
                    else {
                        indicateTeamMember(this, 'issue');
                        teamValidToGo++;
                    }
                }
            });
            console.log(teamValidToGo);
        }
        function ToggleClientTeam() {
            if ($('#hdnProjectID').val() == '0' || $('#hdnProjectID').val() == '') {
                $('.trMainTeam').hide();
                $('.trMainClient').hide();
                console.log($('#hdnProjectID').val());
            }
            else {
                $('.trMainTeam').show(200);
                $('.trMainClient').show(200);
                console.log($('#hdnProjectID').val());
            }

            resizeIframe();
            console.log('resizing iframe');
        }
        function InitializePage2() {
            BindRemoveMember();
            $('.imgAdd').click(function () {
                var newTr = $('.trTeamSample').clone();
                $(newTr).removeClass('trTeamSample').addClass('trTeam');
                if ($('.trTeam').last().size() > 0) {
                    $('.trTeam').last().after(newTr)
                }
                else {
                    $('#tblTeam').append(newTr)
                }
                $(newTr).show(200);
                $(newTr).find('.txtFirstName').focus();
                BindRemoveMember();
                BindInputValidation();
            });
            $('.imgAddClient').click(function () {
                var newTr = $('.trClientSample').clone();
                $(newTr).removeClass('trClientSample').addClass('trClient');
                if ($('.trClient').last().size() > 0) {
                    $('.trClient').last().after(newTr)
                }
                else {
                    $('#tblClient').append(newTr)
                }
                $(newTr).show(200);
                $(newTr).find('.txtFirstName').focus();
                BindRemoveMember();
                BindInputValidation();
            });
            $('.btnAddTeam').click(function () {
                ValidateTeamFields();
                if (teamValidToGo > 0) {
                    console.log('Not good to go..');
                }
                else {
                    console.log('good to go..');
                    $('.trTeam').each(function () {
                        var firstName = $(this).find('.txtFirstName').val();
                        var LastName = $(this).find('.txtLastName').val();
                        var email = $(this).find('.txtEmail').val();
                        var ddlStatus = $(this).find('select[id*="ddlStatus"]').val()
                        addMember($('#hdnDisID').val(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), firstName, LastName, email, ddlStatus, 'VendorMember', this);
                    });
                }
                return false;
            });
            $('.btnAddClient').click(function () {
                ValidateClientFields();
                if (teamValidToGo > 0) {
                    console.log('Not good to go..');
                }
                else {
                    console.log('good to go..');
                    $('.trClient').each(function () {
                        var firstName = $(this).find('.txtFirstName').val();
                        var LastName = $(this).find('.txtLastName').val();
                        var email = $(this).find('.txtEmail').val();
                        var ddlStatus = $(this).find('select[id*="ddlStatus"]').val()
                        addMember($('#hdnDisID').val(), $('#hdnProjectID').val(), $('#hdninstanceid').val(), firstName, LastName, email, ddlStatus, 'ClientMember', this);
                    });
                }
                return false;
            });
            BindInputValidation();
        }
        $(document).ready(function () {
            InitializePage2();
        });
        $(window).load(function () {
            setTimeout(function () {
                resizeIframe();
            }, 200);
            setTimeout(function () {
                resizeIframe();
            }, 400);
            setTimeout(function () {
                resizeIframe();
            }, 600);
            setTimeout(function () {
                resizeIframe();
            }, 800);
            setTimeout(function () {
                resizeIframe();
            }, 1000);
        });
    </script>
    <style>
        .disAction {
            float: right;
            width: 16px !important;
            border: 1px solid black !important;
            margin: -6px;
            display: inline-block;
            position: absolute;
            top: 8px;
            right: 8px;
            /* border-radius: 0 !important; */
            background: #d7e4ed;
            border-width: 0px;
            /* padding: 4px 5px; */
            cursor: pointer;
        }

        .divMDTemp {
            display: none;
        }

        ul {
            padding-left: 40px;
        }

        input[type="submit"] {
            border: none;
            padding: 5px 13px;
            cursor: pointer;
            float: right;
        }

        .tblDis img {
            width: 50px;
            border-radius: 30px;
        }

        .tblDis .tdImage {
            padding: 4px;
            text-align: center;
        }

        .teambubble, .SuperAdmin, .Admin, .VendorMember {
            position: relative;
            /*width: 540px;*/
            padding: 10px 37px 4px 16px;
            background: #d7e4ed;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
            display: inline-block;
        }

            .teambubble:after, .SuperAdmin:after, .Admin:after, .VendorMember:after {
                content: "";
                position: absolute;
                top: 5px;
                left: -15px;
                border-style: solid;
                border-width: 10px 15px 10px 0;
                border-color: transparent #d7e4ed;
                display: block;
                width: 0;
                z-index: 1;
            }

        .clientbubble, .ClientMember {
            position: relative;
            /*width: 540px;*/
            height: 150px;
            padding: 10px 37px 4px 16px;
            display: inline-block;
            background: #fff;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
        }

            .clientbubble:after, .ClientMember:after {
                content: '';
                position: absolute;
                border-style: solid;
                border-width: 19px 0 19px 32px;
                border-color: transparent #fff;
                display: block;
                width: 0;
                z-index: 1;
                right: -32px;
                top: 1px;
            }

        .divMD {
            width: 80% !important;
            /*background-color: #f1f1f1;*/
            margin: 0px 0px 0px 0px;
            padding: 22px 0px;
        }
    </style>
    <script src="_assests/jQuit/js/Context-Menu/src/jquery.ui.position.js"></script>
    <script src="_assests/jQuit/js/Context-Menu/src/jquery.contextMenu.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Repeater ID="rptTempDiscussions" runat="server" OnItemDataBound="rptDiscussions_ItemDataBound">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="divMD" data-team='<%# DataBinder.Eval(Container.DataItem,"TeamMember") %>' data-rowkey='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>' style="width: 1000px">
                        <table class="tblDis" style="width: 100%">
                            <tr>
                                <td class="tdImage" id="tdImage" runat="server" style="width: 120px">
                                    <img class="imgMember" id="imgMember" runat="server" src="" />
                                    <br />
                                    <asp:Label ID="lblMemberTemp" CssClass="lblMember" runat="server" Text=""></asp:Label>
                                </td>
                                <td data-memdisid='<%# DataBinder.Eval(Container.DataItem,"RowKey") %>'>
                                    <div id="divMemDis" runat="server" class="divMemDis" data-role="">
                                        <div id="divMemDisO" runat="server">
                                        </div>
                                        <asp:Repeater ID="rptFiles" runat="server">
                                            <HeaderTemplate>
                                                <div class="files">
                                                    <span style="border-bottom: 1px solid gray; display: block; padding-bottom: 2px;">Files</span>
                                                    <ul style="list-style-type: circle; margin-top: 3px; padding-left: 25px !important">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <a href='<%# DataBinder.Eval(Container.DataItem,"FilePath").ToString() %>' id="lnkFileName" runat="server"><%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %></a>
                                                </li>
                                                <%--<asp:LinkButton ID="lnkFileName" runat="server"   Text='<%# DataBinder.Eval(Container.DataItem,"FileName").ToString() %>'></asp:LinkButton>--%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                                    <br />
                                                <br />
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>

                                    <%--<asp:TextBox runat="server" ID="txtMemDis" CssClass="" TextMode="MultiLine" Width="200px"></asp:TextBox>--%>
                                </td>
                                <td class="tdImage2" id="tdImage2" runat="server" style="width: 120px">
                                    <img class="imgMember2" id="imgMember2" runat="server" src="" />
                                    <br />
                                    <asp:Label ID="lblMemberTemp2" CssClass="lblMember2" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
            </asp:Repeater>
            <asp:HiddenField runat="server" ID="hdnProjectID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnDiscussionID" Value="0" />
            <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
            <asp:HiddenField runat="server" ID="hdn_folderid" Value="0" />
            <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />            
            <asp:HiddenField runat="server" ID="hdnOID" Value="0" />     
        </div>
    </form>
</body>
</html>

