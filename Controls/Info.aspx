﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="CCM.Controls.Info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title id="title" runat="server"></title>
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="div_html">
            <div style="margin: 50px 0; text-align: center; font: 16px/20px arial, helvetica, sans-serif;">
                <a target="blank" href="http://www.avaima.com/">
                    <img style="border: 0;" src="http://avaima.cloudapp.net/AVAIMA-Logo-large.png" alt="AVAIMA" title="AVAIMA"></a>
                <br>
                <div style="width: 650px; margin: 0 auto 0 auto; padding: 40px 0 0 0;">
                    <div id="h1" runat="server" style="margin: 0px 0 0 0; font-size: 50px; line-height: 50px;">404. That's an error.</div>
                    <br>                                                        
                    <h3 id="h2" runat="server" style="margin-top: 17px; line-height: 20px;"></h3>
                </div>
            </div>
        </div>        
    </form>
</body>
</html>
