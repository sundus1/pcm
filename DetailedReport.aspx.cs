﻿using CCM.App_Code;
using Microsoft.WindowsAzure.Storage.Table;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class DetailedReport : System.Web.UI.Page
    {
        App_Code.MemberDiscussion md = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    md = new App_Code.MemberDiscussion();
                    int memberID = Request.QueryString["MemberId"].Decrypt().ToInt32();
                    string instanceID = Request.QueryString["IntanceID"].ToString();
                    string discussionID = Request.QueryString["disID"].Decrypt().ToString();
                    string fromDate = Request.QueryString["fromDate"].ToString();
                    string toDate = Request.QueryString["toDate"].ToString();
                    cldFromDate.Value = fromDate;
                    cldToDate.Value = toDate;
                    LoadData(discussionID, fromDate, toDate, memberID, instanceID);
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.ToString();
                }
            }
        }
        public static string Remove_Html_Tags(string Html)
        {
            string Only_Text = Regex.Replace(Html, @"<(.|\n)*?>", string.Empty);

            return Only_Text;
        }
        private string GetName(int memberId, int discuID, string instanceID)
        {
            string memberName = "";
            Member member = Member.SingleOrDefault(u => u.MemberID == memberId);
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discuID && u.InstanceID == instanceID && u.MemberID == memberId);
            if (dtf != null)
            {
                if (String.IsNullOrEmpty(dtf.Alias))
                { memberName = member.NickName; }
                else { memberName = dtf.Alias; }
            }
            else { memberName = member.NickName; }
            return memberName;
        }
        private void LoadData(string discusId, string fromDate, string toDate, int memberID, string instanceID)
        {
             fromDate = cldFromDate.Value + " 00:00:00";
             toDate = cldToDate.Value + " 23:59:59";
            try
            {
                int PrjeMinut = 0;
                int AllAddedPrjeMinut = 0;
                int AllSubPrjeMinut = 0;

                DataTable dt = CCMDiscussion.GetAllMemberDiscussions(memberID, instanceID);
                DataRow[] dr = dt.Select("DiscussionID = " + discusId);
                if (dr != null)
                {
                    string discussionID = dr[0]["DiscussionID"].ToString();
                    bool HideHours = dr[0]["HideHours"] == DBNull.Value ? false : dr[0]["HideHours"].ToBoolean();
                    int roleid = CCMDiscussion.GetDiscussionMemberRoleID(discussionID.ToInt32(), memberID.ToInt32());

                    if (HideHours == false || (HideHours == true && (roleid != 5 || roleid != 5)))

                    {
                        if (dr != null)
                        {
                            lblProjectName.InnerText = "Detailed Report: " + dr[0]["Title"].ToString();
                        }
                        if (md == null)
                            md = new App_Code.MemberDiscussion();
                        // IList<MemberDiscussions> mj = md.GetAllDiscussionViaDate(discusId.ToInt32(), fromDate, toDate);


                        var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
         new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
                        var tableClient = acc.CreateCloudTableClient();
                        var table = tableClient.GetTableReference("MemberDiscussions");
                        TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
                        TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionID),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)));
                        IEnumerable<MemberDiscussions1> mj = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate)).OrderBy(x => x.CrtDate) ;
                       
                        //Total Added Minutes 
                        var totaladded = table.ExecuteQuery(rangeQuery).Where(a => a.CrtDate >= Convert.ToDateTime(fromDate) && a.CrtDate <= Convert.ToDateTime(toDate) && (a.SubtractedHours == false || string.IsNullOrEmpty(Convert.ToString(a.SubtractedHours))));
                        AllAddedPrjeMinut = totaladded.Sum(a => a.Minutes);
                        
                        string str = "<table id='dataH' border='2' cellpadding='4' cellspacing='4' width='98%' class='table table-hover table-bordered table-striped'><tr><th>Day</th><th>Date</th><th colspan='2' align='center'>Total Time</th><th>Work Summary</th><th>By</th><th></th></tr>";
                        str += "<th></th><th></th><th>In Minutes</th><th>In Hours</th><th></th><th></th><th></th>";
                                                

                        int i=0;
                        foreach (MemberDiscussions1 entity in mj)
                        {
                            int totalminutes =entity.Minutes;
                            if (totalminutes > 0)
                            {                              
                                if (entity.SubtractedHours)
                                {    
                                    str += "<tr id='tr" + i + "_" + discusId + "'><td>" + entity.CrtDate.ToString("ddd") + "</td>";
                                    str += "<td>" + entity.CrtDate.ToString("MMMM d, yyyy") + "</td>";
                                    str += "<td class='subtract-text'> -" + totalminutes + "</td>";

                                    str += "<td class='subtract-text'> -" + Math.Round((Convert.ToDecimal(totalminutes) / 60), 4).ToString() + "</td>";
                                    string con = "";
                                    if (entity.SubtractHrsReason == null)
                                    {
                                        entity.SubtractHrsReason = "";
                                    }

                                    string conwithoutHtml = Remove_Html_Tags(entity.SubtractHrsReason.ToString());

                                    con = conwithoutHtml.ToString().Trim();
                                    if (conwithoutHtml == "")
                                    {
                                        conwithoutHtml = Remove_Html_Tags(entity.Conversation.ToString());
                                        if (conwithoutHtml.Length == 0)
                                        {
                                            con = "";
                                        }
                                        else if (conwithoutHtml.Length > 100)
                                        {
                                            con = conwithoutHtml.Substring(0, 100).ToString().Trim();
                                        }
                                        else
                                        {
                                            con = conwithoutHtml.Substring(0, conwithoutHtml.Length).ToString().Trim();
                                        }
                                    }
                                    if (conwithoutHtml.Length == 0)
                                    {
                                        con = "";
                                    }

                                    str += "<td class='subtract-text'>" + con + "</td>";
                                    string name = GetName(entity.MemberID.ToInt32(), discusId.ToInt32(), instanceID);
                                    str += "<td>" + name + "</td>";
                                    str += "<td><a id='" + i + "_" + discusId + "' class='delete' data-index='" + i + "_" + discusId + "' href='#'>Remove</a></td>";
                                    str += "</tr>";
                                    AllSubPrjeMinut += totalminutes;
                                }
                                else {

                                    //string hour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
                                    // Int16 minutes = Convert.ToInt16(totalminutes % 60);
                                    str += "<tr id='tr" + i + "_" + discusId + "'><td>" + entity.CrtDate.ToString("ddd") + "</td>";
                                    str += "<td>" + entity.CrtDate.ToString("MMMM d, yyyy") + "</td>";
                                    str += "<td>" + totalminutes + "</td>";
                                    // str += "<td>" + hour + " Hours " + minutes.ToString() + " Minutes </td>";

                                    str += "<td>" + Math.Round((Convert.ToDecimal(totalminutes) / 60), 4).ToString() + "</td>";
                                    string con = "";
                                    if (entity.Summary == null)
                                    {
                                        entity.Summary = "";
                                    }



                                    string conwithoutHtml = Remove_Html_Tags(entity.Summary.ToString());

                                    con = conwithoutHtml.ToString().Trim();
                                    if (conwithoutHtml == "")
                                    {
                                        conwithoutHtml = Remove_Html_Tags(entity.Conversation.ToString());
                                        if (conwithoutHtml.Length == 0)
                                        {
                                            con = "";
                                        }
                                        else if (conwithoutHtml.Length > 100)
                                        {
                                            con = conwithoutHtml.Substring(0, 100).ToString().Trim();
                                        }
                                        else
                                        {
                                            con = conwithoutHtml.Substring(0, conwithoutHtml.Length).ToString().Trim();
                                        }
                                    }
                                    if (conwithoutHtml.Length == 0)
                                    {
                                        con = "";
                                    }



                                    str += "<td>" + con + "</td>";
                                    string name = GetName(entity.MemberID.ToInt32(), discusId.ToInt32(), instanceID);
                                    str += "<td>" + name + "</td>";
                                    str += "<td><a id='" + i + "_" + discusId + "' class='delete' data-index='" + i + "_" + discusId + "' href='#'>Remove</a></td>";
                                    str += "</tr>";
                                    PrjeMinut += totalminutes;
                                }
                               
                            }
                            i++;
                        }

                        string projecthour = "";
                        Int16 projectminutes = 0;
                        if (AllSubPrjeMinut > 0)
                        {
                            PrjeMinut = PrjeMinut - AllSubPrjeMinut;

                            projecthour = (Convert.ToDecimal(PrjeMinut) / 60).ToString().Split('.')[0].ToString();
                            projectminutes = Convert.ToInt16(PrjeMinut % 60);
                            string addedprojecthour = (Convert.ToDecimal(AllAddedPrjeMinut) / 60).ToString().Split('.')[0].ToString();
                            Int16 addedprojectminutes = Convert.ToInt16(AllAddedPrjeMinut % 60);
                            string subprojecthour = (Convert.ToDecimal(AllSubPrjeMinut) / 60).ToString().Split('.')[0].ToString();
                            Int16 subprojectminutes = Convert.ToInt16(AllSubPrjeMinut % 60);

                            lblTotalHours.Text = "<b>Time Added= </b>" + addedprojecthour + " Hours " + addedprojectminutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + subprojecthour + " Hours " + subprojectminutes.ToString() + " Minutes  <br/> <b>Net Time=</b>" + projecthour + " Hours " + projectminutes.ToString() + " Minutes";
                            lblTotalHours1.Text = "<b>Time Added= </b>" + addedprojecthour + " Hours " + addedprojectminutes.ToString() + " Minutes <br/> <b>Time Subtracted= </b>" + subprojecthour + " Hours " + subprojectminutes.ToString() + " Minutes  <br/> <b>Net Time=</b>" + projecthour + " Hours " + projectminutes.ToString() + " Minutes";

                        }
                        else {
                            projecthour = (Convert.ToDecimal(PrjeMinut) / 60).ToString().Split('.')[0].ToString();
                            projectminutes = Convert.ToInt16(PrjeMinut % 60);

                            lblTotalHours.Text = "<b>Total Hours Spent= </b>" + projecthour + " Hours " + projectminutes.ToString() + " Minutes";
                            lblTotalHours1.Text = "<b>Total Hours Spent= </b>" + projecthour + " Hours " + projectminutes.ToString() + " Minutes";
                        }


                        str += "</table>";
                        list.InnerHtml = str;
                        hdnAllProjectMinutes.Value = PrjeMinut.ToString();
                        hdnAllProjectAddMin.Value = AllAddedPrjeMinut.ToString();
                        hdnAllProjectSubMin.Value = AllSubPrjeMinut.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.ToString();
            }
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            int memberID = Request.QueryString["MemberId"].Decrypt().ToInt32();
            string instanceID = Request.QueryString["IntanceID"].ToString();
            string discussionID = Request.QueryString["disID"].Decrypt().ToString();
            string fromDate = cldFromDate.Value;
            string toDate = cldToDate.Value;
            LoadData(discussionID, fromDate, toDate, memberID, instanceID);
        }
    }
}