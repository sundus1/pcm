﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="CCM.Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Settings</title>
    <script src="<%= "_assests/jQuit/js/jquery-1.10.2.js?" + DateTime.Now.ToBinary() %>"></script>
    <script src="<%= "_assests/jQuit/js/jquery-ui.js?" + DateTime.Now.ToBinary() %>"></script>
    <link href="_assests/jQuit/css/jquery-ui.css" rel="stylesheet" />
    <script src="<%= "_assests/_otf.js?" + DateTime.Now.ToBinary().ToString() %>"></script>
    <link href="<%= "_assests/Styles/my_style.css?" + DateTime.Now.ToBinary().ToString() %>" rel="stylesheet" />
    <link href="_assests/Tooltipster/css/tooltipster.css" rel="stylesheet" />
    <script src="_assests/Tooltipster/js/jquery.tooltipster.js"></script>
    <link href="_assests/Tooltipster/css/themes/tooltipster-light.css" rel="stylesheet" />
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
    <script>
        $(window).load(function () {
            try {
                resizeIframe();
                resizeIframe();
                resizeIframe();
                resizeIframe();
                resizeIframe();
                resizeIframe();
                resizeIframe();
            } catch (e) {

            }
            
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="space">
            <div class="Tab">
                <div>
                    <h2>Filters</h2>
                    <table>
                        <tr class="smallGrid">
                            <td colspan="2">Keywords, characters and symbols that cannot be used in messages. All new messages will inherit these filters
                            </td>
                        </tr>
                        <tr class="smallGrid">
                            <td>
                                <asp:TextBox ID="txtContents" CssClass="txtContents" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="smallGrid">
                            <td>
                                <asp:Button ID="btnSaveDAContents" CssClass="btnSaveDAContents" runat="server" OnClick="btnSaveDAContents_Click" Text="Save" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="Tab">
                <table class="tblForm">
                    <tr class="smallGrid">
                        <td class="formCaptionTd">Name: 
                        </td>
                        <td>
                            <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="smallGrid">
                        <td class="formCaptionTd">Alias:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMAlias" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="smallGrid">
                        <td class="formCaptionTd">Title:
                        </td>
                        <td>
                            <asp:TextBox ID="txtMTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveMemberInfo" CssClass="btnSaveMemberInfo" runat="server" Text="Update" OnClick="btnSaveMemberInfo_Click" />
                        </td>
                        <td style="padding: 5px">
                            <asp:Label ID="lblInfo" runat="server" Text="" EnableViewState="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:HiddenField ID="hdnMemberID" ClientIDMode="Static" runat="server" Value="0" />
    </form>
</body>
</html>
