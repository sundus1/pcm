﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using MyCCM;
using SubSonic.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
//using CCM.App_Code.Extension;

namespace CCM.App_Code
{
    public class CCM_Helper
    {
        public static DataTable GetLatestDiscussion(string MemberEmail)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetLatestDiscussion(MemberEmail);

            return sp.ExecuteDataSet().Tables[0];
        }
        public static Project GetRootParentProject(Project project)
        {
            if (project != null)
            {
                if (project.Parent_ProjectID != 0)
                {
                    Project tempProject = Project.SingleOrDefault(u => u.ProjectID == project.Parent_ProjectID);
                    if (tempProject != null)
                    {
                        return GetRootParentProject(tempProject);
                    }
                    else
                    {
                        return project;
                    }
                }
                else
                {
                    return project;
                }
            }
            else { return project; }
        }

        // Get Role of member under current discussion
        /// <summary>
        /// Get Role of member under current discussion. 
        /// Prefer Role by discussion first
        /// , Role by Project second
        /// and then Role by RootProject
        /// </summary>
        /// <param name="MemberID"></param>
        /// <param name="DiscussionID"></param>
        /// <param name="ProjectID"></param>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
        //public static Role GetRole(Int32 MemberID, Int32 DiscussionID, Int32 ProjectID, String InstanceID)
        //{
        //    Role role = new Role();
        //    MemberToDiscussion mtd = MemberToDiscussion.SingleOrDefault(u => u.MemberID == MemberID && u.DiscussionID == DiscussionID);
        //    if (mtd != null) { role = Role.SingleOrDefault(u => u.RoleID == mtd.RoleID); }
        //    else
        //    {
        //        MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == MemberID && u.ProjectID == ProjectID);
        //        if (mtp != null) { role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID); }
        //        else
        //        {
        //            Project proj = Project.SingleOrDefault(u => u.ProjectID == ProjectID);
        //            Project rootParentProject = GetRootParentProject(proj);
        //            mtp = MemberToProject.SingleOrDefault(u => u.MemberID == MemberID && u.ProjectID == rootParentProject.ProjectID);
        //            if (mtp != null) { role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID); }
        //        }
        //    }
        //    if (role.RoleID == 0)
        //    {
        //        Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(MemberID));
        //        if (member != null)
        //        {
        //            Project proj = Project.SingleOrDefault(u => u.OwnerID == member.OwnerID && u.ProjectID == Convert.ToInt32(ProjectID) && u.InstanceID == InstanceID);
        //            if (proj != null)
        //            {
        //                role = Role.SingleOrDefault(u => u.RoleID == 1);
        //            }
        //        }
        //    }
        //    return role;
        //}

        /// <summary>
        /// Get Role of member under current discussion only.        
        /// </summary>
        /// <param name="MemberID"></param>
        /// <param name="DiscussionID"></param>
        /// <param name="ProjectID"></param>
        /// <param name="InstanceID"></param>
        /// <returns></returns>
        public static Role GetRole(Int32 MemberID, Int32 DiscussionID, int FolderID, String InstanceID)
        {
            Role role = new Role();
            DiscussionToFolder mtd = DiscussionToFolder.SingleOrDefault(u => u.MemberID == MemberID && u.DiscussionID == DiscussionID);
            if (mtd != null) { role = Role.SingleOrDefault(u => u.RoleID == mtd.RoleID); }
            if (role.RoleID == 0)
            {
                Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(MemberID));
                if (member != null)
                {
                    MemberToFolder folder = MemberToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.FolderID == Convert.ToInt32(FolderID) && u.InstanceID == InstanceID);
                    if (folder != null)
                    {
                        role = Role.SingleOrDefault(u => u.RoleID == 1);
                    }
                    else
                    {
                        role = Role.SingleOrDefault(u => u.RoleID == 1);
                    }
                }
            }
            return role;
        }

        public static List<OwnerToMember> GetMembers(string instanceID, MemberRoles memberRole, int OwnerMemberID)
        {
            List<OwnerToMember> itsMembers = OwnerToMember.Find(u => u.InstanceID == instanceID && (u.RoleID == memberRole.ToInt32() || memberRole.ToInt32() == 0) && u.OwnerMemberID == OwnerMemberID && u.Active == true).ToList();
            List<OwnerToMember> itsOwners = OwnerToMember.Find(u => u.MemberID == OwnerMemberID).ToList();
            foreach (OwnerToMember member in itsOwners)
            {
                Member imember = Member.SingleOrDefault(u => u.MemberID == member.OwnerMemberID);
                if (imember != null)
                {
                    itsMembers.Add(new OwnerToMember()
                    {
                        Active = true,
                        CrtDate = DateTime.Now,
                        Email = imember.Email,
                        InstanceID = instanceID,
                        MemberID = imember.MemberID,
                        OwnerMemberID = OwnerMemberID,
                        RoleID = member.RoleID,
                        ModDate = DateTime.Now,
                        NameAlias = imember.NickName,
                        OTMID = 0
                    });
                }

            }

            if (itsMembers.Count > 0)
            {
                itsMembers.Insert(0, new OwnerToMember() { OTMID = 0, MemberID = 0, NameAlias = "Select" });
            }
            else
            {
                itsMembers.Insert(0, new OwnerToMember() { OTMID = 0, MemberID = 0, NameAlias = "No Records Found" });
            }

            return itsMembers;
        }

        /// <summary>
        /// Delete blob file of discussions
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="discussionID"></param>
        public static void DeleteFile(string filename, string discussionID)
        {
            string datacontainer = "data";

            //storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);
            string storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);

            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                CloudStorageAccount storageAccount = new CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);

                CloudBlockBlob blob = container.GetBlockBlobReference(String.Format("{0}{1}", storagepath, filename));
                blob.DeleteIfExists();
            }
            catch (Exception ex)
            {

            }
        }

        //public static void DeleteDiscussionBlobFolder(string discussionID)
        //{
        //    string datacontainer = "data";

        //    //storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);
        //    string storagepath = String.Format("{0}apps/{1}/Discussion-{2}", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);

        //    try
        //    {
        //        StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
        //        CloudStorageAccount storageAccount = new CloudStorageAccount(objkey, false);
        //        CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
        //        CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);
        //        CloudBlockBlob blob = container.GetBlockBlobReference(storagepath);
        //        blob.DeleteIfExists();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        public static void DeleteSubProjectsOf(int projectID)
        {
            Project parentProject = Project.SingleOrDefault(u => u.ProjectID == projectID);
            if (parentProject != null)
            {
                parentProject.Delete();
                List<Project> projects = Project.Find(u => u.Parent_ProjectID == projectID).ToList();
                foreach (Project proj in projects)
                {
                    DeleteSubProjectsOf(proj.ProjectID);
                }
            }
        }

        public static string DeleteFolderAndItsSubFolders(int folderID, int memberID)
        {
            string infoString = "";
            MemberToFolder memberFolder = MemberToFolder.SingleOrDefault(u => u.FolderID == folderID && u.MemberID == memberID && u.RoleID == 1);
            Folder folder = null;
            if (memberFolder != null)
            {
                folder = Folder.SingleOrDefault(u => u.FolderID == folderID);
            }
            if (folder != null)
            {
                // Delete all discussions
                Info info = DeleteFolderData(folder.FolderID, memberID);
                infoString += "\n" + info.Description + " \n" + info.Extra;
                List<MemberToFolder> memberToFolders = MemberToFolder.Find(u => u.ParentFolderID == folderID).ToList();
                foreach (MemberToFolder item in memberToFolders)
                {
                    infoString += "\n" + DeleteFolderAndItsSubFolders(item.FolderID.ToInt32(), memberID);
                }
            }
            return infoString;
        }

        /// <summary>
        /// Delete Discussion, its files, comments and blob data
        /// </summary>
        /// <param name="disID"></param>
        /// <returns></returns>
        public static Info DeleteDiscussionData(Int32 disID, int MemberID)
        {
            // Check if member is Team Admin or Client Admin
            //DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == disID && u.MemberID == MemberID && (u.RoleID == MemberRoles.Admin.ToInt32() || u.RoleID == MemberRoles.ClientAdmin.ToInt32() || u.RoleID == MemberRoles.SuperAdmin.ToInt32()));
            //if (disToFolder != null) {
            //    return new Info("Error", "Not allowed! You have no sufficient privilages", disID.ToString());
            //}
            //DiscussionToFolder mtd = DiscussionToFolder.SingleOrDefault(u => u.MemberID == MemberID && u.DiscussionID == disID && u.RoleID == 1);
            MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(disID, MemberID);
            if (mr == MemberRoles.ClientMember || mr == MemberRoles.VendorMember || mr == MemberRoles.ClientAdmin)
            {
                return new Info("Error", "No Credentials for this operation!", disID.ToString());
            }

            MyCCM.Discussion discussion = new MyCCM.Discussion();
            discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == disID);
            if (discussion == null)
            {
                return new Info("Error", "No discussion found!", disID.ToString());
            }

            // Delete MemberDiscussion
            MemberDiscussion md = new MemberDiscussion();
            md.DeleteAllByDiscussionID(disID.ToString());

            // Delete DiscussionFiles
            List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.DiscussionID == disID).ToList();
            foreach (DiscussionFile file in disFiles)
            {
                // Delet Blob Files
                DeleteFile(file.FileName, disID.ToString());
                file.Delete();
            }

            // Delete DiscussionToFolder
            List<DiscussionToFolder> discussionToFolders = DiscussionToFolder.Find(u => u.DiscussionID == disID).ToList();
            foreach (DiscussionToFolder discussionToFolder in discussionToFolders)
            {
                discussionToFolder.Delete();
            }

            discussion.Delete();

            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            db.DeleteDiscussionCompletely(discussion.DiscussionID);


            return new Info("Info", " Discussion " + discussion.Title + " deleted Successfully", "");
        }

        /// <summary>
        /// Delete Folder and its data including discussions under it and all data related to discussions
        /// </summary>
        /// <param name="folderID"></param>
        /// <returns></returns>
        public static Info DeleteFolderData(Int32 folderID, int memberID)
        {
            Info info = new Info("", "", "");
            int countDis = 0;
            Folder folder = Folder.SingleOrDefault(u => u.FolderID == folderID);
            if (folder != null)
            {

                // Delete MemberToFolder with deleted folder reference
                List<MemberToFolder> memberToFolders = MemberToFolder.Find(u => u.FolderID == folderID).ToList();
                foreach (MemberToFolder memberToFolder in memberToFolders)
                {
                    memberToFolder.Delete();
                }


                // Fetch assigned folders and move them to root
                List<MemberToFolder> mtf = MemberToFolder.Find(u => (u.RoleID == MemberRoles.ClientMember.ToInt32() || u.RoleID == MemberRoles.VendorMember.ToInt32()) && u.ParentFolderID == folderID && u.MemberID == memberID).ToList();
                foreach (MemberToFolder item in mtf)
                {
                    item.ParentFolderID = 0;
                    item.Update();
                }

                // Fetch assigned discussions and move them to root
                List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => (u.RoleID == MemberRoles.ClientMember.ToInt32() || u.RoleID == MemberRoles.VendorMember.ToInt32()) && u.FolderID == folderID && u.MemberID == memberID).ToList();
                foreach (DiscussionToFolder item in dtfs)
                {
                    item.FolderID = 0;
                    item.Update();
                }
                List<DiscussionToFolder> disFolder = DiscussionToFolder.Find(u => u.FolderID == folderID).ToList();
                //List<MyCCM.Discussion> discussions = MyCCM.Discussion.Find(u => u.FolderID == folder.FolderID).ToList();
                foreach (DiscussionToFolder item in disFolder)
                {
                    Info disInfo = DeleteDiscussionData(item.DiscussionID.ToInt32(), memberID);
                    countDis++;
                }
                folder.Delete();

                info.Title = "Info";
                info.Description = "Folder Deleted!";
                info.Extra += countDis.ToString() + " discussions deleted!";
                return info;
            }
            else
            {
                info = new Info("Error", "No folder found!", "");
                return info;
            }
        }

        public static List<Project> GetProjectsByOwner(string ownerID, string Extra)
        {
            List<Project> projects = Project.Find(u => u.OwnerID == ownerID).OrderBy(u => u.Title).ToList();
            if (!String.IsNullOrEmpty(Extra)) { projects.Insert(0, new Project() { ProjectID = 0, Title = Extra }); }
            return projects;
        }
        public static List<Project> GetProjectsByInstance(string InstanceID, string Extra)
        {
            List<Project> projects = Project.Find(u => u.InstanceID == InstanceID).OrderBy(u => u.Title).ToList();
            if (!String.IsNullOrEmpty(Extra)) { projects.Insert(0, new Project() { ProjectID = 0, Title = Extra }); }
            return projects;
        }

        public static string GetOwnerID()
        {
            AddinstanceWS objinst = new AddinstanceWS();
            String ownerid = "";
            try
            {
                ownerid = objinst.GetUserID(HttpContext.Current.User.Identity.Name);
                if (ownerid == "")
                {
                    ownerid = "teamuser-1069--8587846274950237891"; // sbmuhammadfaizan Owner SuperAdmin
                    //ownerid = "58686386-e67f-4d79-8b35-3028471025ec"; // gmail
                    //_OwnerId = "c827ef9d-fafd-4b48-b737-27bbee6ec54f"; // b3ast667
                    //_OwnerId = "d0670956-4d4c-448c-9e2e-370c295d7b5d"; // faizan outlook
                }
            }
            catch (Exception)
            {
                ownerid = "teamuser-1069--8587846274950237891"; // sbmuhammadfaizan Owner SuperAdmin                
            }

            return ownerid;
        }

        public static string GetMemberRoleString(string memberRole)
        {
            switch (memberRole)
            {
                case "SuperAdmin":
                    return "Super admin";
                case "Admin":
                    return "Admin";
                case "VendorMember":
                    return "Team Member";
                case "ClientMember":
                    return "Client";
                case "ClientAdmin":
                    return "Client Admin";
                default:
                    return "User";
            }
        }
        public static int GetMemberRoleID(string memberRole)
        {
            switch (memberRole)
            {
                case "SuperAdmin":
                    return 1;
                case "Admin":
                    return 2;
                case "VendorMember":
                    return 3;
                case "ClientMember":
                    return 4;
                case "ClientAdmin":
                    return 5;
                case "ProjectManager":
                    return 6;
                case "AsstProjectManager":
                    return 7;
                default:
                    return 8;

            }
        }
        public static String GetMemberRoleName(Int32 roleID)
        {
            switch (roleID)
            {
                case 1:
                    return "SuperAdmin";
                case 2:
                    return "Admin";
                case 3:
                    return "VendorMember";
                case 4:
                    return "ClientMember";
                case 5:
                    return "ClientAdmin";
                case 6:
                    return "ProjectManager";
                case 7:
                    return "AsstProjectManager";
                default:
                    return "NA";

            }
        }
        public static String GetMemberRoleSimplifiedName(Int32 roleID)
        {
            switch (roleID)
            {
                case 1:
                    return "Super Admin";
                case 2:
                    return "Team Admin";
                case 3:
                    return "Team Member";
                case 4:
                    return "Client Member";
                case 5:
                    return "Client Admin";
                case 6:
                    return "Project Manager";
                case 7:
                    return "Assistant Project Manager";
                default:
                    return "NA";

            }
        }
        public static String GetMemberRoleNewSimplifiedName(Int32 roleID)
        {
            switch (roleID)
            {
                case 1:
                    return "Super Admin";
                case 2:
                    return "an administrator";
                case 3:
                    return "a team member";
                case 4:
                    return "a client";
                case 5:
                    return "a client";
                default:
                    return "NA";

            }
        }

        public static String GetDiscussionURL(MyCCM.Discussion discussion, String folderID, String ownerID, String MemberID)
        {
            String url = "http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + folderID.Encrypt() + "&od=" + ownerID.Encrypt() + "&md=" + MemberID.Encrypt() + "&instanceid=" + discussion.InstanceID;
            return url;
        }

        public static String GetDiscussionURL(int discussionID, int MemberID)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == MemberID);
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussionID && u.MemberID == MemberID);
            String url = "http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/Discussion.aspx?did=" + discussionID.ToString().Encrypt() + "&fid=" + dtf.FolderID.ToString().Encrypt() + "&od=" + member.OwnerID.Encrypt() + "&md=" + MemberID.ToString().Encrypt() + "&instanceid=" + dtf.InstanceID;
            return url;
        }

        public static String DefaultDateFormat
        {
            get
            {
                return "ddd, MMM dd yyyy";
            }
        }
        /// <summary>
        /// This function shrink the filename and append "..." with it
        /// </summary>
        /// <param name="a_string">String to shrink</param>
        /// <param name="characters"></param>
        /// <returns></returns>
        public static String GetDotDotDot(String a_string, int characters)
        {
            //string FileName = "This on.e has some bog file name.png";            
            string firstLetters = "";
            Console.WriteLine(a_string);
            if (a_string.Length > characters)
            {
                firstLetters = a_string.Substring(0, characters); return firstLetters + "...";
            }
            else
            { return a_string; }
        }

        public static OwnerToMember GetMemberOwner(Int32 MemberID)
        {
            OwnerToMember owner = OwnerToMember.SingleOrDefault(a=> a.MemberID == MemberID);

            return owner;
        }

        public static Member GetMemberDetails(Int32 MemberID)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == MemberID);
            return member;
        }

        public static DataTable GetMemberDiscussionsList(Int32 MemberID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetMemberDiscussionsList(MemberID);
            return sp.ExecuteDataSet().Tables[0];
        }

    }
    public class CCMFolder
    {
        public static DataTable GetFolderMembersByRole(string InstanceID, int FolderID, int RoleID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetFolderMembersByRole(InstanceID, FolderID, RoleID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("ParentFolderID", ParentFolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }

        public static DataTable GetFolders(string InstanceID, int MemberID, int ParentFolderID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetFolders(InstanceID, MemberID, ParentFolderID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("ParentFolderID", ParentFolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }

        public static DataTable GetChildFoldersOf(int FolderID, int MemberID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetChildFoldersOf(FolderID, MemberID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("ParentFolderID", ParentFolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }

        public static MemberRoles GetFolderMemberRole(Int32 FolderID, Int32 MemberID)
        {
            MemberToFolder mtf = MemberToFolder.SingleOrDefault(u => u.FolderID == FolderID && u.MemberID == MemberID);
            if (mtf != null)
            {
                return (MemberRoles)mtf.RoleID;
            }
            else
            {
                return MemberRoles.ClientMember;
            }
        }
    }
    public class CCMDiscussion
    {
        public static MyCCM.Discussion GetDiscussionData(Int32 DisID)
        {
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == DisID);
            return dis;
        }
        public static DataTable GetDiscussions(string InstanceID, int MemberID, int FolderID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetDiscussions(InstanceID, MemberID, FolderID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("FolderID", FolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }
            

        public static DataTable GetAllMemberDiscussions(int MemberID, string InstanceID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetAllMemberDiscussions(MemberID, InstanceID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("FolderID", FolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }

        public static DataTable GetReportDiscussions(int MemberID, string InstanceID)
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetReportDiscussions(MemberID, InstanceID);
            //sp.Command.AddParameter("InstanceID", InstanceID, DbType.String);
            //sp.Command.AddParameter("MemberID", MemberID, DbType.Int32);
            //sp.Command.AddParameter("FolderID", FolderID, DbType.Int32);
            return sp.ExecuteDataSet().Tables[0];
        }

        public static DataTable GetAllMemberDiscussions()
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetAllDiscussions();
            return sp.ExecuteDataSet().Tables[0];
        }

        public static MemberRoles GetDiscussionMemberRole(Int32 DiscussionID, Int32 MemberID)
        {
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == DiscussionID && u.MemberID == MemberID);
            if (dtf != null)
            {
                return (MemberRoles)dtf.RoleID;
            }
            else
            {
                return MemberRoles.ClientMember;
            }
        }

        public static int GetDiscussionMemberRoleID(Int32 DiscussionID, Int32 MemberID)
        {
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == DiscussionID && u.MemberID == MemberID);
            if (dtf != null)
            {
                return dtf.RoleID.ToInt32();
            }
            else
            {
                return MemberRoles.ClientMember.ToInt32();
            }
        }

        public static DataTable GetDiscussionMemberRoles(Int32 DiscussionID)
        {
            DataTable dt = new DataTable();
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "MemberRoles";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "RoleID";
            dt.Columns.Add(column);

            IList<DiscussionToFolder> dtf = DiscussionToFolder.Find(u => u.DiscussionID == DiscussionID);
            if (dtf.Count > 0)
            {
                for (int i = 0; i < dtf.Count; i++)
                {
                    row = dt.NewRow();
                    row["RoleID"] = dtf[i].RoleID;
                    row["MemberRoles"] = dtf[i].MemberID;
                    dt.Rows.Add(row);
                }
            }
            return dt;

        }
        public static DataTable GetDiscussionMemberAlias(Int32 DiscussionID, string instanceID)
        {

            DataTable dt = new DataTable();
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "MemberID";
            dt.Columns.Add(column);
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Alias";
            dt.Columns.Add(column);

            IList<DiscussionToFolder> dtf = DiscussionToFolder.Find(u => u.DiscussionID == DiscussionID && u.InstanceID == instanceID);

            if (dtf.Count > 0)
            {
                for (int i = 0; i < dtf.Count; i++)
                {
                    row = dt.NewRow();
                    row["MemberID"] = dtf[i].MemberID;
                    row["Alias"] = dtf[i].Alias;
                    dt.Rows.Add(row);
                }
            }
            return dt;

        }

        public static int GetSentEmails()
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.GetSentEmails();
            DataTable dt = sp.ExecuteDataSet().Tables[0];
            return dt.Rows[0][0].ToInt32();
        }
        public static void DeleteOldEmails()
        {
            MyCCM.avaimaTest0001DB db = new avaimaTest0001DB();
            StoredProcedure sp = db.DeleteOldEmail();
            sp.Execute();

        }
    }
    public class Info
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Extra { get; set; }

        public Info(string title, string description, string extra)
        {
            this.Title = title;
            this.Description = description;
            this.Extra = extra;
        }
    }



}