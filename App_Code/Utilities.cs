﻿using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CCM.App_Code
{
    public class Utilities
    {
        public static readonly String AppID = "4a99e861-7e44-4937-a342-11fbd66f5d47";
        public static String AvaimaEmailSignature
        {
            get
            {
                StringBuilder body = new StringBuilder();
                body.Append("<br><br><a href='http://avaima.com'>AVAIMA.COM</a>");
                body.Append("<br><i>Organize your work!</i>");
                body.Append("<br>For questions email <a href='mailto:support@avaima.com'>support@avaima.com</a>");
                body.Append("<br>For information contact: <a href='mailto:info@avaima.com'>info@avaima.com</a>");
                //body.Append("<br>For support contact: <a href='mailto:support@avaima.com '>support@avaima.com </i>");
                return body.ToString();
            }
        }
        public static String GetGoogleFormatedDate(String Date)
        {
            return Convert.ToDateTime(Date).ToString("ddd, MMM dd, yyyy");
        }
        public static String GetUserDateTime(string DateTime, string ownerID)
        {
            AvaimaTimeZoneAPI tAPI = new AvaimaTimeZoneAPI();
            return tAPI.GetDate(DateTime, ownerID) + " at " + tAPI.GetTime(DateTime, ownerID);
        }
        public static String GetUserDate(string DateTime, string ownerID)
        {
            AvaimaTimeZoneAPI tAPI = new AvaimaTimeZoneAPI();
            return tAPI.GetDate(DateTime, ownerID);
        }
        public static String GetUserTime(string DateTime, string ownerID)
        {
            AvaimaTimeZoneAPI tAPI = new AvaimaTimeZoneAPI();
            return tAPI.GetTime(DateTime, ownerID);
        }
        public static String GetUserDateWithoutToday(string dateTime, string ownerID)
        {
            AvaimaTimeZoneAPI tAPI = new AvaimaTimeZoneAPI();
            string date = tAPI.GetDate(dateTime, ownerID);
            if (date.Contains("Today"))
            {
                date = DateTime.Now.ToShortDateString();
            }
            return date;
        }
    }


    public enum MemberRoles
    {
        All = 0,
        SuperAdmin = 1,
        Admin = 2,
        VendorMember = 3,
        ClientMember = 4,
        ClientAdmin = 5,
        ProjectManager = 6,
        AsstProjectManager = 7,
    }

    public class ProjectWithAuth
    {
        public Project project { get; set; }
        public String RoleTitle { get; set; }
    }


}