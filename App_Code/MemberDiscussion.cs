﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System.Configuration;
using MyCCM;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM.App_Code
{
    public class MemberDiscussion
    {
        MemberDiscussionsContext context;
        public MemberDiscussions MemberDis;
        public List<MemberDiscussions> MemberDiscussions;

        public void NewMemberDiscussion()
        {
            MemberDis = new MemberDiscussions();
        }
        public void NewMemberDiscussions()
        {
            MemberDiscussions = new List<MemberDiscussions>();
        }
        public MemberDiscussionsContext GetMemberDiscussionContext()
        {
            StorageCredentialsAccountAndKey creds = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
            CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(creds, false);
            return new MemberDiscussionsContext(cloudStorageAccount.TableEndpoint.ToString(), cloudStorageAccount.Credentials);
        }

        public int GetTotalMinutesByDiscussion(int disscusionId)
        {
            int TotalDiscussionMinutes = 0;
            Dictionary<DiscussionToFolder, List<MemberDiscussions>> returnDictionary = new Dictionary<DiscussionToFolder, List<App_Code.MemberDiscussions>>();
            context = GetMemberDiscussionContext();
            /* int totalm = (context.GetMemberDiscussions.GroupBy(a => a.DiscussionID = disscusionId.ToString()).Select
                 (c=> new context.GetMemberDiscussions {});

             */
            IList<MemberDiscussions> mdb = context.GetMemberDiscussions.
             Where(a => a.DiscussionID == disscusionId.ToString()).ToList();
            for (int i = 0; i < mdb.Count; i++)
            {
                TotalDiscussionMinutes += mdb[i].Minutes;
            }
            return TotalDiscussionMinutes;
        }
        public IList<MemberDiscussions> GetAllDiscussionViaDate(int discussionID, string dateFrom, string dateTo)
        {
            string[] strdateTo = dateTo.Split('/');
            dateTo = strdateTo[0] + "/" + strdateTo[1] + "/" + strdateTo[2];
            dateTo = dateTo + " 23:59:59";

            string[] strdateFrom = dateFrom.Split('/');
            dateFrom = strdateFrom[0] + "/" + strdateFrom[1] + "/" + strdateFrom[2];
            dateFrom = dateFrom + " 00:00:00";
            Dictionary<DiscussionToFolder, List<MemberDiscussions>> returnDictionary = new Dictionary<DiscussionToFolder, List<App_Code.MemberDiscussions>>();
            context = GetMemberDiscussionContext();
            IList<MemberDiscussions> mdb = context.GetMemberDiscussions.
            Where(a => a.DiscussionID == discussionID.ToString() && a.CrtDate >= Convert.ToDateTime(dateFrom) &&
                a.CrtDate <= Convert.ToDateTime(dateTo)).ToList().OrderBy(a => a.CrtDate).ToList();
            return mdb;
        }
        public int GetTotalMinutesByDiscussion(int disscusionId, string dateFrom, string dateTo)
        {
            int TotalDiscussionMinutes = 0;
            Dictionary<DiscussionToFolder, List<MemberDiscussions>> returnDictionary = new Dictionary<DiscussionToFolder, List<App_Code.MemberDiscussions>>();
            context = GetMemberDiscussionContext();
            /* context.GetMemberDiscussions.Aggregate(a=> a.DiscussionID=disscusionId.ToString().Sum(a=>a.Minutes));
             int totalm = (context.GetMemberDiscussions.GroupBy(a => a.DiscussionID = disscusionId.ToString()).Select
                  (c=> context.GetMemberDiscussions {});
             */
            dateTo = dateTo + " 23:59:59";
            dateFrom = dateFrom + " 00:00:00";
            IList<MemberDiscussions> mdb = context.GetMemberDiscussions.
             Where(a => a.DiscussionID == disscusionId.ToString() && a.CrtDate >= Convert.ToDateTime(dateFrom) && a.CrtDate <= Convert.ToDateTime(dateTo)).ToList();
            for (int i = 0; i < mdb.Count; i++)
            {
                TotalDiscussionMinutes += mdb[i].Minutes;
            }
            return TotalDiscussionMinutes;
        }
        public DiscussionToFolder GetMemberDiscussionsByDiscussion(String DiscussionID, int MemberID)
        {
            Dictionary<DiscussionToFolder, List<MemberDiscussions>> returnDictionary = new Dictionary<DiscussionToFolder, List<App_Code.MemberDiscussions>>();
            context = GetMemberDiscussionContext();
            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == DiscussionID.ToInt32() && u.MemberID == MemberID);
            int? unreadPosts = 0;
            if (disToFolder != null)
            {
                unreadPosts = disToFolder.Unread;
                //disToFolder.Unread = 0;
                //disToFolder.Update();
            }
            if (unreadPosts == 0 || unreadPosts == null)
            { unreadPosts = 3; }
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().OrderByDescending(u => u.CrtDate).ToList().Take(unreadPosts.ToInt32()).ToList();
            return disToFolder;
        }
        public void GetMoreMemberDiscussionsByDiscussion(String DiscussionID, int allTop)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().OrderBy(u => u.CrtDate).Take(allTop).ToList().OrderBy(u => u.CrtDate).ToList();
        }

        /// <summary>
        /// Get unique tags from discussion commentss
        /// </summary>
        /// <param name="DiscussionID"></param>
        /// <returns></returns>
        public string GetTags(string DiscussionID)
        {
            context = GetMemberDiscussionContext();
            List<String> strCommentTags = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID && a.Deleted != true).ToList().Select(u => u.Tags).ToList();
            List<String> tags = new List<string>();
            foreach (string item in strCommentTags)
            {
                if (!String.IsNullOrEmpty(item))
                { tags.AddRange(item.Split(',')); }
            }
            return string.Join(",", tags.Distinct().ToArray());
        }

        public List<MemberNote> GetNotes(string DiscussionID)
        {
            context = GetMemberDiscussionContext();
             List<MemberNote> Notes = MemberNote.Find(u => u.DiscussionID == DiscussionID.ToInt32() && u.Active == true).ToList().OrderBy(u => u.CrtDate).ToList();
                        
             return Notes;
        }
       
        public void GetMoreMemberDiscussionsByDiscussionCount(String DiscussionID, int countTop)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().OrderByDescending(u => u.CrtDate).ToList().GetRange(3, 5).ToList().OrderBy(u => u.CrtDate).ToList();
        }

        public void GetMoreMemberDiscussionsFromIndex(String DiscussionID, int index)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().OrderByDescending(u => u.CrtDate).ToList().GetRange(index, 5).ToList().OrderBy(u => u.CrtDate).ToList();
        }
        public void GetMoreMemberDiscussionsByTag(String DiscussionID, string tag)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().Where(a => (a.Tags == null ? false : a.Tags.Contains(tag))).ToList().OrderBy(u => u.CrtDate).ToList();
        }
        public MemberDiscussions GetMemberDiscussionByID(String MemberDisID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDis = context.GetMemberDiscussions.Where(a => a.RowKey == MemberDisID).SingleOrDefault();
            return this.MemberDis;
        }

        //public List<MemberDiscussions> GetAllNullDelete()
        //{
        //    context = GetMemberDiscussionContext();
        //    this.MemberDiscussions = context.GetMemberDiscussions.Where(u => !u.Conversation.Contains("")).ToList();
        //    return this.MemberDiscussions;
        //}

        public Int32 GetMemberDiscussionCountByDiscussionID(String DiscussionID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList();
            return this.MemberDiscussions.Count;
        }

        public Int32 GetMemberActiveDiscussionCountByDiscussionID(String DiscussionID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList().Where(a => (a.Deleted == false) ? false : !a.Deleted).ToList();
            return this.MemberDiscussions.Count;
        }

        public Int32 GetMemberNotDeletedDiscussionCountByDiscussionID(String DiscussionID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID && a.Deleted != true).ToList();
            return this.MemberDiscussions.Count;
        }

        public List<MemberDiscussions> GetMemberDiscussionsByID(String MemberDisID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.RowKey == MemberDisID).ToList();
            return this.MemberDiscussions;
        }

        public void Add()
        {
            context = GetMemberDiscussionContext();

            context.AddMemberDiscussions(this.MemberDis);
        }

        public void Update()
        {
            this.context.UpdateMemberDiscussions(this.MemberDis);
        }

        public void Delete(String RowKey)
        {
            context = GetMemberDiscussionContext();
            this.MemberDis = context.GetMemberDiscussions.Where(a => a.RowKey == RowKey).SingleOrDefault();
            if (this.MemberDis != null)
            {
                context.DeleteMemberDiscussions(this.MemberDis);
            }
        }

        public void DeleteMD(String RowKey, int deletedByMemberID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDis = context.GetMemberDiscussions.Where(a => a.RowKey == RowKey).SingleOrDefault();
            if (this.MemberDis != null)
            {
                //context.DeleteMemberDiscussions(this.MemberDis);  
                this.MemberDis.Deleted = true;
                //this.MemberDis.ModDate = DateTime.Now;
                this.MemberDis.DeletedByMemberID = deletedByMemberID;
                this.Update();
            }
        }

        public void DeleteAllByDiscussionID(String DiscussionID)
        {
            context = GetMemberDiscussionContext();
            this.MemberDiscussions = context.GetMemberDiscussions.Where(a => a.DiscussionID == DiscussionID).ToList();
            foreach (MemberDiscussions item in this.MemberDiscussions)
            {
                if (this.MemberDis != null)
                {
                    context.DeleteMemberDiscussions(this.MemberDis);
                }
            }

        }
    }
    public class MemberDiscussions1 : TableEntity
    {
        // InstanceID

        // MemberDiscussionID

        public string DiscussionID { get; set; }
        public string MemberID { get; set; }
        public string TeamMember { get; set; }
        public string Conversation { get; set; }
        public DateTime CrtDate { get; set; }
        public DateTime ModDate { get; set; }
        public int RoleID { get; set; }
        public string Active { get; set; }
        public Boolean Deleted { get; set; }
        public Int32 DeletedByMemberID { get; set; }
        public String Tags { get; set; }
        public Int32 Minutes { get; set; }
        public string Summary { get; set; }
        public String Notes { get; set; }
        public Boolean SubtractedHours { get; set; }
        public string SubtractHrsReason { get; set; }
    }
    public class MemberDiscussions : TableServiceEntity
    {
        // InstanceID
        public string PartitionKey { get; set; }
        // MemberDiscussionID
        public string RowKey { get; set; }
        public string DiscussionID { get; set; }
        public string MemberID { get; set; }
        public string TeamMember { get; set; }
        public string Conversation { get; set; }
        public DateTime CrtDate { get; set; }
        public DateTime ModDate { get; set; }
        public int RoleID { get; set; }
        public string Active { get; set; }
        public Boolean Deleted { get; set; }
        public Int32 DeletedByMemberID { get; set; }
        public String Tags { get; set; }
        public Int32 Minutes { get; set; }
        public string Summary { get; set; }
        public String Notes { get; set; }
        public Boolean SubtractedHours { get; set; }
        public string SubtractHrsReason { get; set; }

    }
    public class MemberDiscussionsContext : TableServiceContext
    {
        public MemberDiscussionsContext(string baseAddress, StorageCredentials credentials)
            : base(baseAddress, credentials)
        {

        }

        public const string TableName = "MemberDiscussions";

        public IQueryable<MemberDiscussions> GetMemberDiscussions
        {
            get
            {
                return this.CreateQuery<MemberDiscussions>(TableName);
            }
        }

        public void AddMemberDiscussions(MemberDiscussions Discussions)
        {
            try
            {
                Guid guid = new Guid();
                guid = Guid.NewGuid();
                Discussions.RowKey = guid.ToString();
                this.AddObject(TableName, Discussions);
                this.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMemberDiscussions(MemberDiscussions memberDiscussions)
        {
            try
            {
                this.UpdateObject(memberDiscussions);
                this.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteMemberDiscussions(MemberDiscussions memberDiscussions)
        {
            try
            {
                this.DeleteObject(memberDiscussions);
                this.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}