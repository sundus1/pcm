﻿using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CCM.App_Code
{

    public class MemberPreview
    {
        public Int32 MemberID { get; set; }
        public String OwnerID { get; set; }
        public String InstanceID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String NickName { get; set; }
        public String Email { get; set; }
        public String ImagePath { get; set; }
        public DateTime? CrtDate { get; set; }
        public DateTime? ModDate { get; set; }
        public Boolean? Active { get; set; }
        //public Int32 ApplicableTo { get; set; }
        public Int32 RoleID { get; set; }
        public String Alias { get; set; }
        public String Title { get; set; }

        public MemberPreview(Member member, Int32 RoleID, String Alias, String Title)
        {
            this.MemberID = member.MemberID;
            this.OwnerID = member.OwnerID;
            this.InstanceID = member.InstanceID;
            this.FirstName = member.FirstName;
            this.LastName = member.LastName;
            this.NickName = member.NickName;
            this.Email = member.Email;
            this.ImagePath = member.ImagePath;
            this.CrtDate = member.CrtDate;
            this.ModDate = member.ModDate;
            this.Active = member.Active;
            this.RoleID = RoleID;
            this.Alias = Alias;
            this.Title = Title;
        }
    }
}