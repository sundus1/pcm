﻿using AvaimaThirdpartyTool;
using MyCCM;
using CCM.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CCM
{
    public partial class Settings : AvaimaWebPage
    {
        String _OwnerId = "";
        public String _LocalInstanceID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.InstanceID == "0") { _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab"; }
            else { _LocalInstanceID = this.InstanceID; }
            if (!IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            _OwnerId = CCM_Helper.GetOwnerID();
            //_OwnerId = "teamuser-1069--8587846274950237891"; // sbmuhammadfaizan Owner SuperAdmin
            //_OwnerId = "c827ef9d-fafd-4b48-b737-27bbee6ec54f"; // b3ast667
            //_OwnerId = "d0670956-4d4c-448c-9e2e-370c295d7b5d"; // faizan outlook
            Member _ActiveMember = Member.SingleOrDefault(u => u.OwnerID == _OwnerId && u.InstanceID == _LocalInstanceID);
            hdnMemberID.Value = _ActiveMember.MemberID.ToString();
            if (_ActiveMember != null)
            {
                txtMName.Text = _ActiveMember.FirstName;
                txtMTitle.Text = _ActiveMember.Title;
                txtMAlias.Text = _ActiveMember.NickName;
            }

            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == _LocalInstanceID && u.DiscussionID == 0 && u.FolderID == 0);
            if (dc != null)
            {
                txtContents.Text = dc.Contents;
            }
            else
            {
                txtContents.Text = "";
            }
        }

        protected void btnSaveDAContents_Click(object sender, EventArgs e)
        {
            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == 0 && u.FolderID == 0);
            if (dc != null)
            {
                dc.Contents = txtContents.Text;
                dc.ModDate = DateTime.Now;
                dc.Update();
            }
            else
            {
                dc = new DisallowedContent()
                {
                    ModDate = DateTime.Now,
                    CrtDate = DateTime.Now,
                    Contents = txtContents.Text,
                    FolderID = 0,
                    DiscussionID = 0,
                    InstanceID = this.InstanceID,
                    Active = true,
                };
                dc.Add();
            }
        }

        protected void btnSaveMemberInfo_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID);
            if (member != null)
            {
                member.FirstName = txtMName.Text;
                member.NickName = txtMAlias.Text;
                member.Title = txtMTitle.Text;
                member.Update();
                BindData();
                lblInfo.Text = "Profile info has been updated!";
                lblInfo.ForeColor = System.Drawing.Color.Green;
            }
        }
    }
}