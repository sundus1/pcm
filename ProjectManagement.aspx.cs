﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using MyCCM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
//using CCM.App_Code.Extension;

namespace CCM
{
    public partial class ProjectManagement : AvaimaWebPage
    {
        String testInstanceID;

        private void BindProject()
        {
            ddlProject.DataSource = CCM_Helper.GetProjectsByInstance(this.InstanceID, "New Project");
            ddlProject.DataTextField = "Title";
            ddlProject.DataValueField = "ProjectID";
            ddlProject.DataBind();
            ddlProject.SelectedIndex = 0;
        }

        private void BindData()
        {
            if (ddlProject.SelectedValue == "0")
            {
                // 
                //hdnProjectIsNew.Value = "True";
                ToggleMember(false);
                txtTitle.Text = "";
                txtDescription.Text = "";
                //hdnProjectID.Value = "0";
                btnSave.Text = "Add";
                h1.InnerText = "Create Project";
                txtTitle.Focus();
            }
            else
            {
                //hdnProjectIsNew.Value = "False";
                ToggleMember(true);
                // Fetch and fill project
                Project project = Project.SingleOrDefault(u => u.ProjectID == ddlProject.SelectedValue.ToInt32());
                if (project != null)
                {
                    txtTitle.Text = project.Title;
                    txtDescription.Text = project.Description;
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(project.Status));
                    chkActive.Checked = Convert.ToBoolean(project.Active);
                    btnSave.Text = "Update";
                    h1.InnerText = "Edit Project";
                    BindTeam();
                    BindClients();
                }
                //GenerateBreadCrumb();
            }
            chkAddNew.Checked = true;
            BindMembers(SelMemberRole);
            ClearMemberFields();
        }

        public MemberRoles SelMemberRole
        {
            get
            {
                if (rdoClient.Checked)
                {
                    return MemberRoles.ClientMember;
                }
                else
                {
                    return MemberRoles.VendorMember;
                }

            }
        }

        private void BindMembers(MemberRoles memberRole)
        {
            //ddlMembers.DataSource = CCM_Helper.GetMembers(testInstanceID, memberRole);
            //ddlMembers.DataTextField = "NameAlias";
            //ddlMembers.DataValueField = "MemberID";
            //ddlMembers.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testInstanceID = this.InstanceID;
            String _OwnerID = CCM_Helper.GetOwnerID();
            hdnOID.Value = _OwnerID;
            Member member = Member.SingleOrDefault(u => u.OwnerID == _OwnerID & u.InstanceID == this.InstanceID);
            if (member != null)
            { hdnMemberID.Value = member.MemberID.ToString(); }

            hdninstanceid.Value = testInstanceID;
            if (!IsPostBack)
            {
                BindProject();
                ddlProject_SelectedIndexChanged(ddlProject, e);
            }
            lblExInfo.Text = lblInfo.Text = "";

            HideBars();
            //hdnMemberID.Value = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value).MemberID.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == this.InstanceID);
            if (ddlProject.SelectedValue.ToInt32() == 0)
            {
                Project project = new Project();
                project.Title = txtTitle.Text;
                project.Description = txtDescription.Text;
                project.Status = ddlStatus.Text;
                project.Active = chkActive.Checked;
                project.CrtDate = DateTime.Now;
                project.ModDate = DateTime.Now;
                project.OwnerID = hdnOID.Value;
                project.Parent_ProjectID = 0;
                project.InstanceID = testInstanceID;
                project.Add();
                //hdnProjectID.Value = project.ProjectID.ToString();
                MemberToProject mtp = new MemberToProject()
                {
                    RoleID = 1,
                    ProjectID = project.ProjectID,
                    Active = true,
                    ApplicableTo = 0,
                    CrtDate = DateTime.Now,
                    InstanceID = testInstanceID,
                    MemberID = member.MemberID,
                    ModDate = DateTime.Now,
                };
                mtp.Add();
                ShowInfo("Project created successfully.");
                BindProject();
                ddlProject.SelectedIndex = ddlProject.Items.IndexOf(ddlProject.Items.FindByValue(project.ProjectID.ToString()));
                BindData();
                txtTeamFullName.Focus();
                //this.Redirect("ProjectManagement.aspx?pin=false&pid=" + hdnProjectID.Value + "&ppid=" + hdnParenProjectID.Value + "&oID=" + hdnOID.Value);
            }
            else
            {
                Project project = Project.SingleOrDefault(u => u.ProjectID == ddlProject.SelectedValue.ToInt32());
                project.Title = txtTitle.Text;
                project.Description = txtDescription.Text;
                project.Status = ddlStatus.Text;
                project.Active = chkActive.Checked;
                project.ModDate = DateTime.Now;
                project.Update();
                ShowInfo("Project updated successfully.");
                ToggleMember(true);
                BindProject();
                ddlProject.SelectedIndex = ddlProject.Items.IndexOf(ddlProject.Items.FindByValue(project.ProjectID.ToString()));
            }
        }

        [WebMethod]
        public static string SaveData(string isNew, string projectid, string title, string description, string status, string active, string ownerid, string ppid, string instanceid)
        {
            Member member = Member.SingleOrDefault(u => u.OwnerID == ownerid && u.InstanceID == instanceid);
            if (Convert.ToBoolean(isNew))
            {
                Project project = new Project();
                project.Title = title;
                project.Description = description;
                project.Status = status;
                project.Active = Convert.ToBoolean(active);
                project.CrtDate = DateTime.Now;
                project.ModDate = DateTime.Now;
                project.OwnerID = ownerid;
                project.Parent_ProjectID = Convert.ToInt32(ppid);
                project.InstanceID = instanceid;
                project.Add();

                MemberToProject mtp = new MemberToProject()
                {
                    RoleID = 1,
                    ProjectID = project.ProjectID,
                    Active = true,
                    ApplicableTo = 0,
                    CrtDate = DateTime.Now,
                    InstanceID = instanceid,
                    MemberID = member.MemberID,
                    ModDate = DateTime.Now,
                };
                mtp.Add();
                return "Added successfully:" + project.ProjectID;
            }
            else
            {
                Project project = Project.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(projectid));
                project.Title = title;
                project.Description = description;
                project.Status = status;
                project.Active = Convert.ToBoolean(active);
                project.ModDate = DateTime.Now;
                project.Parent_ProjectID = Convert.ToInt32(ppid);
                project.Update();
                return "Updated successfully:" + project.ProjectID;
            }
        }

        [WebMethod]
        public static string AddMember(string projectid, string instanceid, string firstname, string lastname, string email, string type, string ownerid)
        {
            String testInstanceID = instanceid;
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == ownerid && u.InstanceID == testInstanceID);
            Project project = Project.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(projectid));
            if (Convert.ToInt32(projectid) > 0)
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(email) && u.InstanceID == testInstanceID);
                if (member == null)
                {
                    AddinstanceWS objaddinst = new AddinstanceWS();
                    string userid = objaddinst.AddInstance(email, Utilities.AppID, testInstanceID, HttpContext.Current.User.Identity.Name);
                    AvaimaUserProfile objwebser = new AvaimaUserProfile();
                    member = new Member()
                    {
                        OwnerID = userid,
                        //ProjectID = 0,
                        InstanceID = testInstanceID,
                        FirstName = firstname,
                        LastName = "",
                        NickName = firstname,
                        Email = email,
                        //Type = MemberRoles.SuperAdmin.ToString(),
                        CrtDate = DateTime.Now,
                        ModDate = DateTime.Now,
                        Active = true,
                        ImagePath = "_assests/images/" + type + ".png"
                    };
                    member.Add();
                    MemberToProject mtp = new MemberToProject()
                    {
                        ProjectID = Convert.ToInt32(projectid),
                        MemberID = member.MemberID,
                        Active = true,
                        CrtDate = DateTime.Now,
                        ModDate = DateTime.Now,
                        InstanceID = testInstanceID
                    };
                    if (type == "ClientMember")
                    { mtp.RoleID = 4; }
                    else if (type == "VendorMember")
                    { mtp.RoleID = 3; }
                    else if (type == "Admin")
                    { mtp.RoleID = 2; }
                    else if (type == "SuperAdmin")
                    { mtp.RoleID = 1; }
                    mtp.Add();
                    SendInvitationDefault(currMember, member, project, type);
                    return "Info:Member Created and assigned to new project:" + member.MemberID;
                }
                else
                {
                    // Check if member is in the same project by MemberToProject
                    //MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == member.MemberID && u.RoleID == 1 && u.InstanceID == testInstanceID);
                    //if (mtp != null)
                    //{
                    //    return "Error:SuperAdmin can not be added as member of project as he is already a member";
                    //}
                    MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == member.MemberID && u.ProjectID == Convert.ToInt32(projectid));
                    if (mtp == null)
                    {
                        // Assign member to project
                        mtp = new MemberToProject()
                        {
                            ProjectID = Convert.ToInt32(projectid),
                            MemberID = member.MemberID,
                            Active = true,
                            CrtDate = DateTime.Now,
                            ModDate = DateTime.Now,
                            InstanceID = testInstanceID,
                        };
                        // Assign its role                        
                        if (type == "ClientMember")
                            mtp.RoleID = 4;
                        else if (type == "VendorMember")
                            mtp.RoleID = 3;
                        else if (type == "Admin")
                            mtp.RoleID = 2;
                        else if (type == "SuperAdmin")
                            mtp.RoleID = 1;
                        mtp.Add();
                        SendInvitationDefault(currMember, member, project, type);
                        return "Info:Member assigned to new project:" + member.MemberID;
                    }
                    else
                    {
                        // Check if member is activated
                        if (Convert.ToBoolean(mtp.Active))
                        {
                            return "Error:Member already exists as " + Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle;
                        }
                        else
                        {
                            if (type == Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle)
                            {
                                mtp.Active = true;
                                mtp.Update();
                                return "Info:MTP Activated:" + member.MemberID;
                            }
                            else
                            {
                                mtp.RoleID = Role.SingleOrDefault(u => u.RoleTitle == type).RoleID;
                                mtp.Active = true;
                                mtp.Update();
                                return "Info:MTP Activated:" + member.MemberID;
                            }
                        }
                        // Nothing to do member already exists
                    }
                }
            }
            else
            {
                return "Error:No Project";
            }
        }

        private static void SendInvitationDefault(Member currMember, Member member, Project project, string memberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            String body = "Hi " + member.NickName + " " + ", ";
            body += "<br /><br />" + currMember.NickName + "  has added you as a " + memberRole + " in proect " + project.Title + ".";
            body += "<br /><br />";
            body += "CCM is a software application on Avaima which lets you add one or more discussions under one or more projects. Each project has team members and clients/customers. A page for discussion allows all parties to communicate with each other while maintaining privacy of everyone.";
            body += "<br /><br />This is a notification email and does not require any action.";
            //body += "<br /><br />You are invited to Team Communication Application by " + currMember.NickName + " to participate as " + memberRole + " in discussions under project: <b>" + project.Title + ". ";
            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited project's discussions.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "Aviama: CCM Inivitation " + DateTime.Now.ToString("ddd, MM yy");
            emailAPI.send_email(member.Email, currMember.FirstName + " " + currMember.LastName, "noreply@avaima.com", body, subject);
        }

        [WebMethod]
        public static string RemoveMember(string memberid, string projectid, string curroid)
        {
            // Remove member from project
            MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid) && u.ProjectID == Convert.ToInt32(projectid));
            Project project = Project.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(projectid));
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == curroid);

            if (mtp != null)
            {
                mtp.Delete();
                SendMemberRemoveNotification(currMember, member, project, Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle);
                return "Inform:Member Removed Successfully";
            }
            else
            {
                return "Error:No Member Found";
            }
        }

        /// <summary>
        /// To Send Notification email on member role removal
        /// </summary>
        /// <param name="discussionid"></param>
        /// <param name="currMember"></param>
        /// <param name="member"></param>
        /// <param name="project"></param>
        /// <param name="memberRole"></param>
        /// <param name="category">true for MTP, false for MTD</param>
        private static void SendMemberRemoveNotification(Member currMember, Member member, Project project, string memberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            String body = "Hi " + member.FirstName + " " + member.LastName + ", ";

            // MTP
            body += "<br /><br />This is to inform you that your role as " + memberRole + " has been removed from Project: <b>" + project.Title + ". You will not be able to participate in this project. For more information contact your administrator.";

            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited discussion.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "Aviama: CCM Inivitation " + DateTime.Now.ToString("ddd, MM yy");
            emailAPI.send_email(member.Email, currMember.FirstName + " " + currMember.LastName, "noreply@avaima.com", body, subject);
        }

        [WebMethod]
        public static string GetFirstLastName(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(email));
                if (member != null)
                {
                    var obj = JsonConvert.DeserializeObject("{'firstname':'" + member.FirstName + "','lastname':'" + member.LastName + "'}");
                    return JsonConvert.SerializeObject(obj);
                    //return "{'firstname':'" + member.FirstName + "','lastname':'" + member.LastName + "'}";
                }
                else
                {
                    return "Error:No member found";
                }
            }
            else
            {
                return "Error:invalid Email";
            }
        }

        //private void GenerateBreadCrumb()
        //{
        //    List<Project> _projects = Project.All().ToList();
        //    int projID = Convert.ToInt32(ParentProjectID);
        //    List<Project> projects = new List<Project>();
        //    GetBCList(_projects, projID, projects);
        //    //if (projects.Count > 0)
        //    //{
        //    //    projects.RemoveAt(projects.Count - 1);    
        //    //}

        //    //rptBC.DataSource = projects.OrderBy(u => u.ProjectID).ToList();
        //    //rptBC.DataBind();
        //}

        //private static void GetBCList(List<Project> _projects, int projID, List<Project> projects)
        //{
        //    Project proj = _projects.SingleOrDefault(u => u.ProjectID == projID);
        //    if (proj != null)
        //    {
        //        projects.Add(proj);
        //        GetBCList(_projects, Convert.ToInt32(proj.Parent_ProjectID), projects);
        //    }
        //}

        protected void rptBC_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Navigate")
            {
                LinkButton lnkButton = e.Item.FindControl("lnkBC") as LinkButton;
                this.Redirect("Default.aspx?pid=" + e.CommandArgument.ToString());
            }
        }

        protected void btnAddTeamMember_Click(object sender, EventArgs e)
        {

            string response;
            if (!rdoClient.Checked)
            { response = AddMember("VendorMember"); }
            else
            { response = AddMember("ClientMember"); }
            if (response.Split(':')[0].Contains("Info"))
            {
                //ShowInfo(resposen.Split(':')[1]); 
                lblExInfo.Text = lblInfo.Text = response.Split(':')[1];
                lblExInfo.ForeColor = lblInfo.ForeColor = Color.Green;
                ClearMemberFields();
            }
            else if (response.Split(':')[0].Contains("Error"))
            {
                //ShowError(resposen.Split(':')[1]); 
                lblExInfo.Text = lblInfo.Text = response.Split(':')[1];
                lblExInfo.ForeColor = lblInfo.ForeColor = Color.Red;
            }
            BindMembers(SelMemberRole);
            BindTeam();
            BindClients();
            DisplayNewFields();
        }

        private void BindTeam()
        {
            List<MemberToProject> memberToProjects = MemberToProject.Find(u => u.ProjectID == Convert.ToInt32(ddlProject.SelectedValue) && u.RoleID == 3 && u.Active == true).ToList();
            List<Member> members = new List<Member>();
            foreach (MemberToProject item in memberToProjects)
            {
                Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMem != null)
                {
                    members.Add(tempMem);
                }
            }
            if (members.Count > 0)
            {
                rptTeamMember.DataSource = members;
                rptTeamMember.DataBind();
                trM7.Visible = true;
            }
            else
            {
                trM7.Visible = false;
            }
        }

        private void BindClients()
        {
            // Get MTP Active Client Member 
            List<MemberToProject> memberToProjects = MemberToProject.Find(u => u.ProjectID == Convert.ToInt32(ddlProject.SelectedValue) && u.RoleID == 4 && u.Active == true).ToList();
            List<Member> members = new List<Member>();
            foreach (MemberToProject item in memberToProjects)
            {
                Member tempMem = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMem != null)
                {
                    members.Add(tempMem);
                }
            }

            if (members.Count > 0)
            {
                rptClientMembers.DataSource = members;
                rptClientMembers.DataBind();
                trM8.Visible = true;
            }
            else
            {
                trM8.Visible = false;
            }
        }

        private string AddMember(string type)
        {
            int projectid = ddlProject.SelectedValue.ToInt32();
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == this.InstanceID);
            Project project = Project.SingleOrDefault(u => u.ProjectID == projectid);
            if (projectid > 0)
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(txtTeamEmail.Text) && u.InstanceID == this.InstanceID);
                if (member == null)
                {
                    AddinstanceWS objaddinst = new AddinstanceWS();
                    string userid = objaddinst.AddInstance(txtTeamEmail.Text, Utilities.AppID, testInstanceID, HttpContext.Current.User.Identity.Name);
                    AvaimaUserProfile objwebser = new AvaimaUserProfile();
                    member = CreateMemberWithMTP(type, projectid, currMember, project, member, userid);
                    ClearMemberFields();
                    return "Info:Member Created and assigned to new project:" + member.MemberID;
                }
                else
                {
                    if (member.InstanceID != testInstanceID)
                    {
                        member = CreateMemberWithMTP(type, projectid, currMember, project, member, member.OwnerID);
                        ClearMemberFields();
                        return "Info:Member created and assigned to new project:" + member.MemberID;
                    }
                    else
                    {
                        member.FirstName = txtTeamFullName.Text;
                        member.NickName = txtTeamAlias.Text;
                        member.Update();
                        MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == member.MemberID && u.ProjectID == Convert.ToInt32(projectid));
                        if (mtp == null)
                        {
                            // Assign member to project
                            mtp = new MemberToProject()
                            {
                                ProjectID = Convert.ToInt32(projectid),
                                MemberID = member.MemberID,
                                Active = true,
                                CrtDate = DateTime.Now,
                                ModDate = DateTime.Now,
                                InstanceID = testInstanceID,
                                ApplicableTo = 2
                            };
                            // Assign its role                        
                            if (type == "ClientMember")
                                mtp.RoleID = 4;
                            else if (type == "VendorMember")
                                mtp.RoleID = 3;
                            else if (type == "Admin")
                                mtp.RoleID = 2;
                            else if (type == "SuperAdmin")
                                mtp.RoleID = 1;
                            mtp.Add();
                            SendInvitationDefault(currMember, member, project, type);
                            AddOwnerToMember(currMember, member, mtp.RoleID.ToInt32());
                            ClearMemberFields();
                            return "Info:Member assigned to new project:" + member.MemberID;
                        }
                        else
                        {
                            if (type == "ClientMember")
                                mtp.RoleID = 4;
                            else if (type == "VendorMember")
                                mtp.RoleID = 3;
                            else if (type == "Admin")
                                mtp.RoleID = 2;
                            else if (type == "SuperAdmin")
                                mtp.RoleID = 1;
                            mtp.Update();
                            // Check if member is activated
                            if (Convert.ToBoolean(mtp.Active))
                            {
                                return "Info:Member Updated!:" + Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle;
                            }
                            else
                            {
                                if (type == Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle)
                                {
                                    mtp.Active = true;
                                    mtp.Update();
                                    AddOwnerToMember(currMember, member, mtp.RoleID.ToInt32());
                                    ClearMemberFields();
                                    return "Info:MTP Activated:" + member.MemberID;
                                }
                                else
                                {
                                    mtp.RoleID = Role.SingleOrDefault(u => u.RoleTitle == type).RoleID;
                                    mtp.Active = true;
                                    mtp.Update();
                                    AddOwnerToMember(currMember, member, mtp.RoleID.ToInt32());
                                    ClearMemberFields();
                                    return "Info:MTP Activated:" + member.MemberID;
                                }
                            }
                            // Nothing to do member already exists
                        }
                    }
                }
            }
            else
            {
                return "Error:No Project";
            }
        }

        private Member CreateMemberWithMTP(string type, int projectid, Member currMember, Project project, Member member, string userid)
        {
            if (member == null)
            {
                member = new Member()
                {
                    OwnerID = userid,
                    //ProjectID = 0,
                    InstanceID = testInstanceID,
                    FirstName = txtTeamFullName.Text,
                    LastName = "",
                    NickName = txtTeamAlias.Text,
                    Email = txtTeamEmail.Text,
                    //Type = MemberRoles.SuperAdmin.ToString(),
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    ImagePath = "_assests/images/" + type + ".png"
                };
                member.Add();
            }

            MemberToProject mtp = new MemberToProject()
            {
                ProjectID = Convert.ToInt32(projectid),
                MemberID = member.MemberID,
                Active = true,
                CrtDate = DateTime.Now,
                ModDate = DateTime.Now,
                InstanceID = testInstanceID,
                ApplicableTo = 2
            };
            if (type == "ClientMember")
            { mtp.RoleID = 4; }
            else if (type == "VendorMember")
            { mtp.RoleID = 3; }
            else if (type == "Admin")
            { mtp.RoleID = 2; }
            else if (type == "SuperAdmin")
            { mtp.RoleID = 1; }
            mtp.Add();
            SendInvitationDefault(currMember, member, project, type);
            AddOwnerToMember(currMember, member, mtp.RoleID.ToInt32());
            return member;
        }

        private void AddOwnerToMember(Member currMember, Member member, Int32 RoleID)
        {
            OwnerToMember otm = OwnerToMember.SingleOrDefault(u => u.Email.Contains(txtTeamEmail.Text) && u.InstanceID == this.InstanceID);
            if (otm == null)
            {
                otm = new OwnerToMember()
                {
                    OwnerMemberID = currMember.MemberID,
                    InstanceID = testInstanceID,
                    Email = txtTeamEmail.Text,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    MemberID = member.MemberID,
                    NameAlias = member.FirstName,
                    RoleID = RoleID
                };
                otm.Add();
            }
        }

        private void ToggleMember(bool bit)
        {
            trM10.Visible = trM9.Visible = trM8.Visible = trM1.Visible = trM2.Visible = trM3.Visible = trM4.Visible = trM5.Visible = trM7.Visible = bit;
        }

        private void ShowInfo(string text)
        {
            divInfo.InnerText = text;
            divInfo.Style.Remove("display");
        }
        private void ShowError(string text)
        {
            divError.InnerText = text;
            divError.Style.Remove("display");
        }
        private void ShowWarning(string text)
        {
            divWarning.InnerText = text;
            divWarning.Style.Remove("display");
        }
        private void ShowSuccess(string text)
        {
            divSuccess.InnerText = text;
            divSuccess.Style.Remove("display");
        }
        private void HideBars()
        {
            divInfo.Style.Add("display", "none");
            divError.Style.Add("display", "none");
            divWarning.Style.Add("display", "none");
            divSuccess.Style.Add("display", "none");
        }
        private void ClearMemberFields()
        {
            txtTeamAlias.Text = "";
            txtTeamEmail.Text = "";
            txtTeamFullName.Text = "";
        }

        protected void rptClientMembers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                int memberid = Convert.ToInt32(e.CommandArgument.ToString());
                string resposen = RemoveMember(memberid);
                if (resposen.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
                    lblExInfo.ForeColor = lblInfo.ForeColor = Color.Green;
                }
                else if (resposen.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
                    lblExInfo.ForeColor = lblInfo.ForeColor = Color.Red;
                }
                BindClients();
                
            }
        }

        private string RemoveMember(int memberid)
        {
            // Remove member from project
            MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid) && u.ProjectID == ddlProject.SelectedValue.ToInt32());
            Project project = Project.SingleOrDefault(u => u.ProjectID == ddlProject.SelectedValue.ToInt32());
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value);

            if (mtp != null)
            {
                mtp.Delete();
                SendMemberRemoveNotification(currMember, member, project, Role.SingleOrDefault(u => u.RoleID == mtp.RoleID).RoleTitle);
                return "Inform:Member Removed Successfully";
            }
            else
            {
                return "Error:No Member Found";
            }
        }

        protected void rptTeamMember_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                int memberid = Convert.ToInt32(e.CommandArgument.ToString());
                string resposen = RemoveMember(memberid);
                if (resposen.Split(':')[0].Contains("Info"))
                {
                    //ShowInfo(resposen.Split(':')[1]); 
                    lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
                    lblExInfo.ForeColor = lblInfo.ForeColor = Color.Green;
                }
                else if (resposen.Split(':')[0].Contains("Error"))
                {
                    //ShowError(resposen.Split(':')[1]); 
                    lblExInfo.Text = lblInfo.Text = resposen.Split(':')[1];
                    lblExInfo.ForeColor = lblInfo.ForeColor = Color.Red;
                }
                BindTeam();
            }
        }

        protected void btnAddEx_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == ddlMembers.SelectedValue.ToInt32());
            if (member != null)
            {
                txtTeamFullName.Text = member.FirstName;
                txtTeamEmail.Text = member.Email;
                txtTeamAlias.Text = member.NickName;
                hdnUserMemberID.Value = "0";
                btnAddMember.Text = "Add Member";
                txtTeamAlias.Focus();
                btnAddTeamMember_Click(sender, e);
                SetDisplayNone();
            }
            else
            {
                lblExInfo.Text = "No member found";
                lblExInfo.ForeColor = Color.Red;
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            chkAddNew.Checked = true;
        }

        protected void lnkNewProject_Click(object sender, EventArgs e)
        {
            ddlProject.SelectedIndex = 0;
            BindData();
            txtTitle.Focus();
        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            this.Redirect("Default.aspx");
        }

        protected void rdoClient_CheckedChanged(object sender, EventArgs e)
        {
            BindMembers(SelMemberRole);
            ClearMemberFields();
            chkAddNew.Checked = true;
        }

        private void SetDisplayNone()
        {
            trM1.Style.Add("display", "none");
            trM2.Style.Add("display", "none");
            trM3.Style.Add("display", "none");
            trM5.Style.Add("display", "none");
            trEx.Style.Remove("display");
        }

        private void DisplayNewFields()
        {
            trM1.Style.Remove("display");
            trM2.Style.Remove("display");
            trM3.Style.Remove("display");
            trM5.Style.Remove("display");
            //trEx.Style.Add("display","none");
        }
    }
}