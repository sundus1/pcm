﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageReminders.aspx.cs" Inherits="CCM.ManageReminders" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title id="title" runat="server">Manage Project Reminders</title>
    <meta name="Description" content="Team Communication Application - CCM" />
    <link rel="Shortcut Icon" type="image/x-icon" href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/images/favicon.ico" />
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link href="<%= "_discussionsa_assets/css/bootstrap.css" %>" rel="stylesheet" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="<%= "_discussionsa_assets/js/bootstrap.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/font-awesome.css" %>" rel="stylesheet" />
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="<%= "_discussionsa_assets/css/style.css"  %>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="<%="_discussionsa_assets/js/manageReminder.js" %>"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <%-- jQuery Select2 --%>
    <script src="<%= "_discussionsa_assets/multiselection/select2.min.js" %>"></script>
    <link href="<%= "_discussionsa_assets/multiselection/select2.min.css" %>" rel="stylesheet" />


    <link href="<%= "_assests/nProgress/nprogress.css?" + DateTime.Now.ToBinary() %>" rel="stylesheet" />
    <script src="<%= "_assests/nProgress/nprogress.js?" + DateTime.Now.ToBinary() %>"></script>

    <link href="_discussionsa_assets/css/managereminder.css" rel="stylesheet" />


    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
</head>
<body>
    <form id="form1" runat="server">

        <div runat="server" id="managereminder">
            <div id="widget">
                <div>
                    <div id="bar">
                        <div class="col-md-12 chat-heading">
                            <h1 id="h1" runat="server" style="display: inline-block;">Project Reminder
                            </h1>

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9 ChatCont" style="padding-left: 50px; width: 100%">
                            <table width="100%" cellpadding="5" cellspacing="5" class="dataTable">
                                <tr>
                                    <td style="width: 10%;">
                                        <asp:Label runat="server" ID="lblTeamMembers" Text="Select Team Member" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:DropDownList ID="ddlTeamMembers" CssClass="ddlTeamMembers formcontrol" runat="server" ValidationGroup="form">
                                            <asp:ListItem Value="" Text="Select Member"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator runat="server" ID="rqrdMember" ControlToValidate="ddlTeamMembers" SetFocusOnError="true" ErrorMessage="Required!" ForeColor="Red" Font-Bold="true" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblDescription" Text="Description" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="10" Columns="44" ValidationGroup="form"></asp:TextBox>
                                    </td>
                                     <td>
                                        <asp:RequiredFieldValidator runat="server" ID="rqrdDesc" ControlToValidate="txtDescription" SetFocusOnError="true" ErrorMessage="Required!" ForeColor="Red" Font-Bold="true" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">
                                        <asp:Label runat="server" ID="lblRepeat" Text="Repeats" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:DropDownList ID="ddlRepeat" CssClass="ddlRepeat formcontrol"  ValidationGroup="form"
                                            runat="server">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="Daily" Text="Daily"></asp:ListItem>
                                            <asp:ListItem Value="Weekly" Text="Weekly"></asp:ListItem>
                                            <asp:ListItem Value="Monthly" Text="Monthly"></asp:ListItem>
                                            <asp:ListItem Value="Yearly" Text="Yearly"></asp:ListItem>
<%--                                            <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                       <td>
                                        <asp:RequiredFieldValidator runat="server" ID="rqrdRepeat" ControlToValidate="ddlRepeat" SetFocusOnError="true" ErrorMessage="Required!" ForeColor="Red" Font-Bold="true" ValidationGroup="form" InitialValue=""></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">
                                        <asp:Label runat="server" ID="lblRepeatEvery" Text="Repeat every" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:DropDownList ID="ddlRepeatEvery" CssClass="ddlRepeatEvery formcontrol"  ValidationGroup="form"
                                            runat="server">
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                            <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                            <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                            <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                            <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                            <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                            <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                            <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                            <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                            <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                            <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                            <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                            <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                            <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                            <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                            <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                            <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                            <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                     <td>
                                        <asp:RequiredFieldValidator runat="server" ID="rqrdRepeatEvery" ControlToValidate="ddlRepeatEvery" SetFocusOnError="true" ErrorMessage="Required!" ForeColor="Red" Font-Bold="true" ValidationGroup="form"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">
                                        <asp:Label runat="server" ID="lblStartsOn" Text="Starts on" Font-Bold="true" ></asp:Label>
                                    </td>
                                    <td style="width: 30%">
                                        <asp:TextBox ID="txtstartson" runat="server" ReadOnly="true" CssClass="formcontrol disable" ValidationGroup="form" style="width:80% !important;"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <br />
                                        <asp:Button runat="server" ID="btnAdd" Text="Save Reminder" CssClass="btn" OnClick="btnAdd_Click" ValidationGroup="form"/>
                                        &nbsp; &nbsp; 
                                          <asp:Button runat="server" ID="btnView" Text="View Reminders" CssClass="btn" OnClick="btnView_Click" />
                                        <asp:Button runat="server" ID="btnHide" Text="Hide Reminders" CssClass="btn" Visible="false" OnClick="btnHide_Click" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <div id="reminderslist" runat="server">
                                        </div>
                                    </td>
                                    <td></td>
                                     <td></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblerror" runat="server" Text="" ForeColor="Red" />
                                    </td>
                                    <td></td>
                                     <td></td>
                                </tr>

                            </table>
                        </div>


                    </div>
                </div>

                <div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="modal fade" id="editReminder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel7"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content eidtTitlePopup">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel6">Edit Reminder</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Description:</label>
                                    &nbsp;
                        <asp:TextBox ID="eDescription" runat="server" class="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                    <br />
                                </div>
                                <div class="col-xs-6">
                                    <label>Repeats:</label>
                                    &nbsp;
                                 <asp:DropDownList ID="eRepeats" CssClass="formcontrol"
                                     runat="server">
                                     <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                     <asp:ListItem Value="Daily" Text="Daily"></asp:ListItem>
                                     <asp:ListItem Value="Weekly" Text="Weekly"></asp:ListItem>
                                     <asp:ListItem Value="Monthly" Text="Monthly"></asp:ListItem>
                                     <asp:ListItem Value="Yearly" Text="Yearly"></asp:ListItem>
<%--                                     <asp:ListItem Value="Custom" Text="Custom"></asp:ListItem>--%>
                                 </asp:DropDownList>
                                </div>
                                <br />
                                <div class="col-xs-6">
                                    <label>Repeat every:</label>
                                    &nbsp;
                  <asp:DropDownList ID="eRepeatEvery" CssClass="formcontrol"
                      runat="server">
                      <asp:ListItem Text="1" Value="1"></asp:ListItem>
                      <asp:ListItem Text="2" Value="2"></asp:ListItem>
                      <asp:ListItem Text="3" Value="3"></asp:ListItem>
                      <asp:ListItem Text="4" Value="4"></asp:ListItem>
                      <asp:ListItem Text="5" Value="5"></asp:ListItem>
                      <asp:ListItem Text="6" Value="6"></asp:ListItem>
                      <asp:ListItem Text="7" Value="7"></asp:ListItem>
                      <asp:ListItem Text="8" Value="8"></asp:ListItem>
                      <asp:ListItem Text="9" Value="9"></asp:ListItem>
                      <asp:ListItem Text="10" Value="10"></asp:ListItem>
                      <asp:ListItem Text="11" Value="11"></asp:ListItem>
                      <asp:ListItem Text="12" Value="12"></asp:ListItem>
                      <asp:ListItem Text="13" Value="13"></asp:ListItem>
                      <asp:ListItem Text="14" Value="14"></asp:ListItem>
                      <asp:ListItem Text="15" Value="15"></asp:ListItem>
                      <asp:ListItem Text="16" Value="16"></asp:ListItem>
                      <asp:ListItem Text="17" Value="17"></asp:ListItem>
                      <asp:ListItem Text="18" Value="18"></asp:ListItem>
                      <asp:ListItem Text="19" Value="19"></asp:ListItem>
                      <asp:ListItem Text="20" Value="20"></asp:ListItem>
                      <asp:ListItem Text="21" Value="21"></asp:ListItem>
                      <asp:ListItem Text="22" Value="22"></asp:ListItem>
                      <asp:ListItem Text="23" Value="23"></asp:ListItem>
                      <asp:ListItem Text="24" Value="24"></asp:ListItem>
                      <asp:ListItem Text="25" Value="25"></asp:ListItem>
                      <asp:ListItem Text="26" Value="26"></asp:ListItem>
                      <asp:ListItem Text="27" Value="27"></asp:ListItem>
                      <asp:ListItem Text="28" Value="28"></asp:ListItem>
                      <asp:ListItem Text="29" Value="29"></asp:ListItem>
                      <asp:ListItem Text="30" Value="30"></asp:ListItem>
                  </asp:DropDownList>
                                </div>
                                <div class="col-xs-12">
                                    <br />
                                    <label>Select Member:</label>
                                    &nbsp;
                                    <asp:DropDownList ID="eTeamMembers" CssClass="eTeamMembers formcontrol" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btnCancel" data-dismiss="modal">
                                Cancel</button>
                            <button type="button" class="btn btn-primary" id="btnUpdate">
                                Update</button>
                            <asp:HiddenField ID="hdnID" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField runat="server" ID="hdnInstanceID"/>
            <asp:HiddenField runat="server" ID="hdnDisID"/>
            <asp:HiddenField runat="server" ID="hdnOwnerID"/>
        </div>
    </form>
</body>
</html>
