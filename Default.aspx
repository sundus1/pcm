﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CCM.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Team Communication Application</title>
     
    <script>
        setTimeout(function () {
     <%=meta %>       
        }, 5000);


    </script>
    
</head>
<body id="mainBody">
  <p>You will be automatically redirected to your discussion in 5 seconds., If it takes longer then please click <a href="<%=url %>" id="link" target="_blank">here</a></p>
</body>
</html>
