﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mGetDiscussionsList.aspx.cs" Inherits="CCM.mGetDiscussionsList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="_assests/jQuery%20Mobile/jquery.mobile-1.4.5.css" rel="stylesheet" />
    <script src="_assests/jQuery%20Mobile/demos/js/jquery.js"></script>
    <script src="_assests/jQuery%20Mobile/jquery.mobile-1.4.5.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divDiscussionList" class="divDiscussionList">
            <asp:Label runat="server" ID="lblUnread" CssClass="lblUnreadCount" Style="display: none"></asp:Label>
            <div data-role="tabs" id="tabs">
                <div data-role="navbar">
                    <ul>
                        <li><a href="#one" data-ajax="false">Open<asp:Label ID="lblOpenCount" runat="server"></asp:Label></a></li>
                        <li><a href="#two" data-ajax="false">Closed<asp:Label ID="lblArchiveCount" runat="server"></asp:Label></a></li>
                        <li class="li-no">
                            <div class="li-no-edit" style="margin-top: 3px;">
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="one" class="ui-body-d ui-content">
                    <asp:Repeater ID="rptDiscussionList" runat="server" OnItemDataBound="rptDiscussionList_ItemDataBound">
                        <HeaderTemplate>
                            <ul style="list-style-type: none; padding: 0px !important">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="border-bottom: 1px solid #ccc; margin-bottom: 5px;">
                                <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="opendis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' style="width: 92% !important; padding: 10px"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                </div>
                <div id="two">
                    <asp:Repeater ID="rptDiscussionListClosed" runat="server" OnItemDataBound="rptDiscussionListClosed_ItemDataBound">
                        <HeaderTemplate>
                            <ul style="list-style-type: none; padding: 0px !important">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="border-bottom: 1px solid #ccc; margin-bottom: 5px;">
                                <a href='<%# GetDiscussionLink(Convert.ToInt32(DataBinder.Eval(Container.DataItem,"DiscussionID")), Convert.ToInt32(DataBinder.Eval(Container.DataItem,"RoleID"))) %>' runat="server" id="aLinkDiscussion" data-id="aLinkDiscussion" data-type="closedis" data-argument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' style="width: 92% !important; padding: 10px"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                                <%--<asp:LinkButton ID="lnkDiscussion" data-id="lnkDiscussion" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"DiscussionID") %>' CommandName="Navigate" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>'></asp:LinkButton></li>--%>
                        </ItemTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnInstanceID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
    </form>
</body>
</html>
