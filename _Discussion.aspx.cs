﻿using AvaimaThirdpartyTool;
using CCM.App_Code;
using Microsoft.Win32;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using MyCCM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace CCM
{
    public partial class _Discussion : AvaimaWebPage
    {
        public static String _LocalInstanceID = "";
        public string[] DisallowedContents;
        private string storagepath;

        public static string GetVersion()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("_discussionsa_assets/js/custom.js");
            string version = new System.IO.FileInfo(path).LastWriteTime
                .ToString("yyyyMMddHHmmss");
            return version;
        }
        public static string GetVersionCss()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath("_discussionsa_assets/css/bootstrap.css");
            string version = new System.IO.FileInfo(path).LastWriteTime
                .ToString("yyyyMMddHHmmss");
            return version;
        }
        #region ---- PAGE EVENTS ----
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["instanceid"] == null)
            {
                Response.Redirect(Request.Url.AbsoluteUri + "&instanceid=a4841f77-2e42-46d7-904f-54f2c63cafab");
                return;
            }

            if (this.InstanceID == "0") { _LocalInstanceID = "a4841f77-2e42-46d7-904f-54f2c63cafab"; }
            else { _LocalInstanceID = this.InstanceID; }
            if (!IsPostBack)
            {
                initalizeProjectData();
                hdnMoveToBottom.Value = "1";
                hdnRCom.Value = hdnRDL.Value = hdnRFiles.Value = hdnRTags.Value = hdnRNotes.Value = DateTime.Now.ToBinary().ToString();
            }
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID && u.Active == true);

            if (member != null)
            {
                if (hdnDisID.Value.ToInt32() != 0)
                {
                    DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == hdnDisID.Value.ToInt32() && u.Active == true);
                    if (dtf != null)
                    {
                        if (String.IsNullOrEmpty(member.Title) || String.IsNullOrEmpty(member.FirstName))
                        {
                            hdnAskInfo.Value = "1";
                        }
                        else
                        {
                            hdnAskInfo.Value = "0";
                        }
                    }
                    else
                    {
                        //Team Member Management
                        this.Redirect("Controls/info.aspx?h=You don't have the permission to view this discussion.&t=Contact the relevant person for details.");
                        return;
                    }
                }
            }
            else
            {
                //Team Member Management
                this.Redirect("Controls/info.aspx?h=You don't have the permission to view this discussion.&t=Contact the relevant person for details.");
                return;
            }
            if (hdnDisID.Value.ToInt32() == 0)
            {
                MyCCM.Discussion discussion = new MyCCM.Discussion()
                {
                    ProjectID = hdnProjectID.Value.ToInt32(),
                    Active = true,
                    CrtDate = DateTime.Now,
                    InstanceID = _LocalInstanceID,
                    ModDate = DateTime.Now,
                    Status = "Open",
                    Title = "Untitled"
                };
                if (Request.QueryString["dtitle"] != null)
                {
                    discussion.Title = Request.QueryString["dtitle"];
                }
                discussion.sendsummary = false;
                discussion.Add();
                iButtonStatus.Attributes["class"] = "infofont fa fa-check";
                iButtonStatus.Style.Add("color", "green");
                iButtonStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
                DiscussionToFolder disToFolder = new DiscussionToFolder();
                Folder parentFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
                if (parentFolder != null)
                {
                    List<MemberToFolder> adminsOfFolder = MemberToFolder.Find(u => (u.RoleID == 5 || u.RoleID == 2 || u.RoleID == 1) && u.FolderID == hdn_folderid.Value.ToInt32()).ToList();
                    foreach (MemberToFolder adminOfFolder in adminsOfFolder)
                    {

                        disToFolder = new DiscussionToFolder()
                        {
                            InstanceID = _LocalInstanceID,
                            MemberID = adminOfFolder.MemberID,
                            FolderID = hdn_folderid.Value.ToInt32(),
                            Active = true,
                            CrtDate = DateTime.Now,
                            ModDate = DateTime.Now,
                            RoleID = adminOfFolder.RoleID,
                            Status = 1,
                            DiscussionID = discussion.DiscussionID,
                            Alias = adminOfFolder.Alias,
                            Title = adminOfFolder.Title,
                            Unread = 0
                        };
                        disToFolder.Add();

                    }
                }
                else
                {
                    disToFolder = new DiscussionToFolder()
                    {
                        MemberID = hdnMemberID.Value.ToInt32(),
                        FolderID = 0,
                        Active = true,
                        CrtDate = DateTime.Now,
                        DiscussionID = discussion.DiscussionID,
                        InstanceID = _LocalInstanceID,
                        ModDate = DateTime.Now,
                        RoleID = 1,
                        Status = 1,
                        Alias = member.NickName,
                        Title = member.Title,
                        Unread = 0
                    };
                    disToFolder.Add();
                }

                disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID && u.DiscussionID == discussion.DiscussionID);
                if (disToFolder != null)
                {

                    hdnRoleID.Value = disToFolder.RoleID.ToString();
                    SetAccordingToRole(disToFolder.RoleID.ToInt32());
                }
                if (member != null)
                {
                    lblMemberTemp.Text = member.NickName;
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                }
                String url = "Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&ir=" + disToFolder.RoleID.ToString().Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + InstanceID;
                this.Redirect(url);
            }
            else
            {
                AvaimaTimeZoneAPI ati = new AvaimaTimeZoneAPI();
                btninformation.Attributes["Title"] = Utilities.GetGoogleFormatedDate(DateTime.Now.ToString()) + " at " + ati.GetTime(DateTime.Now.ToString(), HttpContext.Current.User.Identity.Name);
                lblInfoExClient.Text = "";
                lblInfoExTeam.Text = "";
                lblInfoTeam.Text = "";
                lblInfoClient.Text = "";
                DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                if (disToFolder == null)
                {
                    this.Redirect("Controls/info.aspx?h=You don't have the permission to view this discussion.&t=Contact the relevant person for details.");
                    return;
                }
                else
                {
                    hdnMN.Value = disToFolder.Alias.Encrypt();
                    hdnRoleID.Value = disToFolder.RoleID.ToString();


                }
                DisplayAccordintToRole(hdnRoleID.Value.ToInt32());
                if (!IsPostBack)
                {
                    BindExistingMembersDLL();
                    FillMembers();
                    //GenerateBreadCrumb();
                    hdnMemberID.Value = member.MemberID.ToString();
                    BindDiscussionData();
                }
                if (member != null)
                {
                    if (String.IsNullOrEmpty(disToFolder.Alias))
                    {
                        lblMemberTemp.Text = member.NickName;
                    }
                    else
                    {
                        lblMemberTemp.Text = disToFolder.Alias;
                    }

                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                    //imgMember.ImageUrl = "_assests/Images/" + mr.ToString() + ".png";
                }
            }
            /*if (hdnDisID != null && hdnDisID.Value != "" && hdnDisID.Value != "0")
            {
                DisplaySummary(hdnDisID.Value);
            }*/
            h1Title.Focus();
            report.HRef = "Report.aspx?MemberId=" + hdnMemberID.Value.Encrypt().ToString() + "&IntanceID=" + hdninstanceid.Value.ToString() + "&DiscussionID=" + hdnDisID.Value.Encrypt();
            downloadProj.HRef = "DownloadDiscussion.aspx?MemberId=" + hdnMemberID.Value.Encrypt().ToString() + "&InstanceID=" + hdninstanceid.Value.ToString() + "&disID=" + hdnDisID.Value.Encrypt(); 
        }

        protected void rptBC_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Navigate")
            {
                LinkButton lnkButton = e.Item.FindControl("lnkBC") as LinkButton;
                if (e.CommandArgument.ToString() != "0")
                {
                    this.Redirect("Default.aspx?fid=" + e.CommandArgument.ToString());
                }
                else
                {
                    this.Redirect("Default.aspx");
                }
            }
        }

        protected void rptDiscussions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl imgMember = e.Item.FindControl("imgMember") as HtmlControl;
                HtmlControl imgMember2 = e.Item.FindControl("imgMember2") as HtmlControl;
                HtmlControl tdImage = e.Item.FindControl("tdImage") as HtmlControl;
                HtmlControl tdImage2 = e.Item.FindControl("tdImage2") as HtmlControl;
                HtmlContainerControl myDivMemDis = e.Item.FindControl("divMemDis") as HtmlContainerControl;
                HtmlContainerControl myDivMemDisO = myDivMemDis.FindControl("divMemDisO") as HtmlContainerControl;
                Label lblMemberTemp = e.Item.FindControl("lblMemberTemp") as Label;
                Label lblMemberTemp2 = e.Item.FindControl("lblMemberTemp2") as Label;
                TextBox txtMemDis = e.Item.FindControl("txtMemDis") as TextBox;
                Int32 memberID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MemberID"));
                String memberDisID = DataBinder.Eval(e.Item.DataItem, "RowKey").ToString();
                Member member = Member.SingleOrDefault(u => u.MemberID == memberID);

                if (member != null)
                {
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), member.MemberID);
                    imgMember.Attributes["src"] = imgMember2.Attributes["src"] = "_assests/Images/" + mr.ToString() + ".png";
                    Image img = new Image();
                    img.ID = "disAction";
                    img.CssClass = "disAction";
                    img.ImageUrl = "_assests/Images/downarrow.png";
                    img.Attributes.Add("data-id", memberDisID);
                    Label lblDateTime = new Label();
                    lblDateTime.Attributes["class"] = "lblDateTime";
                    lblDateTime.Style.Add("float", "right");
                    lblDateTime.Style.Add("margin-right", "-29px");
                    lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                    DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdninstanceid.Value && u.MemberID == memberID);
                    if (dtf != null)
                    {
                        if (String.IsNullOrEmpty(dtf.Alias)) { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                        else { lblMemberTemp2.Text = lblMemberTemp.Text = dtf.Alias; }
                    }
                    else { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                    string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                    myDivMemDis.Attributes["class"] = "Class " + roleTitle;
                    myDivMemDis.Attributes.Add("data-role", roleTitle);
                    myDivMemDisO.InnerHtml += DataBinder.Eval(e.Item.DataItem, "Conversation").ToString();
                    myDivMemDisO.InnerHtml += "</br>";
                    DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                    if (member.MemberID == Convert.ToInt32(hdnMemberID.Value)) { myDivMemDis.Controls.Add(img); }
                    else
                    {
                        if (dtfCurrMember != null)
                        {
                            if (dtfCurrMember.RoleID == 1 || dtfCurrMember.RoleID == 2)
                            {
                                myDivMemDis.Controls.Add(img);
                            }
                        }
                    }
                    myDivMemDis.Controls.Add(lblDateTime);
                    if (roleTitle == "ClientMember") { tdImage.Visible = false; tdImage2.Visible = true; }
                    else if (roleTitle == "VendorMember") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "Admin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "SuperAdmin") { tdImage.Visible = true; tdImage2.Visible = false; }
                    else if (roleTitle == "ClientAdmin") { tdImage.Visible = false; tdImage2.Visible = true; }

                    Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;
                    List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                    if (disFiles.Count > 0)
                    {
                        rptFiles.DataSource = disFiles;
                        rptFiles.DataBind();
                    }
                    else { }
                }
            }
        }

        protected void rptClient_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl trClient = e.Item.FindControl("trClient") as HtmlControl;
                Int32 memberID = Convert.ToInt32(trClient.Attributes["data-id"].ToString());
                Int32 ApplicableTo = Convert.ToInt32(trClient.Attributes["data-applicableto"].ToString());
                DropDownList ddlStatus = e.Item.FindControl("ddlStatus") as DropDownList;
                ddlStatus.SelectedIndex = ApplicableTo;
            }
        }


        protected void rptTeamMembers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (hdnRoleID.Value == "6") // for Project Manager Only
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    LinkButton lbnt = e.Item.FindControl("lnkTeamEdit") as LinkButton;
                    LinkButton lbnt1 = e.Item.FindControl("lnkTeamRemove") as LinkButton;
                    Int32 roleId = Convert.ToInt16(lbnt.Attributes["data-roleID"].ToString());
                    if (roleId == 1 || roleId == 2)
                    {
                        lbnt.Visible = false;
                        lbnt1.Visible = false;

                    }

                }
            }
        }
        protected void rptTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl trTeam = e.Item.FindControl("trTeam") as HtmlControl;
                Int32 memberID = Convert.ToInt32(trTeam.Attributes["data-id"].ToString());
                Int32 ApplicableTo = Convert.ToInt32(trTeam.Attributes["data-applicableto"].ToString());
                DropDownList ddlStatus = e.Item.FindControl("ddlStatus") as DropDownList;
                ddlStatus.SelectedIndex = ApplicableTo;
            }
        }

        protected void btnAddMember_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            MemberRoles mr;
            if (btnSender.CommandName == "Client")
            {
                mr = MemberRoles.ClientMember;
                string response = AddMember(mr);
                if (response.Split(':')[0].Contains("Info"))
                {
                    lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
                    ClearClientFields();
                    hdnSelClientID.Value = "0";
                }
                else if (response.Split(':')[0].Contains("Error"))
                {
                    lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
                }
                BindExistingMembersDLL();
                FillMembers();
                BindDiscussionData();
            }
            else if (btnSender.CommandName == "Team")
            {
                if (chkAdmin.Checked)
                {
                    mr = MemberRoles.Admin;
                }
                else
                {
                    mr = MemberRoles.VendorMember;
                }
                string response = AddMember(mr);
                if (response.Split(':')[0].Contains("Info"))
                {
                    lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                    lblInfoExClient.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
                    ClearTeamFields();
                    hdnSelTeamID.Value = "0";
                }
                else if (response.Split(':')[0].Contains("Error"))
                {
                    lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                    lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
                }
                BindExistingMembersDLL();
                FillMembers();
                BindDiscussionData();
                //divManageTeam.Style.Clear();
            }

        }

        protected void btnAddEx_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(window).on('beforeunload', function () {   NProgress.start(); });", true);

            Button btnSender = (Button)sender;
            Member member;
            if (btnSender.CommandName == "Client")
            {
                string[] SelectedClients = (hdnClientIDs.Value).Split(',');
                foreach (string uid in SelectedClients)
                {
                    if (uid != "")
                    {
                        member = Member.SingleOrDefault(u => u.MemberID == uid.ToInt32());
                        if (member != null)
                        {
                            txtClientEmail.Text = member.Email;
                            txtClientName.Text = member.FirstName;
                            if (String.IsNullOrEmpty(txtClientAlias.Text)) { txtClientAlias.Text = member.NickName; }
                            if (String.IsNullOrEmpty(txtClientAlias.Text)) { txtClientAlias.Text = member.Title; }
                            hdnSelClientID.Value = "0";
                            btnAddClient.Text = "Add";
                            txtClientAlias.Focus();
                            btnAddMember_Click(sender, e);
                            ClearClientFields();
                            hdnSelClientID.Value = "0";
                            divManageClient.Style.Clear();
                        }
                        else
                        {
                            lblInfoExClient.Text = "No member found";
                            lblInfoExClient.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
            else
            {
                string[] SelectedMemberIds = (hdnIDs.Value).Split(',');
                foreach (string uid in SelectedMemberIds)
                {
                    if (uid != "")
                    {
                        member = Member.SingleOrDefault(u => u.MemberID == uid.ToInt32());
                        if (member != null)
                        {
                            txtTeamEmail.Text = member.Email;
                            txtTeamName.Text = member.FirstName;
                            if (String.IsNullOrEmpty(txtTeamAlias.Text)) { txtTeamAlias.Text = member.NickName; }
                            if (String.IsNullOrEmpty(txtTeamTitle.Text)) { txtTeamTitle.Text = member.Title; }
                            hdnSelTeamID.Value = "0";
                            btnAddTeam.Text = "Add";
                            txtTeamName.Focus();
                            btnAddMember_Click(sender, e);
                            ClearTeamFields();
                            hdnSelTeamID.Value = "0";
                            //divManageTeam.Style.Clear();
                        }
                        else
                        {
                            lblInfoExTeam.Text = "No member found";
                            lblInfoExTeam.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }


                //chenge

                //member = Member.SingleOrDefault(u => u.MemberID == ddlTeamMembers.SelectedValue.ToInt32());
                //if (member != null)
                //{
                //    txtTeamEmail.Text = member.Email;
                //    txtTeamName.Text = member.FirstName;
                //    if (String.IsNullOrEmpty(txtTeamAlias.Text)) { txtTeamAlias.Text = member.NickName; }
                //    if (String.IsNullOrEmpty(txtTeamTitle.Text)) { txtTeamTitle.Text = member.Title; }
                //    hdnSelTeamID.Value = "0";
                //    btnAddTeam.Text = "Add";
                //    txtTeamName.Focus();
                //    btnAddMember_Click(sender, e);
                //    ClearTeamFields();
                //    hdnSelTeamID.Value = "0";
                //    //divManageTeam.Style.Clear();
                //}
                //else
                //{
                //    lblInfoExTeam.Text = "No member found";
                //    lblInfoExTeam.ForeColor = System.Drawing.Color.Red;
                //}
                //change

            }
            BindDiscussionData();

            ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:  $(document.getElementById('list')).ready(function() {  NProgress.done();   });  ", true);
        }


        //protected void btnStatus_Click(object sender, ImageClickEventArgs e)
        //{
        //    AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
        //    MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value));
        //    Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
        //    Member currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
        //    DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == hdnDisID.Value.ToInt32());
        //    if (dis != null)
        //    {
        //        List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == dis.DiscussionID && u.InstanceID == _LocalInstanceID).ToList();
        //        foreach (DiscussionToFolder item in dtfs)
        //        {
        //            Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
        //            DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.MemberID == item.MemberID && u.DiscussionID == dis.DiscussionID);
        //            if (member != null)
        //            {
        //                emailAPI.send_email(member.Email, dtf.Alias, "noreply@avaima.com",
        //                    "Hi " + dtfMember.Alias + ",<br /><br />This is to inform you that Discussion: <b>" + dis.Title + "</b> has been marked as <b>" + btnStatus.CommandName + "</b> by " + dtf.Alias + "." + Utilities.AvaimaEmailSignature,
        //                    "Discussion: " + dis.Title + " has been marked as " + btnStatus.CommandName + " by " + dtf.Alias + "."
        //                    );
        //            }
        //        }
        //        dis.Status = btnStatus.CommandName;
        //        dis.ModDate = DateTime.Now;
        //        dis.Update();
        //        //FillMembers();
        //        if (dis.Status == "Open")
        //        {
        //            btnStatus.ImageUrl = "_assests/Images/16 x 16/Tick.png";
        //            btnStatus.CommandName = "Close";
        //            btnStatus.ToolTip = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
        //            btnStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as <b>Closed</b>";
        //        }
        //        else
        //        {
        //            btnStatus.ImageUrl = "_assests/Images/16 x 16/Untick.png";
        //            btnStatus.CommandName = "Open";
        //            btnStatus.ToolTip = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
        //            btnStatus.Attributes["Title"] = "This discussion is currently closed. Click to mark this discussion as <b>Open</b>";
        //        }
        //        //BindDiscussionList();
        //        SetActiveDiscussion(dis.DiscussionID.ToString());
        //    }
        //}

        //protected void rptClient_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    if (e.CommandName == "Remove")
        //    {
        //        int memberid = Convert.ToInt32(e.CommandArgument.ToString());
        //        string resposen = RemoveMember(memberid);
        //        if (resposen.Split(':')[0].Contains("Info"))
        //        {
        //            //ShowInfo(resposen.Split(':')[1]); 
        //            lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
        //            lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
        //        }
        //        else if (resposen.Split(':')[0].Contains("Error"))
        //        {
        //            //ShowError(resposen.Split(':')[1]); 
        //            lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
        //            lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
        //        }
        //        BindClients();
        //        //divManageClient.Style.Clear();
        //    }
        //}

        protected void btnSaveMemberInfo_Click(object sender, EventArgs e)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            if (member != null)
            {
                member.FirstName = txtMName.Text;
                member.NickName = txtMAlias.Text;
                member.Title = txtMTitle.Text;
                member.Update();
                hdnAskInfo.Value = "0";
            }
        }

        //protected void rptTeamMembers_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    if (e.CommandName == "Remove")
        //    {
        //        int memberid = Convert.ToInt32(e.CommandArgument.ToString());
        //        string resposen = RemoveMember(memberid);
        //        if (resposen.Split(':')[0].Contains("Info"))
        //        {
        //            lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
        //            lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
        //        }
        //        else if (resposen.Split(':')[0].Contains("Error"))
        //        {
        //            lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
        //            lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
        //        }
        //        BindTeam();
        //    }

        //}

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                HttpPostedFile postedFile = fileUpload1.PostedFile;
                string discussionID = hdnDisID.Value;
                string memDisID = "";
                //memDisID = context.Session["memDisID"].ToString();
                memDisID = hdnMemDisID.Value;

                string projectID = hdnProjectID.Value;
                //BusinessID = context.Request.Form["BusinessID"].ToString();
                storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);
                string savepath = "";
                string tempPath = "";
                tempPath = "uploads";
                savepath = Server.MapPath(tempPath);

                if (!System.IO.Directory.Exists(savepath))
                    System.IO.Directory.CreateDirectory(savepath);

                string filename = postedFile.FileName;
                string extension = filename.Substring(filename.LastIndexOf('.') + 1, 3);

                //postedFile.SaveAs(savepath + @"\" + filename);
                //context.Response.Write(tempPath + "/" + filename);
                //context.Response.StatusCode = 200;
                MyCCM.DiscussionFile disfile = new DiscussionFile()
                {
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    DiscussionID = Convert.ToInt32(discussionID),
                    FileName = filename,
                    FileType = extension,
                    MemberDisID = memDisID,
                    ProjectID = Convert.ToInt32(projectID),
                };
                disfile.FilePath = storagepath + disfile.FileName;
                UploadAndSaveDisFile(postedFile.InputStream, disfile);
            }
            catch (Exception ex)
            {
                //context.Response.Write("Error: " + ex.Message);
            }
        }

        protected void btnStartNewDiscussion_Click(object sender, EventArgs e)
        {
            String url = "_Discussion.aspx?did=" + ("0").Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + _LocalInstanceID + "&dtitle=" + txtNewDiscussionTitle.Text + "&isHourly=false";
            Response.Redirect(url);
        }

        protected void btnSaveDAContents_Click(object sender, EventArgs e)
        {
            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == hdnDisID.Value.ToInt32() && u.FolderID == 0);
            if (dc != null)
            {
                dc.Contents = txtContents.Text;
                dc.ModDate = DateTime.Now;
                dc.Update();
            }
            else
            {
                dc = new DisallowedContent()
                {
                    ModDate = DateTime.Now,
                    CrtDate = DateTime.Now,
                    Contents = txtContents.Text,
                    FolderID = 0,
                    DiscussionID = hdnDisID.Value.ToInt32(),
                    InstanceID = this.InstanceID,
                    Active = true,
                };
                dc.Add();
            }
            BindDAData();
        }

        protected void btnRemoveMember_Click(object sender, EventArgs e)
        {
            int memberid = Convert.ToInt32(hdnClientToRemoveID.Value);
            string resposen = RemoveMember(memberid);
            if (resposen.Split(':')[0].Contains("Info"))
            {
                //ShowInfo(resposen.Split(':')[1]); 
                lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
                lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
            }
            else if (resposen.Split(':')[0].Contains("Error"))
            {
                //ShowError(resposen.Split(':')[1]); 
                lblInfoExClient.Text = lblInfoClient.Text = resposen.Split(':')[1];
                lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
            }
            BindClients();
        }

        protected void btnRemoveTeamMember_Click(object sender, EventArgs e)
        {
            int memberid = Convert.ToInt32(hdnTeamToRemoveID.Value);
            string resposen = RemoveMember(memberid);
            if (resposen.Split(':')[0].Contains("Info"))
            {
                lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
                lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
            }
            else if (resposen.Split(':')[0].Contains("Error"))
            {
                lblInfoExTeam.Text = lblInfoTeam.Text = resposen.Split(':')[1];
                lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
            }
            BindTeam();
        }

        protected void btndeldiscussion_Click(object sender, EventArgs e)
        {
            Info info = CCM_Helper.DeleteDiscussionData(Convert.ToInt32(hdnDisID.Value), hdnMemberID.Value.ToInt32());
            string infoString = info.Title + ": " + info.Description;

            DataTable dt = CCMDiscussion.GetAllMemberDiscussions(hdnMemberID.Value.ToInt32(), hdninstanceid.Value);
            DataRow[] dr = dt.Select("ModDate = MAX(ModDate)");
            hdnDisID.Value = dr[0]["DiscussionID"].ToString();

            String url = "_Discussion.aspx?did=" + hdnDisID.Value.Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&ir=" + hdnRoleID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + InstanceID;
           this.Redirect(url);


            //      var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
            //new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            //      var tableClient = acc.CreateCloudTableClient();
            //      var table = tableClient.GetTableReference("MemberDiscussions");
            //      TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(           
            //      TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, hdnDisID.Value));
            //      IEnumerable<MemberDiscussions1> mj = table.ExecuteQuery(rangeQuery).OrderBy(x => x.CrtDate);

            //      //TableQuery<App_Code.MemberDiscussions1> delQuery = new TableQuery<MemberDiscussions1>().Where(
            //      //    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal,hdnDisID.Value));

            //      foreach (var entity in mj)
            //      {
            //          //table.Execute(TableOperation.Delete(entity));
            //      }

            //      MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value));
            //      DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == hdnDisID.Value.ToInt32());
            //      if (dis != null)
            //      {
            //          dis.Active = false;
            //          dis.ModDate = DateTime.Now;
            //    //      dis.Update();
            //      }
        }

        protected void btnstatus_Click(object sender, EventArgs e)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value));
            Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
            Member currMember = Member.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32());
            DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == hdnDisID.Value.ToInt32());
            if (dis != null)
            {
                List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == dis.DiscussionID && u.InstanceID == _LocalInstanceID).ToList();
                foreach (DiscussionToFolder item in dtfs)
                {
                    Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                    DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.MemberID == item.MemberID && u.DiscussionID == dis.DiscussionID);
                    if (member != null)
                    {
                        emailAPI.send_email(member.Email, dtf.Alias, "noreply@avaima.com",
                            "Hi " + dtfMember.Alias + ",<br /><br />This is to inform you that Discussion: <b>" + dis.Title + "</b> has been marked as <b>" + btnstatus.CommandName + "</b> by " + dtf.Alias + "." + Utilities.AvaimaEmailSignature,
                            "Discussion: " + dis.Title + " has been marked as " + btnstatus.CommandName + " by " + dtf.Alias + "."
                            );
                    }
                }
                dis.Status = btnstatus.CommandName;
                dis.ModDate = DateTime.Now;
                dis.Update();
                //FillMembers();
                if (dis.Status == "Open")
                {
                    iButtonStatus.Attributes["class"] = "infofont fa fa-check";
                    iButtonStatus.Style.Add("color", "green");
                    iButtonStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as Closed";
                    btnstatus.CommandName = "Close";
                    lblstatus.Text = "This discussion is currently open. Click to mark this discussion as Closed";

                }
                else
                {

                    iButtonStatus.Attributes["class"] = "infofont fa fa-close";
                    iButtonStatus.Style.Add("color", "red");
                    iButtonStatus.Attributes["Title"] = "This discussion is currently closed. Click to mark this discussion as Open";
                    btnstatus.CommandName = "Open";
                    lblstatus.Text = "This discussion is currently closed. Click to mark this discussion as Open";

                }
                //BindDiscussionList();
                //SetActiveDiscussion(dis.DiscussionID.ToString());
            }
        }

        #endregion

        #region ---- HELPER METHODS ----
        /// <summary>
        /// Get DiscussionLink of discussion by discussionid and roleid
        /// </summary>
        /// <param name="discussionID">discussionid</param>
        /// <param name="roleID">roleid</param>
        /// <returns></returns>
        public string GetDiscussionLink(Int32 discussionID, Int32 roleID)
        {
            String url = "Discussion.aspx?did=" + discussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&ir=" + roleID.ToString().Encrypt() + "&instanceid=" + _LocalInstanceID;
            return url;
        }
        private void DisplayAccordintToRole(Int32 RoleID)
        {
            switch (RoleID)
            {
                case 1:
                    // SuperAdmin
                    btnstatus.Visible = true;
                    aAddClient.Visible = aAddTeam.Visible = true;
                    //h1Title.Attributes.Add("contenteditable", "true");
                    divManageTeam.Visible = divManageClient.Visible = true;
                    btnEdit.Visible = true;
                    aTeamSettings.Visible = true;
                    downloadDiv.Visible = true;
                    break;
                case 2:
                    // Team Admin
                    btnstatus.Visible = true;
                    aAddClient.Visible = aAddTeam.Visible = true;
                    // h1Title.Attributes.Add("contenteditable", "true");
                    divManageTeam.Visible = divManageClient.Visible = true;
                    btnEdit.Visible = true;
                    aTeamSettings.Visible = true;
                    downloadDiv.Visible = true;
                    break;
                case 3:
                    // Team Member
                    btnstatus.Visible = false;
                    aAddTeam.Visible = true;
                    aAddClient.Visible = true;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = true;
                    btnEdit.Visible = false;
                    // h1Title.Attributes.Remove("contenteditable");
                    hoursSubtract.Visible = false;
                    aTeamSettings.Visible = false;
                    downloadDiv.Visible = false;
                    break;
                case 4:
                    // Client Member
                    btnstatus.Visible = true;
                    aAddClient.Visible = true;
                    aAddTeam.Visible = false;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = false;
                    btnEdit.Visible = false;
                    // h1Title.Attributes.Add("contenteditable", "false");
                    hoursSubtract.Visible = false;
                    aTeamSettings.Visible = false;
                    downloadDiv.Visible = false;

                    break;
                case 5:
                    // Client Admin
                    btnstatus.Visible = true;
                    aAddClient.Visible = true;
                    aAddTeam.Visible = false;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = false;
                    btnEdit.Visible = false;
                    //  h1Title.Attributes.Add("contenteditable", "true");
                    hoursSubtract.Visible = false;
                    aTeamSettings.Visible = false;
                    downloadDiv.Visible = false;

                    break;
                case 7:
                    // Assintant Project Manager
                    btnstatus.Visible = false;
                    aAddTeam.Visible = true;
                    aAddClient.Visible = true;
                    divManageClient.Visible = true;
                    divManageTeam.Visible = true;
                    btnEdit.Visible = false;
                    // h1Title.Attributes.Remove("contenteditable");
                    hoursSubtract.Visible = false;
                    aTeamSettings.Visible = false;
                    downloadDiv.Visible = false;

                    break;
            }
        }
        public void BindDAData()
        {
            DisallowedContent dc = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == hdnDisID.Value.ToInt32() && u.FolderID == 0);
            DisallowedContent settingsDC = DisallowedContent.SingleOrDefault(u => u.InstanceID == this.InstanceID && u.DiscussionID == 0 && u.FolderID == 0);
            string disallowedContents = "";
            if (dc != null)
            {
                disallowedContents = txtContents.Text = dc.Contents;
                if (settingsDC != null)
                {
                    disallowedContents += (", " + settingsDC.Contents);
                }
                DisallowedContents = disallowedContents.Replace(" ", "").Split(',');
            }
            else
            {
                txtContents.Text = "";
            }
        }
        public static string Serialize(object o)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(o);
        }
        private void BindDiscussionData()
        {
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            String url = "_Discussion.aspx?did=" + ("0").Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&md=" + hdnMemberID.Value.Encrypt() + "&instanceid=" + _LocalInstanceID;
            aStartDiscussion.Attributes["href"] = url;
            AvaimaTimeZoneAPI atz = new AvaimaTimeZoneAPI();
            if (discussion != null)
            {
                /* Set Hourly Hidden Values */
                hdnZeroHourPost.Value = discussion.ZeroHourPost.ToString().ToLower();
                hdnIsHourlyProject.Value = discussion.isHourly.ToString().ToLower();
                HideHours.Value = discussion.HideHours.ToString().ToLower();
                ShowSummary.Value = discussion.SummaryAllow.ToString().ToLower();
                // string test = hdnSendSummary.Value;
                if (discussion.sendsummary != null)
                {
                    if (discussion.sendsummary.Value == true)
                    {
                        hdnSendSummary.Value = "true";
                    }
                    else
                    {
                        hdnSendSummary.Value = "false";
                    }
                }
                else
                {
                    hdnSendSummary.Value = "false";
                }
                if (discussion.sendsummary != null)
                {
                    if (discussion.sendsummary.Value == true)
                    {
                        hdnDuration.Value = discussion.Duration;
                        hdnEmailContent.Value = discussion.EmailContent.ToString();
                    }
                }
                if (discussion.SummaryAllow == true)
                {
                    summarywords.Visible = true;
                }
                else
                {
                    summarywords.Visible = false;
                }
                int mr = CCMDiscussion.GetDiscussionMemberRoleID(hdnDisID.Value.ToInt32(), hdnMemberID.Value.ToInt32());
                if (discussion.HideHours == true && (mr == 5 || mr == 4))
                {
                    SummaryBox.Visible = false;
                }
                hdnHourRequiredEachPost.Value = discussion.HourRequiredEachPost.ToString().ToLower();
                /*End */

                // Set hdnvalues for cache discussionList
                App_Code.MemberDiscussion md1 = new App_Code.MemberDiscussion();
                Int32 CoundComment = md1.GetMemberActiveDiscussionCountByDiscussionID(discussion.DiscussionID.ToString());
                hdnCCC.Value = CoundComment.ToString();
                hdnCCT.Value = md1.GetTags(discussion.DiscussionID.ToString()).Split(',').Count().ToString();


                hdnCCNotes.Value = md1.GetNotes(discussion.DiscussionID.ToString()).Count().ToString();
                //CCM.App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
                ////txtFilterTags.Text = md.GetTags(discussion.DiscussionID.ToString());
                //rptTags.DataSource = md.GetTags(discussion.DiscussionID.ToString()).Split(',').ToList();
                //rptTags.DataBind();
                //if (md.GetTags(discussion.DiscussionID.ToString()).Split(',').ToList().Count > 1)
                //{ divTags.Visible = true; lblTags.Visible = false; }
                //else
                //{ divTags.Visible = false; lblTags.Visible = true; }
                title.Text = "Discussion - " + discussion.Title;
                if (discussion.Title == "Untitled")
                {
                    hdnDiscussionIsNew.Value = "1";
                }
                if (discussion.Status == "Open")
                {
                    iButtonStatus.Attributes["class"] = "infofont fa fa-check";
                    iButtonStatus.Style.Add("color", "green");
                    iButtonStatus.Attributes["Title"] = "This discussion is currently open. Click to mark this discussion as Closed";
                    btnstatus.CommandName = "Close";
                    lblstatus.Text = "This discussion is currently open. Click to mark this discussion as Closed";
                }
                else
                {
                    iButtonStatus.Attributes["class"] = "infofont fa fa-close";
                    iButtonStatus.Style.Add("color", "red");
                    iButtonStatus.Attributes["Title"] = "This discussion is currently closed. Click to mark this discussion as Open";
                    btnstatus.CommandName = "Open";
                    lblstatus.Text = "This discussion is currently closed. Click to mark this discussion as Open";

                }
                String crtOnDate = atz.GetDate(discussion.CrtDate.ToString(), HttpContext.Current.User.Identity.Name);
                if (crtOnDate.Contains("Today"))
                {
                    crtOnDate = DateTime.Now.Date.ToShortDateString();
                }
                String modOnDate = atz.GetDate(discussion.ModDate.ToString(), HttpContext.Current.User.Identity.Name);
                if (modOnDate.Contains("Today"))
                {
                    modOnDate = DateTime.Now.Date.ToShortDateString();
                }
                try
                {
                    btninformation.Attributes["Title"] = "Created: " + Utilities.GetGoogleFormatedDate(crtOnDate) + " at " + atz.GetTime(discussion.CrtDate.ToString(), HttpContext.Current.User.Identity.Name)
                         + " - Modified: " + Utilities.GetGoogleFormatedDate(modOnDate) + " at " + atz.GetTime(discussion.ModDate.ToString(), HttpContext.Current.User.Identity.Name);
                }
                catch (Exception) { }

                h1Title.InnerText = discussion.Title;
            }
            //GenerateBreadCrumb();
            BindDAData();
        }
        private void BindClients()
        {
            // Get and fill Clients
            //Int32 roleID = Convert.ToInt32(MemberRoles.ClientMember);
            List<MemberPreview> _membersPreview = new List<MemberPreview>();
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value) && (u.RoleID == 4 || u.RoleID == 5)).ToList();
            foreach (var item in mtds)
            {
                Member tempMember = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMember != null)
                {
                    _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title));
                }
            }
            if (_membersPreview.Count > 0)
            {

                rptClient.DataSource = _membersPreview;
                rptClient.DataBind();

            }
            else
            {
                rptClient.DataSource = _membersPreview;
                rptClient.DataBind();
            }
            lblClientCount.Text = _membersPreview.Count.ToString();
            ClearClientFields();
        }
        private void BindTeam()
        {
            // Get and fill Team
            //Int32 roleID;
            List<MemberPreview> _membersPreview;
            List<DiscussionToFolder> mtds;
            //roleID = Convert.ToInt32(MemberRoles.VendorMember);
            _membersPreview = new List<MemberPreview>();
            /*--------- Project Manager & Asst Project Manager-------------*/
            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "MemberID";
            table.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Name";
            table.Columns.Add(column);
            row = table.NewRow();
            row["MemberID"] = 0;
            row["Name"] = " ";
            table.Rows.Add(row);
            int selectedPMId = 0;
            int selectedAPMId = 0;
            /*---------End Project Manager & Asst Project Manager-------------*/
            mtds = DiscussionToFolder.Find(u => u.DiscussionID == Convert.ToInt32(hdnDisID.Value) &&
                (u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 1 || u.RoleID == 6 || u.RoleID == 7)).Where(u => u.Active == true).ToList();
            foreach (var item in mtds)
            {
                Member tempMember = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (tempMember != null)
                {
                    if (item.RoleID == 6)
                    {
                        _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title));
                        selectedPMId = item.MemberID.ToInt32();
                        hdnPMId.Value = item.MemberID.ToString();
                    }
                    else if (item.RoleID == 7)
                    {
                        _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title));
                        selectedAPMId = item.MemberID.ToInt32();
                        hdnAPMId.Value = item.MemberID.ToString();
                    }
                    else
                    {
                        _membersPreview.Add(new MemberPreview(tempMember, Convert.ToInt32(item.RoleID), item.Alias, item.Title));
                        // hdnPreviousRoleIds.Value += item.MemberID + "-" + item.RoleID + ";";
                    }

                }
                if (tempMember != null)
                {
                    if (item.RoleID == 3 || item.RoleID == 6 || item.RoleID == 7)
                    {
                        row = table.NewRow();
                        row["MemberID"] = item.MemberID;
                        row["Name"] = item.Alias;
                        table.Rows.Add(row);
                    }
                }
            }
            if (_membersPreview.Count > 0)
            {
                rptTeamMembers.DataSource = _membersPreview;
                rptTeamMembers.DataBind();

            }
            else
            {
                rptTeamMembers.DataSource = _membersPreview;
                rptTeamMembers.DataBind();
            }
            if (table.Rows.Count > 0)
            {  /*---------For Project Manager & Asst Project Manager-------------*/

                ddlPTeamMembers.DataValueField = table.Columns["MemberID"].ToString();
                ddlPTeamMembers.DataTextField = table.Columns["Name"].ToString();
                ddlPTeamMembers.DataSource = table;
                ddlPTeamMembers.DataBind();
                ddlAPTeamMembers.DataValueField = table.Columns["MemberID"].ToString();
                ddlAPTeamMembers.DataTextField = table.Columns["Name"].ToString();
                ddlAPTeamMembers.DataSource = table;
                ddlAPTeamMembers.DataBind();
                if (selectedPMId != 0)
                {
                    ddlPTeamMembers.SelectedValue = selectedPMId.ToString();
                }
                if (selectedAPMId != 0)
                {
                    ddlAPTeamMembers.SelectedValue = selectedAPMId.ToString();
                }
                /*---------End Project Manager & Asst Project Manager-------------*/
            }
            lblTeamCount.Text = _membersPreview.Count.ToString();
            aTeamSettings.HRef = "ManageTeamMembers.aspx?MemberId=" + hdnMemberID.Value.Encrypt().ToString() + "&IntanceID=" + hdninstanceid.Value.ToString() + "&DiscussionID=" + hdnDisID.Value.Encrypt().ToString() + "&Team=" + ("Yes").Encrypt();
            ClearTeamFields();
        }
        private void FillMembers()
        {
            BindClients();
            BindTeam();
        }
        private void SetAccordingToRole(Int32 roleID)
        {
            switch (roleID)
            {
                case 1:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = true;
                    //lnkAddClient.Visible = lnkAddTeam.Visible = true;
                    //lblDash.Text = " - ";
                    break;
                case 2:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = true;
                    //lnkAddTeam.Visible = true;
                    //lnkAddClient.Visible = false;
                    //lblDash.Text = " ";
                    break;
                case 3:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = true;
                    //lnkAddClient.Visible = false;
                    h1Title.Attributes.Remove("contenteditable");
                    btnEdit.Visible = false;
                    //lblDash.Text = " ";
                    break;
                case 5:
                case 4:
                    hdnTeamMember.Value = "false";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = false;
                    //lnkAddClient.Visible = true;
                    h1Title.Attributes.Remove("contenteditable");
                    btnEdit.Visible = false;
                    //lblDash.Text = " ";
                    break;


                case 7:
                    hdnTeamMember.Value = "true";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = true;
                    //lnkAddClient.Visible = false;
                    h1Title.Attributes.Remove("contenteditable");
                    btnEdit.Visible = false;
                    //lblDash.Text = " ";
                    break;
                default:
                    hdnTeamMember.Value = "false";
                    disAllowedContent.Visible = false;
                    //lnkAddTeam.Visible = false;
                    //lnkAddClient.Visible = false;
                    h1Title.Attributes.Remove("contenteditable");
                    btnEdit.Visible = false;
                    break;
            }
        }
        private void initalizeProjectData()
        {

            // OwnerID
            if (Request.QueryString["od"] != null)
            {
                if (String.IsNullOrEmpty(Request.QueryString["od"].Decrypt()))
                {
                    hdnOID.Value = null;
                }
                else
                {
                    hdnOID.Value = Request.QueryString["od"].Decrypt();
                }
            }
            else
            {
                hdnOID.Value = null;
            }

            // DiscussionID
            if (Request.QueryString["did"] != null)
            {
                if (String.IsNullOrEmpty(Request.QueryString["did"].Decrypt()))
                {
                    hdnDisID.Value = "0";
                }
                else
                {
                    hdnDisID.Value = Request.QueryString["did"].Decrypt();
                }
            }
            else
            {
                hdnDisID.Value = "0";
            }

            // ProjectID
            MyCCM.Discussion disc = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            if (disc != null) { hdnProjectID.Value = disc.ProjectID.ToString(); }
            else { hdnProjectID.Value = "0"; }

            // FolderID
            if (Request.QueryString["fid"] != null)
            {
                hdn_folderid.Value = Request.QueryString["fid"].Decrypt();
            }

            if (Request.QueryString["md"] != null)
            {
                hdnMemberID.Value = Request.QueryString["md"].Decrypt();
            }
            if (Request.QueryString["instanceid"] != null)
            {
                hdninstanceid.Value = Request.QueryString["instanceid"];
            }

        }
        private Role GetMemberRole(Int32 memberID)
        {
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberID));
            if (member != null)
            {
                MemberToProject mtp = MemberToProject.SingleOrDefault(u => u.ProjectID == Convert.ToInt32(hdnProjectID.Value) && u.MemberID == Convert.ToInt32(hdnMemberID.Value));
                if (mtp != null)
                {
                    Role role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID);
                    return role;
                }
                else
                {
                    mtp = MemberToProject.SingleOrDefault(u => u.ProjectID == 0 && u.MemberID == Convert.ToInt32(hdnMemberID.Value));
                    if (mtp != null)
                    {
                        Role role = Role.SingleOrDefault(u => u.RoleID == mtp.RoleID);
                        return role;
                    }
                }
            }
            return null;
        }
        //private void GenerateBreadCrumb()
        //{
        //    List<MemberToFolder> t_Folders = MemberToFolder.Find(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.InstanceID == _LocalInstanceID).ToList();
        //    int folderID = Convert.ToInt32(hdn_folderid.Value);
        //    List<Folder> folders = new List<Folder>();
        //    GetBCList(t_Folders, folderID, folders);
        //    rptBC.DataSource = folders.OrderBy(u => u.FolderID).ToList();
        //    rptBC.DataBind();
        //}
        private void GetBCList(List<MemberToFolder> _Folders, int projID, List<Folder> folders)
        {
            MemberToFolder memberToFolder = _Folders.SingleOrDefault(u => u.FolderID == projID && u.MemberID == hdnMemberID.Value.ToInt32());
            if (memberToFolder != null)
            {
                Folder folder = Folder.SingleOrDefault(u => u.FolderID == memberToFolder.FolderID);
                if (folder != null)
                {
                    folders.Add(folder);
                }

                GetBCList(_Folders, Convert.ToInt32(memberToFolder.ParentFolderID.ToInt32()), folders);
            }
        }
        private string AddMember(MemberRoles MemberRole)
        {
            int discussionID = hdnDisID.Value.ToInt32();
            Int32 MemberID = 0;
            String Alias = "", Title = "", Email = "", Name = "";
            if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
            {
                MemberID = hdnSelClientID.Value.ToInt32();
                Alias = txtClientAlias.Text;
                Title = txtClientTitle.Text;
                Email = txtClientEmail.Text;
                Name = txtClientName.Text;
            }
            else
            {
                MemberID = hdnSelTeamID.Value.ToInt32();
                Alias = txtTeamAlias.Text;
                Title = txtTeamTitle.Text;
                Email = txtTeamEmail.Text;
                Name = txtTeamName.Text;
            }
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == _LocalInstanceID && u.MemberID == hdnMemberID.Value.ToInt32());
            MyCCM.Discussion Discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionID);
            if (discussionID > 0)
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(Email) && u.InstanceID == _LocalInstanceID);
                if (member == null)
                {
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        string ownerID = "clientuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (ownerID.Length > 36) { ownerID = ownerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTF(Discussion, currMember, member, ownerID, Email, Title, Alias, Name, MemberRole);
                        return info.Title + ":" + info.Description;
                    }
                    else
                    {
                        string ownerID = "teamuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (ownerID.Length > 36) { ownerID = ownerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTF(Discussion, currMember, member, ownerID, Email, Title, Alias, Name, MemberRole);
                        return info.Title + ":" + info.Description;
                        //AddinstanceWS objaddinst = new AddinstanceWS();
                        //string userid = objaddinst.AddInstance(Email, Utilities.AppID, _LocalInstanceID, HttpContext.Current.User.Identity.Name);
                        //Info info = CreateMemberWithDTF(Discussion, currMember, member, userid, Email, Title, Alias, Name, MemberRole);
                        //return info.Title + ":" + info.Description;
                    }
                }
                else
                {
                    Info info = CreateMemberWithDTF(Discussion, currMember, member, member.OwnerID, Email, Title, Alias, Name, MemberRole);
                    return info.Title + ":" + info.Description + ":" + member.MemberID;
                }
            }
            else
            {
                return "Error:No Discussion Found!";
            }
        }
        private Info CreateMemberWithDTF(MyCCM.Discussion discussion, Member currMember, Member member, string userid, string email, string title, string alias, string name, MemberRoles MemberRole)
        {
            Info info;
            if (member == null)
            {
                member = new Member()
                {
                    OwnerID = userid,
                    //ProjectID = 0,
                    InstanceID = _LocalInstanceID,
                    FirstName = name,
                    LastName = "",
                    NickName = alias,
                    Email = email,
                    Title = title,
                    //Type = MemberRoles.SuperAdmin.ToString(),
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    ImagePath = "_assests/images/" + CCM_Helper.GetMemberRoleName((int)MemberRole) + ".png"
                };
                member.Add();
            }

            //Folder mainFolder = Folder.SingleOrDefault(u => u.FolderID == hdn_folderid.Value.ToInt32());
            //MemberToFolder memberToFolder = new MemberToFolder();
            //if (mainFolder != null && (MemberRole == MemberRoles.Admin || MemberRole == MemberRoles.VendorMember || MemberRole == MemberRoles.SuperAdmin))
            //{
            //    memberToFolder = MemberToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.InstanceID == _LocalInstanceID & u.FolderID == mainFolder.FolderID);
            //    if (memberToFolder == null)
            //    {
            //        memberToFolder = new MemberToFolder()
            //        {
            //            Active = true,
            //            CrtDate = DateTime.Now,
            //            InstanceID = _LocalInstanceID,
            //            MemberID = member.MemberID,
            //            ModDate = DateTime.Now,
            //            ParentFolderID = 0,
            //            FolderID = mainFolder.FolderID,
            //            RoleID = GetFolderRoleID(MemberRole),
            //            Title = title,
            //            Alias = alias,
            //            ApplicableTo = CCM_Helper.GetMemberRoleName(GetFolderRoleID(MemberRole))
            //        };
            //        if (String.IsNullOrEmpty(title)) { memberToFolder.Title = member.Title; }
            //        if (String.IsNullOrEmpty(alias)) { memberToFolder.Alias = member.NickName; }

            //        memberToFolder.Add();
            //    }
            //    else
            //    {
            //        // If
            //        Folder folder = Folder.SingleOrDefault(u => u.FolderID == memberToFolder.ParentFolderID);
            //        if (folder == null)
            //        {
            //            memberToFolder.ParentFolderID = 0;
            //        }
            //        memberToFolder.Update();
            //    }
            //}
            //else
            //{
            //    memberToFolder = new MemberToFolder() { FolderID = 0 };
            //}


            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.DiscussionID == discussion.DiscussionID);
            if (disToFolder == null)
            {
                disToFolder = new DiscussionToFolder()
                {
                    FolderID = 0,
                    MemberID = member.MemberID,
                    InstanceID = _LocalInstanceID,
                    DiscussionID = discussion.DiscussionID,
                    Status = 1,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    RoleID = (int)MemberRole,
                    Title = title,
                    Alias = alias,
                };
                if (MemberRole == MemberRoles.ClientMember || MemberRole == MemberRoles.ClientAdmin)
                {
                    List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == discussion.DiscussionID && u.RoleID == 5).ToList();
                    if (dtfs.Count <= 0)
                    {
                        disToFolder.RoleID = 5;
                        MemberRole = MemberRoles.ClientAdmin;
                    }
                }
                if (String.IsNullOrEmpty(title)) { disToFolder.Title = member.Title; }
                if (String.IsNullOrEmpty(alias)) { disToFolder.Alias = member.NickName; }
                disToFolder.Unread = hdnTotalComments.Value.ToInt32();
                disToFolder.Add();
                if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                {
                    info = new Info("Info", "Client added successfully!", member.MemberID.ToString());
                }
                else
                {

                    info = new Info("Info", "Team added successfully!", member.MemberID.ToString());
                }


            }
            else
            {
                if (disToFolder.RoleID != 1)
                {
                    disToFolder.RoleID = (int)MemberRole;
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    if (disToFolder.Active == false) { disToFolder.Active = true; }
                    disToFolder.Update();
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        info = new Info("Info", "Client updated successfully!", member.MemberID.ToString());
                    }
                    else
                    {

                        info = new Info("Info", "Team updated successfully!", member.MemberID.ToString());
                    }

                }
                else
                {
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    disToFolder.Update();
                    info = new Info("Info", "Superadmin updated!", member.MemberID.ToString());
                    //info = new Info("Error", "Member you are trying to assign is already Super Admin of this discussion.", member.MemberID.ToString());
                }
            }
            SendInvitationDefault(currMember, discussion, member, MemberRole);
            AddOwnerToMember(currMember, member, CCM_Helper.GetMemberRoleID(CCM_Helper.GetMemberRoleName(GetFolderRoleID(MemberRole))), email);
            return info;
        }
        private Int32 GetFolderRoleID(MemberRoles mr)
        {
            if (mr == MemberRoles.ClientMember || mr == MemberRoles.ClientAdmin)
            {
                return 4;
            }
            else if (mr == MemberRoles.Admin || mr == MemberRoles.VendorMember)
            {
                return 3;
            }
            else
            {
                return 0;
            }
        }
        private void AddOwnerToMember(Member currMember, Member member, Int32 RoleID, string email)
        {
            OwnerToMember otm = OwnerToMember.SingleOrDefault(u => u.Email.Contains(email) && u.InstanceID == _LocalInstanceID && u.RoleID == RoleID);
            if (otm == null)
            {
                otm = new OwnerToMember()
                {
                    //OwnerMemberID = currMember.MemberID,
                    OwnerMemberID = hdnMemberID.Value.ToInt32(),
                    InstanceID = _LocalInstanceID,
                    Email = email,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    MemberID = member.MemberID,
                    NameAlias = member.FirstName,
                    RoleID = RoleID
                };
                otm.Add();
            }
        }
        private void SendInvitationDefault(Member currMember, MyCCM.Discussion discussion, Member member, MemberRoles MemberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            //Response.Write("Member ID=" + currMember.MemberID + ";" + "DiscussionID=" + discussion.DiscussionID);
            //return;
            //DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == currMember.MemberID && u.DiscussionID == discussion.DiscussionID);
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == hdnMemberID.Value.ToInt32() && u.DiscussionID == discussion.DiscussionID);

            DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussion.DiscussionID && u.MemberID == member.MemberID);
            String body = "Hi " + dtfMember.Alias + " " + ", ";
            body += "<br /><br />" + currDTF.Alias + "   has added you in the following discussion as " + CCM_Helper.GetMemberRoleNewSimplifiedName((int)MemberRole);

            //if (chkSendLink.Checked)
            //{
            string url = "Discussion.aspx?did=" + discussion.DiscussionID.ToString().Encrypt() + "&fid=" + hdn_folderid.Value.Encrypt() + "&od=" + hdnOID.Value.Encrypt() + "&ir=" + dtfMember.RoleID.ToString().Encrypt() + "&md=" + member.MemberID.ToString().Encrypt() + "&instanceid=" + InstanceID;
            body += "<br /><b><a href=\"http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/" + url + "\">\"" + discussion.Title + "\"</a></b>";
            //}
            body += "<br /><br />Click the link above or copy/paste the following URL in your browser: <br />http://www.avaima.com/4a99e861-7e44-4937-a342-11fbd66f5d47/" + url + " <br />";
            //body += "This discussion uses Avaima CCM. CCM is a software application on Avaima which lets you add one or more discussions under one or more projects. Each project has team members and clients/customers. A page for discussion allows all parties to communicate with each other while maintaining privacy of everyone.";
            //if (dtfMember != null)
            //{
            //    body += "<br /><br />You have been added as a " + CCM_Helper.GetMemberRoleSimplifiedName(GetDiscussionRoleID());
            //}

            //body += "<br /><br />You are invited to Team Communication Application by " + currMember.NickName + " to participate as " + memberRole + " in discussions under project: <b>" + project.Title + ". ";
            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited project's discussions.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "You have been added in a discussion";
            //emailAPI.send_email(member.Email, currMember.FirstName + " " + currMember.LastName, "noreply@avaima.com", body, subject);
            emailAPI.send_email(member.Email, currDTF.Alias, "noreply@avaima.com", body, subject);
        }
        private static void DeleteFile(string filename, string discussionID)
        {
            string datacontainer = "data";
            string storagepath = String.Format("{0}apps/{1}/Discussion-{2}/user_data/Files/", ConfigurationManager.AppSettings.Get("UserBlobPath"), Utilities.AppID, discussionID);

            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                Microsoft.WindowsAzure.CloudStorageAccount storageAccount = new Microsoft.WindowsAzure.CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);

                CloudBlockBlob blob = container.GetBlockBlobReference(String.Format("{0}{1}", storagepath, filename));
                blob.DeleteIfExists();
            }
            catch (Exception ex)
            {

            }
        }
        private void BindExistingMembersDLL()
        {
            List<OwnerToMember> ownerToMembers = CCM_Helper.GetMembers(_LocalInstanceID, MemberRoles.All, hdnMemberID.Value.ToInt32());
            //            foreach (var item in ownerToMembers.Where(u => u.RoleID == 4).ToList())
            //            {
            //                ListItem li = new ListItem(item.NameAlias, item.MemberID.tostrin
            //);
            //                ddlClientMembers.Items.Add()
            //            }
            ddlClientMembers.DataSource = ownerToMembers.Where(u => u.RoleID == 4).ToList();
            ddlClientMembers.DataTextField = "NameAlias";
            ddlClientMembers.DataValueField = "MemberID";
            ddlClientMembers.DataBind();
            ddlTeamMembers.DataSource = ownerToMembers.Where(u => u.RoleID == 2 || u.RoleID == 3 || u.RoleID == 6 || u.RoleID == 7).ToList();
            ddlTeamMembers.DataTextField = "NameAlias";
            ddlTeamMembers.DataValueField = "MemberID";
            ddlTeamMembers.DataBind();
        }
        private void ClearTeamFields()
        {
            txtTeamAlias.Text = "";
            txtTeamEmail.Text = "";
            txtTeamTitle.Text = "";
            txtTeamName.Text = "";
        }
        private void ClearClientFields()
        {
            txtClientAlias.Text = "";
            txtClientEmail.Text = "";
            txtClientTitle.Text = "";
            txtClientName.Text = "";
        }
        private string RemoveMember(int memberid)
        {
            // Remove member from project            
            DiscussionToFolder mtf = DiscussionToFolder.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid) && u.DiscussionID == hdnDisID.Value.ToInt32());
            if (mtf.RoleID == 1)
            {
                return "Superadmin cannot be removed!";
            }
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32());
            Member member = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == hdnOID.Value && u.InstanceID == _LocalInstanceID);
            if (mtf != null)
            {
                mtf.Delete();
                DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid && u.DiscussionID == mtf.DiscussionID);
                if (disToFolder != null)
                {
                    disToFolder.Delete();
                }
                //SendMemberRemoveNotification(currMember, member, discussion, Role.SingleOrDefault(u => u.RoleID == mtf.RoleID).RoleTitle);
                //BindDiscussionList();
                return "Inform:Member Removed!";
            }
            else
            {
                return "Error:No Member Found";
            }
        }
        private static void SendMemberRemoveNotification(Member currMember, Member member, MyCCM.Discussion discussion, string memberRole)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == currMember.MemberID && u.DiscussionID == discussion.DiscussionID);
            DiscussionToFolder dtfMember = DiscussionToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.DiscussionID == discussion.DiscussionID);
            if (dtfMember == null)
            {
                dtfMember = new DiscussionToFolder()
                {
                    Title = member.Title,
                    Alias = member.NickName
                };
            }
            String body = "Hi " + dtfMember.Alias + ", ";

            // MTD
            body += "<br /><br />" + currDTF.Alias + " has removed you from the following discussion:";
            body += "<br /><br />" + discussion.Title;
            body += "<br /><br />For further details contact " + currDTF.Alias;

            //body += "<br />Please go to <a href=\"http://www.avaima.com/main/control_panel/pages/application.aspx?aid=4a99e861-7e44-4937-a342-11fbd66f5d47&oid=teamuser-1069--8587846274950237891&iid=a4841f77-2e42-46d7-904f-54f2c63cafab\">AVAIMA CCM</a> to login and participate in invited discussion.";
            body += Utilities.AvaimaEmailSignature;
            body += "";
            body += "";
            body += "";
            String subject = "You have been removed from a discussion";
            //String subject = currDTF.Alias + " has removed you from discussion: " + discussion.Title + " on " + DateTime.Now.ToString("ddd, MM yy");
            emailAPI.send_email(member.Email, currDTF.Alias, "noreply@avaima.com", body, subject);
        }
        public string GetContentType(string fileName)
        {
            var extension = Path.GetExtension(Server.MapPath(fileName));

            if (String.IsNullOrWhiteSpace(extension))
            {
                return null;
            }

            var registryKey = Registry.ClassesRoot.OpenSubKey(extension);

            if (registryKey == null)
            {
                return null;
            }

            var value = registryKey.GetValue("Content Type") as string;

            return String.IsNullOrWhiteSpace(value) ? null : value;
        }
        public String GetFileName(String FileName)
        {
            //string FileName = "This on.e has some bog file name.png";
            string name = "";
            string firstLetters = "";
            string secondLetters = "";
            string[] _fileName = FileName.Split('.');
            name = FileName.Substring(0, FileName.LastIndexOf('.')); ;
            Console.WriteLine(name);
            if (name.Length > 17)
            {
                firstLetters = name.Substring(0, 11);
                Console.WriteLine(firstLetters);
                Console.WriteLine(name.Length);
                Console.WriteLine(name.Length - 4);
                secondLetters = name.Substring(name.Length - 3, 3);
                Console.WriteLine(secondLetters);
                return firstLetters + "..." + secondLetters + FileName.Substring(FileName.LastIndexOf('.'), _fileName[_fileName.Length - 1].Length + 1);

            }
            else
            {
                return FileName;
            }
        }
        private void UploadAndSaveDisFile(Stream fileStream, DiscussionFile disFile)
        {
            string datacontainer = "data";
            try
            {
                StorageCredentialsAccountAndKey objkey = new StorageCredentialsAccountAndKey(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey"));
                Microsoft.WindowsAzure.CloudStorageAccount storageAccount = new Microsoft.WindowsAzure.CloudStorageAccount(objkey, false);
                CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(datacontainer);


                string _uploaddir = String.Format("{0}{1}", storagepath, disFile.FileName);

                CloudBlockBlob blob = container.GetBlockBlobReference(_uploaddir);
                blob.Properties.ContentType = disFile.FileType;

                BlobRequestOptions objoption = new BlobRequestOptions();
                objoption.Timeout = TimeSpan.FromHours(1);

                //f.InputStream.Position = 0;
                blob.UploadFromStream(fileStream, objoption);
                disFile.Add();
            }
            catch (Exception ex) { }
        }

        #endregion

        public static void SaveViaEmailOrWeb(string email, string memberId, string roleId,
            string dicussionId, string action, string source)
        {
            MemberDiscussionSource ms = new MemberDiscussionSource()
            {
                discussionId = dicussionId.ToInt32(),
                email = email,
                memberId = memberId.ToInt32(),
                roleId = roleId.ToInt32(),
                action = action,
                source = source
            };
            ms.Add();
        }

        public static void SendEmail(string memberdisid, string discussionid,
            string memberid, string conversation, string subject, string folderid, int minutes, bool isUpdate, string messageid, bool subtracthours, string subtractReason)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            AvaimaTimeZoneAPI timeAPI = new AvaimaTimeZoneAPI();
            Member senderMember = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid.ToInt32() && u.DiscussionID == discussionid.ToInt32());
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == discussionid.ToInt32()).ToList();
            List<DiscussionFile> files = DiscussionFile.Find(u => u.MemberDisID == memberdisid).ToList();
            Project project = Project.SingleOrDefault(u => u.ProjectID == discussion.ProjectID);
            string strhours = "";
            int srole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), senderMember.MemberID);
            if (discussion != null)
            {
                discussion.ModDate = DateTime.Now;
                discussion.Update();
            }
            if (project == null)
            {
                project = new Project()
                {
                    ProjectID = 0,
                    Title = "Discussion Update"
                };
            }
            foreach (DiscussionToFolder item in mtds)
            {
                Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                if (member != null)
                {
                    int mrole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), item.MemberID.ToInt32());
                    if (srole != 4 && srole != 5)
                    {
                        if (discussion.HideHours == null)
                        {
                            discussion.HideHours = false;
                        }
                        if (discussion.isHourly == true && (discussion.HideHours == false || (discussion.HideHours == true && (mrole != 4 && mrole != 5))))
                        {
                            string hour = (Convert.ToDecimal(minutes) / 60).ToString().Split('.')[0].ToString();
                            Int16 minutes1 = Convert.ToInt16(minutes % 60);

                            if (subtracthours)
                            {
                                strhours = "<span style='color:rgb(221, 36, 36);'>Time Subtracted= - " + hour + " Hours " + minutes1.ToString() + " Minutes </span>";
                            }
                            else
                            {
                                strhours = "Total Time spent= " + hour + " Hours " + minutes1.ToString() + " Minutes ";
                            }
                        }
                        else
                        {
                            strhours = "";
                        }

                    }
                    StringBuilder strbody = new StringBuilder();
                    String url = CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString());
                    //strbody.Append("Write ABOVE THIS LINE to post a reply through email OR reply on the <a href='" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "'>discussion page</a> <br/>");
                    //strbody.Append("Do not change anything in the subject of this email");
                    strbody.Append("<div style=\"padding:8px 12px 6px 12px;background: whitesmoke;\">");
                    strbody.Append("<h1 style=\"font-weight:normal\">" + discussion.Title + "<br /><span style=\"font-size:12px\">Comment by <b>" + currDTF.Alias + "</b> - " + timeAPI.GetDate(DateTime.Now.ToShortDateString(), member.OwnerID) + " at " + Utilities.GetUserDateWithoutToday(DateTime.Now.ToShortTimeString(), member.OwnerID) + " - Message ID: " + messageid + "</span></h1>For complete details visit the<a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\"> discussion page</a><br />");
                    strbody.Append("<br />");
                    strbody.Append("<h5 style=\"margin-top:9px;padding: 14px;background: #EFC;font-size: 12px;font-weight: normal;margin: 10px -10px !important;margin-left:-12px \">");
                    if (files.Count > 0)
                    {
                        strbody.Append("<b>File attachments:</b><br />");
                        int ifiles = 0;
                        foreach (var file in files)
                        {
                            if (Path.GetExtension(file.FileName) == ".pdf")
                            {
                                strbody.AppendLine("<a href=\"" + file.FilePath + "\" download=\"" + file.FileName + "\">" + file.FileName + "</a>");
                            }
                            else
                            {
                                strbody.AppendLine("<a href=\"" + file.FilePath + "\">" + file.FileName + "</a>");
                            }
                            ifiles++;
                            if (ifiles != files.Count)
                            {
                                strbody.Append(" - ");
                            }
                        }
                        strbody.Append("<br /><hr />" + conversation);
                    }
                    else if (subtracthours)
                    {
                        strbody.Append(conversation + subtractReason);
                    }
                    else
                    {
                        strbody.Append(conversation);
                    }
                    if (discussion.isHourly == true)
                    {
                        strbody.Append("<br/>" + strhours);
                    }
                    strbody.AppendLine("</h5>");
                    strbody.AppendLine("For complete details visit the <a href=\"" + CCM_Helper.GetDiscussionURL(discussion, folderid, member.OwnerID, member.MemberID.ToString()) + "\">Discussion Page</a>");
                    strbody.AppendLine("</div>");
                    strbody.AppendLine(Utilities.AvaimaEmailSignature);
                    if (isUpdate == true)
                    {
                        subject = "Message updated on " + discussion.Title;
                    }
                    else
                    {
                        subject = discussion.Title;
                    }
                    emailAPI.send_email(member.Email, currDTF.Alias, "pcm-71579@avaima.com", strbody.ToString(), subject + " [ID:" + discussionid + "]");
                    if (item.Unread == null)
                    {
                        item.Unread = 1;
                    }
                    else
                    {
                        item.Unread += 1;
                    }
                    item.Update();
                }
            }
        }
        public static string UpdateDiscussionFile(string memberDisID, string updatedMemberDisID)
        {
            try
            {
                string str = "0";
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                foreach (DiscussionFile item in disFiles)
                {
                    item.MemberDisID = updatedMemberDisID;
                    item.Update();
                    str = "1";
                }
                return "Info:Updated:" + str;
            }
            catch (Exception ex)
            {

                return "Error:An error occured " + ex.Message;
            }
        }


        public static void SendNotesEmail(string discussionid, string memberid, string notes, string subject, List<int> excludeRoleIds)
        {
            AvaimaEmailAPI emailAPI = new AvaimaEmailAPI();
            AvaimaTimeZoneAPI timeAPI = new AvaimaTimeZoneAPI();
            Member senderMember = Member.SingleOrDefault(u => u.MemberID == Convert.ToInt32(memberid));
            DiscussionToFolder currDTF = DiscussionToFolder.SingleOrDefault(u => u.MemberID == memberid.ToInt32() && u.DiscussionID == discussionid.ToInt32());
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            List<DiscussionToFolder> mtds = DiscussionToFolder.Find(u => u.DiscussionID == discussionid.ToInt32()).ToList();
            Project project = Project.SingleOrDefault(u => u.ProjectID == discussion.ProjectID);
            string Subject = "";

            int srole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), senderMember.MemberID);
            if (discussion != null)
            {
                discussion.ModDate = DateTime.Now;
                discussion.Update();
            }
            if (project == null)
            {
                project = new Project()
                {
                    ProjectID = 0,
                    Title = "Discussion Update"
                };
            }
            foreach (DiscussionToFolder item in mtds)
            {
                if (!excludeRoleIds.Contains(item.RoleID.ToInt32()) && (item.Active == true))
                {
                    Member member = Member.SingleOrDefault(u => u.MemberID == item.MemberID);
                    if (member != null)
                    {
                        int mrole = CCMDiscussion.GetDiscussionMemberRoleID(discussionid.ToInt32(), item.MemberID.ToInt32());
                        StringBuilder strbody = new StringBuilder();
                        String url = CCM_Helper.GetDiscussionURL(discussion, "0", member.OwnerID, member.MemberID.ToString());
                        //strbody.Append("Write ABOVE THIS LINE to post a reply through email OR reply on the <a href='" + CCM_Helper.GetDiscussionURL(discussion, "0", member.OwnerID, member.MemberID.ToString()) + "'>discussion page</a> <br/>");
                        //strbody.Append("Do not change anything in the subject of this email");
                        strbody.Append("<div style=\"padding:8px 12px 6px 12px;background: whitesmoke;\">");
                        strbody.Append("<h1 style=\"font-weight:normal\">" + discussion.Title + "<br /><span style=\"font-size:12px\">Note by <b>" + currDTF.Alias + "</b> - " + timeAPI.GetDate(DateTime.Now.ToShortDateString(), member.OwnerID) + " at " + Utilities.GetUserDateWithoutToday(DateTime.Now.ToShortTimeString(), member.OwnerID) + "</span></h1>For complete details visit the<a href=\"" + CCM_Helper.GetDiscussionURL(discussion, "0", member.OwnerID, member.MemberID.ToString()) + "\"> discussion page</a><br />");
                        strbody.Append("<br />");
                        strbody.Append("<h5 style=\"margin-top:9px;padding: 14px;background: #EFC;font-size: 12px;font-weight: normal;margin: 10px -10px !important;margin-left:-12px \">");

                        strbody.Append(notes);

                        strbody.AppendLine("</h5>");
                        strbody.AppendLine("For complete details visit the <a href=\"" + CCM_Helper.GetDiscussionURL(discussion, "0", member.OwnerID, member.MemberID.ToString()) + "\">Discussion Page</a>");
                        strbody.AppendLine("</div>");
                        strbody.AppendLine(Utilities.AvaimaEmailSignature);

                        Subject = subject + " on " + discussion.Title;

                        emailAPI.send_email(member.Email, currDTF.Alias, "pcm-71579@avaima.com", strbody.ToString(), Subject + " [ID:" + discussionid + "]");
                        if (item.Unread == null)
                        {
                            item.Unread = 1;
                        }
                        else
                        {
                            item.Unread += 1;
                        }
                        item.Update();
                    }
                }
            }
        }

        public static List<int> ExcludeRoleIdCheckForNotes(bool notifyClient)
        {
            List<int> RoldIds = new List<int>();

            if (notifyClient)
            {
                RoldIds.Add(0);
            }
            else
            {
                RoldIds.Add(4);
                RoldIds.Add(5);
            }

            return RoldIds;
        }

        #region ---- WEB METHODS ----
        [WebMethod]
        public static string SaveDiscussion(string discussionid, string title, string projectid, string instanceid, string status, string memberid)
        {
            MyCCM.Discussion dis = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == Convert.ToInt32(discussionid));
            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32() && u.MemberID == memberid.ToInt32());
            if (disToFolder.RoleID == 1 || disToFolder.RoleID == 2 || disToFolder.RoleID == 5)
            {
                if (dis != null)
                {
                    dis.Title = title;
                    dis.ModDate = DateTime.Now;
                    dis.Update();
                    return "Info:Discussion updated:" + dis.DiscussionID;
                }
                else
                {
                    return "Error:No DIscussion found:" + dis.DiscussionID;
                }
            }
            else
            {
                return "Error:You are not allowed to edit discussion information:" + dis.DiscussionID;
            }
        }
        /*                 Post Comment by Email ID     */
        [WebMethod]
        public static string SaveMemberDiscussionByGmail(string memdisid, string discussionid, string email,
              string conversation, string subject, string tags, string strminutes, string Summary)
        {
            string folderid = "0";
            string active = "true";
            string teammember = "";
            int fmemberid = 0;
            int roleID = 0;
            IList<DiscussionToFolder> disToFolder = DiscussionToFolder.Find(u => u.DiscussionID == discussionid.ToInt32());
            for (int i = 0; i < disToFolder.Count; i++)
            {
                Member memb = Member.SingleOrDefault(u => u.Email == email && u.MemberID == disToFolder[i].MemberID);
                if (memb != null)
                {
                    fmemberid = memb.MemberID;
                    roleID = disToFolder[i].RoleID.ToInt32();
                }
            }
            string instanceid = disToFolder[0].InstanceID;
            switch (roleID)
            {
                case 1:
                    teammember = "true";
                    break;
                case 2:
                    teammember = "true";
                    break;
                case 3:
                    teammember = "true";
                    break;
                case 4:
                    teammember = "false";
                    break;
                case 7:
                    teammember = "true";
                    break;
                default:
                    teammember = "false";
                    break;
            }
            int minutes = 0;
            if (strminutes != null && strminutes != "" && strminutes.ToLower() != "null")
            { minutes = Convert.ToInt16(strminutes); }
            if (tags == null || tags == "null")
            {
                tags = "";
            }
            Role role = CCM_Helper.GetRole(Convert.ToInt32(fmemberid), Convert.ToInt32(discussionid), Convert.ToInt32(folderid), instanceid);
            if (role.RoleID == 0)
            {
                return "Error:Member has no role defined under currect discussion:" + fmemberid;
            }
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());

            md.MemberDis = new App_Code.MemberDiscussions()
            {
                DiscussionID = discussionid,
                PartitionKey = instanceid,
                Active = active,
                Conversation = conversation.Replace('\n', ' '),
                CrtDate = DateTime.Now,
                MemberID = fmemberid.ToString(),
                ModDate = DateTime.Now,
                RoleID = role.RoleID,
                TeamMember = teammember,
                Tags = tags,
                Minutes = minutes,
                Summary = Summary
            };
            discussion.ModDate = DateTime.Now;
            md.MemberDis.Deleted = false;
            discussion.Update();
            md.Add();
            string newmemberDis = md.MemberDis.RowKey;
            SendEmail(newmemberDis, discussionid, fmemberid.ToString(), conversation, subject, folderid, 0, false, "-1", false, "");
            // Discussion.SendEmail(newmemberDis, discussionid, fmemberid.ToString(), conversation, subject, folderid, 0);
            SaveViaEmailOrWeb(email, fmemberid.ToString(), role.RoleID.ToString(), discussionid, "reply", "email");
            return "Info:MemberDiscussion Created:";
        }
        /*                  End     */
        [WebMethod]
        public static string SaveMemberDiscussion(string memdisid, string notes, string tags, string discussionid,
            string instanceid, string memberid, string teammember, string conversation, string active,
            string folderid, string subject, string strminutes, string Summary, string FileDMId, string messageid, bool subtracthours, string subtractReason)
        {
            Member memb = Member.SingleOrDefault(u => u.MemberID == memberid.ToInt32());

            int minutes = 0;
            if (conversation != "updatetagonly")
            {
                minutes = Convert.ToInt16(strminutes);
            }
            Role role = CCM_Helper.GetRole(Convert.ToInt32(memberid), Convert.ToInt32(discussionid), Convert.ToInt32(folderid), instanceid);
            if (role.RoleID == 0)
            {
                return "Error:Member has no role defined under currect discussion:" + memberid;
            }
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            MyCCM.Discussion discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());

            if (md.MemberDis != null)
            {
                string fileUpdate = "0";
                if (conversation == "updatetagonly")
                { md.MemberDis.Tags = tags; md.Update(); }
                else
                {
                    md.MemberDis.DiscussionID = discussionid;
                    md.MemberDis.TeamMember = teammember;
                    md.MemberDis.Conversation = conversation.Replace('\n', ' ');
                    md.MemberDis.ModDate = DateTime.Now;
                    md.MemberDis.RoleID = role.RoleID;
                    md.MemberDis.Deleted = false;
                    md.MemberDis.Tags = tags;
                    md.MemberDis.Summary = Summary;
                    md.MemberDis.SubtractedHours = subtracthours;
                    md.MemberDis.SubtractHrsReason = subtractReason;
                    if (role.RoleID != 4 && role.RoleID != 5 && role.RoleID != 6 && role.RoleID != 7)
                    {
                        md.MemberDis.Minutes = minutes;
                    }
                    md.Update();
                    discussion.ModDate = DateTime.Now;
                    discussion.Update();
                    SaveViaEmailOrWeb(memb.Email, memberid, role.RoleID.ToString(), discussionid, "editreply", "web");
                    fileUpdate = UpdateDiscussionFile(FileDMId, md.MemberDis.RowKey);
                    SendEmail(memdisid, discussionid, memberid, conversation, subject, folderid, minutes, true, messageid, subtracthours, subtractReason);

                }
                return "Info:MemberDiscussion updated::" + fileUpdate;
            }
            else
            {
                string fileUpdate = "";
                if (conversation != "updatetagonly")
                {
                    md.MemberDis = new App_Code.MemberDiscussions()
                    {
                        DiscussionID = discussionid,
                        PartitionKey = instanceid,
                        Active = active,
                        Conversation = conversation.Replace('\n', ' '),
                        CrtDate = DateTime.Now,
                        MemberID = memberid,
                        ModDate = DateTime.Now,
                        RoleID = role.RoleID,
                        TeamMember = teammember,
                        Tags = tags,
                        Minutes = minutes,
                        Summary = Summary,
                        SubtractedHours = subtracthours,
                        SubtractHrsReason = subtractReason
                    };
                    discussion.ModDate = DateTime.Now;
                    md.MemberDis.Deleted = false;
                    discussion.Update();
                    md.Add();
                    HttpContext.Current.Session.Add("memDisID", md.MemberDis.RowKey);
                    SaveViaEmailOrWeb(memb.Email, memberid.ToString(), role.RoleID.ToString(), discussionid, "reply", "web");
                    fileUpdate = UpdateDiscussionFile(FileDMId, md.MemberDis.RowKey);
                    if (messageid == "undefined" || messageid == null)
                    {
                        messageid = "0";
                    }
                    messageid = (Convert.ToInt32(messageid) + 1).ToString();
                    SendEmail(md.MemberDis.RowKey, discussionid, memberid, conversation, subject, folderid, minutes, false, messageid, subtracthours, subtractReason);
                }
                return "Info:MemberDiscussion Created:" + md.MemberDis.RowKey + ":" + fileUpdate;

            }
        }

        [WebMethod]
        public static string SaveMemberNotes(string notes, string discussionid,
            string instanceid, string memberid, string teammember, string active, string isnew, string NoteID, bool notifyTeam, bool notifyClient, bool reminder)
        {
            MemberNote note = MemberNote.SingleOrDefault(u => u.DiscussionID == discussionid.ToInt32());
            string Result = "";
            //"a4841f77-2e42-46d7-904f-54f2c63cafab"

            if (isnew == "true")
            {
                MemberNote AddNote = new MemberNote();
                AddNote.Active = true;
                AddNote.CrtDate = DateTime.Now;
                AddNote.Notes = notes;
                AddNote.DiscussionID = discussionid.ToInt32();
                AddNote.InstanceID = instanceid;
                AddNote.MemberID = memberid.ToInt32();
                AddNote.Remind = reminder;
                AddNote.Add();
                Result = "Note Added";
            }
            if (isnew == "false")
            {
                MemberNote UpdateNote = MemberNote.SingleOrDefault(u => u.NotesID == NoteID.ToInt32());

                UpdateNote.Active = true;
                UpdateNote.ModDate = DateTime.Now;
                UpdateNote.Notes = notes;
                UpdateNote.DiscussionID = discussionid.ToInt32();
                UpdateNote.InstanceID = instanceid;
                UpdateNote.MemberID = memberid.ToInt32();
                UpdateNote.Remind = reminder;
                UpdateNote.Update();
                Result = "Note Updated";
            }

            if (notifyTeam == true || notifyClient == true)
            {
                List<DiscussionToFolder> dtf = DiscussionToFolder.Find(d => d.DiscussionID == discussionid.ToInt32() && d.InstanceID == instanceid).ToList();

                SendNotesEmail(discussionid, memberid, notes, Result, ExcludeRoleIdCheckForNotes(notifyClient));
            }

            return Result;
        }

        [WebMethod]
        public static string DeleteMemberNotes(string NoteID, string discussionid, string memberid, string notes, bool notifyTeam, bool notifyClient)
        {
            string Result = "";
            MemberNote Note = MemberNote.SingleOrDefault(u => u.NotesID == NoteID.ToInt32());

            if (Note != null)
            {
                Note.Delete();
                Result = "Note Deleted";
            }
            if (notifyTeam || notifyClient)
            {
                SendNotesEmail(discussionid, memberid, notes, Result, ExcludeRoleIdCheckForNotes(notifyClient));
            }

            return Result;
        }

        [WebMethod]
        public static string GetReminderNotes(string discussionid)
        {
            List<MemberNote> notes = MemberNote.Find(u => u.Remind == true && u.DiscussionID == discussionid.ToInt32()).ToList();
            if (notes.Count > 0)
            {
                string list = "<ul>";
                string table = "<table class='tblForm'>";
                foreach (var note in notes)
                {
                    table += "<tr><td class='formCaptionTd'><b>" + note.Notes + "  </b></td></tr>";
                    list += "<li>" + note.Notes + "</li>";
                }
                list += "</ul>";
                table += "</table>";
                return list;
            }
            else
                return "";
        }

        [WebMethod]
        public static string DeleteMemberDiscussion(string memdisid, string currmemberid, string currmembername, string oid)
        {
            AvaimaTimeZoneAPI ati = new AvaimaTimeZoneAPI();

            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.GetMemberDiscussionByID(memdisid);
            if (md.MemberDis != null)
            {
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == md.MemberDis.RowKey).ToList();
                try
                {
                    foreach (DiscussionFile item in disFiles)
                    {
                        // Delete Blob Files
                        CCM_Helper.DeleteFile(item.FileName, md.MemberDis.DiscussionID);
                        // DataBase items
                        item.Delete();
                    }
                }
                catch (Exception) { }
                // Member Discussion
                md.DeleteMD(memdisid, currmemberid.ToInt32());
                return "Inform:Delete:This message was deleted by " + currmembername.Decrypt() + " on " + Utilities.GetUserDateTime(md.MemberDis.ModDate.ToString(), oid);
                //return "Inform:Delete:This message was deleted by " + currmembername + " on " + ati.GetDate(DateTime.UtcNow.ToLongDateString(), currmemberid) + " at " + ati.GetTime(DateTime.UtcNow.ToLongTimeString(), currmemberid);
            }
            else
            {
                return "Error:No Discussion found";
            }
        }

        [WebMethod]
        public static string DeleteDiscussionFile(string disfileid)
        {
            DiscussionFile disFile = DiscussionFile.SingleOrDefault(u => u.DisFileID == disfileid.ToInt32());
            if (disFile != null)
            {
                disFile.Delete();
                return "Info:File deleted";
            }
            else
            {
                return "Info:No file found to delete";
            }
        }

        [WebMethod]
        public static string DeleteDiscussionFiles(string memberdisid)
        {
            try
            {
                List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberdisid).ToList();
                foreach (DiscussionFile item in disFiles)
                {
                    item.Delete();
                }
                return "Info:Deleted!";
            }
            catch (Exception ex)
            {
                return "Error:Not Delete " + ex.Message;
            }
        }

        [WebMethod]
        public static string[] DisplaySummary(string discussionid, string isNew)
        {
            string text1 = "";
            string text2 = "";

            var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
          new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
            var tableClient = acc.CreateCloudTableClient();
            var table = tableClient.GetTableReference("MemberDiscussions");
            //TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
            //       TableQuery.CombineFilters(
            //             TableQuery.CombineFilters(
            //   TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid),
            //   TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)),
            //  TableOperators.And, TableQuery.GenerateFilterConditionForBool("SubtractedHours", QueryComparisons.NotEqual, true)
            //   ));
            //Old Query
            TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
              TableQuery.CombineFilters(
          TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid),
          TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)
          ));
            var entities1 = table.ExecuteQuery(rangeQuery).Where(a => a.SubtractedHours == false || string.IsNullOrEmpty(Convert.ToString(a.SubtractedHours)));

            //Get Subtracted Hours            
            TableQuery<App_Code.MemberDiscussions1> rangeQuery1 = new TableQuery<App_Code.MemberDiscussions1>().Where(
                TableQuery.CombineFilters(
                     TableQuery.CombineFilters(
            TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid),
            TableOperators.And, TableQuery.GenerateFilterConditionForBool("SubtractedHours", QueryComparisons.Equal, true)),
                     TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true))
            );
            var resultSet = table.ExecuteQuery(rangeQuery1);
            int totalmin = resultSet.Sum(x => x.Minutes);
            string subHour = (Convert.ToDecimal(totalmin) / 60).ToString().Split('.')[0].ToString();
            Int16 subMminutes = Convert.ToInt16(totalmin % 60);

            //Added Hours Calc
            int totalminutes = entities1.Sum(x => x.Minutes);
            string totalHour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
            Int16 totalMinutes = Convert.ToInt16(totalminutes % 60);


            if (resultSet.Count() > 0)
            {
                text1 = " Total Time Subtracted: " + subHour + " Hrs " + subMminutes.ToString() + " Mins)";

                //Total Time Added
                text1 = " (Total Time Added: " + totalHour + " Hrs " + totalMinutes.ToString() + " Mins , " + text1;

                //Net Time Spent
                int netTotalminutes = totalminutes - totalmin;
                string netHour = (Convert.ToDecimal(netTotalminutes) / 60).ToString().Split('.')[0].ToString();
                Int16 netMinutes = Convert.ToInt16(netTotalminutes % 60);
                text2 = " Net Time: " + netHour + " Hours " + netMinutes.ToString() + " Minutes ";

            }
            else
            {
                //  text1 = " Total Time Subtracted: 0 Hrs 0 Mins)";
                text2 = " Total Time spent: " + totalHour + " Hours " + totalMinutes.ToString() + " Minutes " + text1;
            }


            string[] result = new string[2];
            result[0] = text1;
            result[1] = text2;

            return result;
        }

        //[WebMethod]
        //public static string DisplaySummary(string discussionid)
        //{
        //    var acc = new Microsoft.WindowsAzure.Storage.CloudStorageAccount(
        //  new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(ConfigurationManager.AppSettings.Get("storageacct"), ConfigurationManager.AppSettings.Get("storagekey")), false);
        //    var tableClient = acc.CreateCloudTableClient();
        //    var table = tableClient.GetTableReference("MemberDiscussions");
        //    TableQuery<App_Code.MemberDiscussions1> rangeQuery = new TableQuery<App_Code.MemberDiscussions1>().Where(
        //        TableQuery.CombineFilters(
        //    TableQuery.GenerateFilterCondition("DiscussionID", QueryComparisons.Equal, discussionid),
        //    TableOperators.And, TableQuery.GenerateFilterConditionForBool("Deleted", QueryComparisons.NotEqual, true)));
        //    var entities1 = table.ExecuteQuery(rangeQuery);
        //    int totalminutes = entities1.Sum(x => x.Minutes);
        //    string hour = (Convert.ToDecimal(totalminutes) / 60).ToString().Split('.')[0].ToString();
        //    Int16 minutes = Convert.ToInt16(totalminutes % 60);
        //    return "Total Time spent: " + hour + " Hours " + minutes.ToString() + " Minutes ";

        //}
        /* Create Member Ajax Call Methods */
        [WebMethod]
        public static string AddNewMember(string sender,
          string discussionId, string ownerID, string memberId,
          string teamName, string teamAlias, string teamTitle, string
          teamEmail, string admin, string hdnSelClientID, string hdnSelTeamID, string totalComments)
        {
            string instanceID = _LocalInstanceID;
            string response = "";
            MemberRoles mr;
            /*if (sender == "Client")
            {
                mr = MemberRoles.ClientMember;
                response = AddMemberW(mr);
                if (response.Split(':')[0].Contains("Info"))
                {
                    //lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    //lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Green;
                    //ClearClientFields();
                    //hdnSelClientID.Value = "0";
                }
                else if (response.Split(':')[0].Contains("Error"))
                {
                   // lblInfoExClient.Text = lblInfoClient.Text = response.Split(':')[1];
                    //lblInfoExClient.ForeColor = lblInfoClient.ForeColor = System.Drawing.Color.Red;
                }
                BindExistingMembersDLL();
                FillMembers();
                BindDiscussionData();
            }
            else*/
            if (sender.ToLower() == "Team".ToLower())
            {
                if (admin.ToBoolean() == true)
                {
                    mr = MemberRoles.Admin;
                }
                else
                {
                    mr = MemberRoles.VendorMember;
                }
                response = AddMemberW(mr, discussionId, ownerID, instanceID, memberId,
                    teamName, teamAlias, teamTitle, teamEmail, admin, hdnSelClientID, hdnSelTeamID, totalComments);
                return response;
                /* if (response.Split(':')[0].Contains("Info"))
                 {
                     lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                     lblInfoExClient.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Green;
                     ClearTeamFields();
                     hdnSelTeamID.Value = "0";
                 }
                 else if (response.Split(':')[0].Contains("Error"))
                 {
                     lblInfoExTeam.Text = lblInfoTeam.Text = response.Split(':')[1];
                     lblInfoExTeam.ForeColor = lblInfoTeam.ForeColor = System.Drawing.Color.Red;
                 }*/
                //  BindExistingMembersDLL();
                // FillMembers();
                //BindDiscussionData();
                //divManageTeam.Style.Clear();
            }
            return response;

        }
        public static string AddMemberW(MemberRoles MemberRole,
          string discussionId, string ownerID, string instanceId, string memberId,
          string teamName, string teamAlias, string teamTitle, string
          teamEmail, string admin, string hdnSelClientID, string hdnSelTeamID, string totalComments)
        {
            int discussionID = discussionId.ToInt32();
            Int32 MemberID = 0;
            String Alias = "", Title = "", Email = "", Name = "";
            if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
            {
                MemberID = hdnSelClientID.ToInt32();
                Alias = teamAlias;
                Title = teamTitle;
                Email = teamEmail;
                Name = teamName;
            }
            else
            {
                MemberID = hdnSelTeamID.ToInt32();
                Alias = teamAlias;
                Title = teamTitle;
                Email = teamEmail;
                Name = teamName;
            }
            Member currMember = Member.SingleOrDefault(u => u.OwnerID == ownerID && u.InstanceID == instanceId
                && u.MemberID == memberId.ToInt32());
            MyCCM.Discussion Discussion = MyCCM.Discussion.SingleOrDefault(u => u.DiscussionID == discussionID);
            if (discussionID > 0)
            {
                Member member = Member.SingleOrDefault(u => u.Email.Contains(Email) && u.InstanceID == instanceId);
                if (member == null)
                {
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        string newOwnerID = "clientuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (newOwnerID.Length > 36) { newOwnerID = newOwnerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTFW(Discussion, currMember, member, instanceId, newOwnerID,
                            Email, Title, Alias, Name, MemberRole, memberId, totalComments);
                        return info.Title + ":" + info.Description;
                    }
                    else
                    {
                        string newOwnerID = "teamuser-" + Discussion.DiscussionID + "-" + DateTime.Now.ToBinary();
                        if (newOwnerID.Length > 36) { newOwnerID = newOwnerID.Substring(0, 36); }
                        Info info = CreateMemberWithDTFW(Discussion, currMember, member, instanceId, newOwnerID, Email,
                            Title, Alias, Name, MemberRole, memberId, totalComments);
                        return info.Title + ":" + info.Description;
                    }
                }
                else
                {
                    Info info = CreateMemberWithDTFW(Discussion, currMember, member, instanceId, member.OwnerID
                        , Email, Title, Alias, Name, MemberRole, memberId, totalComments);
                    return info.Title + ":" + info.Description + ":" + member.MemberID;
                }
            }
            else
            {
                return "Error:No Discussion Found!";
            }
        }
        public static Info CreateMemberWithDTFW(MyCCM.Discussion discussion, Member currMember, Member member,
            string instanceId,
            string userid, string email, string title, string alias, string name, MemberRoles MemberRole, string OwnerMemberId,
            string totalComments)
        {
            Info info;
            if (member == null)
            {
                member = new Member()
                {
                    OwnerID = userid,
                    InstanceID = instanceId,
                    FirstName = name,
                    LastName = "",
                    NickName = alias,
                    Email = email,
                    Title = title,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    Active = true,
                    ImagePath = "_assests/images/" + CCM_Helper.GetMemberRoleName((int)MemberRole) + ".png"
                };
                member.Add();
            }
            DiscussionToFolder disToFolder = DiscussionToFolder.SingleOrDefault(u => u.MemberID == member.MemberID && u.DiscussionID == discussion.DiscussionID);
            if (disToFolder == null)
            {
                disToFolder = new DiscussionToFolder()
                {
                    FolderID = 0,
                    MemberID = member.MemberID,
                    InstanceID = instanceId,
                    DiscussionID = discussion.DiscussionID,
                    Status = 1,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    RoleID = (int)MemberRole,
                    Title = title,
                    Alias = alias,
                };
                if (MemberRole == MemberRoles.ClientMember || MemberRole == MemberRoles.ClientAdmin)
                {
                    List<DiscussionToFolder> dtfs = DiscussionToFolder.Find(u => u.DiscussionID == discussion.DiscussionID && u.RoleID == 5).ToList();
                    if (dtfs.Count <= 0)
                    {
                        disToFolder.RoleID = 5;
                        MemberRole = MemberRoles.ClientAdmin;
                    }
                }
                if (String.IsNullOrEmpty(title)) { disToFolder.Title = member.Title; }
                if (String.IsNullOrEmpty(alias)) { disToFolder.Alias = member.NickName; }
                disToFolder.Unread = totalComments.ToInt32();
                disToFolder.Add();
                if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                {
                    info = new Info("Info", "Client added successfully!", member.MemberID.ToString());
                }
                else
                {

                    info = new Info("Info", "Team added successfully!", member.MemberID.ToString());
                }
            }
            else
            {
                if (disToFolder.RoleID != 1)
                {
                    disToFolder.RoleID = (int)MemberRole;
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    disToFolder.Update();
                    if (MemberRole == MemberRoles.ClientAdmin || MemberRole == MemberRoles.ClientMember)
                    {
                        info = new Info("Info", "Client updated successfully!", member.MemberID.ToString());
                    }
                    else
                    {
                        info = new Info("Info", "Team updated successfully!", member.MemberID.ToString());
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(title)) { disToFolder.Title = title; }
                    else { disToFolder.Title = member.Title; }
                    if (!String.IsNullOrEmpty(alias)) { disToFolder.Alias = alias; }
                    else { disToFolder.Alias = member.NickName; }
                    disToFolder.Update();
                    info = new Info("Info", "Superadmin updated!", member.MemberID.ToString());
                }
            }
            //SendInvitationDefault(currMember, discussion, member, MemberRole);
            int FolderRoleID = 0;
            if (MemberRole == MemberRoles.ClientMember || MemberRole == MemberRoles.ClientAdmin)
            {
                FolderRoleID = 4;
            }
            else if (MemberRole == MemberRoles.Admin || MemberRole == MemberRoles.VendorMember)
            {
                FolderRoleID = 3;
            }
            else
            {
                FolderRoleID = 0;
            }
            int RoleID = CCM_Helper.GetMemberRoleID(CCM_Helper.GetMemberRoleName(FolderRoleID));
            // Add Owner To Member \
            OwnerToMember otm = OwnerToMember.SingleOrDefault(u => u.Email.Contains(email) && u.InstanceID == instanceId
                && u.RoleID == RoleID);
            if (otm == null)
            {
                otm = new OwnerToMember()
                {
                    OwnerMemberID = OwnerMemberId.ToInt32(),
                    InstanceID = instanceId,
                    Email = email,
                    Active = true,
                    CrtDate = DateTime.Now,
                    ModDate = DateTime.Now,
                    MemberID = member.MemberID,
                    NameAlias = member.FirstName,
                    RoleID = RoleID
                };
                otm.Add();
            }
            return info;
        }
        /* End --- Create Member Ajax Call Methods */
        #endregion

        [WebMethod]
        public static string GetMemberDiscussionFiles(string memberDisID)
        {
            string str = "";
            List<DiscussionFile> disFiles1 = DiscussionFile.Find(u => u.MemberDisID == memberDisID.ToString()).ToList();
            if (disFiles1.Count > 0)
            {
                //str="<ul style='padding: 0px; margin: 0px;' class='CommentFiles'>";
                for (int i = 0; i < disFiles1.Count; i++)
                {
                    str += "<li class='msgAttachment'><i class='fa fa-link'></i>&nbsp;&nbsp;<a target='_blank' href='" + disFiles1[i].FilePath.ToString() + "' id='lnkFileName' class='anchorFileName' runat='server'>" + disFiles1[i].FileName.ToString() + "</a>&nbsp;&nbsp; <i style='color: #063 !important' class=''></i><a class='fa fa-download' target='_blank' style='color: #063 !important' download='" + disFiles1[i].FileName.ToString() + "' href='" + disFiles1[i].FilePath.ToString() + "' id='A1' runat='server'></a></li>";
                }
                //str += "</ul>";
                return str;
            }
            else { return str; }
        }
        protected void rptTags_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item)
            //{
            //    HtmlContainerControl aTag = e.Item.FindControl("aTag") as HtmlContainerControl;
            //    aTag.InnerText = DataBinder.Eval(e.Item.DataItem, "");
            //}
        }

        

        /* protected void btnUpdateName_Click(object sender, EventArgs e)
         {
             string title = txtDisName.Text;
             bool chIsHourly = chkIsHourly.Checked;
             string discussID = hdnDisID.Value;
         }*/



        //protected void btnSetAllTrue_Click(object sender, EventArgs e)
        //{
        //    CCM.App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
        //    md.GetAllNullDelete();
        //    foreach (MemberDiscussions item in md.MemberDiscussions)
        //    {
        //        item.Deleted = false;
        //    }

        //    md.Update();
        //}
    }
}