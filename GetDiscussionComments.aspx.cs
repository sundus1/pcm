﻿using CCM.App_Code;
using MyCCM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CCM
{
    public partial class GetDiscussionComments : System.Web.UI.Page
    {
        public String GetFileName(String FileName)
        {
            //string FileName = "This on.e has some bog file name.png";
            string name = "";
            string firstLetters = "";
            string secondLetters = "";
            string[] _fileName = FileName.Split('.');
            name = FileName.Substring(0, FileName.LastIndexOf('.')); ;
            Console.WriteLine(name);
            if (name.Length > 17)
            {
                firstLetters = name.Substring(0, 11);
                Console.WriteLine(firstLetters);
                Console.WriteLine(name.Length);
                Console.WriteLine(name.Length - 4);
                secondLetters = name.Substring(name.Length - 3, 3);
                Console.WriteLine(secondLetters);
                return firstLetters + "..." + secondLetters + FileName.Substring(FileName.LastIndexOf('.'), _fileName[_fileName.Length - 1].Length + 1);

            }
            else
            {
                return FileName;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["iid"] != null)
                {
                    hdnInstanceID.Value = Request.QueryString["iid"].ToString();
                }
                if (Request.QueryString["mid"] != null)
                {
                    hdnMemberID.Value = Request.QueryString["mid"].ToString();
                }
                if (Request.QueryString["oid"] != null)
                {
                    hdnOID.Value = Request.QueryString["oid"].ToString();
                }
                if (Request.QueryString["did"] != null)
                {
                    if (Request.QueryString["loadmore"] == null)
                    { BindMemberDiscussions(Request.QueryString["did"].ToString()); }
                    else
                    { BindMemberDiscussions(Request.QueryString["did"].ToString(), Request.QueryString["loadmore"].ToString().ToInt32()); }

                }

            }
        }

        private void BindMemberDiscussions(string discussionid)
        {
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.NewMemberDiscussion();
            var disToFolder = md.GetMemberDiscussionsByDiscussion(discussionid, hdnMemberID.Value.ToInt32());
            rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.ModDate).ToList();
            rptDiscussions.DataBind();
            hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();

            int commentsToLoad;
            if (disToFolder.Unread <= 0 || disToFolder.Unread == null)
            { commentsToLoad = md.GetMemberDiscussionCountByDiscussionID(discussionid) - 3; }
            else
            { commentsToLoad = md.GetMemberDiscussionCountByDiscussionID(discussionid) - disToFolder.Unread.ToInt32(); }

            aLoadMore.InnerText = "Load previous " + commentsToLoad.ToString() + " comments";
            aLoadMore.Attributes["data-commentstoload"] = commentsToLoad.ToString();
            if (commentsToLoad <= 0)
            { divLoadMore.Visible = false; }
            else
            { divLoadMore.Visible = true; }


            disToFolder.Unread = 0;
            disToFolder.Update();

            //int totalComments = md.GetMemberDiscussionCountByDiscussionID(discussionid);


        }

        private void BindMemberDiscussions(string discussionid, int loadmore)
        {
            App_Code.MemberDiscussion md = new App_Code.MemberDiscussion();
            md.NewMemberDiscussion();
            md.GetMoreMemberDiscussionsByDiscussion(discussionid, loadmore);
            //int totalComments = md.GetMemberDiscussionCountByDiscussionID(discussionid);
            rptDiscussions.DataSource = md.MemberDiscussions.OrderBy(u => u.ModDate).ToList();
            rptDiscussions.DataBind();
            hdnTotalDisComments.Value = md.MemberDiscussions.Count.ToString();
        }


        protected void rptDiscussions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl imgMember = e.Item.FindControl("imgMember") as HtmlControl;
                HtmlControl imgMember2 = e.Item.FindControl("imgMember2") as HtmlControl;
                HtmlControl tdImage = e.Item.FindControl("tdImage") as HtmlControl;
                HtmlControl tdImage2 = e.Item.FindControl("tdImage2") as HtmlControl;
                //Panel panel = e.Item.FindControl("divMemDis") as HtmlControl
                HtmlContainerControl myDivMemDis = e.Item.FindControl("divMemDis") as HtmlContainerControl;
                HtmlContainerControl myDivMemDisO = myDivMemDis.FindControl("divMemDisO") as HtmlContainerControl;
                Label lblMemberTemp = e.Item.FindControl("lblMemberTemp") as Label;
                Label lblMemberTemp2 = e.Item.FindControl("lblMemberTemp2") as Label;
                TextBox txtMemDis = e.Item.FindControl("txtMemDis") as TextBox;
                Int32 memberID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MemberID"));
                String memberDisID = DataBinder.Eval(e.Item.DataItem, "RowKey").ToString();
                Member member = Member.SingleOrDefault(u => u.MemberID == memberID);
                //Role role = new Role();
                //if (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")) != 0)
                //{
                //    role = Role.SingleOrDefault(u => u.RoleID == Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID").ToString()));
                //}
                //else
                //{
                //    role = CCM_Helper.GetRole(member.MemberID, Convert.ToInt32(hdnDisID.Value), Convert.ToInt32(hdnProjectID.Value), _LocalInstanceID);
                //}

                if (member != null)
                {
                    MemberRoles mr = CCMDiscussion.GetDiscussionMemberRole(hdnDisID.Value.ToInt32(), member.MemberID);
                    imgMember.Attributes["src"] = imgMember2.Attributes["src"] = "_assests/Images/" + mr.ToString() + ".png";

                    //if (member.ImagePath != "")
                    //{

                    //}
                    //else
                    //{
                    //    imgMember2.Attributes["src"] = imgMember.Attributes["src"] = "_assests/Images/" + role.RoleTitle + ".png";
                    //}

                    if (DataBinder.Eval(e.Item.DataItem, "Deleted").ToBoolean())
                    {

                        //img.ID = "disAction";
                        //img.CssClass = "disAction";
                        //img.ImageUrl = "_assests/Images/downarrow.png";
                        //img.Attributes.Add("data-id", memberDisID);
                        //Label lblDateTime = new Label();
                        //lblDateTime.Attributes["class"] = "lblDateTime";
                        //lblDateTime.Style.Add("float", "right");
                        //lblDateTime.Style.Add("margin-right", "-29px");
                        //lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                        DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == memberID);
                        DiscussionToFolder dtfDeletedBy = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == DataBinder.Eval(e.Item.DataItem, "DeletedByMemberID").ToInt32());

                        if (dtf != null)
                        {
                            if (String.IsNullOrEmpty(dtf.Alias)) { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                            else { lblMemberTemp2.Text = lblMemberTemp.Text = dtf.Alias; }
                        }
                        else { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                        string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                        myDivMemDis.Attributes["class"] = "Class " + roleTitle;
                        myDivMemDis.Attributes.Add("data-role", roleTitle);
                        myDivMemDis.Attributes.Add("data-deleted-discussion", "deleted");
                        myDivMemDisO.Style.Add("padding-bottom", "5px");
                        if (dtfDeletedBy != null)
                        {
                            myDivMemDisO.InnerHtml += "This message was deleted by " + dtfDeletedBy.Alias + " on " + Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                            myDivMemDisO.InnerHtml += "</br>";    
                        }
                        else
                        {
                            myDivMemDisO.InnerHtml += "This message was deleted on " + Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                            myDivMemDisO.InnerHtml += "</br>";
                        }
                        
                        //DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                        //if (member.MemberID == Convert.ToInt32(hdnMemberID.Value)) { myDivMemDis.Controls.Add(img); }
                        //else
                        //{
                        //    if (dtfCurrMember != null)
                        //    {
                        //        if (dtfCurrMember.RoleID == 1 || dtfCurrMember.RoleID == 2)
                        //        {
                        //            myDivMemDis.Controls.Add(img);
                        //        }
                        //    }
                        //}
                        //myDivMemDis.Controls.Add(lblDateTime);
                        if (roleTitle == "ClientMember") { tdImage.Visible = false; tdImage2.Visible = true; }
                        else if (roleTitle == "VendorMember") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "Admin") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "SuperAdmin") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "ClientAdmin") { tdImage.Visible = false; tdImage2.Visible = true; }

                        //Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;
                        //List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                        //if (disFiles.Count > 0)
                        //{
                        //    rptFiles.DataSource = disFiles;
                        //    rptFiles.DataBind();
                        //}
                        //else { }
                    }
                    else
                    {
                        Image img = new Image();
                        img.ID = "disAction";
                        img.CssClass = "disAction";
                        img.ImageUrl = "_assests/Images/downarrow.png";
                        img.Attributes.Add("data-id", memberDisID);
                        Label lblDateTime = new Label();
                        lblDateTime.Attributes["class"] = "lblDateTime";
                        lblDateTime.Style.Add("float", "right");
                        lblDateTime.Style.Add("margin-right", "-29px");
                        lblDateTime.Text = Utilities.GetUserDateTime(DataBinder.Eval(e.Item.DataItem, "ModDate").ToString(), hdnOID.Value);
                        DiscussionToFolder dtf = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.InstanceID == hdnInstanceID.Value && u.MemberID == memberID);
                        if (dtf != null)
                        {
                            if (String.IsNullOrEmpty(dtf.Alias)) { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                            else { lblMemberTemp2.Text = lblMemberTemp.Text = dtf.Alias; }
                        }
                        else { lblMemberTemp2.Text = lblMemberTemp.Text = member.NickName; }
                        string roleTitle = CCM_Helper.GetMemberRoleName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RoleID")));
                        myDivMemDis.Attributes["class"] = "Class " + roleTitle;
                        myDivMemDis.Attributes.Add("data-role", roleTitle);
                        myDivMemDisO.InnerHtml += DataBinder.Eval(e.Item.DataItem, "Conversation").ToString();
                        myDivMemDisO.InnerHtml += "</br>";
                        DiscussionToFolder dtfCurrMember = DiscussionToFolder.SingleOrDefault(u => u.DiscussionID == hdnDisID.Value.ToInt32() && u.MemberID == hdnMemberID.Value.ToInt32());
                        if (member.MemberID == Convert.ToInt32(hdnMemberID.Value)) { myDivMemDis.Controls.Add(img); }
                        else
                        {
                            if (dtfCurrMember != null)
                            {
                                if (dtfCurrMember.RoleID == 1 || dtfCurrMember.RoleID == 2)
                                {
                                    myDivMemDis.Controls.Add(img);
                                }
                            }
                        }
                        myDivMemDis.Controls.Add(lblDateTime);
                        if (roleTitle == "ClientMember") { tdImage.Visible = false; tdImage2.Visible = true; }
                        else if (roleTitle == "VendorMember") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "Admin") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "SuperAdmin") { tdImage.Visible = true; tdImage2.Visible = false; }
                        else if (roleTitle == "ClientAdmin") { tdImage.Visible = false; tdImage2.Visible = true; }

                        Repeater rptFiles = e.Item.FindControl("rptFiles") as Repeater;
                        List<DiscussionFile> disFiles = DiscussionFile.Find(u => u.MemberDisID == memberDisID).ToList();
                        if (disFiles.Count > 0)
                        {
                            rptFiles.DataSource = disFiles;
                            rptFiles.DataBind();
                        }
                        else { }
                    }

                }
            }
        }
    }
}