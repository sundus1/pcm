﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_Discussion.aspx.cs" Inherits="CCM._Discussion"  %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title id="title" runat="server">Team Communication Application</title>
    <meta name="Description" content="Team Communication Application - CCM" />
    <link rel="Shortcut Icon" type="image/x-icon" href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/images/favicon.ico" />
    <!--<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet'
        type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<%= "_discussionsa_assets/js/jquery.splitter-0.14.0.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/style.css"  %>" rel="stylesheet" />
    <script src="<%= "_assests/jQuit/js/jquery-ui.js"  %>"></script>
    <link href="<%= "_assests/jQuit/css/jquery-ui.css"  %>" rel="stylesheet" />
    <!-- Latest compiled and minified CSS -->
    <link href="_discussionsa_assets/css/bootstrap.css?version<%=GetVersionCss() %>" rel="stylesheet" />
    <!-- Latest compiled and minified JavaScript -->
    <script src="<%= "_discussionsa_assets/js/bootstrap.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/font-awesome.css"%>" rel="stylesheet" />
    <script src="<%= "_discussionsa_assets/ckeditor/ckeditor.js"  %>"></script>
    <script src="<%= "_discussionsa_assets/ckeditor/adapters/jquery.js"  %>"></script>
<%--    <link href="<%= "_discussionsa_assets/css/font-awesome.css" %>" rel="stylesheet" />--%>
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="<%= "_assests/jQuit/js/Context-Menu/src/jquery.contextMenu.css" %>" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
    <script src="_assests/Uplodify/swfobject.js"></script>
    <script src="<%= "_assests/Uplodify/jquery.uploadify.min.js"  %>"></script>
    <link href="<%= "_assests/Uplodify/uploadify.css"  %>" rel="stylesheet" />
    <%--Enscroll--%>
    <script src="<%= "_discussionsa_assets/js/enscroll-0.6.1.min.js"  %>"></script>
    <link href="<%= "_discussionsa_assets/css/Enscroll.css" %>" rel="stylesheet" />
    <script src="_discussionsa_assets/js/custom.js?version=<%=GetVersion() %>"></script>
    <%-- jQuery tag implementation --%>
    <script src="<%= "_discussionsa_assets/jQueryTag/jquery.caret.min.js"  %>"></script>
    <script src="<%= "_discussionsa_assets/jQueryTag/jquery.tag-editor.min.js" %>"></script>
    <link href="<%= "_discussionsa_assets/jQueryTag/jquery.tag-editor.css"  %>" rel="stylesheet" />

    <%--Notes Css--%>
    <link href="_discussionsa_assets/css/notes.css" rel="stylesheet" />

    <%-- jQuery Select2 --%>
    <script src="<%= "_discussionsa_assets/multiselection/select2.min.js" %>"></script>
    <link href="<%= "_discussionsa_assets/multiselection/select2.min.css" %>" rel="stylesheet" />

    <style>
        div[aria-describedby="divAskInfo"] {
            z-index: 10000 !important;
        }

        div[aria-describedby="divNoteReminder"] {
            z-index: 10000 !important;
        }

        .select2-search__field {
            width: 100% !important;
        }
    </style>
</head>
<body id="bodyDis">

    <object data="_assests/Uplodify/uploadify.swf" type="application/x-shockwave-flash" width="0" height="0" style="display:none;"> 
              
                <embed src="" quality="high" wmode="transparent" pluginspage="http://www.adobe.com/go/getflash" type="application/x-shockwave-flash" width="930" height="170" />
               
               </object>


    <form id="form1" runat="server" style="height: 100%" class="formdis">
        <div id="widget">
            <div id="foo" class="col-left-chat">
                <div id="a">
                    <div id="x">
                        <a style="display: inline-block; width: 100%;">
                            <img src="_discussionsa_assets/img/logo.png" class="img-responsive" style="margin: 20px auto 30px;" /></a>
                        <div role="tabpanel" class="tabbed-pannel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs discussionlisttab" role="tablist">
                                <li role="presentation" class="active"><a href="#open" aria-controls="home" role="tab"
                                    data-toggle="tab">
                                    <%--<i class="fa fa-check-square-o"></i>&nbsp;&nbsp;&nbsp;--%>Open&nbsp;<label id="lblOpenCount"
                                        class="lblOpenCount"></label></a> </li>
                                <li role="presentation"><a href="#closed" aria-controls="profile" role="tab" data-toggle="tab">
                                    <%--<i class="fa fa-history"></i>&nbsp;&nbsp;&nbsp;--%>Closed&nbsp;<label id="lblCloseCount"
                                        class="lblCloseCount"></label></a> </li>
                                <li role="presentation" class="plusbtnli"><a href="#plus" aria-controls="messages"
                                    role="tab" data-toggle="tab" class="plusbtn" style="margin: 5px;"><i class="fa fa-plus"></i></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style="color: #fff;">
                                <div role="tabpanel" class="tab-pane active tabopen" id="open">
                                    <div class="spacer20">
                                    </div>
                                    <div class="projects" style="margin-left: 20px">
                                        <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading...
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane tabclose" id="closed">
                                    <div class="spacer20">
                                    </div>
                                    <div class="projects" style="margin-left: 20px">
                                        <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading...
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="plus">
                                    <div class="spacer20">
                                    </div>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox runat="server" ID="txtNewDiscussionTitle" CssClass="form-control addDis txtNewDiscussionTitle"
                                                    Rows="4" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                &nbsp;&nbsp;
                                            <asp:LinkButton runat="server" ID="lnkStartNewDiscussion" class="btn btn-default addDis startDiscussion lnkStartNewDiscussion"
                                                OnClick="btnStartNewDiscussion_Click">
                                                    <i class="fa fa-check"></i>
                                                    &nbsp;&nbsp;
                                                    Start
                                            </asp:LinkButton>
                                                <a href="#" runat="server" id="aStartDiscussion" class="aStartDiscussion"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div id="bar">
                    <div class="col-md-12 chat-heading">
                        <button style="display: none" id="btnGmailReply" value="Gmail reply" name="btnGmailReply">
                        </button>
                        <h1 id="h1Title" runat="server" style="display: inline-block;">Envision Adventure
                        </h1>
                        <img id="btnEdit" runat="server" data-toggle="modal" data-target="#EditName" src="_discussionsa_assets/img/Edit.png"
                            alt="Edit" style="width: 22px; cursor: pointer" />
                        <button type="button" runat="server" id="btninformation" style="margin-right: 10px;"
                            class="information" data-toggle="tooltip" data-placement="left" title="">
                            <i class="infofont fa fa-exclamation-circle"></i>
                        </button>
                    </div>
                    <div class="clearfix">
                    </div>
                    <div id="divChats" style="display: none">
                    </div>
                    <div id="divChatArea" class="col-md-9 ChatCont" style="visibility: hidden">
                        <div class="divMD" data-temp="data-temp" style="display: none">
                        </div>
                        <div class="summary" id="SummaryBox" runat="server">
                            <h3 style="margin-bottom: 0px; display: inline">Summary</h3>
                            <a runat="server" id="report" href="Report.aspx?MemberId=<% hdnMemberID.Value.Encrypt().ToString();%>&IntanceID=<% hdninstanceid.Value.Encrypt().ToString();%>"
                                alt="Report" style="text-decoration: underline" target="_blank">View Report</a>                           
                            <br />
                            <div class="timespent">
                                <asp:Label ID="totalDisMin" runat="server" Text="0" ClientIDMode="Static"></asp:Label>
                                <asp:Label ID="totalDisSubHrs" runat="server" Text="0" ClientIDMode="Static" ForeColor="#b0a8a8"></asp:Label>
                            </div>
                        </div>

                        <div class="download" id="downloadDiv" runat="server">                           
                            <a runat="server" id="downloadProj" href="DownloadDiscussion.aspx?MemberId=<% hdnMemberID.Value.Encrypt().ToString();%>&IntanceID=<% hdninstanceid.Value.Encrypt().ToString();%>&disID=<% hdnDisID.Value.Encrypt().ToString();%>"
                                alt="Report" style="text-decoration: underline" target="_blank">Download Discussion Communication
                            </a>
                            <br />
                               <br />
                        </div>
                        
                        <textarea id="txtMemDis" class="txtMemDis" height="230"></textarea>
                        <textarea id="txtMemDisTags" class="txtMemDisTags blacktags"></textarea>
                        <div class="hours">
                            <select id="hoursSubtract" name="hoursSubtract" style="height: 28px; border-radius: 3px;" runat="server">
                                <option value="1" selected="selected">Add</option>
                                <option value="2">Subtract</option>
                            </select>
                            <input type="text" name="hours" maxlength="2" style="width: 45px" id="hours" placeholder="Hrs" />
                            <input type="text" name="minutes" maxlength="2" style="width: 45px" id="minutes"
                                placeholder="Mins" />
                            <input runat="server" type="text" name="summarywords" style="width: 240px;" id="summarywords"
                                placeholder="Few words on work you performed" />
                            <div id="divHrsSubReason" style="display: none;">
                                <asp:Label runat="server" ID="lblReason">Reason: </asp:Label>
                                <select id="HrsSubReason" style="height: 28px; border-radius: 3px;" runat="server">
                                    <option value="Discount">Discount</option>
                                    <option value="Input Correction">Input Correction</option>
                                    <option value="Other">Other</option>
                                </select>
                                <input runat="server" type="text" name="subtractionreason" style="width: 280px; display: none !important;" id="subtractionreason"
                                    placeholder="Few words on hours subtraction reason" />
                            </div>
                        </div>
                        <asp:Button ID="btnSend" CssClass="btnSend btn btn-default addDis" runat="server"
                            Style="width: 123px; float: right" Text="Add Message" />
                        <asp:FileUpload ID="fileUpload1" CssClass="fileUpload1" runat="server" Style="float: left" />
                    </div>
                    <div class="col-md-3 right-col-files">
                        <div id="discussionremindercontainer">
                            <h4>
                                <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading Reminders...</h4>
                        </div>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <div id="discussionnotescontainer">

                            <h4>
                                <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading Notes...</h4>
                        </div>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <div id="discussiontagscontainer">
                            <h4>
                                <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading Tags...</h4>
                            <%--<asp:TextBox runat="server" ID="txtFilterTags" CssClass="txtFilterTags"></asp:TextBox>--%>
                        </div>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <div id="discussionsfiles">
                            <h4>
                                <i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp;Loading Files...</h4>
                            <hr />
                        </div>
                        <%--<h4><i class="fa fa-files-o"></i>&nbsp;&nbsp;All Files <span style="font-size: 13px;">(6)</span></h4>
                        <ul class="TeamMembers">
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Envision Fl...-14.docx</a>&nbsp;&nbsp;&nbsp;<i class="fa fa-download"></i></li>
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Enviison 2.jpg&nbsp;&nbsp;&nbsp;</a><i class="fa fa-download"></i></li>
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Screen Shot... pm.png&nbsp;&nbsp;&nbsp;</a><i class="fa fa-download"></i></li>
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Envision Ad...ate.rar&nbsp;&nbsp;&nbsp;</a><i class="fa fa-download"></i></li>
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Envision Ad...ure.zip&nbsp;&nbsp;&nbsp;</a><i class="fa fa-download"></i></li>
                            <li><i class="fa fa-folder-open"></i>&nbsp;&nbsp;&nbsp;<a href="#">Envision Word Doc.docx&nbsp;&nbsp;&nbsp;</a><i class="fa fa-download"></i></li>
                        </ul>--%>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <h4>
                            <i class="fa fa-users"></i>&nbsp;&nbsp;Team Members (<asp:Label ID="lblTeamCount" runat="server"
                                Text="0"></asp:Label>) &nbsp;&nbsp; <a href="javascript:void(0)" id="aAddTeam" class="aAddTeam spanTeam"
                                    runat="server"><i style="color: #B90000" class="fa fa-plus-circle"></i></a>
                            &nbsp; <a id="aTeamSettings" class="aTeamSettings" runat="server" target="_blank" title="Manage Team Settings"><i style="color: #333" class="fa fa-cog"></i></a>
                        </h4>
                        <div id="divManageTeam" class="divManageTeam" runat="server" style="display: none">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkAddNewTeam" CssClass="chkAddNewTeam" runat="server" Text="Add New"
                                            Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr id="trExTeam" class="trExTeam" style="display: none">
                                    <td width="68">Existing:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlTeamMembers" Style="width: 100%; height: 28px; border: 1px solid #ccc; border-radius: 3px; margin: 3px 0;"
                                            CssClass="ddlTeamMembers" runat="server" multiple="multiple">
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnIDs" />
                                    </td>
                                </tr>
                                <tr class="trTeam trNewTeam trTeamName">
                                    <td style="width: 55px;">Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamName" CssClass="txtTeamName" Style="width: 100%; border: 1px solid #ccc; margin: 3px 0; border-radius: 3px; height: 26px;"
                                            runat="server"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtTeamName" EnableClientScript="true"
                                            ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="trTeam trAlias">
                                    <td style="width: 55px;">Alias:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamAlias" CssClass="txtTeamAlias" Style="width: 100%; border: 1px solid #ccc; margin: 3px 0; border-radius: 3px; height: 26px;"
                                            runat="server"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtTeamAlias" EnableClientScript="true"
                                            ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="trTeam trTitle">
                                    <td>Title:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamTitle" CssClass="txtTeamTitle" Style="width: 100%; border: 1px solid #ccc; margin: 3px 0; border-radius: 3px; height: 26px;"
                                            runat="server"></asp:TextBox>
                                        <%--&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamTitle" EnableTeamScript="true" ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>--%>
                                        <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnSelTeamID" Value="0" />
                                    </td>
                                </tr>
                                <tr class="trNewTeam trTeam">
                                    <td>Email:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTeamEmail" CssClass="txtTeamEmail" runat="server" Style="width: 100%; border: 1px solid #ccc; margin: 3px 0; border-radius: 3px; height: 26px;"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtTeamEmail" EnableClientScript="true"
                                            ValidationGroup="TeamValidation"></asp:RequiredFieldValidator>
                                        &nbsp;&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Email"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="TeamValidation"
                                        ControlToValidate="txtTeamEmail"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="AdminRights" class="AdminRights">
                                    <td></td>
                                    <td>
                                        <br />
                                        <asp:CheckBox ID="chkAdmin" runat="server" CssClass="chkAdmin" Text="Admin rights" />
                                    </td>
                                </tr>
                                <tr class="trNewTeam trTeam">
                                    <td>&nbsp;
                                    </td>
                                    <td>
                                        <!-- <input type="button" class="btnAddTeamW" value="Add" id="btnAddTeam"  style="background: #F3F3F3; border: 1px solid #ccc; border-radius: 3px; margin: 0 0 10px 0;" />-->
                                        <asp:Button ID="btnAddTeam" CssClass="btnAddTeam" Style="background: #F3F3F3; border: 1px solid #ccc; border-radius: 3px; margin: 0 0 10px 0;"
                                            Text="Add" runat="server" OnClick="btnAddMember_Click"
                                            CommandName="Team" ValidationGroup="TeamValidation" />
                                        <asp:Button ID="btnCancelExTeam" CssClass="btnCancelTeam" Style="background: #F3F3F3; border: 1px solid #ccc; border-radius: 3px; margin: 0 0 10px 0;"
                                            Text="Cancel"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr class="trExTeam trTeam" style="display: none">
                                    <td>&nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAddExTeam" Style="background: #F3F3F3; border: 1px solid #ccc; border-radius: 3px; margin: 0 0 10px 0;"
                                            CssClass="btnAddExTeam" CommandName="Team"
                                            runat="server" Text="Add" OnClick="btnAddEx_Click" />
                                        <asp:Button ID="btnCancelTeam" Style="background: #F3F3F3; border: 1px solid #ccc; border-radius: 3px; margin: 0 0 10px 0;"
                                            CssClass="btnCancelTeam" Text="Cancel"
                                            runat="server" />
                                        <asp:Label ID="lblInfoExTeam" runat="server" CssClass="lblInfo"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <asp:Label ID="lblInfoTeam" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                        <asp:Repeater ID="rptTeamMembers" runat="server" OnItemDataBound="rptTeamMembers_ItemDataBound">
                            <HeaderTemplate>
                                <ul class="TeamMembers divTeamMembers ">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li id="trTeam" title="<%# DataBinder.Eval(Container.DataItem,"FirstName") + " " + DataBinder.Eval(Container.DataItem,"LastName") + " - " + DataBinder.Eval(Container.DataItem,"Title") %>">
                                    <i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;<%# DataBinder.Eval(Container.DataItem,"Alias") %>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'
                                    data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-alias='<%# DataBinder.Eval(Container.DataItem,"Alias") %>'
                                    data-title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' data-email='<%# DataBinder.Eval(Container.DataItem,"Email") %>'
                                    data-roleid='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>' data-name='<%# DataBinder.Eval(Container.DataItem,"FirstName") %>'
                                    runat="server" CssClass="lnkTeamEdit" ID="lnkTeamEdit" CommandName="edit">
                                        <i style="color: green" class="fa fa-pencil-square-o" ></i>
                                </asp:LinkButton>
                                    &nbsp;
                                <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'
                                    data-mid='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' runat="server"
                                    ID="lnkTeamRemove" class="lnkTeamRemove lnkRemove" CommandName="Remove" data-toggle="modal"
                                    data-target="#removeteam">
                                        <i style="color: #B90000" class="fa fa-trash"></i>
                                </asp:LinkButton>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <h4>
                            <i class="fa fa-users"></i>&nbsp;&nbsp;Clients (<asp:Label ID="lblClientCount" runat="server"
                                Text="0"></asp:Label>) <a href="javascript:void(0)" id="aAddClient" class="aAddClient spanClient"
                                    runat="server"><i style="color: #B90000" class="fa fa-plus-circle"></i></a>
                        </h4>
                        <div id="divManageClient" class="divManageClient" runat="server" style="display: none">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkAddNewClient" CssClass="chkAddNewClient" runat="server" Text="Add New"
                                            Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr id="trExClient" class="trExClient" style="display: none">
                                    <td>Existing:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlClientMembers" CssClass="ddlClientMembers" runat="server" Style="width: 100%;" multiple="multiple">
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnClientIDs" />
                                    </td>
                                </tr>
                                <tr class="trNewClient trClient trClientName">
                                    <td style="width: 55px;">Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtClientName" CssClass="txtClientName txtBox" runat="server"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtClientName" EnableClientScript="true"
                                            ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="trClient">
                                    <td style="width: 55px;">Alias:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtClientAlias" CssClass="txtClientAlias txtBox" runat="server"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtClientAlias" EnableClientScript="true"
                                            ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="trClient">
                                    <td>Title:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtClientTitle" CssClass="txtClientTitle txtBox" runat="server"></asp:TextBox>
                                        <%--&nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtClientTitle" EnableClientScript="true" ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>--%>
                                        <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnSelClientID" Value="0" />
                                    </td>
                                </tr>
                                <tr class="trNewClient trClient">
                                    <td>Email:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtClientEmail" CssClass="txtClientEmail txtBox" runat="server"></asp:TextBox>
                                        &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ErrorMessage="Required" ControlToValidate="txtClientEmail" EnableClientScript="true"
                                            ValidationGroup="ClientValidation"></asp:RequiredFieldValidator>
                                        &nbsp;&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ClientValidation"
                                        ControlToValidate="txtClientEmail"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr class="trNewClient trClient">
                                    <td>&nbsp;
                                    </td>
                                    <td colspan="2">
                                        <asp:Button ID="btnAddClient" CssClass="btnAddClient btnClient" Text="Add" CommandName="Client"
                                            runat="server" OnClick="btnAddMember_Click" ValidationGroup="ClientValidation" />
                                        <asp:Button ID="btnCancelClient" CssClass="btnCancelClient btnClient" Text="Cancel" runat="server" />
                                    </td>
                                </tr>
                                <tr class="trExClient trClient" style="display: none">
                                    <td>&nbsp;
                                    </td>
                                    <td colspan="2">
                                        <asp:Button ID="btnAddExClient" CssClass="btnAddExClient btnClient" CommandName="Client" runat="server"
                                            Text="Add" OnClick="btnAddEx_Click" />
                                        <asp:Button ID="btnCancelExClient" CssClass="btnCancelClient btnClient" Text="Cancel" runat="server" />
                                        <asp:Label ID="lblInfoExClient" runat="server" CssClass="lblInfo"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <asp:Label ID="lblInfoClient" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                        <asp:Repeater ID="rptClient" runat="server">
                            <HeaderTemplate>
                                <ul class="TeamMembers divClientMembers">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li title="<%# DataBinder.Eval(Container.DataItem,"FirstName") + " " + DataBinder.Eval(Container.DataItem,"LastName") + " - " + DataBinder.Eval(Container.DataItem,"Title") %>">
                                    <i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;<%# DataBinder.Eval(Container.DataItem,"Alias") %>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'
                                    data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-alias='<%# DataBinder.Eval(Container.DataItem,"Alias") %>'
                                    data-title='<%# DataBinder.Eval(Container.DataItem,"Title") %>' data-email='<%# DataBinder.Eval(Container.DataItem,"Email") %>'
                                    data-roleid='<%# DataBinder.Eval(Container.DataItem,"RoleID") %>' data-name='<%# DataBinder.Eval(Container.DataItem,"FirstName") %>'
                                    runat="server" ID="lnkClienkEdit" CssClass="lnkClienkEdit" CommandName="edit">
                                        <i style="color: green" class="fa fa-pencil-square-o" ></i>
                                </asp:LinkButton>
                                    &nbsp;
                                <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'
                                    data-mid='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' runat="server"
                                    ID="lnkClientRemove" CssClass="lnkClientRemove lnkRemove" CommandName="Remove"
                                    data-toggle="modal" data-target="#removeclient">
                                        <i style="color: #B90000" class="fa fa-trash"></i>
                                </asp:LinkButton>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="clearfix">
                        </div>
                        <span class="seperator"></span>
                        <div id="disAllowedContent" runat="server">
                            <h4>
                                <i class="fa fa-filter"></i>&nbsp;&nbsp;Filters</h4>
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <asp:TextBox runat="server" ID="txtContents" CssClass="txtContents form-control addDis"
                                            Rows="2" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <asp:LinkButton runat="server" class="btn btn-default addDis" ID="lnkSaveFilter"
                                            OnClick="btnSaveDAContents_Click"><i class="fa fa-check"></i>&nbsp;&nbsp;Submit</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <p class="filter-text">
                                Set up contents which you want to disallow members to use while communication in
                            this discussion
                            <br />
                                <strong>i.e $, John, Thanks etc</strong>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div id="divAskInfo" class="divAskInfo" style="display: none; z-index: 10000">
            <table class="tblForm">
                <tr>
                    <td class="formCaptionTd">Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Alias:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMAlias" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="formCaptionTd">Title:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="display: none"></td>
                </tr>
            </table>
        </div>

        <div class="divDiscussionListTemp" style="display: none">
        </div>
        <asp:Button ID="btnUpload" runat="server" CssClass="btnUpload" OnClick="btnUpload_Click"
            Style="display: none" />
        <asp:Button ID="btnSaveMemberInfo" CssClass="btnSaveMemberInfo" runat="server" OnClick="btnSaveMemberInfo_Click"
            Style="display: none" />
        <asp:Label ID="lblMemberTemp" CssClass="lblMemberTemp" runat="server" Style="display: none;"></asp:Label>
        <%--<asp:Button ID="btnSetTitle" runat="server" OnClick="btnSetTitle_Click" />--%>
        <asp:HiddenField runat="server" ID="hdnDiscussionIsNew" Value="0" />
        <asp:HiddenField runat="server" ID="hdnProjectID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnTeamMember" Value="0" />
        <asp:HiddenField runat="server" ID="hdninstanceid" Value="0" />
        <asp:HiddenField runat="server" ID="hdnDisID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMemDisID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnSelMemDisID" Value="0" />
        <asp:HiddenField runat="server" ID="hdn_folderid" Value="0" />
        <asp:HiddenField runat="server" ID="hdnRoleID" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMoveToBottom" Value="1" />
        <asp:HiddenField runat="server" ID="hdnTotalComments" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMN" Value="0" />
        <asp:HiddenField ID="hdnAskInfo" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRDL" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRCom" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRTags" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRNotes" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRFiles" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hdnCCC" Value="0" />
        <asp:HiddenField runat="server" ID="hdnCCT" Value="0" />
        <asp:HiddenField runat="server" ID="hdnCCNotes" Value="0" />
        <asp:HiddenField runat="server" ID="hdnIsHourlyProject" Value="0" />
        <asp:HiddenField runat="server" ID="HideHours" Value="0" />
        <asp:HiddenField runat="server" ID="ShowSummary" Value="0" />
        <asp:HiddenField runat="server" ID="hdnHourRequiredEachPost" Value="0" />
        <asp:HiddenField runat="server" ID="hdnZeroHourPost" Value="0" />
        <asp:HiddenField runat="server" ID="hdnPMId" Value="0" />
        <asp:HiddenField runat="server" ID="hdnAPMId" Value="0" />
        <asp:HiddenField runat="server" ID="hdnSendSummary" Value="0" />
        <asp:HiddenField runat="server" ID="hdnDuration" Value="" />
        <asp:HiddenField runat="server" ID="hdnEmailContent" Value="Project Name=@ProjectName Summary=@Summary" />
        <asp:HiddenField ID="hdnNoteReminder" runat="server" Value="0" ClientIDMode="Static" />

        <link href="<%= "_assests/nProgress/nprogress.css?" + DateTime.Now.ToBinary() %>"
            rel="stylesheet" />
        <script src="<%= "_assests/nProgress/nprogress.js?" + DateTime.Now.ToBinary() %>"></script>
        <%--<asp:Button ID="btnSetAllTrue" runat="server" OnClick="btnSetAllTrue_Click" Visible="false" />--%>
        <script>
            $(function () {
                DisallowedContents = <%= Serialize(DisallowedContents) %>;
            })
        </script>
        <!-- Button trigger modal -->
        <%--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
            Launch demo modal
        </button>--%>
        <!-- Remove Client Modal -->
        <div class="modal fade" id="removeclient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remove member?</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to remove this member?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel</button>
                        <asp:Button type="button" CssClass="btn btn-primary" ID="btnRemoveMember" runat="server"
                            Text="Remove" OnClick="btnRemoveMember_Click" />
                        <asp:HiddenField ID="hdnClientToRemoveID" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>
        <!-- Remove Team Modal -->
        <div class="modal fade" id="removeteam" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel2">Remove member?</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to remove this member?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel</button>
                        <asp:Button type="button" CssClass="btn btn-primary" ID="btnRemoveTeamMember" runat="server"
                            Text="Remove" OnClick="btnRemoveTeamMember_Click" />
                        <asp:HiddenField ID="hdnTeamToRemoveID" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>
        <!-- Remove Comment Modal -->
        <div class="modal fade" id="removecomment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel3">Remove this post?</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to remove this post?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel</button>
                        <button type="button" class="btn btn-primary" id="btnRemoveComment">
                            Yes</button>
                        <asp:HiddenField ID="hdnDelMDID" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Comment Modal -->
        <div class="modal fade" id="editcomment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel4">Edit Comment</h4>
                    </div>
                    <div class="modal-body">
                        <asp:HiddenField runat="server" ID="hdnMID" />
                        <textarea id="txtMemDisU" class="txtMemDisU"></textarea>
                        <textarea id="txtMemDisTagsU" class="txtMemDisTagsU blacktags"></textarea>
                        <ul style="padding: 0px; margin: 0px;" id="files">
                        </ul>
                        <asp:FileUpload ID="fileUpload2" CssClass="fileUpload1" runat="server" Style="float: left" />
                        <div class="hours">
                            <select id="ehoursSubtract" name="ehoursSubtract" style="height: 28px; border-radius: 3px;" runat="server">
                                <option value="1" selected="selected">Add</option>
                                <option value="2">Subtract</option>
                            </select>
                            <input type="text" name="hours" maxlength="2" style="width: 60px" id="hoursEdit"
                                value="0" placeholder="Hours" />
                            <input type="text" name="minutes" maxlength="2" style="width: 60px" id="minutesEdit"
                                value="0" placeholder="Minutes" />
                            <input runat="server" type="text" name="summarywords" style="width: 240px;" id="summaryEdit"
                                placeholder="Few words on work you performed" />
                            <div id="edivHrsSubReason">
                                <asp:Label runat="server" ID="lblereason">Reason: </asp:Label>
                                <select id="eHrsSubReason" style="height: 28px; border-radius: 3px;" runat="server">
                                    <option value="Discount">Discount</option>
                                    <option value="Input Correction">Input Correction</option>
                                    <option value="Other">Other</option>
                                </select>
                                <input runat="server" type="text" name="esubtractionreason" style="width: 280px;" id="esubtractionreason"
                                    placeholder="Few words on hours subtraction reason" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default eCancel" data-dismiss="modal">
                            Cancel</button>
                        <button type="button" class="btn btn-primary btnUpdate" id="btnUpdate">
                            Update</button>
                        <asp:HiddenField ID="hdnECom" runat="server" ClientIDMode="Static" Value="0" />
                        <asp:HiddenField ID="hdnIsSubtracted" runat="server" ClientIDMode="Static" Value="False" />
                        <asp:HiddenField ID="hdnSubReason" runat="server" ClientIDMode="Static" Value="" />
                    </div>
                </div>
            </div>
        </div>

        <!-- Add Tag -->
        <div class="modal fade" id="addtag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel5">Add Tags</h4>
                    </div>
                    <div class="modal-body">
                        <textarea id="txtAddTags" class="txtAddTags"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel</button>
                        <button type="button" class="btn btn-primary btnAddTag" id="btnAddTag">
                            Add</button>
                        <asp:HiddenField ID="hdnTCom" runat="server" ClientIDMode="Static" Value="0" />
                    </div>
                </div>
            </div>
        </div>
        <!-- Edit Title and IsHourly-->
        <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-labelledby="myModalLabel6"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content eidtTitlePopup">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="lblDisName">Settings&nbsp;-&nbsp;
                        </h4>
                        <!-- <asp:Label ClientIDMode="Static" runat="server" ID="lblDisName" Text="" />-->
                    </div>
                    <div class="modal-body">
                        Title:
                    <asp:TextBox ID="txtDisName" runat="server" Text="" Width="90%"> </asp:TextBox>
                        &nbsp;
                    <br />
                        <asp:CheckBox ID="chkIsHourly" runat="server" ClientIDMode="Static" Text="This is Hourly Project?" />
                        <div id="isHourlyDiv" style="display: none">
                            <asp:CheckBox ID="HideHoursFromClient" runat="server" ClientIDMode="Static" Text="Hide Hours From Clients? " /><br />
                            <asp:CheckBox ID="ShowSummaryBox" runat="server" ClientIDMode="Static" Text="Summary Required for each post? " /><br />
                            <asp:CheckBox ID="chkHourRequired" runat="server" ClientIDMode="Static" Text="Hours Required for each post? " /><br />
                            <div id="ZeroHour" style="display: none">
                                <asp:CheckBox ID="chkZeroHour" runat="server" ClientIDMode="Static" Text="0 Hours can be posted ? " />
                            </div>
                            <asp:CheckBox ID="chkSendSummary" runat="server" ClientIDMode="Static" Text="Send Summary ? " />
                            <div id="duartion" style="display: none; margin-left: 10px 0px 0px 0px">
                                <table width="95%">
                                    <tr>
                                        <td>Duration (In Days):</td>
                                        <td>
                                            <asp:TextBox ID="txtDuration" runat="server" Text="" placeholder="In days"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>Email Content:</td>
                                        <td>
                                            <asp:TextBox ID="txtEmailContent" Rows="5" runat="server" Text="Project Name: @ProjectName Summary: @Summary"
                                                TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="pm">
                            <hr />
                            <div>
                                Project Manager:
                            <asp:DropDownList ID="ddlPTeamMembers" runat="server">
                            </asp:DropDownList>
                            </div>
                            <hr />
                            <div>
                                Assitant Project Manager:
                            <asp:DropDownList ID="ddlAPTeamMembers" runat="server">
                            </asp:DropDownList>
                            </div>
                        </div>
                        <hr />
                        <div style="padding-right: 40px;">
                            <asp:Label runat="server" ID="lblstatus"></asp:Label>
                            <asp:LinkButton ID="btnstatus" CssClass="status" data-toggle="tooltip" runat="server"
                                data-placement="left" OnClick="btnstatus_Click" OnClientClick="CloseWindow();">
                                <i class="" id="iButtonStatus" runat="server"></i>
                            </asp:LinkButton>
                        </div>

                        <div style="padding-right: 40px;">
                            <asp:Label runat="server" ID="lblDelDiscussion">Delete Discussion</asp:Label>
                            <asp:LinkButton ID="btndeldiscussion" CssClass="status" style="float:none !important;"  data-toggle="tooltip" runat="server"
                                data-placement="left" OnClick="btndeldiscussion_Click" OnClientClick="return ConfirmDelete();">
                                <i class="infofont fa fa-trash-o" id="i1" runat="server"></i>
                            </asp:LinkButton>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel</button>
                        <button type="button" class="btn btn-primary btnUpdateDisName" id="btnUpdateDisName">
                            Update</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Notes -->
        <div class="modal fade" id="addnote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel7"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content eidtTitlePopup">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel6">Add Notes</h4>
                    </div>
                    <div class="modal-body">
                        <textarea id="txtAddNotes" class="txtAddNotes"></textarea>
                        &nbsp;
                    <br />
                        <asp:Label runat="server" ID="lblAddedBy" Font-Italic="true" Font-Bold="true"></asp:Label>
                        <br />
                        <span class="notescheck">
                            <asp:CheckBox runat="server" ID="chk_Notify" ClientIDMode="Static" Text=" Email Team" Checked="true" /><br />
                        </span>
                        <span class="notescheck">
                            <asp:CheckBox runat="server" ID="chk_NotifyClient" ClientIDMode="Static" Text=" Email Client" /><br />
                        </span>
                        <span class="notescheck">
                            <asp:CheckBox runat="server" ID="chk_Reminder" ClientIDMode="Static" Text=" Notify every time a user opens the post" /><br />
                        </span>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btnDelNotes" id="btnDelNotes" onclick="DeleteNote();">
                            Delete</button>
                        <button type="button" class="btn btn-default btnCancel" data-dismiss="modal">
                            Cancel</button>
                        <button type="button" class="btn btn-primary btnAddNotes" id="btnAddNotes">
                            Add</button>
                        <asp:HiddenField ID="hdnNCom" runat="server" ClientIDMode="Static" Value="0" />
                        <asp:HiddenField runat="server" ID="isnew" ClientIDMode="Static" Value="new" />
                        <asp:HiddenField runat="server" ID="hdnNoteID" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>

        <%--Note Reminder PopUp--%>
        <div id="divNoteReminder" class="divNoteReminder" style="display: none">
        </div>
    </form>
</body>
</html>
