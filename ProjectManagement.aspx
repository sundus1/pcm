﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectManagement.aspx.cs" Inherits="CCM.ProjectManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Project</title>
    <script src="_assests/jQuit/js/jquery-1.6.2.min.js"></script>
    <script src="_assests/jQuit/js/jquery-ui-1.8.16.custom.min.js"></script>
    <link href="_assests/jQuit/css/jquery-ui.css" rel="stylesheet" />
    <script src="_assests/_otf.js"></script>
    <link href="_assests/Styles/style.css" rel="stylesheet" />
    <link href="http://www.avaima.com/apps_data/b2303cf1-32f3-439d-9753-4a1a0748a376/5841fc0d-e505-4bed-93c5-785278cc0e25/app-style.css" media="screen" type="text/css" rel="stylesheet" />
    <script>
        var teamValidToGo = 0;
        var buttonID = "";
        $(function () {
            setInterval(function () {
                //console.log("chaling");
                setTimeout(function () {
                    $('.divbar:visible').delay(2000).css('display', 'none');
                    //console.log("visible chaling");
                }, 10000);
            }, 10);
            setInterval(function () {
                //console.log("chaling");
                setTimeout(function () {
                    $('.lblInfo').delay(2000).text('');
                    //console.log("visible chaling");
                }, 10000);
            }, 10);
            $('.btnAddEx').click(function () {

            });
            $('.lnkEdit').live('click', function () {
                var memberID = $(this).parent().parent().attr('data-id');
                var name = $(this).parent().parent().find('.txtFullName').text();
                var alias = $(this).parent().parent().find('.txtAlias').text();
                var email = $(this).parent().parent().find('.txtEmail').text();
                var memberas = $(this).parent().parent().attr('data-as');
                if (memberas == "client") { $("input#rdoClient").prop("checked", true) }
                else { $("input#rdoTeam").prop("checked", true) }
                $('.txtTeamFullName').val(name);
                $('.txtTeamAlias').val(alias);
                $('.txtTeamEmail').val(email);
                $('#hdnUserMemberID').val(memberID);
                $('.btnAddMember').val("Update Member");
                return false;
            });
            $('.lnkAddNew').click(function () {
                $('.txtTeamFullName').focus();
                $('.txtTeamFullName').val('');
                $('.txtTeamAlias').val('');
                $('.txtTeamEmail').val('');
                $('#hdnUserMemberID').val('0');
                $('.btnAddMember').val("Add Member");
                return false;
            });
            $('.lnkAddExisting').click(function () {
                $('.trEx').toggle(200);
                return false;
            });

            $('.chkAddNew input').change(function () {
                if ($(this).is(':checked')) {
                    $('.trEx').hide(200);
                    $('.trNew').show(200);
                    $('.txtTeamFullName').focus();
                    $('.txtTeamFullName').val('');
                    $('.txtTeamAlias').val('');
                    $('.txtTeamEmail').val('');
                    $('#hdnUserMemberID').val('0');
                    $('.btnAddMember').val("Add Member");
                    resizeIframe();
                }
                else {
                    $('.trEx').show(200);
                    $('.trNew').hide(200);
                }                
                return false;
            })
            $('#tabs').tabs();
        })
    </script>
    <link href="_assests/Styles/ccm_style.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
        <div id="formDiv">
            <div id="divProjectDashboard" runat="server">
                <asp:Panel ID="pnlProject" runat="server" DefaultButton="btnSave">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td colspan="2" class="auto-style1">
                                <h2 runat="server" id="h1" style="margin-left: 20px; display: inline-block">Create Project</h2>
                                <asp:LinkButton ID="lnkBack" runat="server" Text="Back" Style="margin-left: 20px;" OnClick="lnkBack_Click"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr id="trM6" runat="server">
                            <td colspan="2" style="text-align: left">
                                <div class="info divbar" id="divInfo" runat="server" style="display: none">Info message</div>
                                <div class="success divbar" id="divSuccess" runat="server" style="display: none">Successful operation message</div>
                                <div class="warning divbar" id="divWarning" runat="server" style="display: none">Warning message</div>
                                <div class="error divbar" id="divError" runat="server" style="display: none">Error message</div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="formCaptionTd" style="padding-top: 10px">Path</td>

                            <td style="padding-bottom: 12px;">
                                <div id="divBreadCrumb" style="margin-top: 10px;">
                                    <asp:Repeater ID="rptBC" runat="server" OnItemCommand="rptBC_ItemCommand">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lnkRoot" runat="server" CommandArgument="0" CommandName="Navigate" CssClass="lnkBC" data-pid="0" Text="Root"></asp:LinkButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            /
                                                <asp:LinkButton ID="lnkProjects" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProjectID") %>' CommandName="Navigate" CssClass="lnkBC" data-pid='<%# DataBinder.Eval(Container.DataItem, "ProjectID") %>' Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="formCaptionTd">Select Project
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProject" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton runat="server" ID="lnkNewProject" OnClick="lnkNewProject_Click" Text="Create New"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd">Title
                            </td>
                            <td>
                                <asp:TextBox ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="formCaptionTd">Description
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Style="width: 380px; height: 175px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none" id="trStatus" runat="server">
                            <td class="formCaptionTd">Status
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" runat="server">
                                    <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                    <asp:ListItem Text="Close" Value="Close"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="display: none" id="trActive" runat="server">
                            <td class="formCaptionTd">Active
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkActive" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding-top: 8px;">
                                <asp:Button ID="btnSave" CssClass="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>

                        <tr id="trM9" runat="server">
                            <td colspan="2">
                                <div class="formTitleDiv" runat="server" id="divTitle">
                                    Manage Members                                    
                                </div>
                                <%--<div style="text-align: right">
                                    <asp:LinkButton ID="lnkAddNew" runat="server" CssClass="lnkAddNew" Text="Add New"></asp:LinkButton>
                                    &nbsp;-&nbsp;
                                <asp:LinkButton ID="lnkAddExisting" runat="server" CssClass="lnkAddExisting" Text="Add Existing"></asp:LinkButton>
                                </div>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="trM4" runat="server">
                            <td class="formCaptionTd">Add as
                            </td>
                            <td style="font-size: 18px">
                                <asp:RadioButton ID="rdoClient" runat="server" Text="Client" GroupName="memberas" Checked="true" Style="float: left" OnCheckedChanged="rdoClient_CheckedChanged" AutoPostBack="true" />
                                <asp:RadioButton ID="rdoTeam" runat="server" Text="Team" GroupName="memberas" Style="float: right; margin-right: 12px" OnCheckedChanged="rdoClient_CheckedChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr id="trM10" runat="server">
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chkAddNew" CssClass="chkAddNew" runat="server" Text="Add New Member" Checked="true" />
                            </td>
                        </tr>
                        <tr class="trEx" id="trEx" runat="server" style="display: none">
                            <td class="formCaptionTd">Existing
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMembers" CssClass="ddlMembers" runat="server">
                                </asp:DropDownList>
                                <asp:Button ID="btnAddEx" CssClass="btnAddEx" runat="server" Text="Select" OnClick="btnAddEx_Click" />
                                <asp:Label ID="lblExInfo" runat="server" CssClass="lblInfo"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trM1" runat="server" class="trNew">
                            <td class="formCaptionTd">Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtTeamFullName" CssClass="txtTeamFullName" runat="server"></asp:TextBox>
                                &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamFullName" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                                <asp:HiddenField ClientIDMode="Static" runat="server" ID="hdnUserMemberID" Value="0" />
                            </td>
                        </tr>
                        <tr id="trM2" runat="server" class="trNew">
                            <td class="formCaptionTd">Alias
                            </td>
                            <td>
                                <asp:TextBox ID="txtTeamAlias" CssClass="txtTeamAlias" runat="server"></asp:TextBox>
                                &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamAlias" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trM3" runat="server" class="trNew">
                            <td class="formCaptionTd">Email
                            </td>
                            <td>
                                <asp:TextBox ID="txtTeamEmail" CssClass="txtTeamEmail" runat="server" Width="200px"></asp:TextBox>
                                &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtTeamEmail" EnableClientScript="true" ValidationGroup="MemberValidation"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="MemberValidation" ControlToValidate="txtTeamEmail"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr id="trM5" runat="server" class="trNew">
                            <td></td>
                            <td>
                                <asp:Button ID="btnAddMember" CssClass="btnAddMember" Text="Add Member" runat="server" OnClick="btnAddTeamMember_Click" ValidationGroup="MemberValidation" />
                                <asp:Label ID="lblInfo" CssClass="lblInfo" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr id="trM7" runat="server">
                            <td class="formCaptionTd">Team</td>
                            <td>
                                <asp:Repeater ID="rptTeamMember" runat="server" OnItemCommand="rptTeamMember_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Full Name
                                                </td>
                                                <td>Alias
                                                </td>
                                                <td>Email
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trTeamData" runat="server" data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-as="team">
                                            <td>
                                                <asp:Label ID="txtFullName" CssClass="txtFullName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="txtAlias" CssClass="txtAlias" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NickName")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="txtEmail" CssClass="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CssClass="lnkEdit">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkRemove" runat="server" CssClass="lnkRemove" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'>Remove</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr id="trM8" runat="server">
                            <td class="formCaptionTd">Clients</td>
                            <td>
                                <asp:Repeater ID="rptClientMembers" runat="server" OnItemCommand="rptClientMembers_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="smallGrid">
                                            <tr class="smallGridHead">
                                                <td>Full Name
                                                </td>
                                                <td>Alias
                                                </td>
                                                <td>Email
                                                </td>
                                                <td></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="trTeamData" runat="server" data-id='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>' data-as="client">
                                            <td>
                                                <asp:Label ID="txtFullName" CssClass="txtFullName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FirstName")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="txtAlias" CssClass="txtAlias" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NickName")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="txtEmail" CssClass="txtEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CssClass="lnkEdit">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkRemove" runat="server" CssClass="lnkRemove" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'>Remove</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>

        <div id="divcpdialog" class="divcpdialog">
            <p id="paraDialog">
            </p>
        </div>
        <%--<asp:HiddenField runat="server" ID="hdnParenProjectID" Value="0" />--%>
        <%--<asp:HiddenField runat="server" ID="hdnProjectIsNew" Value="false" />--%>
        <%--<asp:HiddenField runat="server" ID="hdnProjectID" Value="0" />--%>
        <asp:HiddenField runat="server" ID="hdnOID" Value="0" />
        <asp:HiddenField runat="server" ID="hdninstanceid" Value="0" />
        <asp:HiddenField runat="server" ID="hdnMemberID" Value="0" />
    </form>
</body>
</html>
